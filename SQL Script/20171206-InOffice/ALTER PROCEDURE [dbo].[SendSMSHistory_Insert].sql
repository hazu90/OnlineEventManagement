/****** Object:  StoredProcedure [dbo].[SendSMSHistory_Insert]    Script Date: 12/6/2017 9:26:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HIEUBV
-- Create date: 06/12/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SendSMSHistory_Insert]
	@SMSResponse		INT,
	@Description		NVARCHAR(200),
	@InviteEventId		INT,
	@PhoneNumber		NVARCHAR(50),
	@SMSContent			NVARCHAR(1000),
	@EventId			INT
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [SendSMSHistory]([SMSResponse] ,[Description] ,[InviteEventId] ,[CreatedDate] ,[PhoneNumber]
								,[SMSContent]  ,[EventId] )
						  VALUES(@SMSResponse  ,@Description  ,@InviteEventId  ,GETDATE()	  ,@PhoneNumber 
								,@SMSContent ,@EventId )
	SELECT SCOPE_IDENTITY()
END
