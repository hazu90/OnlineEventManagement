/****** Object:  StoredProcedure [dbo].[InviteEvent_GetByCodeToCheckin]    Script Date: 12/9/2017 11:48:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 09/12/2017
-- Description:	Lấy thông tin khách mời theo id
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_GetByCodeToCheckin]
	@EventId	INT,	
	@Code		NVARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT IE.[Id]
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[IsCheckIn]
		  ,IE.[Note]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[EventId]
		  ,E.[Name] AS EventName
		  ,E.[EventCode]
		  ,IE.[CustomerEmail]
		  ,IE.[CompanyName]
		  ,IE.[CustomerType]
		  ,IE.[PositionName]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[RegisteredSource]
		  ,IE.[IntroduceUser]
		  ,IE.[Status]
		  ,IE.[Code]
		  ,IE.[Sessions]
		  ,IE.[CustomerPosition]
		  ,IE.[IsBcrmProcess]
		  ,IE.[SourceId]
	FROM   [InviteEvent] IE
		INNER JOIN [Event] E ON IE.[EventId] = E.[Id]
	WHERE  E.[Id]		= @EventId
		AND IE.[Code]	= @Code
END
