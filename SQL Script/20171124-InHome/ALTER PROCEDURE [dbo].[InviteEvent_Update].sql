USE [CheckinDB00002]
GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_Update]    Script Date: 11/24/2017 03:48:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 24/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_Update]
	@Id				INT,
	@EventId		INT,
	@CustomerName	NVARCHAR(500),
	@CustomerPhone	NVARCHAR(1000),
	@Note			NVARCHAR(1000),
	@CustomerEmail  NVARCHAR(150),
	@CompanyName	NVARCHAR(250),
	@CustomerType	INT,
	@PositionName	NVARCHAR(200),
	@PlaceOfAttendEvent NVARCHAR(150),
	@RegisteredSource   INT,
	@IntroduceUser		NVARCHAR(150),
	@Sessions			NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE [InviteEvent]
	SET    [EventId]		= @EventId,
		   [CustomerName]	= @CustomerName,
		   [CustomerPhone]  = @CustomerPhone,
		   [Note]			= @Note,
		   [CustomerEmail]  = @CustomerEmail,
		   [CompanyName]	= @CompanyName,
		   [CustomerType]   = @CustomerType,
		   [PositionName]	= @PositionName,
		   [PlaceOfAttendEvent] = @PlaceOfAttendEvent,
		   [RegisteredSource] = @RegisteredSource,
		   [IntroduceUser]	  = @IntroduceUser,
		   [Sessions]		  = @Sessions ,
		   [TextSearch]		  = dbo.ConvertToBasicLatin(@CustomerName)		+	' '
								+	ISNULL(@CustomerPhone, '')					+	' '
								+	ISNULL(@CustomerEmail, '')					+	' '
								+	dbo.ConvertToBasicLatin(@CompanyName)		+	' '
								+	dbo.ConvertToBasicLatin(@PositionName)	+	' '
								+	dbo.ConvertToBasicLatin(@Note)				+	' '
								+	dbo.ConvertToBasicLatin(@PlaceOfAttendEvent)+	' '
								+	dbo.ConvertToBasicLatin(@IntroduceUser)		+	' '
	WHERE  [Id]				= @Id
END

