/****** Object:  StoredProcedure [dbo].[InviteEvent_InsertOnlineInvitee]    Script Date: 11/24/2017 02:54:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 24/11/2017
-- Description:	THêm mới
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_InsertOnlineInvitee]
	@EventId				INT,
	@CustomerName			NVARCHAR(500),
	@IntroduceUser			NVARCHAR(150),
	@CustomerPhone			NVARCHAR(20),
	@CustomerEmail			NVARCHAR(150),
	@CompanyName			NVARCHAR(250),
	@CustomerType			INT,
	@PositionName			NVARCHAR(200),
	@PlaceOfAttendEvent		NVARCHAR(150),
	@RegisteredDate			DATETIME,
	@CreatedBy				NVARCHAR(50),
	@PhoningStatus			INT,
	@ConfirmAttend1st		INT,
	@ConfirmAttend2st		INT,
	@Note					NVARCHAR(1000),
	@RegisteredSource		INT,
	@Code					NVARCHAR(20),
	@Sessions				NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [InviteEvent]([EventId]      ,[CustomerName]  ,[IntroduceUser] ,[CustomerPhone]     ,[CustomerEmail],[PositionName],
							  [CustomerType] ,[RegisteredDate],[CompanyName]   ,[PlaceOfAttendEvent],
							  [CreatedBy]	 ,[CreatedDate]	  ,[PhoningStatus] ,[ConfirmAttend1st]  ,[ConfirmAttend2st] ,[Note]  ,
							  [RegisteredSource],[Code]		  ,[Sessions],
							  [TextSearch]	)
					   VALUES(@EventId       ,@CustomerName   ,@IntroduceUser  ,@CustomerPhone      ,@CustomerEmail ,@PositionName ,
							  @CustomerType  ,@RegisteredDate ,@CompanyName    ,@PlaceOfAttendEvent ,
							  @CreatedBy	 ,GETDATE()		  ,@PhoningStatus  ,@ConfirmAttend1st   ,@ConfirmAttend2st  ,@Note   ,
							  @RegisteredSource,@Code		  ,@Sessions ,
							  dbo.ConvertToBasicLatin(@CustomerName)		+	' '
								+	ISNULL(@CustomerPhone, '')					+	' '
								+	ISNULL(@CustomerEmail, '')					+	' '
								+	dbo.ConvertToBasicLatin(@CompanyName)		+	' '
								+	dbo.ConvertToBasicLatin(@PositionName)	+	' '
								+	dbo.ConvertToBasicLatin(@Note)				+	' '
								+	dbo.ConvertToBasicLatin(@PlaceOfAttendEvent)+	' '
								+	dbo.ConvertToBasicLatin(@IntroduceUser)		+	' ' )
	SELECT SCOPE_IDENTITY()
END

