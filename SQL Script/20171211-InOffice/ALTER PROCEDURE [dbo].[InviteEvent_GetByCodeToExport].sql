USE [LocalCheckIn]
GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_GetByCodeToExport]    Script Date: 12/11/2017 9:19:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 11/12/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_GetByCodeToExport] 
	@EventId		INT,
	@Code			NVARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT IE.[Id]					AS InviteId
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[IsCheckIn]
		  ,IE.[Note]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[EventId]
		  ,E.[Name]					AS EventName
		  ,E.[EventCode]
		  ,IE.[CheckInBy]
		  ,IE.[CheckInDate]
		  ,IE.[CreatedBy]
		  ,IE.[CustomerEmail]
		  ,IE.[PositionName]
		  ,IE.[UserName]
		  ,IE.[CustomerType]
		  ,IE.[CompanyName]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[IntroduceUser]
		  ,IE.[Status]
		  ,IE.[PhoningStatus]
		  ,IE.[ConfirmAttend1st]
		  ,IE.[ConfirmAttend2st]
		  ,IE.[Code]
		  ,IE.[GroupId]
		  ,IE.[BcrmAssignTo]
		  ,IE.[SourceId]
		  ,IE.[Sessions]
		  ,IE.[IsBcrmProcess]
		  ,IE.[CustomerPosition]
	FROM   [InviteEvent] IE
		INNER JOIN [Event] E ON IE.[EventId] = E.[Id]
	WHERE  IE.[EventId] = @EventId
		AND (IE.[Status] = 1 )
		AND IE.[Code]	 = @Code
END
