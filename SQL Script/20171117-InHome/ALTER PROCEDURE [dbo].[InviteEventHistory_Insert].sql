/****** Object:  StoredProcedure [dbo].[InviteEventHistory_Insert]    Script Date: 11/16/2017 23:22:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 17/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InviteEventHistory_Insert]
	@InviteEventId	INT
,	@Action			NVARCHAR(50)
,	@Note			NVARCHAR(1000)
,	@OldValue		NVARCHAR(1000)
,	@NewValue		NVARCHAR(1000)
,	@UpdatedBy		NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO	[InviteEventHistory]
           ([InviteEventId]
           ,[Action]
           ,[Note]
           ,[OldValue]
           ,[NewValue]
		   ,[UpdatedBy]
		   ,[UpdatedDate])
    VALUES
           (@InviteEventId
           ,@Action
           ,@Note
           ,@OldValue
		   ,@NewValue
		   ,@UpdatedBy
           ,GETDATE())
	SELECT SCOPE_IDENTITY()
END

