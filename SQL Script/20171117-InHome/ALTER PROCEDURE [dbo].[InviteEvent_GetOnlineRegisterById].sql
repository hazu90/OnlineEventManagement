/****** Object:  StoredProcedure [dbo].[InviteEvent_GetOnlineRegisterById]    Script Date: 11/17/2017 00:14:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 17/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_GetOnlineRegisterById]
		@Id		INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [Id]
		  ,[CustomerName]
		  ,[CustomerPhone]
		  ,[IsCheckIn]
		  ,[CheckInBy]
		  ,[CheckInDate]
		  ,[Note]
		  ,[CreatedBy]
		  ,[CreatedDate]
		  ,[AccompanyMember]
		  ,[IsNotSuggest]
		  ,[EventId]
		  ,[PositionName]
		  ,[UserName]
		  ,[CustomerType]
		  ,[IsUsingWebsiteBds]
		  ,[PlaceOfAttendEvent]
		  ,[CompanyName]
		  ,[PhoningStatus]
		  ,[RegisteredDate]
		  ,[CustomerEmail]
		  ,[ConfirmAttend1st]
		  ,[ConfirmAttend2st]
		  ,[IntroduceUser]
		  ,[RegisteredSource]
		  ,[ApproveBy]
		  ,[ApproveDate]
		  ,[Status]
		  ,[Code]
		  ,[Sessions]
		  ,[SendTicketStatus]
	FROM   [InviteEvent] IE
	WHERE IE.[Id] = @Id
END

