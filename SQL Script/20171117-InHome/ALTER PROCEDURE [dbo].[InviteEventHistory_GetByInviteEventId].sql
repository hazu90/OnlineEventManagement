/****** Object:  StoredProcedure [dbo].[InviteEventHistory_GetByInviteEventId]    Script Date: 11/16/2017 23:57:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 17/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InviteEventHistory_GetByInviteEventId]
	@InviteEventId		INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT	[Id]
      ,		[InviteEventId]
      ,		[Action]
      ,		[Note]
	  ,		[OldValue]
	  ,		[NewValue]
      ,		[UpdatedBy]
      ,		[UpdatedDate]
  FROM		[InviteEventHistory]
  WHERE [InviteEventId] = @InviteEventId
  ORDER BY Id DESC
END

