USE [LocalCheckIn]
GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_Update]    Script Date: 11/23/2017 7:36:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_Update]
	@Id				INT,
	@EventId		INT,
	@CustomerName	NVARCHAR(500),
	@CustomerPhone	NVARCHAR(1000),
	@Note			NVARCHAR(1000),
	@CustomerEmail  NVARCHAR(150),
	@CompanyName	NVARCHAR(250),
	@CustomerType	INT,
	@PositionName	NVARCHAR(200),
	@PlaceOfAttendEvent NVARCHAR(150),
	@RegisteredSource   INT,
	@IntroduceUser		NVARCHAR(150),
	@Sessions			NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE [InviteEvent]
	SET    [EventId]		= @EventId,
		   [CustomerName]	= @CustomerName,
		   [CustomerPhone]  = @CustomerPhone,
		   [Note]			= @Note,
		   [CustomerEmail]  = @CustomerEmail,
		   [CompanyName]	= @CompanyName,
		   [CustomerType]   = @CustomerType,
		   [PositionName]	= @PositionName,
		   [PlaceOfAttendEvent] = @PlaceOfAttendEvent,
		   [RegisteredSource] = @RegisteredSource,
		   [IntroduceUser]	  = @IntroduceUser,
		   [Sessions]		  = @Sessions
	WHERE  [Id]				= @Id
END
