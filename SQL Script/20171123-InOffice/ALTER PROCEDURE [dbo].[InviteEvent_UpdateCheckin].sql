USE [LocalCheckIn]
GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_UpdateCheckin]    Script Date: 11/23/2017 8:00:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 22/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_UpdateCheckin]
	@Id				 INT,
	@IsCheckIn		 BIT,
	@CheckInBy		 NVARCHAR(50),
	@AccompanyMember INT
AS
BEGIN
	SET NOCOUNT ON;
	IF @IsCheckIn = 1
		UPDATE [InviteEvent]
		 SET   [IsCheckIn]   = @IsCheckIn,
			   [CheckInBy]   = @CheckInBy,
			   [CheckInDate] = GETDATE(),
			   [AccompanyMember] = @AccompanyMember,
			   [BcrmStatus] = 15
		WHERE [Id] = @Id
	ELSE
		UPDATE [InviteEvent]
		 SET   [IsCheckIn]   = @IsCheckIn,
			   [CheckInBy]   = NULL,
			   [CheckInDate] = NULL,
			   [AccompanyMember] = 0
			   --[BcrmStatus] = 16
		WHERE [Id] = @Id		   	
END	
