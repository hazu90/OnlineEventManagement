USE [LocalCheckIn]
GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_UpdateStatus]    Script Date: 11/23/2017 7:41:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 23/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_UpdateStatus]
	@Id				INT,
	@Status			BIT,
	@ApproveDate	DATETIME,
	@ApproveBy		NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	IF @Status	=1
		UPDATE		[InviteEvent]
		SET			[Status]			= @Status,
					[ApproveDate]		= @ApproveDate,
					[ApproveBy]			= @ApproveBy,
					[BcrmStatus]		= 16
		WHERE		[Id]				= @Id
	ELSE
		UPDATE		[InviteEvent]
		SET			[Status]			= @Status,
					[ApproveDate]		= NULL,
					[ApproveBy]			= NULL
		WHERE		[Id]				= @Id
END
