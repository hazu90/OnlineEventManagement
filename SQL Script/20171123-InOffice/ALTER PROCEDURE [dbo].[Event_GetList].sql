USE [LocalCheckIn]
GO
/****** Object:  StoredProcedure [dbo].[Event_GetList]    Script Date: 11/23/2017 1:52:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 20/04/2017
-- Description:	Lấy danh sách các sự kiện
-- =============================================
ALTER PROCEDURE [dbo].[Event_GetList]
	@DeployId		INT,
	@FilterText		NVARCHAR(500),
	@StartDate		DATE,
	@EndDate		DATE
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [Id]
		  ,[Name]
		  ,[Description]
		  ,[StartDate]
		  ,[EndDate]
		  ,[IsEnable]
		  ,[Deadline]
		  ,[CreatedDate]
		  ,[CreatedBy]
		  ,[EventCode]
		  ,[SMSTitle]
	FROM [Event]
	WHERE (@FilterText IS NULL OR @FilterText ='' OR [Name] like '%'+ @FilterText +'%')
	  AND (@StartDate IS NULL  OR @StartDate <= [StartDate])
	  AND (@EndDate   IS NULL  OR [EndDate]  <= @EndDate)	
	  AND (@DeployId =0		   OR [Deploy] ='0' OR ([Deploy] IS NOT NULL AND @DeployId IN (SELECT [Data] FROM dbo.Split([Deploy],',' ) ) ) )
END
