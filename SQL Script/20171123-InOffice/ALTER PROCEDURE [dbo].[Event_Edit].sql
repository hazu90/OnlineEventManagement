/****** Object:  StoredProcedure [dbo].[Event_Edit]    Script Date: 11/23/2017 3:41:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 23/11/2017
-- Description:	Sửa sự kiện
-- =============================================
ALTER PROCEDURE [dbo].[Event_Edit]
	@EventCode			NVARCHAR(50),
	@IsEnable			BIT,
	@Name				NVARCHAR(500),
	@StartDate			DATE,
	@EndDate			DATE,
	@Description		NVARCHAR(1000),
	@Id					INT,
	@SMSTitle			NVARCHAR(1000),
	@Deploy				NVARCHAR(500),
	@Deadline		  DATE,	
	@RevenueStartDate DATE,
	@RevenueEndDate   DATE,
	@RevenueCalculation   INT,
	@IsAllowAttach	  BIT,
	@IsBcrmUse		  BIT		
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE [Event]
	SET    [EventCode]   = @EventCode,
		   [IsEnable]    = @IsEnable ,
		   [Name]	     = @Name,
		   [Description] = @Description,
		   [StartDate]   = @StartDate,
		   [EndDate]     = @EndDate,
		   [SMSTitle]	 = @SMSTitle,
		   [Deploy]		 = @Deploy,
		   [Deadline]	 = @Deadline,
		   [RevenueStartDate] = @RevenueStartDate,
		   [RevenueEndDate]   = @RevenueEndDate ,
		   [RevenueCalculation] = @RevenueCalculation ,
		   [IsAllowAttach]	 = @IsAllowAttach ,
		   [IsBcrmUse]		 = @IsBcrmUse
	WHERE  [Id]			 = @Id
END
