USE [LocalCheckIn]
GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_UpdateSendTicketStatus]    Script Date: 11/23/2017 7:47:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 23/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_UpdateSendTicketStatus]
		@Id					INT,
		@SendTicketStatus	INT,
		@BcrmStatus			INT
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE	[InviteEvent]
	SET		[SendTicketStatus] = @SendTicketStatus ,
			[BcrmStatus]	   = @BcrmStatus
	WHERE	[Id]			   = @Id
END
