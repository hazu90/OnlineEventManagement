-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 20/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE SessionCheckin_GetByInviteeIds
	@EventId		INT,
	@InviteeIds		NVARCHAR(4000),
	@SessionId		INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [Data] 
	INTO #tempIds
	FROM dbo.Split(@InviteeIds,',')

	SELECT [InviteEventId]
		  ,[SessionId]
		  ,[CheckinBy]
		  ,[CheckinDate]
		  ,[EventId]
	FROM   [SessionCheckin]
	WHERE  [EventId] = @EventId
	  AND  [InviteEventId] IN (SELECT [Data] FROM #tempIds )
	  AND  (@SessionId=0 OR [SessionId]	= @SessionId) 

	DROP TABLE #tempIds
END
GO
