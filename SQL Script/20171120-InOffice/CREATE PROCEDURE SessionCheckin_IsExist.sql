-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 20/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE SessionCheckin_IsExist
	@InviteEventId			INT,
	@SessionId				INT
AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS(SELECT [SessionId] FROM [SessionCheckin] WHERE [InviteEventId] = @InviteEventId AND [SessionId] = @SessionId )
		SELECT 1
	ELSE 
		SELECT 0
END
GO
