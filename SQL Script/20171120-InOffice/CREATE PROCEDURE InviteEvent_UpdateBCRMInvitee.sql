-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 20/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE InviteEvent_UpdateBCRMInvitee
	@Id					INT,
	@CustomerName		NVARCHAR(500),
	@CustomerPhone		NVARCHAR(1000),
	@CustomerEmail		NVARCHAR(150),
	@CustomerPosition	INT,
	@Sessions			NVARCHAR(50),
	@Note				NVARCHAR(1000)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE	[InviteEvent]
	SET		[CustomerName]			= @CustomerName,
			[CustomerPhone]			= @CustomerPhone,
			[CustomerEmail]			= @CustomerEmail,
			[CustomerPosition]		= @CustomerPosition,
			[Sessions]				= @Sessions,
			[Note]					= @Note
	WHERE   [Id]					= @Id
END
GO
