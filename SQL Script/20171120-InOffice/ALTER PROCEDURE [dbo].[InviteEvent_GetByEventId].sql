/****** Object:  StoredProcedure [dbo].[InviteEvent_GetByEventId]    Script Date: 11/20/2017 8:49:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 20/11/2017
-- Description:	Lấy danh sách khách mời theo sự kiện
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_GetByEventId]
	@EventId	INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT IE.[Id]
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[IsCheckIn]
		  ,IE.[Note]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[EventId]
		  ,E.[Name] AS EventName
		  ,E.[EventCode]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[Code]
		  ,IE.[CompanyName]
		  ,IE.[IntroduceUser]
		  ,IE.[Sessions]
	FROM   [InviteEvent] IE
		INNER JOIN [Event] E ON IE.[EventId] = E.[Id]
	WHERE  IE.[EventId] = @EventId  AND IE.[Status] = 1
END
