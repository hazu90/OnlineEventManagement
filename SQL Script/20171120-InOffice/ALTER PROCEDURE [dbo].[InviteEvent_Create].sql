/****** Object:  StoredProcedure [dbo].[InviteEvent_Create]    Script Date: 11/20/2017 7:24:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 20/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_Create]
	@EventId			INT,
	@CustomerName		NVARCHAR(500),
	@CustomerPhone		NVARCHAR(1000),
	@Note				NVARCHAR(1000),
	@CreatedBy			NVARCHAR(50),
	@IsNotSuggest		BIT,
	@CustomerEmail		NVARCHAR(150),
	@PositionName		NVARCHAR(200),
	@CompanyName		NVARCHAR(250),
	@CustomerType		INT,
	@PlaceOfAttendEvent NVARCHAR(150),
	@RegisteredSource   INT,
	@Code				NVARCHAR(20),
	@Sessions			NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT  INTO [InviteEvent]([EventId],[CustomerName],[CustomerPhone],[Note],[CreatedBy],[CreatedDate],[IsNotSuggest],
							   [CustomerEmail],[PositionName],[CompanyName],[CustomerType],[PlaceOfAttendEvent],
							   [RegisteredSource],[Code],[Sessions] )
						VALUES(@EventId ,@CustomerName ,@CustomerPhone ,@Note ,@CreatedBy ,GETDATE(),@IsNotSuggest,
							   @CustomerEmail,@PositionName  ,@CompanyName ,@CustomerType ,@PlaceOfAttendEvent ,
							   @RegisteredSource,@Code ,@Sessions ) 
	SELECT SCOPE_IDENTITY()
END
