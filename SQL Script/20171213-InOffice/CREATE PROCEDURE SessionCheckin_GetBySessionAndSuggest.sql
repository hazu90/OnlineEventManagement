-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 13/12/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE SessionCheckin_GetBySessionAndSuggest
	@EventId			INT,
	@SessionId			INT,
	@IsNotSuggest		BIT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT SC.[InviteEventId]
		  ,SC.[SessionId]
		  ,SC.[CheckinBy]
		  ,SC.[CheckinDate]
		  ,SC.[EventId]
		  ,SC.[AccompanyMember]
	FROM   [SessionCheckin] SC
			INNER JOIN [InviteEvent] IE ON SC.[InviteEventId] = IE.[Id]
	WHERE	IE.[EventId] = @EventId
		AND ISNULL(IE.[IsNotSuggest],0) = @IsNotSuggest
		AND ( @SessionId = 0 OR SC.[SessionId] = @SessionId )
END
GO
