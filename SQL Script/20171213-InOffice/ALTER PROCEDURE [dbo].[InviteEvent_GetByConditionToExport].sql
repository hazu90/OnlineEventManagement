/****** Object:  StoredProcedure [dbo].[InviteEvent_GetByConditionToExport]    Script Date: 12/13/2017 11:10:30 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 13/12/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_GetByConditionToExport] 
	@EventId		INT,
	@IsCheckIn		BIT,
	@IsNotSuggest   BIT,
	@SessionId		INT,
	@TextSearch		NVARCHAR(200)
AS
BEGIN
	SET NOCOUNT ON;
	IF @SessionId > 0 AND @IsCheckIn IS NOT NULL
		SELECT IE.[Id]					AS InviteId
			  ,IE.[CustomerName]
			  ,IE.[CustomerPhone]
			  ,IE.[IsCheckIn]
			  ,IE.[Note]
			  ,IE.[AccompanyMember]
			  ,IE.[IsNotSuggest]
			  ,IE.[EventId]
			  ,E.[Name]					AS EventName
			  ,E.[EventCode]
			  ,IE.[CheckInBy]
			  ,IE.[CheckInDate]
			  ,IE.[CreatedBy]
			  ,IE.[CustomerEmail]
			  ,IE.[PositionName]
			  ,IE.[UserName]
			  ,IE.[CustomerType]
			  ,IE.[CompanyName]
			  ,IE.[PlaceOfAttendEvent]
			  ,IE.[IntroduceUser]
			  ,IE.[Status]
			  ,IE.[PhoningStatus]
			  ,IE.[ConfirmAttend1st]
			  ,IE.[ConfirmAttend2st]
			  ,IE.[Code]
			  ,IE.[GroupId]
			  ,IE.[BcrmAssignTo]
			  ,IE.[SourceId]
			  ,IE.[Sessions]
			  ,IE.[CustomerPosition]
			  ,IE.[IsBcrmProcess]
		FROM   [InviteEvent] IE
			INNER JOIN [Event] E ON IE.[EventId] = E.[Id]
			LEFT  JOIN (SELECT [InviteEventId],[SessionId] FROM [SessionCheckin] WHERE [SessionId] = @SessionId )  SC ON IE.[Id]	   = SC.[InviteEventId]
		WHERE  IE.[EventId] = @EventId
			AND (@SessionId = 0 OR IE.[Sessions] ='0' OR (IE.[Sessions] IS NOT NULL AND @SessionId IN (SELECT [Data] FROM dbo.Split(IE.[Sessions],',') ) ) )
			--AND	(@IsCheckIn IS NULL OR (IE.[IsCheckIn] IS NULL AND @IsCheckIn =0) OR (IE.[IsCheckIn]= @IsCheckIn) )
			AND (@IsNotSuggest IS NULL OR IE.[IsNotSuggest] = @IsNotSuggest)
			AND (IE.[Status] = 1 )
			AND (@TextSearch ='' OR IE.[TextSearch] LIKE '%'+@TextSearch +'%' )
			AND ((@IsCheckIn = 0 AND SC.InviteEventId IS NULL  ) OR (@IsCheckIn = 1 AND SC.InviteEventId IS NOT NULL  ) )
	ELSE

		SELECT IE.[Id]					AS InviteId
			  ,IE.[CustomerName]
			  ,IE.[CustomerPhone]
			  ,IE.[IsCheckIn]
			  ,IE.[Note]
			  ,IE.[AccompanyMember]
			  ,IE.[IsNotSuggest]
			  ,IE.[EventId]
			  ,E.[Name]					AS EventName
			  ,E.[EventCode]
			  ,IE.[CheckInBy]
			  ,IE.[CheckInDate]
			  ,IE.[CreatedBy]
			  ,IE.[CustomerEmail]
			  ,IE.[PositionName]
			  ,IE.[UserName]
			  ,IE.[CustomerType]
			  ,IE.[CompanyName]
			  ,IE.[PlaceOfAttendEvent]
			  ,IE.[IntroduceUser]
			  ,IE.[Status]
			  ,IE.[PhoningStatus]
			  ,IE.[ConfirmAttend1st]
			  ,IE.[ConfirmAttend2st]
			  ,IE.[Code]
			  ,IE.[GroupId]
			  ,IE.[BcrmAssignTo]
			  ,IE.[SourceId]
			  ,IE.[Sessions]
			  ,IE.[CustomerPosition]
			  ,IE.[IsBcrmProcess]
		FROM   [InviteEvent] IE
			INNER JOIN [Event] E ON IE.[EventId] = E.[Id]
		WHERE  IE.[EventId] = @EventId
			AND (@SessionId = 0 OR IE.[Sessions] ='0' OR (IE.[Sessions] IS NOT NULL AND @SessionId IN (SELECT [Data] FROM dbo.Split(IE.[Sessions],',') ) ) )
			AND	(@IsCheckIn IS NULL OR (IE.[IsCheckIn] IS NULL AND @IsCheckIn =0) OR (IE.[IsCheckIn]= @IsCheckIn) )
			AND (@IsNotSuggest IS NULL OR IE.[IsNotSuggest] = @IsNotSuggest)
			AND (IE.[Status] = 1 )
			AND (@TextSearch ='' OR IE.[TextSearch] LIKE '%'+@TextSearch +'%' )
END
