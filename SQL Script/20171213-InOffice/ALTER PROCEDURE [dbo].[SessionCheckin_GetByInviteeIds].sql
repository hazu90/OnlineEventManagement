/****** Object:  StoredProcedure [dbo].[SessionCheckin_GetByInviteeIds]    Script Date: 12/13/2017 9:27:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 13/12/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SessionCheckin_GetByInviteeIds]
	@EventId		INT,
	@InviteeIds		NVARCHAR(40000),
	@SessionId		INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [Data] 
	INTO #tempIds
	FROM dbo.Split(@InviteeIds,',')

	SELECT [InviteEventId]
		  ,[SessionId]
		  ,[CheckinBy]
		  ,[CheckinDate]
		  ,[EventId]
		  ,[AccompanyMember]
	FROM   [SessionCheckin]
	WHERE  [EventId] = @EventId
	  AND  [InviteEventId] IN (SELECT [Data] FROM #tempIds )
	  AND  (@SessionId=0 OR [SessionId]	= @SessionId) 

	DROP TABLE #tempIds
END
