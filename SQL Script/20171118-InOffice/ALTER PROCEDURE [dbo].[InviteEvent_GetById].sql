USE [LocalCheckIn]
GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_GetById]    Script Date: 11/18/2017 5:09:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 18/11/2017
-- Description:	Lấy thông tin khách mời theo id
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_GetById]
	@Id		INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT E.[Name]	AS EventName
		  ,E.[EventCode]
		  ,IE.[Id]
		  ,IE.[EventId]
		  ,IE.[CustomerId]
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[CustomerSex]
		  ,IE.[CustomerEmail]
		  ,IE.[CustomerBirthday]
		  ,IE.[CustomerPosition]
		  ,IE.[PositionName]
		  ,IE.[CustomerType]
		  ,IE.[CompanyId]
		  ,IE.[CompanyName]
		  ,IE.[CompanyAddress]
		  ,IE.[CompanySize]
		  ,IE.[TicketAddress]
		  ,IE.[GroupId]
		  ,IE.[AbilityAttend]
		  ,IE.[IsCheckIn]
		  ,IE.[CheckInBy]
		  ,IE.[CheckInDate]
		  ,IE.[Note]
		  ,IE.[CreatedBy]
		  ,IE.[CreatedDate]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[UserName]
		  ,IE.[IsUsingWebsiteBds]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[PhoningStatus]
		  ,IE.[RegisteredDate]
		  ,IE.[ConfirmAttend1st]
		  ,IE.[ConfirmAttend2st]
		  ,IE.[IntroduceUser]
		  ,IE.[RegisteredSource]
		  ,IE.[ApproveBy]
		  ,IE.[ApproveDate]
		  ,IE.[Status]
		  ,IE.[IsSelftRegistered]
		  ,IE.[Code]
		  ,IE.[AssignTo]
		  ,IE.[StatusNote]
		  ,IE.[CustomerVip]
		  ,IE.[SendTicketType]
		  ,IE.[Sessions]
		  ,IE.[SourceId]
		  ,IE.[CustomerRevenue]
		  ,IE.[BcrmStatus]
		  ,IE.[IsBcrmProcess]
		  ,IE.[BcrmAssignTo]
		  ,IE.[SendTicketStatus]
	FROM [InviteEvent] IE
		INNER JOIN [Event] E ON IE.[EventId] = E.[Id]
	WHERE  IE.[Id] = @Id
END


