USE [BCRMv3]
GO

/****** Object:  Table [dbo].[UserRole]    Script Date: 11/18/2017 4:02:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UserRole](
	[RoleId] [int] NOT NULL,
	[PersonId] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[IsInherit] [bit] NOT NULL CONSTRAINT [DF_Permissions_IsInherit]  DEFAULT ((0)),
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC,
	[PersonId] ASC,
	[Type] ASC,
	[IsInherit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


