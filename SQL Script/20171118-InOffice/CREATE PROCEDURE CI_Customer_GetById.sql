SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 18/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE CI_Customer_GetById
	@Id			INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT  C.[Id]
		,	C.[FullName]
		,	C.[PhoneNumber]
		,	C.[Status]
		,	C.[Type]
		,	C.[AssignTo]
		,	C.[Description]
		,	C.[LastModifiedDate]
		,	C.[IsDeleted]
		,	C.[Email]
		,	C.[CareDate]
		,	C.[UtilityInfo]
		,	C.[ExtensionTime]
		,	C.[UseOwnerService]
		,	C.[Regional]
		,	C.[CreatedDate]
		,	C.[IsOldCustomer]
	FROM		[Customer]		C	WITH	(NOLOCK)
		WHERE	C.[Id]		= @Id
END
GO
