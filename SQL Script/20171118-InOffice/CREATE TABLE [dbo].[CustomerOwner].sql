USE [BCRMv3]
GO

/****** Object:  Table [dbo].[CustomerOwner]    Script Date: 11/18/2017 6:15:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CustomerOwner](
	[Id] [int] IDENTITY(100000000,1) NOT NULL,
	[FullName] [nvarchar](250) NULL,
	[PhoneNumber] [nvarchar](1000) NULL,
	[DOB] [datetime] NULL,
	[Status] [int] NULL,
	[Type] [int] NULL,
	[Email] [nvarchar](250) NULL,
	[Description] [nvarchar](4000) NULL,
	[CityId] [int] NULL,
	[DistrictId] [int] NULL,
	[AssignTo] [nvarchar](50) NULL,
	[SourceId] [int] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastModifiedBy] [nvarchar](50) NULL,
	[LastModifiedDate] [datetime] NULL,
	[IsDeleted] [bit] NULL,
	[Potential] [int] NULL,
	[ICareDate] [int] NULL,
	[CareDate] [datetime] NULL,
	[UtilityInfo] [nvarchar](4000) NULL,
	[CompanyId] [int] NULL,
	[ExtensionTime] [datetime] NULL,
	[OriginalCustId] [int] NULL,
	[StartCareDate] [datetime] NULL,
 CONSTRAINT [PK_CustomerOwner] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


