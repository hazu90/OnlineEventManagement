USE [BCRMv3]
GO

/****** Object:  Table [dbo].[Users]    Script Date: 11/18/2017 4:01:06 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](50) NULL,
	[DisplayName] [nvarchar](100) NULL,
	[Mobile] [nvarchar](256) NULL,
	[Status] [int] NOT NULL,
	[DOB] [datetime] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[LastActivityDate] [datetime] NULL,
	[GroupId] [int] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[Avatar] [nvarchar](500) NULL,
	[BdsUserId] [int] NULL,
	[BdsUserName] [nvarchar](50) NULL,
	[StartWork] [datetime] NULL,
	[OpenDate] [date] NULL,
	[ReleaseDate] [date] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


