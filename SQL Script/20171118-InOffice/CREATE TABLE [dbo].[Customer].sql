USE [BCRMv3]
GO

/****** Object:  Table [dbo].[Customer]    Script Date: 11/18/2017 6:15:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Customer](
	[Id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[FullName] [nvarchar](250) NOT NULL,
	[PhoneNumber] [nvarchar](1000) NULL,
	[DOB] [datetime] NULL,
	[Status] [int] NULL,
	[Type] [int] NULL,
	[Email] [nvarchar](255) NULL,
	[Description] [nvarchar](4000) NULL,
	[CityId] [int] NULL,
	[DistrictId] [int] NULL,
	[AssignTo] [nvarchar](50) NULL,
	[SourceId] [int] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_Customers_CreatedDate]  DEFAULT (getdate()),
	[LastModifiedBy] [nvarchar](50) NULL,
	[LastModifiedDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Customer_IsDeleted]  DEFAULT ((0)),
	[Potential] [int] NULL CONSTRAINT [DF_Customer_Potential]  DEFAULT ((0)),
	[ICareDate] [int] NULL,
	[CareDate] [datetime] NULL,
	[UtilityInfo] [nvarchar](4000) NULL,
	[CompanyId] [int] NULL,
	[ExtensionTime] [datetime] NULL,
	[UseOwnerService] [bit] NULL,
	[StartCareDate] [datetime] NULL,
	[Regional] [int] NULL CONSTRAINT [DF_Customer_Regional]  DEFAULT ((0)),
	[IsOldCustomer] [bit] NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


