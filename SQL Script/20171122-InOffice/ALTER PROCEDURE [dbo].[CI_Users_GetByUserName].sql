USE [BCRMv3]
GO
/****** Object:  StoredProcedure [dbo].[CI_Users_GetByUserName]    Script Date: 11/22/2017 2:43:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 22/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[CI_Users_GetByUserName]
	@UserName	NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT	U.[UserName]
		,	U.[Avatar]
		,	U.[GroupId]
	FROM	  [Users]		U
	WHERE	U.[UserName]	= @UserName
END
