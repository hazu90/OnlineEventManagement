/****** Object:  StoredProcedure [dbo].[SessionCheckin_GetByInviteEventId]    Script Date: 11/30/2017 2:20:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 30/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SessionCheckin_GetByInviteEventId]
	@InviteEventId		INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [InviteEventId]
		  ,[SessionId]
		  ,[CheckinBy]
		  ,[CheckinDate]
		  ,[EventId]
		  ,[AccompanyMember]
   FROM	   [SessionCheckin]
   WHERE   [InviteEventId]		 = @InviteEventId	
END
