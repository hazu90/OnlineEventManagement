/****** Object:  StoredProcedure [dbo].[SessionCheckin_GetByInviteeIds]    Script Date: 11/30/2017 3:57:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 30/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SessionCheckin_GetByInviteeIds]
	@EventId		INT,
	@InviteeIds		NVARCHAR(4000),
	@SessionId		INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [Data] 
	INTO #tempIds
	FROM dbo.Split(@InviteeIds,',')

	SELECT [InviteEventId]
		  ,[SessionId]
		  ,[CheckinBy]
		  ,[CheckinDate]
		  ,[EventId]
		  ,[AccompanyMember]
	FROM   [SessionCheckin]
	WHERE  [EventId] = @EventId
	  AND  [InviteEventId] IN (SELECT [Data] FROM #tempIds )
	  AND  (@SessionId=0 OR [SessionId]	= @SessionId) 

	DROP TABLE #tempIds
END
