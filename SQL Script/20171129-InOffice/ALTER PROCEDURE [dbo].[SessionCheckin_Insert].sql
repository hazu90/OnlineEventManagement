SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 29/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SessionCheckin_Insert]
	@InviteEventId			INT,
	@SessionId				INT,
	@CheckinBy				NVARCHAR(50),
	@CheckinDate			DATETIME,
	@EventId				INT,
	@AccompanyMember		INT
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [SessionCheckin]([InviteEventId],[SessionId],[CheckinBy],[CheckinDate],[EventId],[AccompanyMember] )
						  VALUES(@InviteEventId ,@SessionId ,@CheckinBy ,@CheckinDate ,@EventId ,@AccompanyMember  )
END

