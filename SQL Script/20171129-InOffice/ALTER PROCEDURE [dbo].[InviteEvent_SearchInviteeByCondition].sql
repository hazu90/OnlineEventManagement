/****** Object:  StoredProcedure [dbo].[InviteEvent_SearchInviteeByCondition]    Script Date: 11/29/2017 2:45:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 29/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_SearchInviteeByCondition]
	@EventId			INT,
	@AssignTo			NVARCHAR(250),
	@SessionId			INT,
	@PhoningStatus		INT,
	@ConfirmAttend2st	INT,
	@ConfirmAttend1st	INT,
	@Status				BIT,
	@SendTicketStatus	INT,
	@SourceId			INT,
	@TextSearch			NVARCHAR(200),
	@StartDate			DATETIME,
	@EndDate			DATETIME
AS
BEGIN
	SET NOCOUNT ON;
	IF @ConfirmAttend1st = 0 OR @ConfirmAttend2st = 0
	BEGIN
	SELECT IE.[Id]
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[IsCheckIn]
		  ,IE.[CheckInBy]
		  ,IE.[CheckInDate]
		  ,IE.[Note]
		  ,IE.[CreatedBy]
		  ,IE.[CreatedDate]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[EventId]
		  ,IE.[PositionName]
		  ,IE.[UserName]
		  ,IE.[CustomerType]
		  ,IE.[IsUsingWebsiteBds]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[CompanyName]
		  ,IE.[PhoningStatus]
		  ,IE.[RegisteredDate]
		  ,IE.[CustomerEmail]
		  ,IE.[ConfirmAttend1st]
		  ,IE.[ConfirmAttend2st]
		  ,IE.[IntroduceUser]
		  ,IE.[RegisteredSource]
		  ,IE.[Status]
		  ,IE.[ApproveBy]
		  ,IE.[ApproveDate]
		  ,E.[EventCode]
		  ,E.[Name]		AS EventName
		  ,IE.[IsSelftRegistered]
		  ,IE.[Code]
		  ,IE.[AssignTo]
		  ,IE.[SourceId]
		  ,IE.[IsBcrmProcess]
		  ,IE.[Sessions]
		  ,IE.[SendTicketStatus]
		  ,IE.[BcrmAssignTo]
	FROM   [InviteEvent] IE
		INNER JOIN [Event] E   ON IE.[EventId]  = E.[Id]
	WHERE	 E.[Id] = @EventId 
		AND IE.[IsNotSuggest] = 0
		AND (@AssignTo	='' OR IE.[AssignTo] LIKE '%'+@AssignTo+'%' )
		AND (@SessionId =0  OR IE.[Sessions] = '0' OR (IE.[Sessions] IS NOT NULL AND @SessionId IN (SELECT [Data] FROM dbo.Split(IE.[Sessions],',') )) )
		AND (@PhoningStatus		= 0 OR (@PhoningStatus = -1 AND IE.[PhoningStatus] IS NULL ) OR ( IE.[ConfirmAttend1st] IS NULL AND IE.[PhoningStatus] = @PhoningStatus  )  )
		AND (@ConfirmAttend1st	= 0 OR ( IE.[ConfirmAttend2st] IS NULL AND IE.[ConfirmAttend1st] = @ConfirmAttend1st  ) )
		AND (@ConfirmAttend2st  = 0 OR ( IE.[ConfirmAttend2st] = @ConfirmAttend2st ) )
		AND (@Status IS NULL OR IE.[Status] = @Status ) 
		AND (@SendTicketStatus =0 OR  IE.[SendTicketStatus] = @SendTicketStatus )
		AND (@SourceId =0 OR IE.[SourceId] = @SourceId )
		AND (@TextSearch ='' OR IE.[TextSearch]	 LIKE ('%' + @TextSearch +'%'))
		AND (IE.[CreatedDate] BETWEEN @StartDate AND @EndDate )
	END
	ELSE	
	BEGIN
		SELECT IE.[Id]
			  ,IE.[CustomerName]
			  ,IE.[CustomerPhone]
			  ,IE.[IsCheckIn]
			  ,IE.[CheckInBy]
			  ,IE.[CheckInDate]
			  ,IE.[Note]
			  ,IE.[CreatedBy]
			  ,IE.[CreatedDate]
			  ,IE.[AccompanyMember]
			  ,IE.[IsNotSuggest]
			  ,IE.[EventId]
			  ,IE.[PositionName]
			  ,IE.[UserName]
			  ,IE.[CustomerType]
			  ,IE.[IsUsingWebsiteBds]
			  ,IE.[PlaceOfAttendEvent]
			  ,IE.[CompanyName]
			  ,IE.[PhoningStatus]
			  ,IE.[RegisteredDate]
			  ,IE.[CustomerEmail]
			  ,IE.[ConfirmAttend1st]
			  ,IE.[ConfirmAttend2st]
			  ,IE.[IntroduceUser]
			  ,IE.[RegisteredSource]
			  ,IE.[Status]
			  ,IE.[ApproveBy]
			  ,IE.[ApproveDate]
			  ,E.[EventCode]
			  ,E.[Name]		AS EventName
			  ,IE.[IsSelftRegistered]
			  ,IE.[Code]
			  ,IE.[AssignTo]
			  ,IE.[SourceId]
			  ,IE.[IsBcrmProcess]
			  ,IE.[Sessions]
			  ,IE.[SendTicketStatus]
			  ,IE.[BcrmAssignTo]
		FROM   [InviteEvent] IE
			INNER JOIN [Event] E   ON IE.[EventId]  = E.[Id]
		WHERE	 E.[Id] = @EventId 
			AND IE.[IsNotSuggest] = 0
			AND (@AssignTo	='' OR IE.[AssignTo] LIKE '%'+@AssignTo+'%' )
			AND (@SessionId =0  OR IE.[Sessions] = '0' OR (IE.[Sessions] IS NOT NULL AND @SessionId IN (SELECT [Data] FROM dbo.Split(IE.[Sessions],',') )) )
			AND (( IE.[ConfirmAttend2st] IS NULL AND IE.[ConfirmAttend1st] = @ConfirmAttend1st  ) 
				OR (( IE.[ConfirmAttend2st] = @ConfirmAttend2st ) ))
			AND (@SourceId =0 OR IE.[SourceId] = @SourceId )
			AND (@TextSearch ='' OR IE.[TextSearch]	 LIKE ('%' + @TextSearch +'%'))
			AND (IE.[CreatedDate] BETWEEN @StartDate AND @EndDate )
	END
END

