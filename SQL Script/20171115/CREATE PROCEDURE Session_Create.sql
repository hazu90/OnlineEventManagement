-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE Session_Create
	@EventId		INT,
	@Name			NVARCHAR(500),
	@StartDate		DATETIME,
	@EndDate		DATETIME,
	@Description	NVARCHAR(1000)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [Session]([EventId],[Name],[StartDate],[EndDate],[Description])
				   VALUES(@EventId ,@Name ,@StartDate ,@EndDate ,@Description  )
	SELECT SCOPE_IDENTITY()			   	
END
GO
