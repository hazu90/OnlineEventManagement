USE [BCRMv3]
GO
/****** Object:  StoredProcedure [dbo].[Company_GetById]    Script Date: 11/21/2017 1:29:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		HieuBV
-- Create date: 21/11/2014
-- Description:	Get ById
-- =============================================
CREATE PROCEDURE [dbo].[CI_Company_GetById]
	@Id INT
AS
BEGIN
	SET NOCOUNT ON;
    SELECT	[Id]
		,	[Code]
		,	[Name]
		,	[Address]
		,	[Manager]
		,	[CityId]
		,	[DistrictId]
		,	[TaxCode]
		,	[Type]
		,	[Size]
		,	[Status]
		,	[CreatedDate]
		,	[CreatedBy]
		,	[ApprovedDate]
		,	[ApprovedBy]
		,	[ParentId]
		,	[TotalCust]
		,	[GroupId]
	FROM	[Company]
	WHERE	[Id]	=	@Id
END

