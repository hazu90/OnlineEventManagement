/****** Object:  StoredProcedure [dbo].[InviteEvent_SyncToBCRM]    Script Date: 11/21/2017 3:20:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 21/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_SyncToBCRM]
	@Id					INT,
	@CustomerId			INT,
	@CustomerBirthday	DATETIME,
	@CustomerType		INT,
	@CompanyId			INT,
	@CompanyName		NVARCHAR(250),
	@CompanyAddress		NVARCHAR(500),
	@CompanySize		INT,
	@CustomerRevenue	INT,
	@BcrmStatus			INT,
	@IsBcrmProcess		BIT,
	@BcrmAssignTo		NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE	[InviteEvent]
	SET	 	[CustomerId]			= @CustomerId
		   ,[CustomerBirthday]		= @CustomerBirthday
		   ,[CustomerType]			= @CustomerType
		   ,[CompanyId]				= @CompanyId
		   ,[CompanyName]			= @CompanyName
		   ,[CompanyAddress]		= @CompanyAddress
		   ,[CompanySize]			= @CompanySize
		   ,[CustomerRevenue]		= @CustomerRevenue
		   ,[BcrmStatus]			= @BcrmStatus
		   ,[IsBcrmProcess]			= @IsBcrmProcess
		   ,[BcrmAssignTo]			= @BcrmAssignTo
		   ,[TextSearch]			=	CAST(@CustomerId	AS	NVARCHAR(10))		+	' '
										+	dbo.ConvertToBasicLatin([CustomerName])		+	' '
										+	ISNULL([CustomerPhone], '')					+	' '
										+	ISNULL([CustomerEmail], '')					+	' '
										+	dbo.ConvertToBasicLatin(@CompanyName)		+	' '
										+	dbo.ConvertToBasicLatin(@CompanyAddress)	+	' '
										+	dbo.ConvertToBasicLatin([Note])				+	' '
										+	dbo.ConvertToBasicLatin([TicketAddress])		+	' '
										+	dbo.ConvertToBasicLatin([StatusNote])		+	' '
	WHERE [Id]	= @Id
END
