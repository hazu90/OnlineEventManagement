/****** Object:  StoredProcedure [dbo].[InviteEvent_GetByCode]    Script Date: 11/21/2017 9:08:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 18/11/2017
-- Description:	Tìm kiếm danh sách khách mời theo mã vé
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_GetByCode]
	@EventId	INT,
	@Code		NVARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT IE.[Id]
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[IsCheckIn]
		  ,IE.[CheckInBy]
		  ,IE.[CheckInDate]
		  ,IE.[Note]
		  ,IE.[CreatedBy]
		  ,IE.[CreatedDate]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[EventId]
		  ,IE.[PositionName]
		  ,IE.[UserName]
		  ,IE.[CustomerType]
		  ,IE.[IsUsingWebsiteBds]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[CompanyName]
		  ,IE.[PhoningStatus]
		  ,IE.[RegisteredDate]
		  ,IE.[CustomerEmail]
		  ,IE.[ConfirmAttend1st]
		  ,IE.[ConfirmAttend2st]
		  ,IE.[IntroduceUser]
		  ,IE.[RegisteredSource]
		  ,IE.[Status]
		  ,IE.[ApproveBy]
		  ,IE.[ApproveDate]
		  ,E.[EventCode]
		  ,E.[Name]		AS EventName
		  ,IE.[IsSelftRegistered]
		  ,IE.[Code]
		  ,IE.[AssignTo]
		  ,IE.[SourceId]
		  ,IE.[IsBcrmProcess]
		  ,IE.[Sessions]
	FROM   [InviteEvent] IE
		INNER JOIN [Event] E   ON IE.[EventId]  = E.[Id]
	WHERE	 E.[Id]   = @EventId 
		AND IE.[Code] = @Code
END

