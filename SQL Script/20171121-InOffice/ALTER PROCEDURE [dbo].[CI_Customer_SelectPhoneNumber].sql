USE [BCRMv3]
GO
/****** Object:  StoredProcedure [dbo].[CI_Customer_SelectPhoneNumber]    Script Date: 11/21/2017 1:20:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 21/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[CI_Customer_SelectPhoneNumber]
	@IsDeleted		VARCHAR(1),
	@PhoneNumber	NVARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT TOP 1 C.[Id]
		,	C.[FullName]
		,	C.[PhoneNumber]
		,	C.[DOB]
		,	C.[Type]	
		,	C.[CompanyId]
		,	C.[Status]
		,	C.[Type]
		,	C.[AssignTo]
		,	C.[Description]
		,	C.[LastModifiedDate]
		,	C.[IsDeleted]
		,	C.[Email]
		,	C.[CareDate]
		,	C.[UtilityInfo]
		,	C.[ExtensionTime]
		,	C.[UseOwnerService]
		,	C.[Regional]
		,	C.[CreatedDate]
		,	C.[IsOldCustomer]
		,	O.[Id]		AS	[CustOwnerId]
	FROM		[Customer]		C	WITH	(NOLOCK)
	LEFT JOIN	[CustomerOwner] O	ON	O.[OriginalCustId]	=	C.[Id]
	WHERE	(C.[IsDeleted]	=	@IsDeleted)
		AND		CONTAINS(C.[PhoneNumber], @PhoneNumber)
	ORDER BY C.[CareDate] DESC
END
