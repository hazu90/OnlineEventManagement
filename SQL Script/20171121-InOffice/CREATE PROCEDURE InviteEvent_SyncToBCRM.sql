-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 21/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE InviteEvent_SyncToBCRM
	@Id					INT,
	@CustomerId			INT,
	@CustomerBirthday	DATETIME,
	@CustomerType		INT,
	@CompanyId			INT,
	@CompanyName		NVARCHAR(250),
	@CompanyAddress		NVARCHAR(500),
	@CompanySize		INT,
	@CustomerRevenue	INT,
	@BcrmStatus			INT,
	@IsBcrmProcess		BIT,
	@BcrmAssignTo		NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE	[InviteEvent]
	SET	 	[CustomerId]			= @CustomerId
		   ,[CustomerBirthday]		= @CustomerBirthday
		   ,[CustomerType]			= @CustomerType
		   ,[CompanyId]				= @CompanyId
		   ,[CompanyName]			= @CompanyName
		   ,[CompanyAddress]		= @CompanyAddress
		   ,[CompanySize]			= @CompanySize
		   ,[CustomerRevenue]		= @CustomerRevenue
		   ,[BcrmStatus]			= @BcrmStatus
		   ,[IsBcrmProcess]			= @IsBcrmProcess
		   ,[BcrmAssignTo]			= @BcrmAssignTo
		   ,[TextSearch]			=	CAST([CustomerId]	AS	NVARCHAR(10))		+	' '
										+	dbo.ConvertToBasicLatin([CustomerName])		+	' '
										+	ISNULL([CustomerPhone], '')					+	' '
										+	ISNULL([CustomerEmail], '')					+	' '
										+	dbo.ConvertToBasicLatin(@CompanyName)		+	' '
										+	dbo.ConvertToBasicLatin(@CompanyAddress)	+	' '
										+	dbo.ConvertToBasicLatin([Note])				+	' '
										+	dbo.ConvertToBasicLatin([TicketAddress])		+	' '
										+	dbo.ConvertToBasicLatin([StatusNote])		+	' '
END
GO
