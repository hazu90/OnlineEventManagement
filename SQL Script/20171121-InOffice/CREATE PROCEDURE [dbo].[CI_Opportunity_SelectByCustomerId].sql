USE [BCRMv3]
GO
/****** Object:  StoredProcedure [dbo].[Opportunity_SelectByCustomerId]    Script Date: 11/21/2017 1:39:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CI_Opportunity_SelectByCustomerId]
	@CustomerId INT
AS
BEGIN
	SELECT	O.[Id]
		,	O.[Viptype]
		,	O.[Name]
		,	O.[CustomerId]
		,	O.[BdsCustomerId]
		,	O.[AssignTo]
		,	O.[UserId]
		,	O.[Status]
		,	O.[Amount]
		,	O.[CreatedDate]
		,	O.[CreatedBy]
		,	O.[TypeOfPayment]
		,	O.[Package]
		,	T.[Amount]	AS	[TransactionAmount]
		,	T.[PaymentDate]	
		,	T.[TransactionId]		
		,	T.[IsConfirmed]
		,	T.[IsRoot]
		,	T.[VAT]
		,	T.[Discount]
		,	T.[RevenueType]
		,	T.[RevenueMonth]
		,	T.[BankCode]
		,	T.[IsProposalIncentive]
	FROM	[Opportunity]			O	WITH	(NOLOCK)
	LEFT	JOIN	[Transaction]	T	WITH	(NOLOCK)	ON	O.[Id]	=	T.[OpportunityId]
	WHERE	[CustomerId]	=	@CustomerId
	ORDER	BY	[CreatedDate]	DESC
END


