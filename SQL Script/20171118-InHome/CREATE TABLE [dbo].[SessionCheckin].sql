
/****** Object:  Table [dbo].[SessionCheckin]    Script Date: 11/18/2017 02:32:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SessionCheckin](
	[InviteEventId] [int] NOT NULL,
	[SessionId] [int] NOT NULL,
	[CheckinBy] [nvarchar](50) NULL,
	[CheckinDate] [datetime] NULL,
	[EventId] [int] NULL,
 CONSTRAINT [PK_SessionCheckin] PRIMARY KEY CLUSTERED 
(
	[InviteEventId] ASC,
	[SessionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


