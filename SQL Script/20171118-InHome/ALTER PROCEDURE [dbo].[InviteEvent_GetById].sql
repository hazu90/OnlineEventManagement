/****** Object:  StoredProcedure [dbo].[InviteEvent_GetById]    Script Date: 11/18/2017 03:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 18/11/2017
-- Description:	Lấy thông tin khách mời theo id
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_GetById]
	@Id		INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT IE.[Id]
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[IsCheckIn]
		  ,IE.[Note]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[EventId]
		  ,E.[Name] AS EventName
		  ,E.[EventCode]
		  ,IE.[CustomerEmail]
		  ,IE.[CompanyName]
		  ,IE.[CustomerType]
		  ,IE.[PositionName]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[RegisteredSource]
		  ,IE.[IntroduceUser]
		  ,IE.[Status]
		  ,IE.[Code]
		  ,IE.[AssignTo]
		  ,IE.[Sessions]
	FROM   [InviteEvent] IE
		INNER JOIN [Event] E ON IE.[EventId] = E.[Id]
	WHERE  IE.[Id] = @Id
END


