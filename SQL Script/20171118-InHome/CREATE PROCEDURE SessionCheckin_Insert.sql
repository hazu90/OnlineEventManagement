-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 18/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE SessionCheckin_Insert
	@InviteEventId			INT,
	@SessionId				INT,
	@CheckinBy				NVARCHAR(50),
	@CheckinDate			DATETIME,
	@EventId				INT
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [SessionCheckin]([InviteEventId],[SessionId],[CheckinBy],[CheckinDate],[EventId] )
						  VALUES(@InviteEventId ,@SessionId ,@CheckinBy ,@CheckinDate ,@EventId )
END
GO
