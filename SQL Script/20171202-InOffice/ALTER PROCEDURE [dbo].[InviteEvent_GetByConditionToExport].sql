SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 24/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_GetByConditionToExport] 
	@EventId		INT,
	@IsCheckIn		BIT,
	@IsNotSuggest   BIT,
	@SessionId		INT,
	@TextSearch		NVARCHAR(200)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT IE.[Id]					AS InviteId
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[IsCheckIn]
		  ,IE.[Note]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[EventId]
		  ,E.[Name]					AS EventName
		  ,E.[EventCode]
		  ,IE.[CheckInBy]
		  ,IE.[CheckInDate]
		  ,IE.[CreatedBy]
		  ,IE.[CustomerEmail]
		  ,IE.[PositionName]
		  ,IE.[UserName]
		  ,IE.[CustomerType]
		  ,IE.[CompanyName]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[IntroduceUser]
		  ,IE.[Status]
		  ,IE.[PhoningStatus]
		  ,IE.[ConfirmAttend1st]
		  ,IE.[ConfirmAttend2st]
		  ,IE.[Code]
		  ,IE.[GroupId]
		  ,IE.[BcrmAssignTo]
		  ,IE.[SourceId]
		  ,IE.[Sessions]
	FROM   [InviteEvent] IE
		INNER JOIN [Event] E ON IE.[EventId] = E.[Id]
	WHERE  IE.[EventId] = @EventId
		AND (@SessionId = 0 OR IE.[Sessions] ='0' OR (IE.[Sessions] IS NOT NULL AND @SessionId IN (SELECT [Data] FROM dbo.Split(IE.[Sessions],',') ) ) )
		AND	(@IsCheckIn IS NULL OR (IE.[IsCheckIn] IS NULL AND @IsCheckIn =0) OR (IE.[IsCheckIn]= @IsCheckIn) )
		AND (@IsNotSuggest IS NULL OR IE.[IsNotSuggest] = @IsNotSuggest)
		AND (IE.[Status] = 1 )
		AND (@TextSearch ='' OR IE.[TextSearch] LIKE '%'+@TextSearch +'%' )
END
