/****** Object:  StoredProcedure [dbo].[InviteEvent_UpdateBCRMInvitee]    Script Date: 11/24/2017 5:10:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 24/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_UpdateBCRMInvitee]
	@Id					INT,
	@CustomerName		NVARCHAR(500),
	@CustomerPhone		NVARCHAR(1000),
	@CustomerEmail		NVARCHAR(150),
	@CustomerPosition	INT,
	@Sessions			NVARCHAR(50),
	@Note				NVARCHAR(1000)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE	[InviteEvent]
	SET		[CustomerName]			= @CustomerName,
			[CustomerPhone]			= @CustomerPhone,
			[CustomerEmail]			= @CustomerEmail,
			[CustomerPosition]		= @CustomerPosition,
			[Sessions]				= @Sessions,
			[Note]					= @Note,
			[TextSearch]			=		CAST(@Id AS NVARCHAR(20))					+	' '
										+   CAST([CustomerId]	AS	NVARCHAR(10))		+	' '
										+	dbo.ConvertToBasicLatin(@CustomerName)		+	' '
										+	ISNULL(@CustomerPhone, '')					+	' '
										+	ISNULL(@CustomerEmail, '')					+	' '
										+	dbo.ConvertToBasicLatin([CompanyName])		+	' '
										+	dbo.ConvertToBasicLatin([CompanyAddress])	+	' '
										+	dbo.ConvertToBasicLatin(@Note)				+	' '
										+	dbo.ConvertToBasicLatin([TicketAddress])	+	' '
										+	dbo.ConvertToBasicLatin([StatusNote])		+	' '
										+	dbo.ConvertToBasicLatin([PlaceOfAttendEvent]) +	' '
										+	dbo.ConvertToBasicLatin([IntroduceUser])	  +	' '
										+	dbo.ConvertToBasicLatin([PositionName])		  +	' '
	WHERE   [Id]					= @Id
END

