/****** Object:  StoredProcedure [dbo].[InviteEvent_SyncToBCRM]    Script Date: 11/24/2017 4:35:12 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 24/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_SyncToBCRM]
	@Id					INT,
	@CustomerId			INT,
	@CustomerBirthday	DATETIME,
	@CustomerType		INT,
	@CompanyId			INT,
	@CompanyName		NVARCHAR(250),
	@CompanyAddress		NVARCHAR(500),
	@CompanySize		INT,
	@CustomerRevenue	INT,
	@BcrmStatus			INT,
	@IsBcrmProcess		BIT,
	@BcrmAssignTo		NVARCHAR(50),
	@GroupId			INT
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE	[InviteEvent]
	SET	 	[CustomerId]			= @CustomerId
		   ,[CustomerBirthday]		= @CustomerBirthday
		   ,[CustomerType]			= @CustomerType
		   ,[CompanyId]				= @CompanyId
		   ,[CompanyName]			= @CompanyName
		   ,[CompanyAddress]		= @CompanyAddress
		   ,[CompanySize]			= @CompanySize
		   ,[CustomerRevenue]		= @CustomerRevenue
		   ,[BcrmStatus]			= @BcrmStatus
		   ,[IsBcrmProcess]			= @IsBcrmProcess
		   ,[BcrmAssignTo]			= @BcrmAssignTo
		   ,[GroupId]				= @GroupId
		   ,[TextSearch]			=		CAST(@CustomerId	AS	NVARCHAR(10))		  +	' '
										+	dbo.ConvertToBasicLatin([CustomerName])		  +	' '
										+	ISNULL([CustomerPhone], '')					  +	' '
										+	ISNULL([CustomerEmail], '')					  +	' '
										+	dbo.ConvertToBasicLatin(@CompanyName)		  +	' '
										+	dbo.ConvertToBasicLatin(@CompanyAddress)	  +	' '
										+	dbo.ConvertToBasicLatin([Note])				  +	' '
										+	dbo.ConvertToBasicLatin([TicketAddress])	  +	' '
										+	dbo.ConvertToBasicLatin([StatusNote])		  +	' '
										+	dbo.ConvertToBasicLatin([PlaceOfAttendEvent]) +	' '
										+	dbo.ConvertToBasicLatin([IntroduceUser])	  +	' '
										+	dbo.ConvertToBasicLatin([PositionName])		  +	' '
	WHERE [Id]	= @Id
END
