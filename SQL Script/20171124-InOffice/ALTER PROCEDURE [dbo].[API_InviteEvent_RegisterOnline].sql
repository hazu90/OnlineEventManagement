USE [LocalCheckIn]
GO
/****** Object:  StoredProcedure [dbo].[API_InviteEvent_RegisterOnline]    Script Date: 11/24/2017 8:32:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 24/11/2017
-- Description:	Thêm mới khách mời qua API
-- =============================================
ALTER PROCEDURE [dbo].[API_InviteEvent_RegisterOnline]
	@CustomerName			NVARCHAR(500),
	@PositionName			NVARCHAR(200),
	@CustomerPhone			NVARCHAR(20),
	@IsUsingWebsiteBds		BIT,
	@CustomerEmail			NVARCHAR(150),
	@UserName				NVARCHAR(150),
	@CustomerType			INT,
	@RegisteredDate			DATETIME,
	@CompanyName			NVARCHAR(250),
	@PlaceOfAttendEvent		NVARCHAR(150),
	@EventId				INT,
	@RegisteredSource		INT,
	@Code					NVARCHAR(20)					
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [InviteEvent]([CustomerName] ,[PositionName],[CustomerPhone],[IsUsingWebsiteBds],
							  [CustomerEmail],[CustomerType],[RegisteredDate],[CompanyName],[PlaceOfAttendEvent],
							  [CreatedBy]	 ,[CreatedDate]	,[EventId] ,[RegisteredSource] ,[IsSelftRegistered],[UserName],
							  [Code]		 ,[TextSearch]  )
					   VALUES(@CustomerName  ,@PositionName ,@CustomerPhone ,@IsUsingWebsiteBds ,
							  @CustomerEmail ,@CustomerType ,@RegisteredDate ,@CompanyName ,@PlaceOfAttendEvent,
							  'Online'		 ,GETDATE()		,@EventId  ,@RegisteredSource , 1,@UserName,
							  @Code			 ,dbo.ConvertToBasicLatin(@CustomerName)		+	' '
												+	ISNULL(@CustomerPhone, '')					+	' '
												+	ISNULL(@CustomerEmail, '')					+	' '
												+	dbo.ConvertToBasicLatin(@CompanyName)		+	' '
												+	dbo.ConvertToBasicLatin(@PositionName)	+	' '
												+	dbo.ConvertToBasicLatin(@PlaceOfAttendEvent)+	' ')
	SELECT SCOPE_IDENTITY()
END
