USE [BCRMv3]
GO
/****** Object:  StoredProcedure [dbo].[Groups_GetAll]    Script Date: 11/10/2017 8:20:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Groups_GetAll]
	@Branch	NVARCHAR(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- Interfering with select statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE	@Cnt	INT
	DECLARE	@GroupTree	TABLE
	(	
		GroupId		INT, 
		GroupName	NVARCHAR(50),
		GroupAlias	NVARCHAR(50),
		Description	NVARCHAR(2000),
		ParentId	INT, 
		Leader		INT,
		Regional	INT,
		HLevel		INT,
		[Order]		INT,
		Row			VARCHAR(1000)
	)
	INSERT INTO	@GroupTree
	SELECT	GroupId, GroupName, GroupAlias, Description, ParentId, Leader, Regional, 0,[Order],CONVERT(VARCHAR(10),[Order])+RIGHT('000'+ CONVERT(VARCHAR(10),GroupId), 3)
	FROM	Groups	G
	WHERE	ParentId		=	0
	ORDER	BY	G.[Order]	ASC

	SELECT	@Cnt = 0
	WHILE	@@ROWCOUNT	>	0
	BEGIN
		SELECT	@Cnt	=	@Cnt	+	1
		INSERT 	@GroupTree
		SELECT	G.GroupId, G.GroupName, G.GroupAlias, G.Description, G.ParentId, G.Leader, G.Regional, @Cnt,G.[Order], row + CONVERT(VARCHAR(10),G.[Order])+RIGHT('000' + CONVERT(VARCHAR(10),G.GroupId), 3)
		FROM	Groups				G
		INNER	JOIN	@GroupTree	T	ON	G.ParentId	=	T.GroupId
		WHERE	T.HLevel		=	@Cnt - 1
		ORDER	BY	G.[Order]	ASC		
	END

	SELECT	GroupId, 
			GroupName = REPLICATE('--', HLevel) + GroupName,
			GroupAlias,
			Description,
			ParentId,
			Leader,
			Regional,
			HLevel,			
			[Order]
	FROM	@GroupTree
	ORDER	BY Row
END



