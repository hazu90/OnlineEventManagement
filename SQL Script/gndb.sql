USE [LocalCheckIn]
GO
/****** Object:  User [pmnb]    Script Date: 12/1/2017 7:38:00 PM ******/
CREATE USER [pmnb] FOR LOGIN [pmnb] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [pmnb]
GO
/****** Object:  FullTextCatalog [FTSearch]    Script Date: 12/1/2017 7:38:00 PM ******/
CREATE FULLTEXT CATALOG [FTSearch]WITH ACCENT_SENSITIVITY = ON

GO
/****** Object:  UserDefinedFunction [dbo].[ConvertToBasicLatin]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Doãn Hải Vân
-- Create date: <Create Date, ,>
-- Description:	Convert sang tiếng việt không dấu
-- =============================================
CREATE FUNCTION [dbo].[ConvertToBasicLatin]
(
	-- Add the parameters for the function here
	@Value	NVARCHAR(4000)
)
RETURNS VARCHAR(4000)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Results		VARCHAR(4000)
	DECLARE @UniChars		NVARCHAR(140)
	DECLARE	@UnsignChars	NVARCHAR(140)
	DECLARE	@AllowChars		VARCHAR(68)
   	DECLARE @Position		INT
   	DECLARE	@Index			INT
   	DECLARE	@Char			NCHAR(1)
   	DECLARE	@NonChar		NCHAR(1)
   	DECLARE	@PreviousChar	NCHAR(1)

	-- Add the T-SQL statements to compute the return value here
	SET	@Results		= ''
	SET @Position		= 1
	SET @UniChars		= N'àáảãạâầấẩẫậăằắẳẵặèéẻẽẹêềếểễệđìíỉĩịòóỏõọôồốổỗộơờớởỡợùúủũụưừứửữựỳýỷỹỵÀÁẢÃẠÂẦẤẨẪẬĂẰẮẲẴẶÈÉẺẼẸÊỀẾỂỄỆĐÌÍỈĨỊÒÓỎÕỌÔỒỐỔỖỘƠỜỚỞỠỢÙÚỦŨỤƯỪỨỬỮỰỲÝỶỸỴÂĂĐÔƠƯ'
	SET @UnsignChars	= N'aaaaaaaaaaaaaaaaaeeeeeeeeeeediiiiiooooooooooooooooouuuuuuuuuuuyyyyyAAAAAAAAAAAAAAAAAEEEEEEEEEEEDIIIOOOOOOOOOOOOOOOOOOOUUUUUUUUUUUYYYYYAADOOU'
	SET @AllowChars		= 'abcdefghijklmnopqrstxyzuvxw0123456789,./<>?!@#$%^&*()-_=+''\"|[]{}~`'			
	SET	@PreviousChar	= ''
	WHILE @Position		<=	DATALENGTH(@Value) / 2
		BEGIN
			SET	@Char	= ''
			SET	@NonChar= ''
			SET	@Index	= 0
			SET	@Char	= LOWER(SUBSTRING(@Value, @Position, 1))
			IF	@Char	=	' '
				BEGIN
					IF	@PreviousChar	!=	' '
						BEGIN
							SET	@Results	=	@Results + ' '
						END
					SET	@PreviousChar	=	@Char
				END
			ELSE
				BEGIN
					SET	@Index	= CHARINDEX(@Char , @UniChars)
					IF	@Index	> 0
						BEGIN
							SET	@NonChar		=	LOWER(SUBSTRING(@UnsignChars, @Index, 1))
							SET	@Results		=	@Results + @NonChar
							SET	@PreviousChar	=	@Char
						END
					ELSE
						BEGIN
							SET	@Index	=	0
							SET	@Index	=	CHARINDEX(@Char , @AllowChars)
							IF	@Index	>	0
								BEGIN
									SET	@Results		=	@Results + @Char	
									SET	@PreviousChar	=	@Char
								END
						END
				END			
			SET @Position	=	@Position + 1
		END

	-- Return the result of the function
	RETURN	@Results
END



GO
/****** Object:  UserDefinedFunction [dbo].[Split]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[Split]
(
	@RowData	NVARCHAR(4000),
	@SplitOn	NVARCHAR(5)
)  
RETURNS @RtnValue TABLE 
(
	Id		INT	IDENTITY(1,1),
	Data	NVARCHAR(100)
) 
AS  
BEGIN 
	DECLARE @Cnt	INT
	SET		@Cnt	=	1

	WHILE (CHARINDEX(@SplitOn,@RowData)>0)
	BEGIN
		INSERT	INTO	@RtnValue (Data)
		SELECT	Data		= LTRIM(RTRIM(SUBSTRING(@RowData,1,CHARINDEX(@SplitOn,@RowData)-1)))
		SET		@RowData	= SUBSTRING(@RowData,CHARINDEX(@SplitOn,@RowData)+1,LEN
		(@RowData))
		SET		@Cnt		= @Cnt + 1
	END
	INSERT	INTO @RtnValue (Data)
	SELECT	Data = LTRIM(RTRIM(@RowData))
	RETURN
END

GO
/****** Object:  Table [dbo].[CheckInTimer]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CheckInTimer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EventId] [int] NULL,
	[StartTime] [datetime] NULL,
	[EndTime] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[IsInWebCheckin] [bit] NULL,
 CONSTRAINT [PK_CheckInTimer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Event]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Event](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Description] [nvarchar](1000) NULL,
	[StartDate] [date] NULL,
	[EndDate] [date] NULL,
	[IsEnable] [bit] NULL,
	[Deadline] [date] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[EventCode] [nvarchar](50) NULL,
	[SMSTitle] [nvarchar](1000) NULL,
	[Deploy] [nvarchar](500) NULL,
	[RevenueStartDate] [date] NULL,
	[RevenueEndDate] [date] NULL,
	[RevenueCalculation] [int] NULL,
	[IsAllowAttach] [bit] NULL,
	[IsBcrmUse] [bit] NULL,
 CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InviteEvent]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InviteEvent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EventId] [int] NULL,
	[CustomerId] [int] NULL,
	[CustomerName] [nvarchar](500) NULL,
	[CustomerPhone] [nvarchar](1000) NULL,
	[CustomerSex] [bit] NULL,
	[CustomerEmail] [nvarchar](150) NULL,
	[CustomerBirthday] [date] NULL,
	[CustomerPosition] [int] NULL,
	[PositionName] [nvarchar](200) NULL,
	[CustomerType] [int] NULL,
	[CompanyId] [int] NULL,
	[CompanyName] [nvarchar](250) NULL,
	[CompanyAddress] [nvarchar](500) NULL,
	[CompanySize] [int] NULL,
	[TicketAddress] [nvarchar](500) NULL,
	[GroupId] [int] NULL,
	[AbilityAttend] [int] NULL,
	[IsCheckIn] [bit] NULL,
	[CheckInBy] [nvarchar](50) NULL,
	[CheckInDate] [datetime] NULL,
	[Note] [nvarchar](1000) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[AccompanyMember] [int] NULL,
	[IsNotSuggest] [bit] NULL CONSTRAINT [DF__InviteEve__IsNot__2C3393D0]  DEFAULT ((0)),
	[UserName] [nvarchar](150) NULL,
	[IsUsingWebsiteBds] [bit] NULL,
	[PlaceOfAttendEvent] [nvarchar](150) NULL,
	[PhoningStatus] [int] NULL,
	[RegisteredDate] [datetime] NULL,
	[ConfirmAttend1st] [int] NULL,
	[ConfirmAttend2st] [int] NULL,
	[IntroduceUser] [nvarchar](150) NULL,
	[RegisteredSource] [int] NULL,
	[ApproveBy] [nvarchar](50) NULL,
	[ApproveDate] [datetime] NULL,
	[Status] [bit] NULL,
	[IsSelftRegistered] [bit] NULL CONSTRAINT [DF_InviteEvent_IsSelftRegistered]  DEFAULT ((0)),
	[Code] [nvarchar](20) NULL,
	[AssignTo] [nvarchar](250) NULL,
	[StatusNote] [nvarchar](1000) NULL,
	[TextSearch] [nvarchar](max) NULL,
	[CustomerVip] [int] NULL,
	[SendTicketType] [int] NULL,
	[Sessions] [varchar](50) NULL,
	[SourceId] [int] NULL CONSTRAINT [DF_InviteEvent_SourceId]  DEFAULT ((2)),
	[CustomerRevenue] [int] NULL,
	[BcrmStatus] [int] NULL,
	[IsBcrmProcess] [bit] NULL CONSTRAINT [DF_InviteEvent_IsBcrmProcess]  DEFAULT ((0)),
	[BcrmAssignTo] [nvarchar](50) NULL,
	[SendTicketStatus] [int] NULL CONSTRAINT [DF_InviteEvent_SendTicketStatus]  DEFAULT ((0)),
 CONSTRAINT [PK_InviteEvent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InviteEventAttach]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InviteEventAttach](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[InviteEventId] [int] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Phone] [nvarchar](100) NULL,
	[Email] [nvarchar](200) NULL,
	[Position] [int] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[IsCheckIn] [bit] NULL,
	[CheckInBy] [nvarchar](50) NULL,
	[CheckInDate] [datetime] NULL,
	[CustomerVip] [int] NULL,
	[AccompanyMember] [int] NULL,
	[Code] [varchar](20) NULL,
 CONSTRAINT [PK_InviteEventAttach] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InviteEventHistory]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InviteEventHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OldPhoningStatus] [int] NULL,
	[NewPhoningStatus] [int] NULL,
	[OldConfirmAttend1st] [int] NULL,
	[NewConfirmAttend1st] [int] NULL,
	[OldConfirmAttend2st] [int] NULL,
	[NewConfirmAttend2st] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Status] [int] NULL,
	[OldPlaceOfAttendEvent] [nvarchar](150) NULL,
	[NewPlaceOfAttendEvent] [nvarchar](150) NULL,
	[InviteEventId] [int] NULL,
	[OtherChange] [nvarchar](max) NULL,
	[Action] [nvarchar](50) NULL,
	[OldValue] [nvarchar](1000) NULL,
	[NewValue] [nvarchar](1000) NULL,
	[Note] [nvarchar](1000) NULL,
 CONSTRAINT [PK_InviteEventHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LuckyNumber]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LuckyNumber](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EventId] [int] NULL,
	[IsInWebCheckin] [bit] NULL,
	[Code] [nvarchar](20) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_LuckyNumber] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SendSMSHistory]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SendSMSHistory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SMSResponse] [int] NULL,
	[Description] [nvarchar](200) NULL,
	[InviteEventId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[PhoneNumber] [nvarchar](1000) NULL,
 CONSTRAINT [PK_SendSMSHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Session]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Session](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EventId] [int] NULL,
	[Name] [nvarchar](500) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Session] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SessionCheckin]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SessionCheckin](
	[InviteEventId] [int] NOT NULL,
	[SessionId] [int] NOT NULL,
	[CheckinBy] [nvarchar](50) NULL,
	[CheckinDate] [datetime] NULL,
	[EventId] [int] NULL,
	[AccompanyMember] [int] NULL,
 CONSTRAINT [PK_SessionCheckin] PRIMARY KEY CLUSTERED 
(
	[InviteEventId] ASC,
	[SessionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SetDefaultEvent]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SetDefaultEvent](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EventId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[IsInWebCheckin] [bit] NULL,
	[IsCancel] [bit] NULL CONSTRAINT [DF_SetDefaultEvent_IsCancel]  DEFAULT ((0)),
 CONSTRAINT [PK_SetDefaultEvent] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Source]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Source](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_System] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NULL,
	[Status] [int] NULL,
	[Role] [int] NULL,
	[LockTime] [int] NULL,
	[LockedStartDate] [datetime] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[RoleId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  StoredProcedure [dbo].[API_InviteEvent_RegisterOnline]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 27/11/2017
-- Description:	Thêm mới khách mời qua API
-- =============================================
CREATE PROCEDURE [dbo].[API_InviteEvent_RegisterOnline]
	@CustomerName			NVARCHAR(500),
	@PositionName			NVARCHAR(200),
	@CustomerPhone			NVARCHAR(20),
	@IsUsingWebsiteBds		BIT,
	@CustomerEmail			NVARCHAR(150),
	@UserName				NVARCHAR(150),
	@CustomerType			INT,
	@RegisteredDate			DATETIME,
	@CompanyName			NVARCHAR(250),
	@PlaceOfAttendEvent		NVARCHAR(150),
	@EventId				INT,
	@RegisteredSource		INT,
	@Code					NVARCHAR(20),
	@Sessions				NVARCHAR(50)					
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [InviteEvent]([CustomerName] ,[PositionName],[CustomerPhone],[IsUsingWebsiteBds],
							  [CustomerEmail],[CustomerType],[RegisteredDate],[CompanyName],[PlaceOfAttendEvent],
							  [CreatedBy]	 ,[CreatedDate]	,[EventId] ,[RegisteredSource] ,[IsSelftRegistered],[UserName],
							  [Code]		 ,[Sessions] ,[IsBcrmProcess],[SourceId],[SendTicketStatus], [TextSearch]  )
					   VALUES(@CustomerName  ,@PositionName ,@CustomerPhone ,@IsUsingWebsiteBds ,
							  @CustomerEmail ,@CustomerType ,@RegisteredDate ,@CompanyName ,@PlaceOfAttendEvent,
							  'Online'		 ,GETDATE()		,@EventId  ,@RegisteredSource , 1,@UserName,
							  @Code			 ,@Sessions , 0 ,2			 ,0							  , dbo.ConvertToBasicLatin(@CustomerName)			+	' '
																										+	ISNULL(@CustomerPhone, '')					+	' '
																										+	ISNULL(@CustomerEmail, '')					+	' '
																										+	dbo.ConvertToBasicLatin(@CompanyName)		+	' '
																										+	dbo.ConvertToBasicLatin(@PositionName)		+	' '
																										+	dbo.ConvertToBasicLatin(@PlaceOfAttendEvent)+	' ')
	SELECT SCOPE_IDENTITY()
END

GO
/****** Object:  StoredProcedure [dbo].[BCRM_Event_GetAll]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 09/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_Event_GetAll]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT	[Id]
		,	[Name]
		,	[Description]
		,	[StartDate]
		,	[EndDate]
		,	[Deploy]
		,	[IsEnable]
		,	[Deadline]
		,	[RevenueStartDate]
		,	[RevenueEndDate]
		,	[RevenueCalculation]
		,	[CreatedDate]
		,	[CreatedBy]
		,	[IsAllowAttach]
		,	[EventCode]
		,	[SMSTitle]
	FROM	[Event]
	WHERE	[IsBcrmUse] = 1
	ORDER	BY	[StartDate]	DESC
END


GO
/****** Object:  StoredProcedure [dbo].[BCRM_Event_GetByBranchId]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 09/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_Event_GetByBranchId]
	-- Add the parameters for the stored procedure here
	@BranchId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	[Id]
		,	[Name] 
		,	[Description]
		,	[StartDate]
		,	[EndDate]
		,	[Deploy]
		,	[IsEnable]
		,	[Deadline]
		,	[CreatedDate]
		,	[CreatedBy]
		,	[IsAllowAttach]
		,	[EventCode]
		,	[SMSTitle]
	FROM	[Event]
	WHERE	[IsBcrmUse] = 1
		AND	(@BranchId	=	0	
		OR	[Deploy]	=	'0'	
		OR	@BranchId	IN	(SELECT [Data] FROM dbo.Split([Deploy],',')))
END


GO
/****** Object:  StoredProcedure [dbo].[BCRM_Event_GetByCondition]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 09/11/2017
-- Description:	@Description,
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_Event_GetByCondition]-- exec Event_GetByCondition null,'58',1,null, null
	-- Add the parameters for the stored procedure here
	@Name			NVARCHAR(500),
	@Deploy			INT,
	@StartDate		DATE,
	@EndDate		DATE,
	@PageIndex		INT,
	@PageSize		INT,
	@TotalRecord	INT OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	[Id]
		,	[Name]
		,	[Description]
		,	[StartDate]
		,	[EndDate]
		,	[Deadline]
		,	[Deploy]
		,	[IsEnable]
		,	[RevenueStartDate]
		,	[RevenueEndDate]
		,	[RevenueCalculation]
		,	[CreatedDate]
		,	[CreatedBy]
		,	[IsAllowAttach]
		,	[EventCode]
		,	[SMSTitle]
	FROM	(
				SELECT	[Id]
					,	[Name]
					,	[Description]
					,	[StartDate]
					,	[EndDate]
					,	[Deadline]
					,	[Deploy]
					,	[IsEnable]
					,	[RevenueStartDate]
					,	[RevenueEndDate]
					,	[RevenueCalculation]
					,	[CreatedDate]
					,	[CreatedBy]
					,	[IsAllowAttach]
					,	[EventCode]
					,	[SMSTitle]
					,	ROW_NUMBER() OVER(ORDER BY Id DESC) [Row]
				FROM	[Event]
				WHERE	[IsBcrmUse] = 1	
					AND (@Name		IS	NULL	OR	[Name] LIKE '%'	+	@Name	+	'%'	)
					AND	(@StartDate	IS	NULL	OR	[StartDate] >=	@StartDate	)
					AND	(@EndDate	IS	NULL	OR	[EndDate]	<=	@EndDate	)
					AND	(
							@Deploy		=	0		
						OR
							[Deploy]	=	'0'
						OR	
							@Deploy		IN	(SELECT [data] FROM dbo.Split([Deploy],','))
						)
			)	Temp
	WHERE	[Row]	BETWEEN	((@PageIndex - 1) * @PageSize + 1)	AND	@PageIndex * @PageSize 

	SELECT	@TotalRecord = COUNT(Id) 
	FROM	[Event]
	WHERE	[IsBcrmUse] = 1	
		AND (@Name		IS	NULL	OR	[Name] LIKE '%'	+	@Name	+	'%'	)
		AND	(@StartDate	IS	NULL	OR	[StartDate] >=	@StartDate	)
		AND	(@EndDate	IS	NULL	OR	[EndDate]	<=	@EndDate	)
		AND	(
				@Deploy		=	0		
			OR
				[Deploy]	=	'0'
			OR	
				@Deploy		IN	(SELECT [Data] FROM dbo.Split([Deploy],','))
			)
END


GO
/****** Object:  StoredProcedure [dbo].[BCRM_Event_GetById]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 09/11/2017
-- Description:	@Description,
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_Event_GetById]
	-- Add the parameters for the stored procedure here
	@Id	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	[Id]
		,	[Name]
		,	[Description]
		,	[StartDate]
		,	[EndDate]
		,	[Deadline]
		,	[Deploy]
		,	[IsEnable]
		,	[RevenueStartDate]
		,	[RevenueEndDate]
		,	[RevenueCalculation]
		,	[CreatedDate]
		,	[CreatedBy]
		,	[IsAllowAttach]
		,	[EventCode]
		,	[SMSTitle]
	FROM	[Event]
	WHERE	[Id]	=	@Id
END


GO
/****** Object:  StoredProcedure [dbo].[BCRM_Event_GetByManyId]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 09/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_Event_GetByManyId]
	-- Add the parameters for the stored procedure here
	@Ids	NVARCHAR(1000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	[Id]
		,	[Name]
		,	[Description]
		,	[StartDate]
		,	[EndDate]
		,	[Deploy]
		,	[IsEnable]
		,	[Deadline]
		,	[RevenueStartDate]
		,	[RevenueEndDate]
		,	[RevenueCalculation]
		,	[CreatedDate]
		,	[CreatedBy]
		,	[IsAllowAttach]
		,	[EventCode]
		,	[SMSTitle]
	FROM	[Event]
	WHERE	[Id]	IN	(SELECT [Data] FROM dbo.Split(@Ids, ','))
END


GO
/****** Object:  StoredProcedure [dbo].[BCRM_Event_GetCurrentActive]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TungNT
-- Create date: 09/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_Event_GetCurrentActive]
	-- Add the parameters for the stored procedure here
	@GroupIds	VARCHAR(1000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	[Id]
		,	[Name]
		,	[Description]
		,	[RevenueCalculation]
		,	[RevenueStartDate]
		,	[RevenueEndDate]
		,	[IsAllowAttach]
		,	[EventCode]
		,	[SMSTitle]
		,	[Deadline]
	FROM	[Event]
	WHERE	[StartDate]	<=	GETDATE()
		AND	[EndDate]	>=	GETDATE()
		AND	[IsEnable]	=	1
		AND	[IsBcrmUse]	=	1
		AND	(	
				'0'		=	[Deploy]
			OR	
				0		<	(SELECT COUNT([Id]) FROM dbo.Split([Deploy], ',') WHERE [Data] IN (SELECT [Data] FROM dbo.Split(@GroupIds, ',')))
			)
	ORDER	BY	[StartDate]	DESC
END



GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEvent_Create]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TungNT
-- Create date: 09/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEvent_Create]
	-- Add the parameters for the stored procedure here
	@EventId			INT
,   @CustomerId			INT
,   @CustomerName		NVARCHAR(500)
,   @CustomerPhone		NVARCHAR(1000)
,   @CustomerBirthday	DATE
,   @CustomerSex		BIT
,   @CustomerEmail		NVARCHAR(50)
,   @CustomerRevenue	INT
,   @CustomerType		INT
,   @CustomerPosition	INT
,	@CustomerVip		INT
,   @CompanyId			INT
,   @CompanyName		NVARCHAR(500)
,	@CompanySize		INT
,   @CompanyAddress		NVARCHAR(500)
,	@TicketAddress		NVARCHAR(500)
,	@GroupId			INT
,   @AbilityAttend		INT
,   @Note				NVARCHAR(1000)
,   @BcrmStatus			INT
,   @CreatedBy			NVARCHAR(50)
,	@TextSearchAttach	NVARCHAR(1000)
,	@SendTicketType		INT
,	@Code				VARCHAR(20)
,	@Sessions			VARCHAR(50)
,	@UserName			NVARCHAR(150)
,	@IsBcrmProcess		BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @DuplicateCode INT
	SELECT  @DuplicateCode = COUNT(Id) FROM InviteEvent WHERE [EventId] = @EventId AND [Code] = @Code
	IF	@DuplicateCode = 0
	BEGIN
		INSERT INTO	[InviteEvent]
           (
				[EventId]
           ,	[CustomerId]
           ,	[CustomerName]
           ,	[CustomerPhone]
           ,	[CustomerBirthday]
           ,	[CustomerSex]
           ,	[CustomerEmail]
           ,	[CustomerRevenue]
           ,	[CustomerType]
           ,	[CustomerPosition]
		   ,	[CustomerVip]
           ,	[CompanyId]
           ,	[CompanyName]
		   ,	[CompanySize]
           ,	[CompanyAddress]
		   ,	[TicketAddress]
           ,	[GroupId]
           ,	[AbilityAttend]
           ,	[Note]
           ,	[BcrmStatus]
           ,	[CreatedBy]
           ,	[CreatedDate]
		   ,	[TextSearch]
		   ,	[SendTicketType]
		   ,	[Code]
		   ,	[Sessions]
		   ,	[SourceId]
		   ,	[IntroduceUser]
		   ,	[IsBcrmProcess]
		   ,	[BcrmAssignTo]
		   )
    VALUES
           (
				@EventId
           ,	@CustomerId
           ,	@CustomerName
           ,	@CustomerPhone
           ,	@CustomerBirthday
           ,	@CustomerSex
           ,	@CustomerEmail
           ,	@CustomerRevenue
           ,	@CustomerType
           ,	@CustomerPosition
		   ,	@CustomerVip
           ,	@CompanyId
           ,	@CompanyName
		   ,	@CompanySize
           ,	@CompanyAddress
		   ,	@TicketAddress
           ,	@GroupId
           ,	@AbilityAttend
           ,	@Note
           ,	@BcrmStatus
           ,	@CreatedBy
           ,	GETDATE()
		   ,	CAST(@CustomerId	AS	NVARCHAR(10))			+	' '
			+	dbo.ConvertToBasicLatin(@CustomerName)		+	' '
			+	ISNULL(@CustomerPhone, '')					+	' '
			+	ISNULL(@CustomerEmail, '')					+	' '
			+	dbo.ConvertToBasicLatin(@CompanyName)		+	' '
			+	dbo.ConvertToBasicLatin(@CompanyAddress)	+	' '
			+	dbo.ConvertToBasicLatin(@Note)				+	' '
			+	dbo.ConvertToBasicLatin(@TicketAddress)		+	' '
			+	dbo.ConvertToBasicLatin(@TextSearchAttach)  +	' '
			,	@SendTicketType
			,	@Code
			,	@Sessions
			,	1
			,	@UserName
			,	@IsBcrmProcess
			,	@CreatedBy)
		SELECT	@@IDENTITY
		
	END
	ELSE
		SELECT 0
END


GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEvent_Delete]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 09/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEvent_Delete]
	-- Add the parameters for the stored procedure here
	@Id			INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN	TRY
		BEGIN	TRANSACTION	InviteEvent_Delete
			DELETE	
			FROM	[InviteEventHistory]
			WHERE	[InviteEventId]	=	@Id

			DELETE	
			FROM	[InviteEvent]
			WHERE	[Id]			=	@Id
		COMMIT	TRANSACTION	InviteEvent_Delete
		SELECT	1
	END		TRY
	BEGIN	CATCH
		ROLLBACK	TRANSACTION	InviteEvent_Delete
		SELECT	0
	END		CATCH
END


GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEvent_EditStatus]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TungNT
-- Create date: 09/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEvent_EditStatus]
	-- Add the parameters for the stored procedure here
	@Id			INT
,	@BcrmStatus		INT
,	@StatusNote	NVARCHAR(1000)
,	@ApproveBy	NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN	TRY
		BEGIN	TRANSACTION	InviteEvent_EditStatus
			UPDATE	[InviteEvent]
			SET		[BcrmStatus]	=	@BcrmStatus
				,	[StatusNote]	=	@StatusNote
				,	[ApproveBy]		=	@ApproveBy
				,	[ApproveDate]	=	GETDATE()	
				,	[TextSearch]	=	CAST([CustomerId]	AS	NVARCHAR(10))		+	' '
									+	dbo.ConvertToBasicLatin([CustomerName])		+	' '
									+	ISNULL([CustomerPhone], '')					+	' '
									+	ISNULL([CustomerEmail], '')					+	' '
									+	dbo.ConvertToBasicLatin([CompanyName])		+	' '
									+	dbo.ConvertToBasicLatin([CompanyAddress])	+	' '
									+	dbo.ConvertToBasicLatin([Note])				+	' '
									+	dbo.ConvertToBasicLatin([TicketAddress])	+	' '
									+	dbo.ConvertToBasicLatin(@StatusNote)
									+	(	
											SELECT	dbo.ConvertToBasicLatin
											(
												STUFF
												(
													(
														SELECT	' ' + [Name] + ' ' + [Phone] + ' ' + ISNULL([Email], '')
														FROM	[InviteEventAttach]
														WHERE	[InviteEventId]	=	@Id
														FOR		XML	PATH(''), TYPE
													).value('.', 'NVARCHAR(MAX)'), 1, 1, ''
												)
											)
										)
			WHERE	[Id]			=	@Id

		--CÁC TRẠNG THÁI PHONENINGSTATUS LÀ TRẠNG THÁI CỦA PHẦN MỀM CHECKIN
		--Trạng thái mới | bổ sung thông tin
		IF	@BcrmStatus = 0 OR @BcrmStatus = 10
			UPDATE InviteEvent SET [PhoningStatus] = NULL WHERE Id = @Id
		--Trạng thái TP duyệt
		IF	@BcrmStatus = 2
			UPDATE InviteEvent SET [PhoningStatus] = 6 WHERE Id = @Id
		--Trạng thái BTC duyệt
		IF	@BcrmStatus = 4
			UPDATE InviteEvent SET [PhoningStatus] = NULL WHERE Id = @Id
		--Trạng thái BTC từ chối, TP từ chối
		IF	@BcrmStatus = 3 OR @BcrmStatus = 5
			UPDATE InviteEvent SET [PhoningStatus] = 5 WHERE Id = @Id
		--Trạng thái KH đồng ý tham dự
		IF	@BcrmStatus = 11
			UPDATE InviteEvent SET [ConfirmAttend1st] = 1, [PhoningStatus] = 1 WHERE Id = @Id
		--Trạng thái KH từ chối tham dự
		IF	@BcrmStatus = 12
			UPDATE InviteEvent SET [ConfirmAttend1st] = 2, [PhoningStatus] = 1 WHERE Id = @Id
		--Trạng thái KH xem xét tham dự
		IF	@BcrmStatus = 14
			UPDATE InviteEvent SET [ConfirmAttend1st] = 3, [PhoningStatus] = 1 WHERE Id = @Id
		--BTC xác nhận KH tham dự
		IF	@BcrmStatus = 8
			UPDATE InviteEvent SET [ConfirmAttend2st] = 1,[ConfirmAttend1st] = 1, [PhoningStatus] = 1 WHERE Id = @Id
		-- BTC xác nhận KH không tham dự
		IF	@BcrmStatus = 9
			UPDATE InviteEvent SET [ConfirmAttend2st] = 2,[ConfirmAttend1st] = 1, [PhoningStatus] = 1 WHERE Id = @Id
		--BTC xác nhận KH đang xem xét tham dự
		IF	@BcrmStatus = 13
			UPDATE InviteEvent SET [ConfirmAttend2st] = 3,[ConfirmAttend1st] = 1, [PhoningStatus] = 1 WHERE Id = @Id
		--Trạng thái gửi SMS
		IF	@BcrmStatus = 16 
			UPDATE InviteEvent SET [Status] = 1, [SendTicketStatus] = 1  WHERE Id = @Id
		--Trạng thái gửi lại SMS
		IF	@BcrmStatus = 17
			UPDATE InviteEvent SET	[SendTicketStatus] = 2  WHERE Id = @Id
		COMMIT	TRANSACTION	InviteEvent_EditStatus
		SELECT	1
	END		TRY
	BEGIN	CATCH
		ROLLBACK	TRANSACTION	InviteEvent_EditStatus
		SELECT	0
	END		CATCH
END





GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEvent_EditStatusNote]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		TungNT
-- Create date: 09/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEvent_EditStatusNote]
	-- Add the parameters for the stored procedure here
	@Id			INT
,	@BcrmStatus		INT
,	@StatusNote	NVARCHAR(1000)
,	@CreatedBy	NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN	TRY
		BEGIN	TRANSACTION	InviteEvent_EditStatusNote
			UPDATE	[InviteEvent]
			SET		[StatusNote]	=	@StatusNote
				,	[TextSearch]	=	CAST([CustomerId]	AS	NVARCHAR(10))		+	' '
									+	dbo.ConvertToBasicLatin([CustomerName])		+	' '
									+	ISNULL([CustomerPhone], '')					+	' '
									+	ISNULL([CustomerEmail], '')					+	' '
									+	dbo.ConvertToBasicLatin([CompanyName])		+	' '
									+	dbo.ConvertToBasicLatin([CompanyAddress])	+	' '
									+	dbo.ConvertToBasicLatin([Note])				+	' '
									+	dbo.ConvertToBasicLatin([TicketAddress])	+	' '
									+	dbo.ConvertToBasicLatin(@StatusNote)		
									+	(	
											SELECT	dbo.ConvertToBasicLatin
											(
												STUFF
												(
													(
														SELECT	' ' + [Name] + ' ' + [Phone] + ' ' + ISNULL([Email], '')
														FROM	[InviteEventAttach]
														WHERE	[InviteEventId]	=	@Id
														FOR		XML	PATH(''), TYPE
													).value('.', 'NVARCHAR(MAX)'), 1, 1, ''
												)
											)
										)
			WHERE	[Id]			=	@Id
		

		COMMIT	TRANSACTION	InviteEvent_EditStatusNote
		SELECT	1
	END		TRY
	BEGIN	CATCH
		ROLLBACK	TRANSACTION	InviteEvent_EditStatusNote
		SELECT	0
	END		CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEvent_GetByCode]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 09/11/2017
-- Description:	Lấy thông tin InviteEvent sử dung mã code
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEvent_GetByCode] 
	-- Add the parameters for the stored procedure here
	@Code VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	[Id]
		,	[EventId]
		,	[CustomerId]
		,	[CustomerName]
		,	[CustomerPhone]
		,	[CustomerBirthday]
		,	[CustomerSex]
		,	[CustomerEmail]
		,	[CustomerRevenue]
		,	[CustomerType]
		,	[CustomerPosition]
		,	[CompanyId]
		,	[CompanyName]
		,	[CompanySize]
		,	[CompanyAddress]
		,	[TicketAddress]
		,	[GroupId]
		,	[AbilityAttend]
		,	[Note]
		,	[BcrmStatus]
		,	[StatusNote]
		,	[ApproveBy]
		,	[ApproveDate]
		,	[CreatedBy]
		,	[CreatedDate]
		,	[IsCheckIn]
		,	[CheckInBy]
		,	[CheckInDate]
		,	[CustomerVip]
		,	[AccompanyMember]
		,	[SendTicketType]
		,	[Code]
	FROM	[InviteEvent]
	WHERE	[Code]	=	@Code 
END


GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEvent_GetByCondition]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 09/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEvent_GetByCondition]	-- declare @Total int exec InviteEvent_GetByCondition 'Tung',0,0,-1,0,0,'',null,null,null,1,25,@Total
	-- Add the parameters for the stored procedure here
	@Keyword		NVARCHAR(100),
	@Id				INT,
	@EventId		INT,
	@GroupIds		VARCHAR(100),
	@CreatedBy		NVARCHAR(50),
	@BcrmStatus			INT,
	@CustomerType	INT,
	@CompanySize	INT,
	@CustomerSex	INT,
	@StartDate		DATE,
	@EndDate		DATE,
	@TypeOfOrder	INT,
	@PageIndex		INT,
	@PageSize		INT,
	@TotalRecord	INT OUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @Sql	NVARCHAR(MAX)
		,	@Join	NVARCHAR(1000)
		,	@Where	NVARCHAR(MAX)
		,	@Total	INT
		,	@Order	NVARCHAR(50)

	IF	@TypeOfOrder = 0
		SET @Order	=	'I.[Id] DESC'

	IF	@TypeOfOrder = 1
		SET @Order	=	'I.[CustomerRevenue] DESC'

	IF	@TypeOfOrder = 2
		SET @Order	=	'I.[CustomerRevenue]'

	SET	@Join	=	''
	IF	@GroupIds <> '-1'
		SET @Join	=	'INNER	JOIN	dbo.Split(''' + @GroupIds + ''', '','')	G	ON	G.[Data]	=	I.[GroupId]'
	
	SET	@Where	=	''
	IF	@Id > 0
		SET	@Where	=	@Where + '	AND I.[Id]		=	' +	CAST(@Id	AS VARCHAR(10))
	IF	@EventId > 0
		SET	@Where	=	@Where + '	AND I.[EventId]		=	' +	CAST(@EventId	AS VARCHAR(10))

	IF	@CreatedBy IS NOT NULL AND @CreatedBy != ''
		SET	@Where	=	@Where + '	AND (I.[CreatedBy]	=	''' + @CreatedBy + ''' OR I.[BcrmAssignTo]	=	''' + @CreatedBy + ''')'

	IF	@BcrmStatus	> 0
		SET	@Where	=	@Where + '	AND I.[BcrmStatus]		=	' +	CAST(@BcrmStatus AS VARCHAR(10))

	IF	@CustomerType > -1
		SET	@Where	=	@Where + '	AND I.[CustomerType]=	' +	CAST(@CustomerType AS VARCHAR(10))

	IF	@CustomerSex > -1
		SET	@Where	=	@Where + '	AND I.[CustomerSex]	=	' +	CAST(@CustomerSex AS VARCHAR(10))

	IF	@CompanySize > 0
		SET	@Where	=	@Where + '	AND I.[CompanySize]	=	' +	CAST(@CompanySize AS VARCHAR(10))

	IF	(@StartDate IS NOT NULL)
		IF	@BcrmStatus > 0
			SET	@Where	=	@Where + '	AND	I.[ApprovedDate]>=	''' + CONVERT(VARCHAR(10), @StartDate, 111) + ''''
		ELSE
			SET	@Where	=	@Where + '	AND	I.[CreatedDate]	>=	''' + CONVERT(VARCHAR(10), @StartDate, 111) + ''''

	IF	(@EndDate IS NOT NULL)
		IF	@BcrmStatus > 0
			SET	@Where	=	@Where + '	AND	I.[ApprovedDate]<	''' + CONVERT(VARCHAR(10), @EndDate, 111) + ''''
		ELSE
			SET	@Where	=	@Where + '	AND	I.[CreatedDate]	<	''' + CONVERT(VARCHAR(10), @EndDate, 111) + ''''

	IF	(@Keyword IS NOT NULL AND @Keyword != '')
		SET	@Where	=	@Where + '	AND	CONTAINS(I.[TextSearch] , N''' + REPLACE(dbo.ConvertToBasicLatin(@Keyword),' ','+') + ''')'	
	
	SET	@Sql = 'SELECT	[Id]
					,	[EventId]
					,	[CustomerId]
					,	[CustomerName]
					,	[CustomerPhone]
					,	[CustomerEmail]
					,	[CustomerRevenue]
					,	[CustomerType]
					,	[CustomerPosition]
					,	[CompanyName]
					,	[CompanySize]
					,	[CompanyAddress]
					,	[Note]
					,	[BcrmStatus]
					,	[StatusNote]
					,	[CreatedBy]
					,	[CreatedDate]
					,	[CustomerVip]
					,	[IsCheckIn]
					,	[CheckInDate]
					,	[Code]
					,	[SourceId]
					,	[BcrmAssignTo]
				FROM	(	SELECT	I.[Id]
								,	I.[EventId]
								,	I.[CustomerId]
								,	I.[CustomerName]
								,	I.[CustomerPhone]
								,	I.[CustomerEmail]
								,	I.[CustomerRevenue]
								,	I.[CustomerType]
								,	I.[CustomerPosition]
								,	I.[CompanyName]
								,	I.[CompanyAddress]
								,	I.[CompanySize]
								,	I.[Note]
								,	I.[BcrmStatus]
								,	I.[StatusNote]
								,	I.[CreatedBy]
								,	I.[CreatedDate]
								,	I.[CustomerVip]
								,	I.[IsCheckIn]
								,	I.[CheckInDate]
								,	I.[Code]
								,	I.[BcrmAssignTo]
								,	I.[SourceId]
								,	ROW_NUMBER() OVER(ORDER BY ' + @Order + ') Row
							FROM	[InviteEvent]	I
							' +	@Join + '
							WHERE	I.IsBcrmProcess = 1
							' + @Where	+ ')	Temp
				WHERE	[Row]	BETWEEN	' + CAST(((@PageIndex - 1)*@PageSize + 1) AS VARCHAR(5)) + ' AND ' + CAST(@PageSize*@PageIndex AS VARCHAR(5)) + '
				SELECT	@Total	=	COUNT(I.Id) 
				FROM	[InviteEvent]	I
				' + @Join + '
				WHERE	I.IsBcrmProcess = 1
				' + @Where
	--PRINT	@Sql
	EXECUTE	sp_executesql @Sql, N'@Total INT OUTPUT', @Total = @TotalRecord OUTPUT
END


GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEvent_GetByCustomerId]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 16/11
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEvent_GetByCustomerId]
	-- Add the parameters for the stored procedure here
	@EventId	INT,
	@CustomerIds VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	[Id]
	,		[CustomerId]
	,		[EventId] 
	FROM	[InviteEvent] 
	WHERE (@EventId = 0 OR [EventId] = @EventId) AND [CustomerId] IN (SELECT [data] FROM dbo.Split(@CustomerIds,','))
END

GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEvent_GetById]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 09/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEvent_GetById]	
	-- Add the parameters for the stored procedure here
	@Id	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	[Id]
		,	[EventId]
		,	[CustomerId]
		,	[CustomerName]
		,	[CustomerPhone]
		,	[CustomerBirthday]
		,	[CustomerSex]
		,	[CustomerEmail]
		,	[CustomerRevenue]
		,	[CustomerType]
		,	[CustomerPosition]
		,	[CompanyId]
		,	[CompanyName]
		,	[CompanySize]
		,	[CompanyAddress]
		,	[TicketAddress]
		,	[GroupId]
		,	[AbilityAttend]
		,	[Note]
		,	[BcrmStatus]
		,	[StatusNote]
		,	[ApproveBy]
		,	[ApproveDate]
		,	[CreatedBy]
		,	[CreatedDate]
		,	[IsCheckIn]
		,	[CheckInBy]
		,	[CheckInDate]
		,	[CustomerVip]
		,	[AccompanyMember]
		,	[SendTicketType]
		,	[Code]
		,	[Sessions]
		,	[BcrmAssignTo]
	FROM	[InviteEvent]
	WHERE	[Id]	=	@Id
END


GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEvent_GetByPhone]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 09/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEvent_GetByPhone]
	-- Add the parameters for the stored procedure here
	@EventId		INT
,	@CustomerPhone	NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	[Id]
		,	[EventId]
		,	[CustomerId]
		,	[CustomerName]
		,	[CustomerPhone]
		,	[CustomerBirthday]
		,	[CustomerSex]
		,	[CustomerEmail]
		,	[CustomerRevenue]
		,	[CustomerType]
		,	[CustomerPosition]
		,	[CompanyId]
		,	[CompanySize]
		,	[CompanyName]
		,	[CompanyAddress]
		,	[TicketAddress]
		,	[GroupId]
		,	[AbilityAttend]
		,	[Note]
		,	[BcrmStatus]
		,	[StatusNote]
		,	[ApproveBy]
		,	[ApproveDate]
		,	[CreatedBy]
		,	[CreatedDate]
		,	[IsCheckIn]
		,	[CheckInBy]
		,	[CheckInDate]
		,	[CustomerVip]
		,	[SendTicketType]
		,	[Code]
	FROM	[InviteEvent]
	WHERE	[EventId]	=	@EventId
		AND	[CustomerPhone] = @CustomerPhone
END


GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEvent_GetByPhoneNEmail]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 09/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEvent_GetByPhoneNEmail]
	-- Add the parameters for the stored procedure here
	@EventId		INT
,	@CustomerPhone	NVARCHAR(50)
,	@Email			NVARCHAR(150)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	[Id]
		,	[EventId]
		,	[CustomerId]
		,	[CustomerName]
		,	[CustomerPhone]
		,	[CustomerBirthday]
		,	[CustomerSex]
		,	[CustomerEmail]
		,	[CustomerRevenue]
		,	[CustomerType]
		,	[CustomerPosition]
		,	[CompanyId]
		,	[CompanySize]
		,	[CompanyName]
		,	[CompanyAddress]
		,	[TicketAddress]
		,	[GroupId]
		,	[AbilityAttend]
		,	[Note]
		,	[BcrmStatus]
		,	[StatusNote]
		,	[ApproveBy]
		,	[ApproveDate]
		,	[CreatedBy]
		,	[CreatedDate]
		,	[IsCheckIn]
		,	[CheckInBy]
		,	[CheckInDate]
		,	[CustomerVip]
		,	[SendTicketType]
		,	[Code]
	FROM	[InviteEvent]
	WHERE	[EventId]	=	@EventId
		AND	([CustomerPhone] = @CustomerPhone	OR	[CustomerEmail] = @Email)
END


GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEvent_GetForReport]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 09/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEvent_GetForReport]-- exec InviteEvent_GetForRepot '49,66,50,58,2,197,192,207,5,4,129,39,59,190,191,45,89,90,46',0,0
	-- Add the parameters for the stored procedure here
	@GroupIds	VARCHAR(MAX),
	@EventId	INT,
	@BcrmStatus		VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT [Id]
      ,[EventId]
      ,[CustomerId]
      ,[CustomerName]
      ,[CustomerPhone]
      ,[CustomerBirthday]
      ,[CustomerSex]
      ,[CustomerEmail]
      ,[CustomerRevenue]
      ,[CustomerType]
      ,[CustomerPosition]
      ,[CompanyId]
      ,[CompanySize]
      ,[CompanyName]
      ,[CompanyAddress]
	  ,[TicketAddress]
      ,[GroupId]
      ,[AbilityAttend]
      ,[Note]
      ,[BcrmStatus]
      ,[StatusNote]
      ,[ApproveBy]
      ,[ApproveDate]
      ,[CreatedBy]
      ,[CreatedDate]
	  ,[IsCheckIn]
	  ,[CheckInBy]
	  ,[CheckInDate]
	  ,[CustomerVip]
	  ,[SendTicketType]
	  ,[GroupId]
	FROM [dbo].[InviteEvent]
	WHERE	[IsBcrmProcess] = 1
		AND (	@EventId	=	0	OR	[EventId]	=	@EventId	)
		AND	(	@BcrmStatus		=	''	OR	[Status]	IN	(SELECT [data] FROM dbo.Split(@BcrmStatus,',')))
		AND	(	@GroupIds	=	'0'	OR	[GroupId]	IN	(SELECT CAST([data] AS INT) FROM dbo.Split(@GroupIds,',')))
END


GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEvent_GetToExport]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		TungNT
-- Create date: 09/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEvent_GetToExport]
	-- Add the parameters for the stored procedure here
	@Keyword		NVARCHAR(100),
	@EventId		INT,
	@GroupIds		VARCHAR(100),
	@CreatedBy		NVARCHAR(50),
	@BcrmStatus			INT,
	@CustomerType	INT,
	@CompanySize	INT,
	@CustomerSex	INT,
	@StartDate		DATE,
	@EndDate		DATE,
	@TypeOfOrder	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @Sql	NVARCHAR(MAX)
	DECLARE	@Join	NVARCHAR(500)
	DECLARE @Where	NVARCHAR(MAX)
	DECLARE	@Total	INT
	DECLARE @Order	NVARCHAR(50)
	IF	@TypeOfOrder = 0
		SET @Order	=	'I.[Id] DESC'
	IF	@TypeOfOrder = 1
		SET @Order	=	'I.[CustomerRevenue] DESC'
	IF	@TypeOfOrder = 2
		SET @Order	=	'I.[CustomerRevenue]'
	
	SET	@Join	=	''
	IF	@GroupIds <> '-1'
		SET @Join	=	'INNER	JOIN	dbo.Split(''' + @GroupIds + ''','','')	G	ON	I.[GroupId]	=	G.[Data]'

	SET	@Where	=	''
	IF	@EventId > 0
		SET	@Where	=	@Where + '	AND I.[EventId]		=	' +	CAST(@EventId	AS VARCHAR(10))

	IF	@CreatedBy IS NOT NULL AND @CreatedBy != ''
		SET	@Where	=	@Where + '	AND I.[CreatedBy]	=	''' + @CreatedBy + ''''

	IF	@BcrmStatus	> 0
		SET	@Where	=	@Where + '	AND I.[BcrmStatus]		=	' +	CAST(@BcrmStatus AS VARCHAR(10))

	IF	@CustomerType > -1
		SET	@Where	=	@Where + '	AND I.[CustomerType]=	' +	CAST(@CustomerType AS VARCHAR(10))

	IF	@CustomerSex > -1
		SET	@Where	=	@Where + '	AND I.[CustomerSex]	=	' +	CAST(@CustomerSex AS VARCHAR(10))

	IF	@CompanySize > 0
		SET	@Where	=	@Where + '	AND I.[CompanySize]	=	' +	CAST(@CompanySize AS VARCHAR(10))

	IF	(@StartDate IS NOT NULL)
		SET	@Where	=	@Where + '	AND	I.[CreatedDate]	>=	''' + CONVERT(VARCHAR(10), @StartDate, 111) + ''''

	IF	(@EndDate IS NOT NULL)
		SET	@Where	=	@Where + '	AND	I.[CreatedDate]	<	''' + CONVERT(VARCHAR(10), @EndDate, 111) + ''''

	IF	(@Keyword IS NOT NULL AND @Keyword != '')
		SET	@Where	=	@Where + '	AND	CONTAINS(I.[TextSearch] , N''' + REPLACE(dbo.ConvertToBasicLatin(@Keyword),' ','+') + ''')'	
	
	SET @Sql = 'SELECT	I.[Id]
					,	I.[EventId]
					,	I.[CustomerName]
					,	I.[CustomerPhone]
					,	I.[CustomerBirthday]
					,	I.[CustomerSex]
					,	I.[CustomerEmail]
					,	I.[CustomerRevenue]
					,	I.[CustomerType]
					,	I.[CustomerPosition]
					,	I.[CompanyId]
					,	I.[CompanyName]
					,	I.[CompanyAddress]
					,	I.[TicketAddress]
					,	I.[CompanySize]
					,	I.[AbilityAttend]
					,	I.[Note]
					,	I.[BcrmStatus]
					,	I.[StatusNote]
					,	I.[CreatedBy]
					,	I.[CustomerVip]
					,	I.[GroupId]
					,	I.[SendTicketType]
					,	I.GroupId
					,	I.[Code]			AS [SystemCode]
					,	A.[Id]				AS	[AttachId]
					,	A.[InviteEventId]
					,	A.[Name]			AS	[AttachName]
					,	A.[Phone]			AS	[AttachPhone]
					,	A.[Email]			AS	[AttachEmail]
					,	A.[Position]		AS	[AttachPosition]
					,	A.[CustomerVip]		AS	[AttachCustomerVip]
					,	A.[Code]			AS	[AttachSystemCode]
				FROM			[InviteEvent]		I
				' + @Join + '
				LEFT	JOIN	[InviteEventAttach]	A	ON	I.[Id]	=	A.[InviteEventId]
				WHERE	I.IsBcrmProcess = 1
					' + @Where + '
				ORDER	BY	' + @Order
	--PRINT @Sql
	EXECUTE	sp_executesql @Sql
END



GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEvent_SyncCompanyNameNAddress]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Doãn Hải Vân
-- Create date: 20160721
-- Description:	Dùng để đồng bộ tên và địa chỉ doanh nghiệp khi thay đổi ở danh sách công ty hoặc thay đổi doanh nghiệp trên file khách hàng
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEvent_SyncCompanyNameNAddress]
	-- Add the parameters for the stored procedure here
	@CompanyId		INT
,	@CompanyName	NVARCHAR(500)
,	@CompanyAddress	NVARCHAR(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE			I
	SET				I.[CompanyName]		=	@CompanyName
			,		I.[CompanyAddress]	=	@CompanyAddress
			,		I.[TextSearch]		=	CAST([CustomerId]	AS	NVARCHAR(10))		+	' '
										+	dbo.ConvertToBasicLatin([CustomerName])		+	' '
										+	ISNULL([CustomerPhone], '')					+	' '
										+	ISNULL([CustomerEmail], '')					+	' '
										+	dbo.ConvertToBasicLatin(@CompanyName)		+	' '
										+	dbo.ConvertToBasicLatin(@CompanyAddress)	+	' '
										+	dbo.ConvertToBasicLatin([Note])				+	' '
										+	dbo.ConvertToBasicLatin([TicketAddress])	+	' '
										+	dbo.ConvertToBasicLatin([StatusNote])
										+	(	
												SELECT	dbo.ConvertToBasicLatin
												(
													STUFF
													(
														(
															SELECT	' ' + [Name] + ' ' + [Phone] + ' ' + ISNULL([Email], '')
															FROM	[InviteEventAttach]
															WHERE	[InviteEventId]	=	I.[Id]
															FOR		XML	PATH(''), TYPE
														).value('.', 'NVARCHAR(MAX)'), 1, 1, ''
													)
												)
											)
	FROM			[InviteEvent]		AS	I
	INNER	JOIN	[Event]				AS	E	ON	E.[Id]	=	I.[EventId]
	WHERE			I.[CompanyId]		=	@CompanyId
			AND		E.[StartDate]		<=	GETDATE()
			AND		E.[Deadline]		>=	GETDATE()
END




GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEvent_Update]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEvent_Update]
	-- Add the parameters for the stored procedure here
	@Id					INT
,	@EventId			INT
,	@CustomerName		NVARCHAR(500)
,	@CustomerPhone		NVARCHAR(1000)
,	@CustomerBirthday	DATE
,	@CustomerSex		BIT
,	@CustomerEmail		NVARCHAR(50)
,	@CustomerType		INT
,	@CustomerPosition	INT
,	@CustomerVip		INT
,	@CompanyId			INT
,	@CompanyName		NVARCHAR(500)
,	@CompanySize		INT
,	@CompanyAddress		NVARCHAR(500)
,	@TicketAddress		NVARCHAR(500)
,	@AbilityAttend		INT
,	@Note				NVARCHAR(1000)
,	@BcrmStatus				INT
,	@CreatedBy			NVARCHAR(50)
,	@TextSearchAttach	NVARCHAR(1000)
,	@SendTicketType		INT
,	@Sessions			VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	BEGIN	TRY
		BEGIN	TRANSACTION	InviteEvent_Update
			UPDATE	[InviteEvent]
			SET		[EventId]			=	@EventId
				,	[CustomerName]		=	@CustomerName
				,	[CustomerPhone]		=	@CustomerPhone
				,	[CustomerBirthday]	=	@CustomerBirthday
				,	[CustomerSex]		=	@CustomerSex
				,	[CustomerEmail]		=	@CustomerEmail
				,	[CustomerType]		=	@CustomerType
				,	[CustomerPosition]	=	@CustomerPosition
				,	[CustomerVip]		=	@CustomerVip
				,	[CompanyId]			=	@CompanyId
				,	[CompanySize]		=	@CompanySize
				,	[CompanyName]		=	@CompanyName
				,	[CompanyAddress]	=	@CompanyAddress
				,	[TicketAddress]		=	@TicketAddress
				,	[AbilityAttend]		=	@AbilityAttend
				,	[Note]				=	@Note
				,	[BcrmStatus]		=	@BcrmStatus
				,	[SendTicketType]	=	@SendTicketType
				,	[Sessions]			=	@Sessions
				,	[TextSearch]		=	CAST([CustomerId]	AS	NVARCHAR(10))		+	' '
										+	dbo.ConvertToBasicLatin(@CustomerName)		+	' '
										+	ISNULL(@CustomerPhone, '')					+	' '
										+	ISNULL(@CustomerEmail, '')					+	' '
										+	dbo.ConvertToBasicLatin(@CompanyName)		+	' '
										+	dbo.ConvertToBasicLatin(@CompanyAddress)	+	' '
										+	dbo.ConvertToBasicLatin(@Note)				+	' '
										+	dbo.ConvertToBasicLatin(@TicketAddress)		+	' '
										+	dbo.ConvertToBasicLatin([StatusNote])		+	' '
										+	dbo.ConvertToBasicLatin(@TextSearchAttach)	+	' '
										+	dbo.ConvertToBasicLatin([PositionName])		+	' '
										+	dbo.ConvertToBasicLatin([PlaceOfAttendEvent])+	' '
										+	dbo.ConvertToBasicLatin([IntroduceUser])	+	' '
			WHERE	[Id]				=	@Id

			--EXEC	[InviteEventHistory_Create]	@Id, @Status, N'', @CreatedBy
		COMMIT	TRANSACTION	InviteEvent_Update
		SELECT	1
	END		TRY
	BEGIN	CATCH
		ROLLBACK	TRANSACTION	InviteEvent_Update
		SELECT	0
	END		CATCH
END




GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEvent_UpdateCompany]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEvent_UpdateCompany]
	-- Add the parameters for the stored procedure here
	@CustomerId		INT
,	@CompanyId		INT
,	@CompanyName	NVARCHAR(500)
,	@CompanyAddress	NVARCHAR(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE			I
	SET				I.[CompanyId]		=	@CompanyId
			,		I.[CompanyName]		=	@CompanyName
			,		I.[CompanyAddress]	=	@CompanyAddress
			,		I.[TextSearch]		=	CAST([CustomerId]	AS	NVARCHAR(10))		+	' '
										+	dbo.ConvertToBasicLatin([CustomerName])		+	' '
										+	ISNULL([CustomerPhone], '')					+	' '
										+	ISNULL([CustomerEmail], '')					+	' '
										+	dbo.ConvertToBasicLatin(@CompanyName)		+	' '
										+	dbo.ConvertToBasicLatin(@CompanyAddress)	+	' '
										+	dbo.ConvertToBasicLatin([Note])				+	' '
										+	dbo.ConvertToBasicLatin([TicketAddress])	+	' '
										+	dbo.ConvertToBasicLatin([StatusNote])
										+	(	
												SELECT	dbo.ConvertToBasicLatin
												(
													STUFF
													(
														(
															SELECT	' ' + [Name] + ' ' + [Phone] + ' ' + ISNULL([Email], '')
															FROM	[InviteEventAttach]
															WHERE	[InviteEventId]	=	I.[Id]
															FOR		XML	PATH(''), TYPE
														).value('.', 'NVARCHAR(MAX)'), 1, 1, ''
													)
												)
											)
	FROM			[InviteEvent]		AS	I
	INNER	JOIN	[Event]				AS	E	ON	E.[Id]	=	I.[EventId]
	WHERE			I.[CustomerId]		=	@CustomerId
			AND		E.[StartDate]		<=	GETDATE()
			AND		E.[Deadline]		>=	GETDATE()

	SELECT	@@ROWCOUNT
END




GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEvent_UpdateCompanySize]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 26/7/2017
-- Description:	Được sử dụng khi thay đổi thông tin doanh nghiệp thì cũng phải cập nhật lời mời tham gia sự kiện
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEvent_UpdateCompanySize]
	@CompanyId		INT,
	@CompanySize	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRY
		UPDATE [InviteEvent] SET [CompanySize] = @CompanySize WHERE [CompanyId] = @CompanyId
		SELECT 1
	END TRY
	BEGIN CATCH
		SELECT 0
	END CATCH
    
END


GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEventAttach_CountByListInviteEvent]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BCRM_InviteEventAttach_CountByListInviteEvent]
	-- Add the parameters for the stored procedure here
	@InviteEventIds	NVARCHAR(1000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT		[InviteEventId]	AS	[Key]
			,	COUNT([Id])		AS	[Value]
	FROM		[InviteEventAttach]
	WHERE		[InviteEventId]	IN	(SELECT	[Data] FROM dbo.Split(@InviteEventIds, ','))
	GROUP	BY	[InviteEventId]
END

GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEventAttach_DeleteOut]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 12/1/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEventAttach_DeleteOut] 
	-- Add the parameters for the stored procedure here
	@InviteEventId	INT
,	@Ids			NVARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE	
	FROM	[InviteEventAttach]
	WHERE	[InviteEventId]	=	@InviteEventId
		AND	[Id]			NOT	IN	(SELECT [Data] FROM dbo.Split(@Ids, ','))

	SELECT	@@ROWCOUNT
END


GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEventAttach_GetByInviteEvent]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEventAttach_GetByInviteEvent] 
	-- Add the parameters for the stored procedure here
	@InviteEventId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	[Id]
		,	[InviteEventId]
		,	[Name]
		,	[Email]
		,	[Phone]
		,	[Position]
		,	[IsCheckIn]
		,	[CheckInBy]
		,	[CheckInDate]
		,	[CustomerVip]
	FROM	[InviteEventAttach]
	WHERE	[InviteEventId]	=	@InviteEventId
END
GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEventAttach_GetByInviteEventIds]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BCRM_InviteEventAttach_GetByInviteEventIds] 
	-- Add the parameters for the stored procedure here
	@InviteEventIds	NTEXT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	[Id]
		,	[InviteEventId]
		,	[Name]
		,	[Phone]
		,	[Position]
		,	[CreatedBy]
		,	[CreatedDate]
		,	[UpdatedBy]
		,	[UpdatedDate]
		,	[IsCheckIn]
		,	[CheckInBy]
		,	[CheckInDate]
		,	[CustomerVip]
	FROM	[InviteEventAttach]
	WHERE	[InviteEventId]	IN ( SELECT CAST([data] AS INT) FROM dbo.Split(@InviteEventIds,','))
END

GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEventAttach_GetCheckedIn]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 12/1/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEventAttach_GetCheckedIn]
	-- Add the parameters for the stored procedure here
	@EventId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT		 IE.[Id]			AS 	InviteId,
				 IE.[EventId]		AS  EventId,
				 IEA.[Id]			AS  AttachId,
				 IEA.[Name]			AS  CustomerName,
				 IEA.[CustomerVip]  AS  CustomerVip,
				 IEA.[Phone]		AS  CustomerPhone,
				 IEA.[Email]		AS  CustomerEmail,
				 IEA.[Position]		AS  CustomerPosition,
				 IE.[CompanyName]	AS  CompanyName,
				 IE.[CustomerName]  AS  OwnerCustomer,
				 IEA.[IsCheckIn]    AS  IsCheckIn
	FROM [InviteEventAttach] IEA
		inner join 
		(
			SELECT IEE.[Id],
				   IEE.[EventId],
				   IEE.[CustomerName],
				   IEE.[CompanyName]
			FROM       [InviteEvent] IEE
				     INNER JOIN 
					   [Event] EE ON IEE.[EventId] = EE.[Id]
			WHERE EE.[Id] = @EventId AND EE.[IsEnable] = 1				   	
		) IE ON IEA.[InviteEventId] = IE.[Id]
	WHERE IEA.IsCheckIn = 1
END


GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEventAttach_UpdateWithoutVip]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 12/1/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEventAttach_UpdateWithoutVip]
	-- Add the parameters for the stored procedure here
	@Id				INT
,	@Name			NVARCHAR(100)
,   @Phone			NVARCHAR(100)
,   @Position		INT
,   @Email			NVARCHAR(200)
,   @UpdatedBy		NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE	[InviteEventAttach]
	SET		[Name]			=	@Name
		,	[Phone]			=	@Phone
		,	[Position]		=	@Position
		,	[Email]			=	@Email
		,	[UpdatedBy]		=	@UpdatedBy
		,	[UpdatedDate]	=	GETDATE()
	WHERE	[Id]			=	@Id

	SELECT	@@ROWCOUNT
END


GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEventHistory_Create]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[BCRM_InviteEventHistory_Create]
	-- Add the parameters for the stored procedure here
	@InviteEventId	INT
,	@Action			NVARCHAR(50)
,	@Note			NVARCHAR(1000)
,	@OldValue		NVARCHAR(1000)
,	@NewValue		NVARCHAR(1000)
,	@UpdatedBy		NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO	[InviteEventHistory]
           ([InviteEventId]
           ,[Action]
           ,[Note]
           ,[OldValue]
           ,[NewValue]
		   ,[UpdatedBy]
		   ,[UpdatedDate])
    VALUES
           (@InviteEventId
           ,@Action
           ,@Note
           ,@OldValue
		   ,@NewValue
		   ,@UpdatedBy
           ,GETDATE())
END
GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEventHistory_GetByInviteEventId]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 15/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEventHistory_GetByInviteEventId]
	-- Add the parameters for the stored procedure here
	@InviteEventId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   SELECT	[Id]
      ,		[InviteEventId]
      ,		[Action]
      ,		[Note]
	  ,		[OldValue]
	  ,		[NewValue]
      ,		[UpdatedBy]
      ,		[UpdatedDate]
  FROM		[InviteEventHistory]
  WHERE [InviteEventId] = @InviteEventId
  ORDER BY Id DESC
	
END

GO
/****** Object:  StoredProcedure [dbo].[BCRM_InviteEventHistory_GetByPhone]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 15/11/2017
-- Description:	Lấy thông tin mời tham gia sự kiện của khách hàng theo số điện thoại
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_InviteEventHistory_GetByPhone]
	@PhoneNumber NVARCHAR(1000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT		I.[Id],
				E.[Name] AS EventName,
				I.[CreatedBy],
				I.[CustomerName],
				I.[CustomerPhone],
				I.[CustomerEmail],
				I.[CustomerPosition],
				IH.[Status],
				IH.[UpdatedBy]		AS ApprovedBy,
				IH.[UpdatedDate]	AS ApprovedDate
	FROM		[InviteEvent] I
	INNER JOIN	[InviteEventHistory] IH ON I.[Id] = IH.[InviteEventId]
	INNER JOIN	[Event] E ON E.[Id] = I.[EventId]
	WHERE		I.[CustomerPhone] IN (SELECT [data] FROM dbo.Split(@PhoneNumber,','))
	ORDER BY	IH.[Id] DESC
END


GO
/****** Object:  StoredProcedure [dbo].[BCRM_Session_GetBySessionIds]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 22/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_Session_GetBySessionIds]
	@SessionIds VARCHAR(200)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT	[Id]
		,	[EventId]
		,	[Name]
		,	[StartDate]
		,	[EndDate]
		,	[Description]
	FROM	[Session]
	WHERE	[Id] IN (SELECT CAST(data AS INT) FROM dbo.split(@SessionIds,','))
END

GO
/****** Object:  StoredProcedure [dbo].[BCRM_SessionCheckin_GetByInviteEventId]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 22/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_SessionCheckin_GetByInviteEventId]
	@InviteEventId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT		SC.[InviteEventId]
			,	SC.[SessionId]
			,	SC.[CheckinBy]
			,	SC.[CheckinDate]
			,	SC.[EventId]
			,	E.[Name] AS EventName
			,	S.[Name] AS SessionName
	FROM		[SessionCheckin] SC 
	INNER JOIN	[Session] S ON S.[Id] = SC.[SessionId] 
	INNER JOIN	[Event] E ON E.[Id] = SC.[EventId]
	WHERE InviteEventId = @InviteEventId
END

GO
/****** Object:  StoredProcedure [dbo].[BCRM_SessionCheckin_GetByInviteEventIds]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 23/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BCRM_SessionCheckin_GetByInviteEventIds]
	@InviteEventIds	VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT		SC.[InviteEventId]
			,	SC.[SessionId]
			,	SC.[CheckinBy]
			,	SC.[CheckinDate]
			,	SC.[EventId]
			,	E.[Name] AS EventName
			,	S.[Name] AS SessionName
	FROM		[SessionCheckin] SC 
	INNER JOIN	[Session] S ON S.[Id] = SC.[SessionId] 
	INNER JOIN	[Event] E ON E.[Id] = SC.[EventId]
	WHERE InviteEventId IN (SELECT CAST(data AS INT) FROM dbo.Split(@InviteEventIds,',')) 
END

GO
/****** Object:  StoredProcedure [dbo].[CheckInTimer_Delete]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 25/11/2016
-- Description:	Xóa
-- =============================================
CREATE PROCEDURE [dbo].[CheckInTimer_Delete]
	-- Add the parameters for the stored procedure here
	@Id		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [CheckInTimer]
		WHERE [Id] = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[CheckInTimer_Exists]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 25/11/2016
-- Description:	Kiểm tra event đã có thông tin gia hạn checkIn hay chưa
-- =============================================
CREATE PROCEDURE [dbo].[CheckInTimer_Exists]
	-- Add the parameters for the stored procedure here
	@EventId			INT,
	@IsInWebCheckin		BIT
AS
BEGIN
	SET NOCOUNT ON;

	IF((SELECT COUNT(1) FROM   [CheckInTimer] CIT WHERE EventId = @EventId AND ISNULL(IsInWebCheckin,0) = @IsInWebCheckin ) >0)
		SELECT 1
	ELSE
		SELECT 0
	
END

GO
/****** Object:  StoredProcedure [dbo].[CheckInTimer_GetAll]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[CheckInTimer_GetAll]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [Id]
		,[EventId]
		,[StartTime]
		,[EndTime]
		,[CreatedBy]
		,[CreatedDate]
		,[UpdatedBy]
		,[UpdatedDate]
		,[IsInWebCheckin]
	 FROM [CheckInTimer]
END

GO
/****** Object:  StoredProcedure [dbo].[CheckInTimer_GetByEventId]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungBT
-- Create date: 28/11/2016
-- Description:	Lấy theo EventId
-- =============================================
CREATE PROCEDURE [dbo].[CheckInTimer_GetByEventId]
	-- Add the parameters for the stored procedure here
	@EventId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	[Id]
		,	[EventId]
		,	[StartTime]
		,	[EndTime]
		,	[CreatedBy]
		,	[CreatedDate]
		,	[UpdatedBy]
		,	[UpdatedDate]
		,	[IsInWebCheckin]
	FROM	[CheckInTimer]
	WHERE	[EventId] = @EventId
		AND [IsInWebCheckin] = 0
END

GO
/****** Object:  StoredProcedure [dbo].[CheckInTimer_GetByEventIds]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HIEUBV
-- Create date: 25-11-2016
-- Description:	Lấy danh sách CheckInTimer theo chuỗi EventIds
-- =============================================
CREATE PROCEDURE [dbo].[CheckInTimer_GetByEventIds]
	-- Add the parameters for the stored procedure here
	@EventIds NVARCHAR(250),
	@IsInWebCheckin	BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [Id]
		,[EventId]
		,[StartTime]
		,[EndTime]
		,[CreatedBy]
		,[CreatedDate]
		,[UpdatedBy]
		,[UpdatedDate]
		,[IsInWebCheckin]
	FROM [CheckInTimer] CIT
	WHERE CIT.[EventId] IN (SELECT [Data] FROM dbo.Split(@EventIds,',')) 
	 AND ISNULL(CIT.[IsInWebCheckin],0) = @IsInWebCheckin	
END

GO
/****** Object:  StoredProcedure [dbo].[CheckInTimer_GetById]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 25/11/2016
-- Description:	Lấy theo Id
-- =============================================
CREATE PROCEDURE [dbo].[CheckInTimer_GetById]
	-- Add the parameters for the stored procedure here
	@Id		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [Id]
		,[EventId]
		,[StartTime]
		,[EndTime]
		,[CreatedBy]
		,[CreatedDate]
		,[UpdatedBy]
		,[UpdatedDate]
	FROM [CheckInTimer] CIT
	WHERE CIT.[Id] = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[CheckInTimer_Insert]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 2016-11-25
-- Description:	THêm mới CheckInTimer
-- =============================================
CREATE PROCEDURE [dbo].[CheckInTimer_Insert]
	-- Add the parameters for the stored procedure here
	@EventId		INT,
	@StartTime		DATETIME,
	@EndTime		DATETIME,
	@IsInWebCheckin BIT,
	@CreatedBy		NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO CheckInTimer([EventId],[StartTime],[EndTime],[CreatedBy],[CreatedDate],[IsInWebCheckin])
					VALUES (@EventId  ,@StartTime ,@EndTime ,@CreatedBy ,GETDATE(),@IsInWebCheckin) 
END

GO
/****** Object:  StoredProcedure [dbo].[CheckInTimer_Update]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 25/11/2016
-- Description:	Cập nhật thông tin CheckInTimer
-- =============================================
CREATE PROCEDURE [dbo].[CheckInTimer_Update]
	-- Add the parameters for the stored procedure here
	@EventId		INT,
	@StartTime		DATETIME,
	@EndTime		DATETIME,
	@UpdatedBy		NVARCHAR(50),
	@Id				INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [CheckInTimer]
	SET	 [EventId]		= @EventId,
		 [StartTime]	= @StartTime,
		 [EndTime]		= @EndTime,
		 [UpdatedBy]	= @UpdatedBy,
		 [UpdatedDate]	= GETDATE()
	WHERE [Id]			= @Id
END

GO
/****** Object:  StoredProcedure [dbo].[Event_Create]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 10/11/2017
-- Description:	Thêm mới sự kiện
-- =============================================
CREATE PROCEDURE [dbo].[Event_Create]
	@Name			NVARCHAR(500),
	@Description	NVARCHAR(1000),
	@StartDate		DATE,
	@EndDate		DATE,
	@IsEnable		BIT,
	@CreatedBy		NVARCHAR(50),
	@EventCode		NVARCHAR(50),
	@SMSTitle		NVARCHAR(1000),
	@Deploy			NVARCHAR(500),
	@Deadline		  DATE,	
	@RevenueStartDate DATE,
	@RevenueEndDate   DATE,
	@RevenueCalculation   INT,
	@IsAllowAttach	  BIT,
	@IsBcrmUse		  BIT		
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [Event]([Name],[EventCode],[Description],[StartDate],[EndDate],[IsEnable],[CreatedDate],[CreatedBy],[SMSTitle]
					,	[Deploy] , [RevenueStartDate] , [RevenueEndDate] ,[RevenueCalculation] ,[IsAllowAttach] ,[IsBcrmUse]
					,   [Deadline] )
				VALUES (@Name ,@EventCode , @Description ,@StartDate ,@EndDate ,@IsEnable ,GETDATE() ,@CreatedBy   ,@SMSTitle
				    ,	@Deploy,   @RevenueStartDate  , @RevenueEndDate  ,@RevenueCalculation  ,@IsAllowAttach  ,@IsBcrmUse  
					,   @Deadline )
	SELECT SCOPE_IDENTITY()
END

GO
/****** Object:  StoredProcedure [dbo].[Event_Edit]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 23/11/2017
-- Description:	Sửa sự kiện
-- =============================================
CREATE PROCEDURE [dbo].[Event_Edit]
	@EventCode			NVARCHAR(50),
	@IsEnable			BIT,
	@Name				NVARCHAR(500),
	@StartDate			DATE,
	@EndDate			DATE,
	@Description		NVARCHAR(1000),
	@Id					INT,
	@SMSTitle			NVARCHAR(1000),
	@Deploy				NVARCHAR(500),
	@Deadline		  DATE,	
	@RevenueStartDate DATE,
	@RevenueEndDate   DATE,
	@RevenueCalculation   INT,
	@IsAllowAttach	  BIT,
	@IsBcrmUse		  BIT		
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE [Event]
	SET    [EventCode]   = @EventCode,
		   [IsEnable]    = @IsEnable ,
		   [Name]	     = @Name,
		   [Description] = @Description,
		   [StartDate]   = @StartDate,
		   [EndDate]     = @EndDate,
		   [SMSTitle]	 = @SMSTitle,
		   [Deploy]		 = @Deploy,
		   [Deadline]	 = @Deadline,
		   [RevenueStartDate] = @RevenueStartDate,
		   [RevenueEndDate]   = @RevenueEndDate ,
		   [RevenueCalculation] = @RevenueCalculation ,
		   [IsAllowAttach]	 = @IsAllowAttach ,
		   [IsBcrmUse]		 = @IsBcrmUse
	WHERE  [Id]			 = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[Event_GetAll]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Event_GetAll]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [Id]
		  ,[Name]
		  ,[Description]
		  ,[StartDate]
		  ,[EndDate]
		  ,[IsEnable]
		  ,[Deadline]
		  ,[CreatedDate]
		  ,[CreatedBy]
		  ,[EventCode]
	FROM   [Event]
END

GO
/****** Object:  StoredProcedure [dbo].[Event_GetByCode]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Event_GetByCode]
	@EventCode		NVARCHAR(50),
	@Id				INT
AS
BEGIN
	
	SET NOCOUNT ON;
	SELECT [Id]
		  ,[Name]
		  ,[Description]
		  ,[StartDate]
		  ,[EndDate]
		  ,[IsEnable]
		  ,[Deadline]
		  ,[CreatedDate]
		  ,[CreatedBy]
		  ,[EventCode]
		  ,[SMSTitle]
	FROM   [Event]
	WHERE  [EventCode] = @EventCode
	 AND   [Id]		   <> @Id	
END

GO
/****** Object:  StoredProcedure [dbo].[Event_GetById]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 10/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Event_GetById]
	@Id		INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [Id]
		  ,[Name]
		  ,[EventCode]
		  ,[Description]
		  ,[StartDate]
		  ,[EndDate]
		  ,[IsEnable]
		  ,[Deadline]
		  ,[CreatedDate]
		  ,[CreatedBy]
		  ,[SMSTitle]
		  ,[Deadline]
		  ,[Deploy]
		  ,[RevenueStartDate]
		  ,[RevenueEndDate]
		  ,[RevenueCalculation]
		  ,[IsAllowAttach]
		  ,[IsBcrmUse]
	FROM [Event]
	WHERE [Id] = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[Event_GetList]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 20/04/2017
-- Description:	Lấy danh sách các sự kiện
-- =============================================
CREATE PROCEDURE [dbo].[Event_GetList]
	@DeployId		INT,
	@FilterText		NVARCHAR(500),
	@StartDate		DATE,
	@EndDate		DATE
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [Id]
		  ,[Name]
		  ,[Description]
		  ,[StartDate]
		  ,[EndDate]
		  ,[IsEnable]
		  ,[Deadline]
		  ,[CreatedDate]
		  ,[CreatedBy]
		  ,[EventCode]
		  ,[SMSTitle]
	FROM [Event]
	WHERE (@FilterText IS NULL OR @FilterText ='' OR [Name] like '%'+ @FilterText +'%')
	  AND (@StartDate IS NULL  OR @StartDate <= [StartDate])
	  AND (@EndDate   IS NULL  OR [EndDate]  <= @EndDate)	
	  AND (@DeployId =0		   OR [Deploy] ='0' OR ([Deploy] IS NOT NULL AND @DeployId IN (SELECT [Data] FROM dbo.Split([Deploy],',' ) ) ) )
END

GO
/****** Object:  StoredProcedure [dbo].[Event_UpdateStatus]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Event_UpdateStatus]
	@Id				INT,
	@IsEnable		BIT
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE [Event]
	SET	   [IsEnable] = @IsEnable
	WHERE  [Id]		  = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_Create]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 24/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_Create]
	@EventId			INT,
	@CustomerName		NVARCHAR(500),
	@CustomerPhone		NVARCHAR(1000),
	@Note				NVARCHAR(1000),
	@CreatedBy			NVARCHAR(50),
	@IsNotSuggest		BIT,
	@CustomerEmail		NVARCHAR(150),
	@PositionName		NVARCHAR(200),
	@CompanyName		NVARCHAR(250),
	@CustomerType		INT,
	@PlaceOfAttendEvent NVARCHAR(150),
	@RegisteredSource   INT,
	@Code				NVARCHAR(20),
	@Sessions			NVARCHAR(50),
	@IntroduceUser		NVARCHAR(150)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT  INTO [InviteEvent]([EventId],[CustomerName],[CustomerPhone],[Note],[CreatedBy],[CreatedDate],[IsNotSuggest],
							   [CustomerEmail],[PositionName],[CompanyName],[CustomerType],[PlaceOfAttendEvent],
							   [RegisteredSource],[Code],[Sessions] ,[IntroduceUser],
							   [TextSearch] )
						VALUES(@EventId ,@CustomerName ,@CustomerPhone ,@Note ,@CreatedBy ,GETDATE(),@IsNotSuggest,
							   @CustomerEmail,@PositionName  ,@CompanyName ,@CustomerType ,@PlaceOfAttendEvent ,
							   @RegisteredSource,@Code ,@Sessions  , @IntroduceUser,
							   dbo.ConvertToBasicLatin(@CustomerName)		+	' '
								+	ISNULL(@CustomerPhone, '')					+	' '
								+	ISNULL(@CustomerEmail, '')					+	' '
								+	dbo.ConvertToBasicLatin(@CompanyName)		+	' '
								+	dbo.ConvertToBasicLatin(@PositionName)	+	' '
								+	dbo.ConvertToBasicLatin(@Note)				+	' '
								+	dbo.ConvertToBasicLatin(@PlaceOfAttendEvent)+	' '
								+	dbo.ConvertToBasicLatin(@IntroduceUser)		+	' ' ) 
	SELECT SCOPE_IDENTITY()
END


GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_ChangeSession]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 18/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_ChangeSession]
	@Id				INT,
	@Sessions		NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE		[InviteEvent]
	SET			[Sessions]		= @Sessions
	WHERE		[Id]			= @Id
END

GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_Delete]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_Delete]
	@Id		INT
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM [InviteEvent] WHERE [Id] = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_ExistInEventId]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 08/08/2017
-- Description:	Kiếm tra sự kiện đã tạo khách mời hay chưa
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_ExistInEventId]
	@EventId		INT
AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS(SELECT [Id] FROM InviteEvent WHERE [EventId] = @EventId)
		SELECT 1
	ELSE
		SELECT 0
END

GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_GetByCode]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 22/11/2017
-- Description:	Tìm kiếm danh sách khách mời theo mã vé
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_GetByCode]
	@EventId	INT,
	@Code		NVARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT IE.[Id]
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[IsCheckIn]
		  ,IE.[CheckInBy]
		  ,IE.[CheckInDate]
		  ,IE.[Note]
		  ,IE.[CreatedBy]
		  ,IE.[CreatedDate]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[EventId]
		  ,IE.[PositionName]
		  ,IE.[UserName]
		  ,IE.[CustomerType]
		  ,IE.[IsUsingWebsiteBds]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[CompanyName]
		  ,IE.[PhoningStatus]
		  ,IE.[RegisteredDate]
		  ,IE.[CustomerEmail]
		  ,IE.[ConfirmAttend1st]
		  ,IE.[ConfirmAttend2st]
		  ,IE.[IntroduceUser]
		  ,IE.[RegisteredSource]
		  ,IE.[Status]
		  ,IE.[ApproveBy]
		  ,IE.[ApproveDate]
		  ,E.[EventCode]
		  ,E.[Name]		AS EventName
		  ,IE.[IsSelftRegistered]
		  ,IE.[Code]
		  ,IE.[AssignTo]
		  ,IE.[SourceId]
		  ,IE.[IsBcrmProcess]
		  ,IE.[Sessions]
		  ,IE.[SendTicketStatus]
		  ,IE.[BcrmAssignTo]
	FROM   [InviteEvent] IE
		INNER JOIN [Event] E   ON IE.[EventId]  = E.[Id]
	WHERE	 E.[Id]   = @EventId 
		AND IE.[Code] = @Code
END


GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_GetByCodeToCheckin]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 20/11/2017
-- Description:	Lấy thông tin khách mời theo id
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_GetByCodeToCheckin]
	@EventId	INT,	
	@Code		NVARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT IE.[Id]
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[IsCheckIn]
		  ,IE.[Note]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[EventId]
		  ,E.[Name] AS EventName
		  ,E.[EventCode]
		  ,IE.[CustomerEmail]
		  ,IE.[CompanyName]
		  ,IE.[CustomerType]
		  ,IE.[PositionName]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[RegisteredSource]
		  ,IE.[IntroduceUser]
		  ,IE.[Status]
		  ,IE.[Code]
		  ,IE.[Sessions]
	FROM   [InviteEvent] IE
		INNER JOIN [Event] E ON IE.[EventId] = E.[Id]
	WHERE  E.[Id]		= @EventId
		AND IE.[Code]	= @Code
END

GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_GetByCodeToExport]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 07/08/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_GetByCodeToExport] 
	@EventId		INT,
	@Code			NVARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT IE.[Id]					AS InviteId
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[IsCheckIn]
		  ,IE.[Note]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[EventId]
		  ,E.[Name]					AS EventName
		  ,E.[EventCode]
		  ,IE.[CheckInBy]
		  ,IE.[CheckInDate]
		  ,IE.[CreatedBy]
		  ,IE.[CustomerEmail]
		  ,IE.[PositionName]
		  ,IE.[UserName]
		  ,IE.[CustomerType]
		  ,IE.[CompanyName]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[IntroduceUser]
		  ,IE.[Status]
		  ,IE.[PhoningStatus]
		  ,IE.[ConfirmAttend1st]
		  ,IE.[ConfirmAttend2st]
		  ,IE.[Code]
	FROM   [InviteEvent] IE
		INNER JOIN [Event] E ON IE.[EventId] = E.[Id]
	WHERE  IE.[EventId] = @EventId
		AND (IE.[Status] = 1 )
		AND IE.[Code]	 = @Code
END

GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_GetByCondition]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 24/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_GetByCondition] 
	@EventId		INT,
	@IsCheckIn		BIT,
	@IsNotSuggest   BIT,
	@SessionId		INT,
	@TextSearch		NVARCHAR(200)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT IE.[Id]
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[IsCheckIn]
		  ,IE.[Note]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[EventId]
		  ,E.[Name] AS EventName
		  ,E.[EventCode]
		  ,IE.[CustomerEmail]
		  ,IE.[CompanyName]
		  ,IE.[PositionName]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[CustomerType]
		  ,IE.[CreatedDate]
		  ,IE.[Code]
		  ,IE.[Sessions]
	FROM   [InviteEvent] IE
		INNER JOIN [Event] E ON IE.[EventId] = E.[Id]
	WHERE  IE.[EventId] = @EventId
		AND (@SessionId = 0 OR IE.[Sessions] ='0' OR (IE.[Sessions] IS NOT NULL AND @SessionId IN (SELECT [Data] FROM dbo.Split(IE.[Sessions],',') ) ) )  
		AND	(@IsCheckIn IS NULL OR (IE.[IsCheckIn] IS NULL AND @IsCheckIn =0) OR (IE.[IsCheckIn]= @IsCheckIn) )
		AND (@IsNotSuggest IS NULL OR IE.[IsNotSuggest] = @IsNotSuggest)
		AND (IE.[Status] = 1 )
		AND (@TextSearch ='' OR IE.[TextSearch] LIKE '%'+@TextSearch +'%' )
END


GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_GetByConditionToExport]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 24/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_GetByConditionToExport] 
	@EventId		INT,
	@IsCheckIn		BIT,
	@IsNotSuggest   BIT,
	@SessionId		INT,
	@TextSearch		NVARCHAR(200)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT IE.[Id]					AS InviteId
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[IsCheckIn]
		  ,IE.[Note]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[EventId]
		  ,E.[Name]					AS EventName
		  ,E.[EventCode]
		  ,IE.[CheckInBy]
		  ,IE.[CheckInDate]
		  ,IE.[CreatedBy]
		  ,IE.[CustomerEmail]
		  ,IE.[PositionName]
		  ,IE.[UserName]
		  ,IE.[CustomerType]
		  ,IE.[CompanyName]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[IntroduceUser]
		  ,IE.[Status]
		  ,IE.[PhoningStatus]
		  ,IE.[ConfirmAttend1st]
		  ,IE.[ConfirmAttend2st]
		  ,IE.[Code]
	FROM   [InviteEvent] IE
		INNER JOIN [Event] E ON IE.[EventId] = E.[Id]
	WHERE  IE.[EventId] = @EventId
		AND (@SessionId = 0 OR IE.[Sessions] ='0' OR (IE.[Sessions] IS NOT NULL AND @SessionId IN (SELECT [Data] FROM dbo.Split(IE.[Sessions],',') ) ) )
		AND	(@IsCheckIn IS NULL OR (IE.[IsCheckIn] IS NULL AND @IsCheckIn =0) OR (IE.[IsCheckIn]= @IsCheckIn) )
		AND (@IsNotSuggest IS NULL OR IE.[IsNotSuggest] = @IsNotSuggest)
		AND (IE.[Status] = 1 )
		AND (@TextSearch ='' OR IE.[TextSearch] LIKE '%'+@TextSearch +'%' )
END

GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_GetByEventId]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 20/11/2017
-- Description:	Lấy danh sách khách mời theo sự kiện
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_GetByEventId]
	@EventId	INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT IE.[Id]
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[IsCheckIn]
		  ,IE.[Note]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[EventId]
		  ,E.[Name] AS EventName
		  ,E.[EventCode]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[Code]
		  ,IE.[CompanyName]
		  ,IE.[IntroduceUser]
		  ,IE.[Sessions]
	FROM   [InviteEvent] IE
		INNER JOIN [Event] E ON IE.[EventId] = E.[Id]
	WHERE  IE.[EventId] = @EventId  AND IE.[Status] = 1
END

GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_GetById]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 18/11/2017
-- Description:	Lấy thông tin khách mời theo id
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_GetById]
	@Id		INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT E.[Name]	AS EventName
		  ,E.[EventCode]
		  ,IE.[Id]
		  ,IE.[EventId]
		  ,IE.[CustomerId]
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[CustomerSex]
		  ,IE.[CustomerEmail]
		  ,IE.[CustomerBirthday]
		  ,IE.[CustomerPosition]
		  ,IE.[PositionName]
		  ,IE.[CustomerType]
		  ,IE.[CompanyId]
		  ,IE.[CompanyName]
		  ,IE.[CompanyAddress]
		  ,IE.[CompanySize]
		  ,IE.[TicketAddress]
		  ,IE.[GroupId]
		  ,IE.[AbilityAttend]
		  ,IE.[IsCheckIn]
		  ,IE.[CheckInBy]
		  ,IE.[CheckInDate]
		  ,IE.[Note]
		  ,IE.[CreatedBy]
		  ,IE.[CreatedDate]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[UserName]
		  ,IE.[IsUsingWebsiteBds]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[PhoningStatus]
		  ,IE.[RegisteredDate]
		  ,IE.[ConfirmAttend1st]
		  ,IE.[ConfirmAttend2st]
		  ,IE.[IntroduceUser]
		  ,IE.[RegisteredSource]
		  ,IE.[ApproveBy]
		  ,IE.[ApproveDate]
		  ,IE.[Status]
		  ,IE.[IsSelftRegistered]
		  ,IE.[Code]
		  ,IE.[AssignTo]
		  ,IE.[StatusNote]
		  ,IE.[CustomerVip]
		  ,IE.[SendTicketType]
		  ,IE.[Sessions]
		  ,IE.[SourceId]
		  ,IE.[CustomerRevenue]
		  ,IE.[BcrmStatus]
		  ,IE.[IsBcrmProcess]
		  ,IE.[BcrmAssignTo]
		  ,IE.[SendTicketStatus]
	FROM [InviteEvent] IE
		INNER JOIN [Event] E ON IE.[EventId] = E.[Id]
	WHERE  IE.[Id] = @Id
END



GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_GetByIdToExport]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_GetByIdToExport]
	@Id		INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT IE.[Id]					AS InviteId
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[IsCheckIn]
		  ,IE.[Note]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[EventId]
		  ,E.[Name]					AS EventName
		  ,E.[EventCode]
		  ,IE.[CheckInBy]
		  ,IE.[CheckInDate]
		  ,IE.[CreatedBy]
		  ,IE.[CustomerEmail]
		  ,IE.[PositionName]
		  ,IE.[UserName]
		  ,IE.[CustomerType]
		  ,IE.[CompanyName]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[IntroduceUser]
		  ,IE.[Status]
		  ,IE.[PhoningStatus]
		  ,IE.[ConfirmAttend1st]
		  ,IE.[ConfirmAttend2st]
		  ,IE.[Code]
	FROM   [InviteEvent] IE
		INNER JOIN [Event] E ON IE.[EventId] = E.[Id]
	WHERE  IE.[Id] = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_GetListOnlineRegisterer]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 22/11/2017
-- Description:	Lấy danh sách khách mời đăng kí online
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_GetListOnlineRegisterer]
	@EventId	INT,
	@AssignTo	NVARCHAR(250),
	@SessionId	INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT IE.[Id]
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[IsCheckIn]
		  ,IE.[CheckInBy]
		  ,IE.[CheckInDate]
		  ,IE.[Note]
		  ,IE.[CreatedBy]
		  ,IE.[CreatedDate]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[EventId]
		  ,IE.[PositionName]
		  ,IE.[UserName]
		  ,IE.[CustomerType]
		  ,IE.[IsUsingWebsiteBds]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[CompanyName]
		  ,IE.[PhoningStatus]
		  ,IE.[RegisteredDate]
		  ,IE.[CustomerEmail]
		  ,IE.[ConfirmAttend1st]
		  ,IE.[ConfirmAttend2st]
		  ,IE.[IntroduceUser]
		  ,IE.[RegisteredSource]
		  ,IE.[Status]
		  ,IE.[ApproveBy]
		  ,IE.[ApproveDate]
		  ,E.[EventCode]
		  ,E.[Name]		AS EventName
		  ,IE.[IsSelftRegistered]
		  ,IE.[Code]
		  ,IE.[AssignTo]
		  ,IE.[SourceId]
		  ,IE.[IsBcrmProcess]
		  ,IE.[Sessions]
		  ,IE.[SendTicketStatus]
		  ,IE.[BcrmAssignTo]
	FROM   [InviteEvent] IE
		INNER JOIN [Event] E   ON IE.[EventId]  = E.[Id]
	WHERE E.[Id] = @EventId 
		AND IE.[IsNotSuggest] = 0
		AND (@AssignTo	='' OR IE.[AssignTo] LIKE '%'+@AssignTo+'%' )
		AND (@SessionId =0  OR IE.[Sessions] = '0' OR (IE.[Sessions] IS NOT NULL AND @SessionId IN (SELECT [Data] FROM dbo.Split(IE.[Sessions],',') )) )
END


GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_GetOnlineRegisterById]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 17/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_GetOnlineRegisterById]
		@Id		INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [Id]
		  ,[CustomerName]
		  ,[CustomerPhone]
		  ,[IsCheckIn]
		  ,[CheckInBy]
		  ,[CheckInDate]
		  ,[Note]
		  ,[CreatedBy]
		  ,[CreatedDate]
		  ,[AccompanyMember]
		  ,[IsNotSuggest]
		  ,[EventId]
		  ,[PositionName]
		  ,[UserName]
		  ,[CustomerType]
		  ,[IsUsingWebsiteBds]
		  ,[PlaceOfAttendEvent]
		  ,[CompanyName]
		  ,[PhoningStatus]
		  ,[RegisteredDate]
		  ,[CustomerEmail]
		  ,[ConfirmAttend1st]
		  ,[ConfirmAttend2st]
		  ,[IntroduceUser]
		  ,[RegisteredSource]
		  ,[ApproveBy]
		  ,[ApproveDate]
		  ,[Status]
		  ,[Code]
		  ,[Sessions]
		  ,[SendTicketStatus]
	FROM   [InviteEvent] IE
	WHERE IE.[Id] = @Id
END


GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_InsertOnlineInvitee]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 24/11/2017
-- Description:	THêm mới
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_InsertOnlineInvitee]
	@EventId				INT,
	@CustomerName			NVARCHAR(500),
	@IntroduceUser			NVARCHAR(150),
	@CustomerPhone			NVARCHAR(20),
	@CustomerEmail			NVARCHAR(150),
	@CompanyName			NVARCHAR(250),
	@CustomerType			INT,
	@PositionName			NVARCHAR(200),
	@PlaceOfAttendEvent		NVARCHAR(150),
	@RegisteredDate			DATETIME,
	@CreatedBy				NVARCHAR(50),
	@PhoningStatus			INT,
	@ConfirmAttend1st		INT,
	@ConfirmAttend2st		INT,
	@Note					NVARCHAR(1000),
	@RegisteredSource		INT,
	@Code					NVARCHAR(20),
	@Sessions				NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [InviteEvent]([EventId]      ,[CustomerName]  ,[IntroduceUser] ,[CustomerPhone]     ,[CustomerEmail],[PositionName],
							  [CustomerType] ,[RegisteredDate],[CompanyName]   ,[PlaceOfAttendEvent],
							  [CreatedBy]	 ,[CreatedDate]	  ,[PhoningStatus] ,[ConfirmAttend1st]  ,[ConfirmAttend2st] ,[Note]  ,
							  [RegisteredSource],[Code]		  ,[Sessions],
							  [TextSearch]	)
					   VALUES(@EventId       ,@CustomerName   ,@IntroduceUser  ,@CustomerPhone      ,@CustomerEmail ,@PositionName ,
							  @CustomerType  ,@RegisteredDate ,@CompanyName    ,@PlaceOfAttendEvent ,
							  @CreatedBy	 ,GETDATE()		  ,@PhoningStatus  ,@ConfirmAttend1st   ,@ConfirmAttend2st  ,@Note   ,
							  @RegisteredSource,@Code		  ,@Sessions ,
							  dbo.ConvertToBasicLatin(@CustomerName)		+	' '
								+	ISNULL(@CustomerPhone, '')					+	' '
								+	ISNULL(@CustomerEmail, '')					+	' '
								+	dbo.ConvertToBasicLatin(@CompanyName)		+	' '
								+	dbo.ConvertToBasicLatin(@PositionName)	+	' '
								+	dbo.ConvertToBasicLatin(@Note)				+	' '
								+	dbo.ConvertToBasicLatin(@PlaceOfAttendEvent)+	' '
								+	dbo.ConvertToBasicLatin(@IntroduceUser)		+	' ' )
	SELECT SCOPE_IDENTITY()
END


GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_IsEmailExist]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		HieuBv
-- Create date: 21/04/2016
-- Description:	Kiểm tra thông tin email đã tồn tại chưa
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_IsEmailExist]
	@Id					INT,
	@EventId			INT,
	@CustomerEmail		NVARCHAR(50)	
AS
BEGIN
	SET NOCOUNT ON;
	IF((	
		SELECT COUNT(1) 
		FROM [InviteEvent] IE 
		WHERE	IE.[Id] <> @Id 
			AND IE.[CustomerEmail]	= @CustomerEmail  
			AND IE.[EventId]		= @EventId)>0
			)
		SELECT 1
	ELSE 
		SELECT 0
END


GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_IsExistCode]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 08/08/2017
-- Description:	Kiểm tra mã vé đã tồn tại chưa
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_IsExistCode]
	@EventId		INT,
	@Code			NVARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS(SELECT [Id] FROM [InviteEvent] WHERE [EventId] = @EventId AND [Code] = @Code )
		SELECT 1
	ELSE 
		SELECT 0
END

GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_IsHasInviteeInEvent]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 24/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_IsHasInviteeInEvent]
	@EventId			INT
AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS(SELECT [Id] FROM [InviteEvent] WHERE [EventId] = @EventId )
		SELECT 1
	ELSE
		SELECT 0

END

GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_IsPhoneExist]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBv
-- Create date: 21/04/2016
-- Description:	Kiểm tra thông tin số điện thoại đã tồn tại chưa
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_IsPhoneExist]
	@Id					INT,
	@EventId			INT,
	@CustomerPhone		NVARCHAR(1000)	
AS
BEGIN
	SET NOCOUNT ON;
	IF((	
		SELECT COUNT(1) 
		FROM [InviteEvent] IE 
		WHERE	IE.[Id] <> @Id 
			AND IE.[CustomerPhone] = @CustomerPhone  
			AND IE.[EventId]= @EventId)>0
			)
		SELECT 1
	ELSE 
		SELECT 0
END

GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_SearchInviteeByCondition]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 30/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_SearchInviteeByCondition]
	@EventId			INT,
	@AssignTo			NVARCHAR(250),
	@SessionId			INT,
	@PhoningStatus		INT,
	@ConfirmAttend2st	INT,
	@ConfirmAttend1st	INT,
	@Status				BIT,
	@SendTicketStatus	INT,
	@SourceId			INT,
	@TextSearch			NVARCHAR(200),
	@StartDate			DATETIME,
	@EndDate			DATETIME
AS
BEGIN
	SET NOCOUNT ON;
	IF @ConfirmAttend1st = 0 OR @ConfirmAttend2st = 0
	BEGIN
	SELECT IE.[Id]
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[IsCheckIn]
		  ,IE.[CheckInBy]
		  ,IE.[CheckInDate]
		  ,IE.[Note]
		  ,IE.[CreatedBy]
		  ,IE.[CreatedDate]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[EventId]
		  ,IE.[PositionName]
		  ,IE.[UserName]
		  ,IE.[CustomerType]
		  ,IE.[IsUsingWebsiteBds]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[CompanyName]
		  ,IE.[PhoningStatus]
		  ,IE.[RegisteredDate]
		  ,IE.[CustomerEmail]
		  ,IE.[ConfirmAttend1st]
		  ,IE.[ConfirmAttend2st]
		  ,IE.[IntroduceUser]
		  ,IE.[RegisteredSource]
		  ,IE.[Status]
		  ,IE.[ApproveBy]
		  ,IE.[ApproveDate]
		  ,E.[EventCode]
		  ,E.[Name]		AS EventName
		  ,IE.[IsSelftRegistered]
		  ,IE.[Code]
		  ,IE.[AssignTo]
		  ,IE.[SourceId]
		  ,IE.[IsBcrmProcess]
		  ,IE.[Sessions]
		  ,IE.[SendTicketStatus]
		  ,IE.[BcrmAssignTo]
	FROM   [InviteEvent] IE
		INNER JOIN [Event] E   ON IE.[EventId]  = E.[Id]
	WHERE	 E.[Id] = @EventId 
		AND IE.[IsNotSuggest] = 0
		AND (@AssignTo	='' OR IE.[AssignTo] LIKE '%'+@AssignTo+'%' )
		AND (@SessionId =0  OR IE.[Sessions] = '0' OR (IE.[Sessions] IS NOT NULL AND @SessionId IN (SELECT [Data] FROM dbo.Split(IE.[Sessions],',') )) )
		AND (@PhoningStatus		= 0 OR (@PhoningStatus = -1 AND IE.[PhoningStatus] IS NULL ) OR ( IE.[ConfirmAttend1st] IS NULL AND IE.[PhoningStatus] = @PhoningStatus  )  )
		AND (@ConfirmAttend1st	= 0 OR ( IE.[ConfirmAttend2st] IS NULL AND IE.[ConfirmAttend1st] = @ConfirmAttend1st  ) )
		AND (@ConfirmAttend2st  = 0 OR ( IE.[ConfirmAttend2st] = @ConfirmAttend2st ) )
		AND (@Status IS NULL OR ISNULL(IE.[Status],0)  = @Status ) 
		AND (@SendTicketStatus =0 OR  IE.[SendTicketStatus] = @SendTicketStatus )
		AND (@SourceId =0 OR IE.[SourceId] = @SourceId )
		AND (@TextSearch ='' OR IE.[TextSearch]	 LIKE ('%' + @TextSearch +'%'))
		AND (IE.[CreatedDate] BETWEEN @StartDate AND @EndDate )
	END
	ELSE	
	BEGIN
		SELECT IE.[Id]
			  ,IE.[CustomerName]
			  ,IE.[CustomerPhone]
			  ,IE.[IsCheckIn]
			  ,IE.[CheckInBy]
			  ,IE.[CheckInDate]
			  ,IE.[Note]
			  ,IE.[CreatedBy]
			  ,IE.[CreatedDate]
			  ,IE.[AccompanyMember]
			  ,IE.[IsNotSuggest]
			  ,IE.[EventId]
			  ,IE.[PositionName]
			  ,IE.[UserName]
			  ,IE.[CustomerType]
			  ,IE.[IsUsingWebsiteBds]
			  ,IE.[PlaceOfAttendEvent]
			  ,IE.[CompanyName]
			  ,IE.[PhoningStatus]
			  ,IE.[RegisteredDate]
			  ,IE.[CustomerEmail]
			  ,IE.[ConfirmAttend1st]
			  ,IE.[ConfirmAttend2st]
			  ,IE.[IntroduceUser]
			  ,IE.[RegisteredSource]
			  ,IE.[Status]
			  ,IE.[ApproveBy]
			  ,IE.[ApproveDate]
			  ,E.[EventCode]
			  ,E.[Name]		AS EventName
			  ,IE.[IsSelftRegistered]
			  ,IE.[Code]
			  ,IE.[AssignTo]
			  ,IE.[SourceId]
			  ,IE.[IsBcrmProcess]
			  ,IE.[Sessions]
			  ,IE.[SendTicketStatus]
			  ,IE.[BcrmAssignTo]
		FROM   [InviteEvent] IE
			INNER JOIN [Event] E   ON IE.[EventId]  = E.[Id]
		WHERE	 E.[Id] = @EventId 
			AND IE.[IsNotSuggest] = 0
			AND (@AssignTo	='' OR IE.[AssignTo] LIKE '%'+@AssignTo+'%' )
			AND (@SessionId =0  OR IE.[Sessions] = '0' OR (IE.[Sessions] IS NOT NULL AND @SessionId IN (SELECT [Data] FROM dbo.Split(IE.[Sessions],',') )) )
			AND (( IE.[ConfirmAttend2st] IS NULL AND IE.[ConfirmAttend1st] = @ConfirmAttend1st  ) 
				OR (( IE.[ConfirmAttend2st] = @ConfirmAttend2st ) ))
			AND (@SourceId =0 OR IE.[SourceId] = @SourceId )
			AND (@TextSearch ='' OR IE.[TextSearch]	 LIKE ('%' + @TextSearch +'%'))
			AND (IE.[CreatedDate] BETWEEN @StartDate AND @EndDate )
	END
END


GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_SearchOnlineRegistererById]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 22/11/2017
-- Description:	Lấy khách mời đăng kí theo id
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_SearchOnlineRegistererById]
	@Id	INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT IE.[Id]
		  ,IE.[CustomerName]
		  ,IE.[CustomerPhone]
		  ,IE.[IsCheckIn]
		  ,IE.[CheckInBy]
		  ,IE.[CheckInDate]
		  ,IE.[Note]
		  ,IE.[CreatedBy]
		  ,IE.[CreatedDate]
		  ,IE.[AccompanyMember]
		  ,IE.[IsNotSuggest]
		  ,IE.[EventId]
		  ,IE.[PositionName]
		  ,IE.[UserName]
		  ,IE.[CustomerType]
		  ,IE.[IsUsingWebsiteBds]
		  ,IE.[PlaceOfAttendEvent]
		  ,IE.[CompanyName]
		  ,IE.[PhoningStatus]
		  ,IE.[RegisteredDate]
		  ,IE.[CustomerEmail]
		  ,IE.[ConfirmAttend1st]
		  ,IE.[ConfirmAttend2st]
		  ,IE.[IntroduceUser]
		  ,IE.[RegisteredSource]
		  ,IE.[Status]
		  ,IE.[ApproveBy]
		  ,IE.[ApproveDate]
		  ,IE.[IsSelftRegistered]
		  ,E.[EventCode]
		  ,E.[Name]		AS EventName
		  ,IE.[Code]
		  ,IE.[AssignTo]
		  ,IE.[SourceId]
		  ,IE.[IsBcrmProcess]
		  ,IE.[Sessions]
		  ,IE.[SendTicketStatus]
		  ,IE.[BcrmAssignTo]
	FROM   [InviteEvent] IE
		INNER JOIN [Event] E   ON IE.[EventId]  = E.[Id]
	WHERE IE.[Id] = @Id AND IE.[IsNotSuggest] = 0
END


GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_SyncToBCRM]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 24/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_SyncToBCRM]
	@Id					INT,
	@CustomerId			INT,
	@CustomerBirthday	DATETIME,
	@CustomerType		INT,
	@CompanyId			INT,
	@CompanyName		NVARCHAR(250),
	@CompanyAddress		NVARCHAR(500),
	@CompanySize		INT,
	@CustomerRevenue	INT,
	@BcrmStatus			INT,
	@IsBcrmProcess		BIT,
	@BcrmAssignTo		NVARCHAR(50),
	@GroupId			INT
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE	[InviteEvent]
	SET	 	[CustomerId]			= @CustomerId
		   ,[CustomerBirthday]		= @CustomerBirthday
		   ,[CustomerType]			= @CustomerType
		   ,[CompanyId]				= @CompanyId
		   ,[CompanyName]			= @CompanyName
		   ,[CompanyAddress]		= @CompanyAddress
		   ,[CompanySize]			= @CompanySize
		   ,[CustomerRevenue]		= @CustomerRevenue
		   ,[BcrmStatus]			= @BcrmStatus
		   ,[IsBcrmProcess]			= @IsBcrmProcess
		   ,[BcrmAssignTo]			= @BcrmAssignTo
		   ,[GroupId]				= @GroupId
		   ,[TextSearch]			=		CAST(@CustomerId	AS	NVARCHAR(10))		  +	' '
										+	dbo.ConvertToBasicLatin([CustomerName])		  +	' '
										+	ISNULL([CustomerPhone], '')					  +	' '
										+	ISNULL([CustomerEmail], '')					  +	' '
										+	dbo.ConvertToBasicLatin(@CompanyName)		  +	' '
										+	dbo.ConvertToBasicLatin(@CompanyAddress)	  +	' '
										+	dbo.ConvertToBasicLatin([Note])				  +	' '
										+	dbo.ConvertToBasicLatin([TicketAddress])	  +	' '
										+	dbo.ConvertToBasicLatin([StatusNote])		  +	' '
										+	dbo.ConvertToBasicLatin([PlaceOfAttendEvent]) +	' '
										+	dbo.ConvertToBasicLatin([IntroduceUser])	  +	' '
										+	dbo.ConvertToBasicLatin([PositionName])		  +	' '
	WHERE [Id]	= @Id
END

GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_Update]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 24/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_Update]
	@Id				INT,
	@EventId		INT,
	@CustomerName	NVARCHAR(500),
	@CustomerPhone	NVARCHAR(1000),
	@Note			NVARCHAR(1000),
	@CustomerEmail  NVARCHAR(150),
	@CompanyName	NVARCHAR(250),
	@CustomerType	INT,
	@PositionName	NVARCHAR(200),
	@PlaceOfAttendEvent NVARCHAR(150),
	@RegisteredSource   INT,
	@IntroduceUser		NVARCHAR(150),
	@Sessions			NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE [InviteEvent]
	SET    [EventId]		= @EventId,
		   [CustomerName]	= @CustomerName,
		   [CustomerPhone]  = @CustomerPhone,
		   [Note]			= @Note,
		   [CustomerEmail]  = @CustomerEmail,
		   [CompanyName]	= @CompanyName,
		   [CustomerType]   = @CustomerType,
		   [PositionName]	= @PositionName,
		   [PlaceOfAttendEvent] = @PlaceOfAttendEvent,
		   [RegisteredSource] = @RegisteredSource,
		   [IntroduceUser]	  = @IntroduceUser,
		   [Sessions]		  = @Sessions ,
		   [TextSearch]		  = dbo.ConvertToBasicLatin(@CustomerName)		+	' '
								+	ISNULL(@CustomerPhone, '')					+	' '
								+	ISNULL(@CustomerEmail, '')					+	' '
								+	dbo.ConvertToBasicLatin(@CompanyName)		+	' '
								+	dbo.ConvertToBasicLatin(@PositionName)	+	' '
								+	dbo.ConvertToBasicLatin(@Note)				+	' '
								+	dbo.ConvertToBasicLatin(@PlaceOfAttendEvent)+	' '
								+	dbo.ConvertToBasicLatin(@IntroduceUser)		+	' '
	WHERE  [Id]				= @Id
END


GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_UpdateAccompanyMember]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_UpdateAccompanyMember]
	@Id					INT,
	@AccompanyMember	INT
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE [InviteEvent]
	SET		[AccompanyMember]	= @AccompanyMember
	WHERE  [Id]					= @Id	
END

GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_UpdateAssignTo]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_UpdateAssignTo]
	@Id				INT,
	@AssignTo		NVARCHAR(250)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE [InviteEvent]
		SET [AssignTo]		 = @AssignTo
	WHERE	[Id]			 = @Id 
END

GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_UpdateBCRMInvitee]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 24/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_UpdateBCRMInvitee]
	@Id					INT,
	@CustomerName		NVARCHAR(500),
	@CustomerPhone		NVARCHAR(1000),
	@CustomerEmail		NVARCHAR(150),
	@CustomerPosition	INT,
	@Sessions			NVARCHAR(50),
	@Note				NVARCHAR(1000)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE	[InviteEvent]
	SET		[CustomerName]			= @CustomerName,
			[CustomerPhone]			= @CustomerPhone,
			[CustomerEmail]			= @CustomerEmail,
			[CustomerPosition]		= @CustomerPosition,
			[Sessions]				= @Sessions,
			[Note]					= @Note,
			[TextSearch]			=		CAST(@Id AS NVARCHAR(20))					+	' '
										+   CAST([CustomerId]	AS	NVARCHAR(10))		+	' '
										+	dbo.ConvertToBasicLatin(@CustomerName)		+	' '
										+	ISNULL(@CustomerPhone, '')					+	' '
										+	ISNULL(@CustomerEmail, '')					+	' '
										+	dbo.ConvertToBasicLatin([CompanyName])		+	' '
										+	dbo.ConvertToBasicLatin([CompanyAddress])	+	' '
										+	dbo.ConvertToBasicLatin(@Note)				+	' '
										+	dbo.ConvertToBasicLatin([TicketAddress])	+	' '
										+	dbo.ConvertToBasicLatin([StatusNote])		+	' '
										+	dbo.ConvertToBasicLatin([PlaceOfAttendEvent]) +	' '
										+	dbo.ConvertToBasicLatin([IntroduceUser])	  +	' '
										+	dbo.ConvertToBasicLatin([PositionName])		  +	' '
	WHERE   [Id]					= @Id
END


GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_UpdateCheckin]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 22/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_UpdateCheckin]
	@Id				 INT,
	@IsCheckIn		 BIT,
	@CheckInBy		 NVARCHAR(50),
	@AccompanyMember INT
AS
BEGIN
	SET NOCOUNT ON;
	IF @IsCheckIn = 1
		UPDATE [InviteEvent]
		 SET   [IsCheckIn]   = @IsCheckIn,
			   [CheckInBy]   = @CheckInBy,
			   [CheckInDate] = GETDATE(),
			   [AccompanyMember] = @AccompanyMember,
			   [BcrmStatus] = 15
		WHERE [Id] = @Id
	ELSE
		UPDATE [InviteEvent]
		 SET   [IsCheckIn]   = @IsCheckIn,
			   [CheckInBy]   = NULL,
			   [CheckInDate] = NULL,
			   [AccompanyMember] = 0
			   --[BcrmStatus] = 16
		WHERE [Id] = @Id		   	
END	

GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_UpdateOnlineInvitee]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 24/11/2017
-- Description:	Cập nhật
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_UpdateOnlineInvitee]
	@Id						INT,
	@EventId				INT,
	@CustomerName			NVARCHAR(500),
	@IntroduceUser			NVARCHAR(150),
	@CustomerPhone			NVARCHAR(20),
	@CustomerEmail			NVARCHAR(150),
	@CompanyName			NVARCHAR(250),
	@CustomerType			INT,
	@PositionName			NVARCHAR(200),
	@PlaceOfAttendEvent		NVARCHAR(150),
	@PhoningStatus			INT,
	@ConfirmAttend1st		INT,
	@ConfirmAttend2st		INT,
	@Note					NVARCHAR(1000),
	@RegisteredSource		INT,
	@Sessions				NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE [InviteEvent]
	SET	    [EventId]		= @EventId ,
			[CustomerName]  = @CustomerName,
			[IntroduceUser] = @IntroduceUser,
			[CustomerPhone] = @CustomerPhone,
			[CustomerEmail] = @CustomerEmail,
			[PositionName]	= @PositionName,
			[CustomerType]  = @CustomerType,
			[CompanyName]   = @CompanyName,
			[PlaceOfAttendEvent] = @PlaceOfAttendEvent,
			[PhoningStatus] = @PhoningStatus,
			[ConfirmAttend1st] =@ConfirmAttend1st ,
			[ConfirmAttend2st] =@ConfirmAttend2st,
			[Note]			   = @Note,
			[RegisteredSource] = @RegisteredSource,
			[Sessions]		   = @Sessions,
			[TextSearch]	   = dbo.ConvertToBasicLatin(@CustomerName)		+	' '
								+	ISNULL(@CustomerPhone, '')					+	' '
								+	ISNULL(@CustomerEmail, '')					+	' '
								+	dbo.ConvertToBasicLatin(@CompanyName)		+	' '
								+	dbo.ConvertToBasicLatin(@PositionName)	+	' '
								+	dbo.ConvertToBasicLatin(@Note)				+	' '
								+	dbo.ConvertToBasicLatin(@PlaceOfAttendEvent)+	' '
								+	dbo.ConvertToBasicLatin(@IntroduceUser)		+	' '
	WHERE   [Id]			   = @Id	   
END


GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_UpdateSendTicketStatus]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 23/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_UpdateSendTicketStatus]
		@Id					INT,
		@SendTicketStatus	INT,
		@BcrmStatus			INT
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE	[InviteEvent]
	SET		[SendTicketStatus] = @SendTicketStatus ,
			[BcrmStatus]	   = @BcrmStatus
	WHERE	[Id]			   = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_UpdateStatus]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 23/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEvent_UpdateStatus]
	@Id				INT,
	@Status			BIT,
	@ApproveDate	DATETIME,
	@ApproveBy		NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	IF @Status	=1
		UPDATE		[InviteEvent]
		SET			[Status]			= @Status,
					[ApproveDate]		= @ApproveDate,
					[ApproveBy]			= @ApproveBy,
					[BcrmStatus]		= 16
		WHERE		[Id]				= @Id
	ELSE
		UPDATE		[InviteEvent]
		SET			[Status]			= @Status,
					[ApproveDate]		= NULL,
					[ApproveBy]			= NULL
		WHERE		[Id]				= @Id
END

GO
/****** Object:  StoredProcedure [dbo].[InviteEventHistory_GetByInviteEventId]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 17/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEventHistory_GetByInviteEventId]
	@InviteEventId		INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT	[Id]
      ,		[InviteEventId]
      ,		[Action]
      ,		[Note]
	  ,		[OldValue]
	  ,		[NewValue]
      ,		[UpdatedBy]
      ,		[UpdatedDate]
  FROM		[InviteEventHistory]
  WHERE [InviteEventId] = @InviteEventId
  ORDER BY Id DESC
END


GO
/****** Object:  StoredProcedure [dbo].[InviteEventHistory_Insert]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 17/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[InviteEventHistory_Insert]
	@InviteEventId	INT
,	@Action			NVARCHAR(50)
,	@Note			NVARCHAR(1000)
,	@OldValue		NVARCHAR(1000)
,	@NewValue		NVARCHAR(1000)
,	@UpdatedBy		NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO	[InviteEventHistory]
           ([InviteEventId]
           ,[Action]
           ,[Note]
           ,[OldValue]
           ,[NewValue]
		   ,[UpdatedBy]
		   ,[UpdatedDate])
    VALUES
           (@InviteEventId
           ,@Action
           ,@Note
           ,@OldValue
		   ,@NewValue
		   ,@UpdatedBy
           ,GETDATE())
	SELECT SCOPE_IDENTITY()
END


GO
/****** Object:  StoredProcedure [dbo].[LuckyNumber_Delete]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 12/08/2017
-- Description:	Xóa
-- =============================================
CREATE PROCEDURE [dbo].[LuckyNumber_Delete]
	@Id			INT
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM [LuckyNumber] WHERE [Id] = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[LuckyNumber_GetByCodeAndEventId]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 11/08/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[LuckyNumber_GetByCodeAndEventId]
	@EventId		INT,
	@Code			NVARCHAR(20),
	@IsInWebCheckin BIT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [Id]
		  ,[EventId]
		  ,[IsInWebCheckin]
		  ,[Code]
		  ,[CreatedBy]
		  ,[CreatedDate]
	FROM   [LuckyNumber]
	WHERE  [EventId]		= @EventId
	 AND   [IsInWebCheckin] = @IsInWebCheckin
	 AND   [Code]			= @Code 
END

GO
/****** Object:  StoredProcedure [dbo].[LuckyNumber_GetByEventId]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 08/08/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[LuckyNumber_GetByEventId]
	@EventId			INT,
	@IsInWebCheckin		BIT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [Id]
		  ,[EventId]
		  ,[IsInWebCheckin]
		  ,[Code]
		  ,[CreatedBy]
		  ,[CreatedDate]
    FROM [LuckyNumber]
	WHERE [EventId]			= @EventId
     AND  [IsInWebCheckin] 	= @IsInWebCheckin	
END

GO
/****** Object:  StoredProcedure [dbo].[LuckyNumber_GetById]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 12/08/2017
-- Description:	Lấy theo Id
-- =============================================
CREATE PROCEDURE [dbo].[LuckyNumber_GetById]
	@Id			INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [Id]
		  ,[EventId]
		  ,[IsInWebCheckin]
		  ,[Code]
		  ,[CreatedBy]
		  ,[CreatedDate]
	FROM [LuckyNumber]
	WHERE [Id] = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[LuckyNumber_Insert]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 08/08/2017
-- Description:	Thêm mới
-- =============================================
CREATE PROCEDURE [dbo].[LuckyNumber_Insert]
	@EventId			INT,
	@IsInWebCheckin		BIT,
	@Code				NVARCHAR(20),
	@CreatedBy			NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [LuckyNumber]([EventId],[Code],[IsInWebCheckin],[CreatedBy],[CreatedDate])
					   VALUES(@EventId ,@Code ,@IsInWebCheckin ,@CreatedBy ,GETDATE()  )
	SELECT SCOPE_IDENTITY()
END

GO
/****** Object:  StoredProcedure [dbo].[SendSMSHistory_Insert]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HIEUBV
-- Create date: 22/05/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SendSMSHistory_Insert]
	@SMSResponse		INT,
	@Description		NVARCHAR(200),
	@InviteEventId		INT,
	@PhoneNumber		NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [SendSMSHistory]([SMSResponse] ,[Description] ,[InviteEventId] ,[CreatedDate] ,[PhoneNumber])
						  VALUES(@SMSResponse  ,@Description  ,@InviteEventId  ,GETDATE()	  ,@PhoneNumber )
	SELECT SCOPE_IDENTITY()
END

GO
/****** Object:  StoredProcedure [dbo].[Session_Create]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Session_Create]
	@EventId		INT,
	@Name			NVARCHAR(500),
	@StartDate		DATETIME,
	@EndDate		DATETIME,
	@Description	NVARCHAR(1000)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [Session]([EventId],[Name],[StartDate],[EndDate],[Description])
				   VALUES(@EventId ,@Name ,@StartDate ,@EndDate ,@Description  )
	SELECT SCOPE_IDENTITY()			   	
END

GO
/****** Object:  StoredProcedure [dbo].[Session_Delete]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 22/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Session_Delete]
	@Id			INT
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM [Session]	WHERE [Id]		= @Id
END

GO
/****** Object:  StoredProcedure [dbo].[Session_GetByEventId]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 16/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Session_GetByEventId]
	@EventId		INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [Id]
		  ,[EventId]
		  ,[Name]
		  ,[StartDate]
		  ,[EndDate]
		  ,[Description]
   FROM    [Session]
   WHERE [EventId] = @EventId
END

GO
/****** Object:  StoredProcedure [dbo].[Session_GetByEventIds]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		TungNT
-- Create date: 10/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Session_GetByEventIds]
	-- Add the parameters for the stored procedure here
	@EventIds	VARCHAR(1000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT	[Id]
		,	[EventId]
		,	[Name]
		,	[StartDate]
		,	[EndDate]
		,	[Description]
	FROM	[Session]
	WHERE [EventId] IN (SELECT CAST(data AS INT) FROM dbo.Split(@EventIds,','))
END

GO
/****** Object:  StoredProcedure [dbo].[SessionCheckin_GetByInviteeIds]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 30/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SessionCheckin_GetByInviteeIds]
	@EventId		INT,
	@InviteeIds		NVARCHAR(4000),
	@SessionId		INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [Data] 
	INTO #tempIds
	FROM dbo.Split(@InviteeIds,',')

	SELECT [InviteEventId]
		  ,[SessionId]
		  ,[CheckinBy]
		  ,[CheckinDate]
		  ,[EventId]
		  ,[AccompanyMember]
	FROM   [SessionCheckin]
	WHERE  [EventId] = @EventId
	  AND  [InviteEventId] IN (SELECT [Data] FROM #tempIds )
	  AND  (@SessionId=0 OR [SessionId]	= @SessionId) 

	DROP TABLE #tempIds
END

GO
/****** Object:  StoredProcedure [dbo].[SessionCheckin_GetByInviteEventId]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 30/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SessionCheckin_GetByInviteEventId]
	@InviteEventId		INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [InviteEventId]
		  ,[SessionId]
		  ,[CheckinBy]
		  ,[CheckinDate]
		  ,[EventId]
		  ,[AccompanyMember]
   FROM	   [SessionCheckin]
   WHERE   [InviteEventId]		 = @InviteEventId	
END

GO
/****** Object:  StoredProcedure [dbo].[SessionCheckin_Insert]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 29/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SessionCheckin_Insert]
	@InviteEventId			INT,
	@SessionId				INT,
	@CheckinBy				NVARCHAR(50),
	@CheckinDate			DATETIME,
	@EventId				INT,
	@AccompanyMember		INT
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [SessionCheckin]([InviteEventId],[SessionId],[CheckinBy],[CheckinDate],[EventId],[AccompanyMember] )
						  VALUES(@InviteEventId ,@SessionId ,@CheckinBy ,@CheckinDate ,@EventId ,@AccompanyMember  )
END


GO
/****** Object:  StoredProcedure [dbo].[SessionCheckin_IsExist]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 20/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SessionCheckin_IsExist]
	@InviteEventId			INT,
	@SessionId				INT
AS
BEGIN
	SET NOCOUNT ON;
	IF EXISTS(SELECT [SessionId] FROM [SessionCheckin] WHERE [InviteEventId] = @InviteEventId AND [SessionId] = @SessionId )
		SELECT 1
	ELSE 
		SELECT 0
END

GO
/****** Object:  StoredProcedure [dbo].[SessionCheckin_Reject]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 20/11/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SessionCheckin_Reject]
	@InviteeId			INT,
	@SessionId			INT
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM [SessionCheckin] WHERE  [InviteEventId] = @InviteeId
									AND [SessionId]		= @SessionId
END

GO
/****** Object:  StoredProcedure [dbo].[SetDefaultEvent_Cancel]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SetDefaultEvent_Cancel]
	@Id			INT
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE	[SetDefaultEvent]
	SET		[IsCancel] = 1
	WHERE	[Id]	   = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[SetDefaultEvent_Create]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 20/04/2017
-- Description:	Thêm mới
-- =============================================
CREATE PROCEDURE [dbo].[SetDefaultEvent_Create]
	@EventId		INT,
	@CreatedBy		NVARCHAR(50),
	@IsInWebCheckin BIT
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [SetDefaultEvent] ([EventId],[CreatedDate],[CreatedBy],[IsInWebCheckin])
							VALUES(@EventId ,GETDATE()    ,@CreatedBy ,@IsInWebCheckin) 

	SELECT SCOPE_IDENTITY()
END

GO
/****** Object:  StoredProcedure [dbo].[SetDefaultEvent_GetLastest]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SetDefaultEvent_GetLastest]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT TOP 1 [Id]
				,[EventId]
				,[CreatedDate]
				,[CreatedBy]
				,[IsInWebCheckin]
				,[IsCancel]
	FROM [SetDefaultEvent]
	ORDER BY [CreatedDate] DESC
END

GO
/****** Object:  StoredProcedure [dbo].[User_ChangeStatus]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[User_ChangeStatus]
	-- Add the parameters for the stored procedure here
	@UserName  NVARCHAR(50),
	@Status    INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [User]
	SET [Status]=@Status
	WHERE [UserName] = @UserName
END

GO
/****** Object:  StoredProcedure [dbo].[User_Delete]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[User_Delete]
	-- Add the parameters for the stored procedure here
	@UserName  NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [User]
	WHERE [UserName] = @UserName
END

GO
/****** Object:  StoredProcedure [dbo].[User_GetByRole]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[User_GetByRole]
	@Role		INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT U.[UserName],
		   U.[Role]
	FROM	[User] U
		INNER JOIN  [UserRole] UR ON U.[UserName] = UR.[UserName]
	WHERE   UR.[RoleId] = @Role
		AND U.[Status] = 1
END

GO
/****** Object:  StoredProcedure [dbo].[User_GetByUserName]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[User_GetByUserName]
	@UserName	NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [UserName], [Status], [Role] FROM [User] WHERE [UserName] = @UserName
END

GO
/****** Object:  StoredProcedure [dbo].[User_GetByUserNamePassword]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 23/11/2016
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[User_GetByUserNamePassword]
	-- Add the parameters for the stored procedure here
	@UserName NVARCHAR(50),
	@Password NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	SELECT U.[UserName],
		   U.[Password],
		   U.[Status],
		   U.[Role],
		   U.[LockTime],
		   U.[LockedStartDate] 
	FROM	[User] U
	WHERE   U.[UserName] = @UserName
		AND U.[Password] = @Password
		AND U.[Status] = 1
END

GO
/****** Object:  StoredProcedure [dbo].[User_GetByUserNameWithLockTime]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 13/12/2016
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[User_GetByUserNameWithLockTime]
	-- Add the parameters for the stored procedure here
	@UserName	NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  U.[UserName],
			U.[Status],
			U.[Role],
			U.[LockTime],
			U.[LockedStartDate]
	FROM	[User] U
	WHERE	U.[UserName] = @UserName
END

GO
/****** Object:  StoredProcedure [dbo].[User_GetList]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		HieuBV
-- Create date: 13/12/2016
-- Description:	Tìm kiếm danh sách người dùng
-- =============================================
CREATE PROCEDURE [dbo].[User_GetList]
	-- Add the parameters for the stored procedure here
	@UserName		NVARCHAR(50),
	@PageIndex		INT,
	@PageSize		INT,
	@TotalRecord	INT OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT temp.[UserName],
	       temp.[Password],
		   temp.[Role],
		   temp.[Status],
		   temp.[LockTime],
		   temp.[LockedStartDate]
	FROM 
	(
		SELECT U.[UserName],
	       U.[Password],
		   U.[Role],
		   U.[Status],
		   U.[LockTime],
		   U.[LockedStartDate],
		  ROW_NUMBER() OVER(ORDER BY U.[UserName] DESC)	Row
		FROM   [User] U
		WHERE  (@UserName IS NULL OR @UserName ='' OR U.[UserName] LIKE  @UserName + '%')
	)temp
	WHERE Row BETWEEN	((@PageIndex-1)* @PageSize + 1)	AND	@PageIndex * @PageSize

	 SELECT @TotalRecord = COUNT(U.[UserName])
	FROM   [User] U
	WHERE  (@UserName IS NULL OR @UserName ='' OR U.[UserName] LIKE  @UserName + '%')
END


GO
/****** Object:  StoredProcedure [dbo].[User_Insert]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[User_Insert] 
	-- Add the parameters for the stored procedure here
	@UserName  NVARCHAR(50),
	@Password  NVARCHAR(50),
	@Role 	   INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF ((SELECT COUNT(1) FROM [User] WHERE [UserName] = @UserName) <1) 
	BEGIN
		INSERT INTO [User]([UserName],[Password],[Status],[Role])
		VALUES (@UserName,@Password,1,@Role)
		SELECT 1
	END
	ELSE
		SELECT 0
		
END


GO
/****** Object:  StoredProcedure [dbo].[User_LockTemporary]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 12/12/2016
-- Description:	Khóa tạm thời người dùng
-- =============================================
CREATE PROCEDURE [dbo].[User_LockTemporary]
	-- Add the parameters for the stored procedure here
	@LockTime			INT,
	@LockedStartDate	DATETIME,
	@UserName			NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [User]
	SET	[LockedStartDate] = @LockedStartDate,
		[LockTime]		  = @LockTime
	WHERE [UserName]	  = @UserName		
END

GO
/****** Object:  StoredProcedure [dbo].[User_UnLockTemporary]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 13/12/2016
-- Description:	Mở khóa tạm cho tài khoản
-- =============================================
CREATE PROCEDURE [dbo].[User_UnLockTemporary]
	-- Add the parameters for the stored procedure here
	@UserName		NVARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [User]
	SET		[LockTime]			= NULL,
			[LockedStartDate]	= NULL
	WHERE	[UserName]			= @UserName

	SELECT 1
END

GO
/****** Object:  StoredProcedure [dbo].[User_Update]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[User_Update] 
	-- Add the parameters for the stored procedure here
	@UserName  NVARCHAR(50),
	@Password  NVARCHAR(50),
	@Role 	   INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (@Password IS NULL OR  @Password ='') 
		UPDATE [User] SET [Role]= @Role
		WHERE [UserName] = @UserName
	ELSE
		UPDATE [User] SET [Password] = @Password,[Role]= @Role
		WHERE [UserName] = @UserName
	SELECT 1	
END


GO
/****** Object:  StoredProcedure [dbo].[UserRole_Delete]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 15/05/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UserRole_Delete]
	@Id		INT
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM [UserRole] WHERE [Id] = @Id
END

GO
/****** Object:  StoredProcedure [dbo].[UserRole_GetByUserName]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 15/05/2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[UserRole_GetByUserName] 
	@UserName		NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT  [Id],
			[UserName],
			[RoleId]
    FROM    [UserRole]
	WHERE	[UserName]   = @UserName
END

GO
/****** Object:  StoredProcedure [dbo].[UserRole_Insert]    Script Date: 12/1/2017 7:38:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 15/05/2017
-- Description:	Thêm mới Role
-- =============================================
CREATE PROCEDURE [dbo].[UserRole_Insert]
	@UserName		NVARCHAR(50),
	@Role			INT
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO [UserRole]([UserName],[RoleId])
					VALUES(@UserName ,@Role)
END

GO
