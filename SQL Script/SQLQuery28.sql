USE [BCRMv3]
GO

/****** Object:  Table [dbo].[Groups]    Script Date: 11/10/2017 8:20:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Groups](
	[GroupId] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[GroupName] [nvarchar](50) NOT NULL,
	[GroupAlias] [nvarchar](50) NULL,
	[Description] [nvarchar](2000) NULL,
	[ParentId] [int] NULL,
	[Leader] [int] NULL,
	[Regional] [int] NULL,
	[Order] [int] NULL CONSTRAINT [DF_Groups_Order]  DEFAULT ((0)),
 CONSTRAINT [PK_Groups] PRIMARY KEY CLUSTERED 
(
	[GroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


