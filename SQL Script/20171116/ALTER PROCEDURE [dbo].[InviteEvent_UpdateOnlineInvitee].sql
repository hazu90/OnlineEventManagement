USE [LocalCheckIn]
GO
/****** Object:  StoredProcedure [dbo].[InviteEvent_UpdateOnlineInvitee]    Script Date: 11/16/2017 4:50:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 16/11/2017
-- Description:	Cập nhật
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_UpdateOnlineInvitee]
	@Id						INT,
	@EventId				INT,
	@CustomerName			NVARCHAR(500),
	@IntroduceUser			NVARCHAR(150),
	@CustomerPhone			NVARCHAR(20),
	@CustomerEmail			NVARCHAR(150),
	@CompanyName			NVARCHAR(250),
	@CustomerType			INT,
	@PositionName			NVARCHAR(200),
	@PlaceOfAttendEvent		NVARCHAR(150),
	@PhoningStatus			INT,
	@ConfirmAttend1st		INT,
	@ConfirmAttend2st		INT,
	@Note					NVARCHAR(1000),
	@RegisteredSource		INT,
	@Sessions				NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE [InviteEvent]
	SET	    [EventId]		= @EventId ,
			[CustomerName]  = @CustomerName,
			[IntroduceUser] = @IntroduceUser,
			[CustomerPhone] = @CustomerPhone,
			[CustomerEmail] = @CustomerEmail,
			[PositionName]	= @PositionName,
			[CustomerType]  = @CustomerType,
			[CompanyName]   = @CompanyName,
			[PlaceOfAttendEvent] = @PlaceOfAttendEvent,
			[PhoningStatus] = @PhoningStatus,
			[ConfirmAttend1st] =@ConfirmAttend1st ,
			[ConfirmAttend2st] =@ConfirmAttend2st,
			[Note]			   = @Note,
			[RegisteredSource] = @RegisteredSource,
			[Sessions]		   = @Sessions
	WHERE   [Id]			   = @Id	   
END
