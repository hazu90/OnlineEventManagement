SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		HieuBV
-- Create date: 16/11/2017
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[InviteEvent_GetOnlineRegisterById]
		@Id		INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [Id]
		  ,[CustomerName]
		  ,[CustomerPhone]
		  ,[IsCheckIn]
		  ,[CheckInBy]
		  ,[CheckInDate]
		  ,[Note]
		  ,[CreatedBy]
		  ,[CreatedDate]
		  ,[AccompanyMember]
		  ,[IsNotSuggest]
		  ,[EventId]
		  ,[PositionName]
		  ,[UserName]
		  ,[CustomerType]
		  ,[IsUsingWebsiteBds]
		  ,[PlaceOfAttendEvent]
		  ,[CompanyName]
		  ,[PhoningStatus]
		  ,[RegisteredDate]
		  ,[CustomerEmail]
		  ,[ConfirmAttend1st]
		  ,[ConfirmAttend2st]
		  ,[IntroduceUser]
		  ,[RegisteredSource]
		  ,[ApproveBy]
		  ,[ApproveDate]
		  ,[Status]
		  ,[Code]
		  ,[Sessions]
	FROM   [InviteEvent] IE
	WHERE IE.[Id] = @Id
END
