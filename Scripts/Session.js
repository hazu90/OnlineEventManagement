﻿var session_manage = {
    init: function (wrapContainer) {
        session_manage.action(wrapContainer);
    },
    action: function (wrapContainer) {
        $('button[name=btnAddSession]',wrapContainer).off('click').on('click', function () {
            var tblAddSession = $('table', wrapContainer);
            var html = '<tr>';
            html += '    <td>';
            html += '        <div class="form-group">';
            html += '            <div class="input-group">';
            html += '                <input class="form-control text-input" type="text" name="txtName" style="width: 100%;" />';
            html += '            </div>';
            html += '           <div class="show-message"></div>';
            html += '        </div>';
            html += '    </td>';
            html += '    <td>';
            html += '        <div class="form-group">';
            html += '            <div class="input-group">';
            html += '                <input type="text" class="form-control date" name="txtStartDate" style="width:160px;margin-right: 5px; " /> ';
            html += '                <input type="text" class="form-control" name="txtStartDateTime" value="08:00" style="width:80px;margin-left: 5px; " /> ';
            html += '            </div>';
            html += '           <div class="show-message"></div>';
            html += '        </div>';
            html += '    </td>';
            html += '    <td>';
            html += '       <div class="form-group">';
            html += '           <div class="input-group">';
            html += '               <input type="text" class="form-control date" name="txtEndDate"  style="width:160px;margin-right: 5px; " /> ';
            html += '               <input type="text" class="form-control" name="txtEndDateTime" value="08:00" style="width:80px;margin-left: 5px;" /> ';
            html += '           </div>';
            html += '           <div class="show-message"></div>';
            html += '       </div>';
            html += '    </td>';
            html += '    <td>';
            html += '        <div class="form-group">';
            html += '           <div class="input-group">';
            html += '               <input class="form-control text-input" type="text" name="txtDescription" style="width: 100%;" /> ';
            html += '           </div>';
            html += '           <div class="show-message"></div>';
            html += '       </div>';
            html += '    </td>';
            html += '    <td>';
            html += '        <button name="btnDelete">Xóa</button>';
            html += '        <input type="hidden" name="hddSessionId" value="0" />';
            html += '    </td>';
            html += '</tr>';
            $('tbody', tblAddSession).append(html);
            session_manage.grid_action(wrapContainer);
        });
    },
    grid_action: function (wrapContainer) {
        $('button[name=btnDelete]', wrapContainer).each(function () {
            $(this).off('click').on('click', function () {
                $(this).parent().parent().remove();
            });
        });
        $('input[name=txtStartDate],input[name=txtEndDate]', wrapContainer).each(function () {
            $(this).datepicker();
        });
        $('input[name=txtStartDateTime],input[name=txtEndDateTime]', wrapContainer).each(function () {
            $(this).timepicker({
                minuteStep: 1,
                appendWidgetTo: 'body',
                showSeconds: false,
                showMeridian: false,
                defaultTime: false
            });
        });
    },
    validate: function (wrapContainer) {
        sys_utility.clear_errors(wrapContainer);
        var isValid = true;
        $('input[name=txtName]',wrapContainer).each(function () {
            if(!sys_utility.require_text_box($(this))){
                isValid = false;
                sys_control.show_msg($(this),'Bạn phải nhập tên phiên !',1);
            }
        });
        $('input[name=txtStartDate]',wrapContainer).each(function () {
            if(!sys_utility.require_text_box($(this))){
                isValid = false;
                sys_control.show_msg($(this),'Bạn phải nhập thời gian bắt đầu !' ,1);
            }
        });
        $('input[name=txtStartDateTime]',wrapContainer).each(function () {
            if(!sys_utility.require_text_box($(this))){
                isValid = false;
                sys_control.show_msg($(this),'Bạn phải nhập thời gian bắt đầu !' ,1);
            }
        });
        $('input[name=txtEndDate]',wrapContainer).each(function () {
           if(!sys_utility.require_text_box($(this))){
               isValid = false;
               sys_control.show_msg($(this),'Bạn phải nhập thời gian kết thúc !',1);
           }
        });
        $('input[name=txtEndDateTime]',wrapContainer).each(function () {
            if(!sys_utility.require_text_box($(this))){
                isValid = false;
                sys_control.show_msg($(this),'Bạn phải nhập thời gian kết thúc !',1);
            }
        });
        $('input[name=txtDescription]',wrapContainer).each(function () {
            if(!sys_utility.require_text_box($(this))){
                isValid = false;
                sys_control.show_msg($(this),'Bạn phải nhập thông tin mô tả !',1);
            }
        });
        return isValid;
    },
    has_row: function (wrapContainer) {
        var arrSession = [];
        $('table tbody tr',wrapContainer).each(function () {
            var name = $('input[name=txtName]',$(this)).val();
            var startDate = sys_control.get_val_to_ajax($('input[name=txtStartDate]',$(this))) ;
            var endDate = sys_control.get_val_to_ajax($('input[name=txtEndDate]',$(this))) ;
            var description = sys_control.get_val_to_ajax($('input[name=txtDescription]',$(this)));
            var id = $('input[name=hddSessionId]',$(this)).val();
            arrSession.push({
                Id:id,
                Name : name,
                StartDate : startDate,
                StartTime : $('input[name=txtStartDateTime]',$(this)).val(),
                EndDate : endDate,
                EndTime : $('input[name=txtEndDateTime]',$(this)).val(),
                Description : description
            });
        });

        if(arrSession.length >0){
            return true;
        }
        else{
            return false;
        }
    },
    get_by_eventid: function (eventId,ctrl,divContainer) {
        $.ajax({
            url: "/Session/GetByEventId",
            type: "POST",
            data: {
                eventId:eventId
            },
            dataType: "json",
            async: true,
            beforeSend: function () {

            },
            error: function (jqXHR, textStatus, errorThrown) { },
            success: function (response) {
                if(response.Data != null){
                    if(response.Data.length >0){
                        divContainer.removeClass('hide');
                        var html = '';
                        for(var index = 0 ;index < response.Data.length;index++ ){
                            html +='<option value="'+ response.Data[index].Id + '">'+ response.Data[index].Name +'</option>';
                        }
                        ctrl.html(html);
                        ctrl.trigger('liszt:updated');
                        $(".chzn-select",divContainer).chosen();
                        $(".chzn-select-deselect",divContainer).chosen({allow_single_deselect:true});
                        $(".chzn-choices",divContainer).attr('style','min-height:32px;');
                    }
                    else{
                        divContainer.addClass('hide');
                    }
                }
                else{
                    divContainer.addClass('hide');
                }

            },
            complete: function () {

            }
        });
    }
}