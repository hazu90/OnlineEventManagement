﻿var lucky_number  ={
    init:function(){
        $(document).ready(function () {
            $('#frmRotate').parent().removeClass('hide');
            // Đảo vị trí các con số
            var indexSlot = 1;
            $('.slot').each(function(){
                // Lấy ra số đứng đầu tiên
                //var firstNumber = Math.floor( Math.random() * 10 );
                var firstNumber = 0;
                var html ='';
                var tmp= firstNumber;
                for(var index = 0;index <10 ;index++)
                {
                    html += '<li><span>'+ tmp.toString() +'</span></li>';
                    if(tmp ===0)
                    {
                        tmp = 9;
                    }
                    else
                    {
                        tmp= tmp -1;
                    }
                }
                $(this).html(html);

                var options={
                    number: 1,
                    winnerNumber: 1,
                    spinner: '#btnPlay',
                    spinEvent: 'click',
                    onStart: $.noop,
                    onEnd: $.noop,
                    onWin: $.noop,
                    easing: 'swing',
                    time: 14000000,
                    loops: 12,
                    endNumbers: [],
                    spinningStopper:'#btnStop',
                    slotIndex:indexSlot,
                    firstNumber:firstNumber
                };
                if(indexSlot == 1){
                    options.onStart = function(){
                        $('#slot-spin h1').removeClass('hide');
                        $('#slot-spin .result').addClass('hide');
                        $('#slot-spin .list-item-count2').addClass('hide');
                        $('#slot-spin .list-item-count').removeClass('hide');
                        lucky_number.generate_lucky_number();
                    };
                    options.loops = 12000;
                }
                else if( indexSlot == 5){
                    options.onEnd = function(){
                        $('#slot-spin h1').addClass('hide');
                        $('#slot-spin .result').removeClass('hide');
                        $('#slot-spin .list-item-count2').removeClass('hide');
                        $('#slot-spin .list-item-count').addClass('hide');

                        $('#btnPlay').removeAttr('disabled');
                        $('#btnPlay').removeClass('hide');
                        $('#btnStop').addClass('hide');
                    };
                    options.loops = 8000;
                }
                else{
                    options.loops = 10000;
                }
                indexSlot++;
                $(this).jSlots(options);
            });
        })
    },
    generate_lucky_number:function(){
        var eventId =$('#hiddEventId').val();
        var iWebCheckin = $('#hiddIsWebCheckin').val();
        var bIsInWebCheckin = false;
        if(iWebCheckin == '1')
        {
            bIsInWebCheckin = true;
        }
        else
        {
            bIsInWebCheckin = false;
        }

        $.ajax({
            url: "/LuckyNumber/Generate",
            type: "POST",
            data: {
                isWebCheckin: bIsInWebCheckin,
                eventId: eventId
            },
            dataType: "json",
            async: true,
            beforeSend: function () {
                $('#btnPlay').attr('disabled','disabled');
            },
            error: function (jqXHR, textStatus, errorThrown) { },
            success: function (response) {
                if(response.Code == ResponseCode.Success){
                    $('#btnStop').attr('uid', response.Data.Number);
                    $('#slot-spin .result .name').html('<b> '+response.Data.InviteeName +'</b>');
                    if(response.Data.CityName==null ){
                        response.Data.CityName = '';
                    }
                    if(response.Data.DepartmentName==null){
                        response.Data.DepartmentName='';
                    }
                    if(response.Data.CityName =='' && response.Data.DepartmentName==''){
                        $('#slot-spin .result .department').html('');
                    }
                    else if(response.Data.CityName ==''){
                        $('#slot-spin .result .department').html(' (' +response.Data.DepartmentName +')' );
                    }
                    else if(response.Data.DepartmentName == ''){
                        $('#slot-spin .result .department').html(' (' +response.Data.CityName +')' );
                    }
                    else{
                        $('#slot-spin .result .department').html(' ('+response.Data.CityName +' - <span>'+response.Data.DepartmentName +'</span>)');
                    }

                    var strNumber = response.Data.Number.toString();
                    var iii = 0;
                    $('.list-item-count2 span').each(function () {
                        $(this).html(strNumber[iii]);
                        iii++;
                    });

                    $('#btnStop').removeAttr('disabled');
                    $('#btnStop').removeClass('hide');
                    $('#btnPlay').addClass('hide');
                }
                else if(response.Code == ResponseCode.Error){
                    $('#btnStop').attr('uid', '00000');
                    $('#slot-spin .result .name').html('<b> '+response.Message +'</b>');
                    $('#slot-spin .result .department').html('');
                    $('#btnStop').removeAttr('disabled');
                    $('#btnStop').removeClass('hide');
                    $('#btnPlay').addClass('hide');
                }
                else{
                    $('#btnStop').attr('uid', '00000');
                    $('#slot-spin .result .name').html('<b> '+response.Message +'</b>');
                    $('#slot-spin .result .department').html('');
                    $('#btnStop').removeAttr('disabled');
                    $('#btnStop').removeClass('hide');
                    $('#btnPlay').addClass('hide');
                }
            },
            complete: function () {
            }
        });
    }
};

var lucky_number_manager = {
    register_event:function(){
        $('#btnClose',$('#ShowSlotRotate')).off('click').on('click',function(){
            event_index.display('SearchEvent');
        });
    },
    register_grid_event:function(){
        $('a[name=btnDelete]',$('#ShowSlotRotate')).each(function () {
            $(this).off('click').on('click', function () {
                var id = $(this).attr('uid');

                sysmess.confirm('Bạn có chắc muốn xóa thông tin này !', function () {
                    $.ajax({
                        url: "/LuckyNumber/Delete",
                        type: "POST",
                        data: {
                            Id: id
                        },
                        dataType: "json",
                        async: true,
                        beforeSend: function () {

                        },
                        error: function (jqXHR, textStatus, errorThrown) { },
                        success: function (response) {
                            if(response.Code == ResponseCode.Success)
                            {
                                lucky_number_manager.search( $('#hiddEventId',$('#ShowSlotRotate')).val(),$('#hiddIsInWebCheckin',$('#ShowSlotRotate')).val() );
                                sysmess.success(response.Message, function () {});
                            }
                            else if(response.Code == ResponseCode.Error)
                            {
                                sysmess.error(response.Message, function () {});
                            }
                            else
                            {
                                sysmess.warning(response.Message, function () {});
                            }
                        },
                        complete: function () {
                        }
                    });
                });
            })
        });
    },
    search:function(eventId,iWebCheckin){
        var bIsInWebCheckin = false;
        if(iWebCheckin == '1'){
            bIsInWebCheckin = true;
        }
        else{
            bIsInWebCheckin = false;
        }
        $.ajax({
            url: "/LuckyNumber/Search",
            type: "GET",
            data: {
                eventId: eventId,
                isWebCheckin: bIsInWebCheckin
            },
            dataType: "html",
            async: true,
            beforeSend: function () {

            },
            error: function (jqXHR, textStatus, errorThrown) { },
            success: function (response) {
                $('#ShowSlotRotate').html(response);
                lucky_number_manager.register_event();
                lucky_number_manager.register_grid_event();
            },
            complete: function () {
            }
        });
    }
}