﻿var assign_to_sale = {
    action: function () {
        var frm = $('#wrapAssignToSale');
        $('button[name=btnAssignToSale]',frm).off('click').on('click', function () {
            var assignTo = $('select[name=ddlAssignToSale]',frm).val();
            $.ajax({
                url: "/RegisteredInvitee/AssignToSaleAction",
                type: "json",
                data: {
                    inviteeId : $(this).attr('iid'),
                    assignTo : assignTo
                },
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $("#btnAssignToSale",frm).attr("disabled","disabled");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    sysmess.error('Có lỗi trong quá trình xử lý ! Liên hệ IT để có thông tin chi tiết .',function(){});
                },
                success: function (response) {
                    if(response.Code == ResponseCode.Success){
                        sysmess.success(response.Message,function(){});
                    }
                    else if(response.Code == ResponseCode.Error){
                        sysmess.error(response.Message,function(){});
                    }
                    else{
                        sysmess.warning(response.Message,function(){});
                    }
                },
                complete: function () {
                    $("#btnAssignToSale",frm).removeAttr("disabled");
                }
            });
        });
        $('button[name=btnClose]',frm).off('click').on('click', function () {
            registered_invitee_index.display('SearchRegisteredInvitee');
        });
    }
};