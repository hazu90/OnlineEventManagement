var registered_invitee_index = {
    row_hitting_event:null,
    init:function(){
        $(document).ready(function(){
            registered_invitee_index.register_events();
            registered_invitee_index.search();
            $(".chzn-select",$('#wrapAssignTo')).chosen();
            $(".chzn-select-deselect",$('#wrapAssignTo')).chosen({allow_single_deselect:true});
            $(".chzn-choices",$('#wrapAssignTo')).attr('style','min-height:32px;');
            $("#txtStartRegisterDate",$('#frmSearchRegisteredInvitee')).datepicker();
            $("#txtEndRegisterDate",$('#frmSearchRegisteredInvitee')).datepicker();
            $("#frmSearchRegisteredInvitee").off('keydown');
            $("#frmSearchRegisteredInvitee").keydown(registered_invitee_index.hitting_enter);

            var eventId = $('#selEvent',$('#frmSearchRegisteredInvitee')).val();
            $.ajax({
                url: "/Session/GetByEventId",
                type: "POST",
                data: {
                    eventId:eventId
                },
                dataType: "json",
                async: true,
                beforeSend: function () {

                },
                error: function (jqXHR, textStatus, errorThrown) { },
                success: function (response) {
                    var html = '<option value="0">--Chọn phiên--</option>';
                    if(response.Data != null){
                        if(response.Data.length >0){
                            for(var index = 0 ;index < response.Data.length;index++ ){
                                html +='<option value="'+ response.Data[index].Id + '">'+ response.Data[index].Name +'</option>';
                            }
                            $('#dllSessions',$('#frmSearchRegisteredInvitee')).html(html);
                        }
                        else{
                            $('#dllSessions',$('#frmSearchRegisteredInvitee')).html(html);
                        }
                    }
                    else{
                        $('#dllSessions',$('#frmSearchRegisteredInvitee')).html(html);
                    }
                },
                complete: function () {

                }
            });
        });
    },
    hitting_enter:function(e){
        if(e.which == 13) {
            e.preventDefault(); //stops default action: submitting form
            $(this).blur();
            registered_invitee_index.search();
        }
    },
    register_events:function(){
        var form_search = $('#frmSearchRegisteredInvitee');
        $('#btnAddInvitee',form_search).off('click');
        $('#btnAddInvitee',form_search).on('click',function(){
            registered_invitee_index.display('AddRegisteredInvitee');
            $.ajax({
                url: "/RegisteredInvitee/Create",
                type: "html",
                data: {
                },
                dataType: "html",
                async: true,
                beforeSend: function () { },
                error: function (jqXHR, textStatus, errorThrown) { },
                success: function (response) {
                    $(".panel-body",$("#AddRegisteredInvitee")).html(response);
                    registered_invitee_create.init();
                },
                complete: function () {
                }
            });
        });

        $('#btnSearch',form_search).off('click');
        $('#btnSearch',form_search).on('click',function(){
            registered_invitee_index.search();
        });

        $('#txtInviteeCode',form_search).off('keypress');
        $('#txtInviteeCode',form_search).on('keypress',function(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        });

        $('#txtSearchText',form_search).off('focus');
        $('#txtSearchText',form_search).on('focus',function(){
            $('#txtInviteeCode',form_search).val('');
            $('#txtCode',form_search).val('');
        });

        $('#txtInviteeCode',form_search).off('focus');
        $('#txtInviteeCode',form_search).on('focus',function(){
            $('#txtSearchText',form_search).val('');
            $('#txtCode',form_search).val('');
            $('#txtIntroduceUser',form_search).val('');
        });

        $('#txtIntroduceUser',form_search).off('focus');
        $('#txtIntroduceUser',form_search).on('focus',function(){
            $('#txtInviteeCode',form_search).val('');
            $('#txtCode',form_search).val('');
        });

        $('#txtCode',form_search).off('focus');
        $('#txtCode',form_search).on('focus',function(){
            $('#txtInviteeCode',form_search).val('');
            $('#txtSearchText',form_search).val('');
        });

        $('#selEvent',form_search).off('change').on('change', function () {
            var eventId = $(this).val();
            $.ajax({
                url: "/Session/GetByEventId",
                type: "POST",
                data: {
                    eventId:eventId
                },
                dataType: "json",
                async: true,
                beforeSend: function () {

                },
                error: function (jqXHR, textStatus, errorThrown) { },
                success: function (response) {
                    var html = '<option value="0">--Chọn phiên--</option>';
                    if(response.Data != null){
                        if(response.Data.length >0){
                            for(var index = 0 ;index < response.Data.length;index++ ){
                                html +='<option value="'+ response.Data[index].Id + '">'+ response.Data[index].Name +'</option>';
                            }
                            $('#dllSessions',form_search).html(html);
                        }
                        else{
                            $('#dllSessions',form_search).html(html);
                        }
                    }
                    else{
                        $('#dllSessions',form_search).html(html);
                    }
                },
                complete: function () {

                }
            });
        });

        $('#btnExport',form_search).off('click').on('click', function () {
            $('#hdfSearchStartDate',form_search).val(sys_control.get_val_to_ajax_with_default($('#txtStartRegisterDate',form_search),'') );
            $('#hdfSearchEndDate',form_search).val(sys_control.get_val_to_ajax_with_default($('#txtEndRegisterDate',form_search),'') );
            $('#registeredInviteeForm').submit();
        });
    },
    register_grid_events:function(){
        $('.dropdown-toggle').dropdown();
        var grid_search = $('#tablRegisteredInvitee');
        $('[name=btnEdit]',grid_search).each(function(){
            $(this).off('click');
            $(this).on('click',function(){
                var id = $(this).attr('uid');
                var sourceId = $(this).attr('sid');
                var ibcrm= $(this).attr('ibcrm');
                if(ibcrm == '0'){
                    registered_invitee_edit.old_scroll_position = $(window).scrollTop();
                    $.ajax({
                        url: "/RegisteredInvitee/Edit",
                        type: "GET",
                        data: {
                            id:id
                        },
                        dataType: "html",
                        async: true,
                        beforeSend: function () { },
                        error: function (jqXHR, textStatus, errorThrown) { },
                        success: function (response) {
                            registered_invitee_index.display('AddRegisteredInvitee');
                            $(".panel-body",$("#AddRegisteredInvitee")).html(response);
                            registered_invitee_edit.init();
                        },
                        complete: function () {
                        }
                    });
                }
                else{
                    bcrm_invitee_edit.old_scroll_position = $(window).scrollTop();
                    $.ajax({
                        url: "/RegisteredInvitee/BCRMEdit",
                        type: "GET",
                        data: {
                            id:id
                        },
                        dataType: "html",
                        async: true,
                        beforeSend: function () { },
                        error: function (jqXHR, textStatus, errorThrown) { },
                        success: function (response) {
                            registered_invitee_index.display('AddRegisteredInvitee');
                            $(".panel-body",$("#AddRegisteredInvitee")).html(response);
                            bcrm_invitee_edit.init();
                        },
                        complete: function () {
                        }
                    });
                }

            });
        });
        $('[name=btnApprove]',grid_search).each(function(){
            $(this).off('click');
            $(this).on('click',function(){
                var id = $(this).attr('uid');
                $.ajax({
                    url: "/RegisteredInvitee/Approve",
                    type: "POST",
                    data: {
                        id:id
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () { },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        if(response.Code == ResponseCode.Success)
                        {
                            sysmess.success(response.Message);
                            var pageIndex = 0;
                            $(".dataTables_paginate .active a").each(
                                function () {
                                    if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                        pageIndex = parseInt($(this).html().trim());
                                    }
                                }
                            );
                            registered_invitee_index.search(pageIndex);
                        }
                        else if(response.Code == ResponseCode.Error)
                        {
                            sysmess.error(response.Message);
                        }
                        else
                        {
                            sysmess.warning(response.Message);
                        }
                    },
                    complete: function () {
                    }
                });
            });
        });
        $('[name=btnUnApprove]',grid_search).each(function(){
            $(this).off('click');
            $(this).on('click',function(){
                var id = $(this).attr('uid');
                sysmess.confirm('Bạn có muốn hủy duyệt thông tin này !',function(){
                    $.ajax({
                        url: "/RegisteredInvitee/UnApprove",
                        type: "POST",
                        data: {
                            id:id
                        },
                        dataType: "json",
                        async: true,
                        beforeSend: function () { },
                        error: function (jqXHR, textStatus, errorThrown) { },
                        success: function (response) {
                            if(response.Code == ResponseCode.Success)
                            {
                                sysmess.success(response.Message);
                                var pageIndex = 0;
                                $(".dataTables_paginate .active a").each(
                                    function () {
                                        if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                            pageIndex = parseInt($(this).html().trim());
                                        }
                                    }
                                );
                                registered_invitee_index.search(pageIndex);
                            }
                            else if(response.Code == ResponseCode.Error)
                            {
                                sysmess.error(response.Message);
                            }
                            else
                            {
                                sysmess.warning(response.Message);
                            }
                        },
                        complete: function () {
                        }
                    });
                });
            });
        });
        $('[name=btnDelete]',grid_search).each(function () {
            $(this).off('click');
            $(this).on('click',function(){
                var id = $(this).attr('uid');
                sysmess.confirm('Bạn có muốn xóa thông tin này !',function(){
                    $.ajax({
                        url: "/RegisteredInvitee/DeleteAction",
                        type: "POST",
                        data: {
                            id:id
                        },
                        dataType: "json",
                        async: true,
                        beforeSend: function () { },
                        error: function (jqXHR, textStatus, errorThrown) { },
                        success: function (response) {
                            if(response.Code == ResponseCode.Success)
                            {
                                sysmess.success(response.Message);
                                var pageIndex = 0;
                                $(".dataTables_paginate .active a").each(
                                    function () {
                                        if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                            pageIndex = parseInt($(this).html().trim());
                                        }
                                    }
                                );
                                registered_invitee_index.search(pageIndex);
                            }
                            else if(response.Code == ResponseCode.Error)
                            {
                                sysmess.error(response.Message);
                            }
                            else
                            {
                                sysmess.warning(response.Message);
                            }
                        },
                        complete: function () {
                        }
                    });
                });
            });
        });
        $('[name=btnAssignTo]',grid_search).each(function () {
            $(this).off('click').on('click', function () {
                registered_invitee_index.row_hitting_event = $(this).parent().parent().parent().parent().parent();
                $('#wrapAssignTo').removeClass('hide');
                var inviteeId = $(this).attr('uid');
                show_dialog_checkin.show($('#frmAssignTo'),function(){
                    var frmAssignTo = $('#frmAssignTo');
                    $('#wrapShowErrAssignTo',frmAssignTo).addClass('hide');
                    $('#btnSaveAssignTo',frmAssignTo).attr('uid',inviteeId);
                    $.ajax({
                        url: "/RegisteredInvitee/GetAssignTo",
                        type: "POST",
                        data: {
                            inviteeId:inviteeId
                        },
                        dataType: "json",
                        async: true,
                        beforeSend: function () { },
                        error: function (jqXHR, textStatus, errorThrown) { },
                        success: function (response) {
                            if(response.Data != null && response.Data != ''){
                                $('#selAssignTo option[value='+response.Data +']',frmAssignTo).attr('selected','selected');
                            }
                            else{
                                $('#selAssignTo',frmAssignTo).val('');
                            }
                            $('#selAssignTo',frmAssignTo).trigger('liszt:updated');
                        },
                        complete: function () {

                        }
                    });

                    $('#btnSaveAssignTo',frmAssignTo).off('click').on('click', function () {
                        var is_valid = true;
                        if(!sys_utility.require_select($('#selAssignTo',frmAssignTo))){
                            sys_control.show_msg($('#selAssignTo',frmAssignTo),'Bạn phải chọn người cần giao !',1);
                            is_valid = false;
                        }

                        if(is_valid){
                            var inviteeId = $(this).attr('uid');
                            var strAssign=sys_control.get_val_to_ajax($('#selAssignTo',frmAssignTo));
                            $.ajax({
                                url: "/RegisteredInvitee/AssignTo",
                                type: "POST",
                                data: {
                                    inviteeId:inviteeId,
                                    assignTo:strAssign
                                },
                                dataType: "json",
                                async: true,
                                beforeSend: function () {
                                    $('#btnSaveAssignTo',frmAssignTo).attr('disabled','disabled');
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    $('#btnSaveAssignTo',frmAssignTo).removeAttr('disabled');
                                },
                                success: function (response) {
                                    $('#btnCancelAssignTo',frmAssignTo).trigger('click');
                                    if(response.Code == ResponseCode.Success){
                                        sysmess.success(response.Message);
                                        $('[name=divShowAssignTo]',registered_invitee_index.row_hitting_event).html('<a href="javascript:void(0);">Giao cho :</a> '+ response.Data);
                                    }
                                    else if(response.Code == ResponseCode.Error){
                                        sysmess.error(response.Message);
                                    }
                                    else{
                                        sysmess.warning(response.Message);
                                    }
                                },
                                complete: function () {
                                    $('#btnSaveAssignTo',frmAssignTo).removeAttr('disabled');
                                }
                            });
                        }
                    });
                    $('a[name=lnkAssignTo]',frmAssignTo).each(function () {
                        $(this).off('click').on('click', function () {
                            var pr = $(this).parent();
                            if($('input[name=grAssignTo]',pr).attr('checked')=='checked'){
                                $('input[name=grAssignTo]',pr).removeAttr('checked');
                            }
                            else{
                                $('input[name=grAssignTo]',pr).attr('checked','checked');
                            }
                        });
                    });
                });
            });
        });
        $('a[name=btnAssignToSale]',grid_search).each(function () {
            $(this).off('click').on('click', function () {
                var eventId = $(this).attr('uid');
                $.ajax({
                    url: "/RegisteredInvitee/AssignToSale",
                    type: "GET",
                    data: {
                        inviteeId:eventId
                    },
                    dataType: "html",
                    async: true,
                    beforeSend: function () { },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        $(".panel-body",$("#wrapAssignToSale")).html(response);
                        registered_invitee_index.display('wrapAssignToSale');
                        invitee_assign_to_sale.init();
                    },
                    complete: function () {}
                });
            });
        });
        $('a[name=btnReSendTicket]',grid_search).each(function () {
            $(this).off('click').on('click', function () {
                var id = $(this).attr('uid');
                sysmess.confirm('Bạn có muốn gửi lại vé cho khách mời này !', function () {
                    $.ajax({
                        url: '/RegisteredInvitee/ResendTicket',
                        type: "POST",
                        data: {
                            id:id
                        },
                        dataType: "json",
                        async: true,
                        beforeSend: function () {},
                        error: function (jqXHR, textStatus, errorThrown) {
                            sysmess.error("Có lỗi trong quá trình xử lý", function () {});
                        },
                        success: function (response) {
                            if(response.Code == ResponseCode.Success){
                                sysmess.success(response.Message, function () {});
                                var pageIndex = 0;
                                $(".dataTables_paginate .active a").each(
                                    function () {
                                        if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                            pageIndex = parseInt($(this).html().trim());
                                        }
                                    }
                                );
                                registered_invitee_index.search(pageIndex);
                            }
                            else if(response.Code == ResponseCode.Error){
                                sysmess.error(response.Message, function () {});
                            }
                            else{
                                sysmess.warning(response.Message, function () {});
                            }
                        },
                        complete: function () {}
                    });
                });
            });
        });
        $('a[name=btnSendTicket]',grid_search).each(function () {
            $(this).off('click').on('click', function () {
                var id = $(this).attr('uid');
                sysmess.confirm('Bạn có muốn gửi vé cho khách mời này !', function () {
                    $.ajax({
                        url: '/RegisteredInvitee/SendTicket',
                        type: "POST",
                        data: {
                            id:id
                        },
                        dataType: "json",
                        async: true,
                        beforeSend: function () {},
                        error: function (jqXHR, textStatus, errorThrown) {
                            sysmess.error("Có lỗi trong quá trình xử lý", function () {});
                        },
                        success: function (response) {
                            if(response.Code == ResponseCode.Success){
                                sysmess.success(response.Message, function () {});
                                var pageIndex = 0;
                                $(".dataTables_paginate .active a").each(
                                    function () {
                                        if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                            pageIndex = parseInt($(this).html().trim());
                                        }
                                    }
                                );
                                registered_invitee_index.search(pageIndex);
                            }
                            else if(response.Code == ResponseCode.Error){
                                sysmess.error(response.Message, function () {});
                            }
                            else{
                                sysmess.warning(response.Message, function () {});
                            }
                        },
                        complete: function () {}
                    });
                });
            });
        });
    },
    display:function(divId){
        if(divId == 'SearchRegisteredInvitee'){
            if($('#SearchRegisteredInvitee').hasClass('hide')){
                $('#SearchRegisteredInvitee').removeClass('hide');
            }
        }
        else{
            if(!$('#SearchRegisteredInvitee').hasClass('hide')){
                $('#SearchRegisteredInvitee').addClass('hide');
            }
        }
        if(divId == 'AddRegisteredInvitee'){
            if($('#AddRegisteredInvitee').hasClass('hide')){
                $('#AddRegisteredInvitee').removeClass('hide');
            }
        }
        else{
            if(!$('#AddRegisteredInvitee').hasClass('hide')){
                $('#AddRegisteredInvitee').addClass('hide');
            }
        }
        if(divId == 'wrapAssignToSale'){
            if($('#wrapAssignToSale').hasClass('hide')){
                $('#wrapAssignToSale').removeClass('hide');
            }
        }
        else{
            if(!$('#wrapAssignToSale').hasClass('hide')){
                $('#wrapAssignToSale').addClass('hide');
            }
        }

    },
    search:function(page){
        registered_invitee_index.display('SearchRegisteredInvitee');
        var form_search = $('#frmSearchRegisteredInvitee');
        var searchText = $("#txtSearchText", form_search).val();
        var selectedEvent = $("#selEvent", form_search).val();
        var inviteeCode = $("#txtInviteeCode",form_search).val();
        var introduceUser = $('#txtIntroduceUser',form_search).val();
        var sourceId = '';
        if($('#selCustomerType',form_search).val() ==null || $('#selCustomerType',form_search).val() ==''){
            sourceId = 0;
        }
        else{
            sourceId = $('#selCustomerType',form_search).val();
        }

        var inviteeResponseStatus = 0;
        if($('#selIsCheckIn',form_search).val() =='' || $('#selIsCheckIn',form_search).val() ==null){
            inviteeResponseStatus = 0;
        }
        else{
            inviteeResponseStatus =$('#selIsCheckIn',form_search).val();
        }

        if(typeof page=='undefined'|| page == null){
            page=1;
        }
        var code = $('#txtCode',form_search).val();
        if(code !=null && code !=''){
            code = code.trim();
        }

        $.ajax({
            url: "/RegisteredInvitee/Search",
            type: "GET",
            data: {
                TextSearch:searchText,
                Code:code,
                EventId:selectedEvent,
                SourceId:sourceId,
                InviteeResponseStatus:inviteeResponseStatus,
                InviteeCode:inviteeCode,
                IntroduceUser:introduceUser,
                AssignTo:$('#selAssignTo',form_search).val(),
                SessionId : $('#dllSessions',form_search).val(),
                StartDate:sys_control.get_val_to_ajax_with_default($('#txtStartRegisterDate',form_search),''),
                EndDate:sys_control.get_val_to_ajax_with_default($('#txtEndRegisterDate',form_search),''),
                FilterAssignTo:sys_control.get_val_to_ajax($('#selFilterAssignTo',form_search)),
                PageIndex: page,
                PageSize: 25
            },
            dataType: "html",
            async: true,
            beforeSend: function () { },
            error: function (jqXHR, textStatus, errorThrown) { },
            success: function (response) {
                $(".panel-body",$("#SearchRegisteredInvitee")).html(response);
                registered_invitee_index.register_grid_events();
            },
            complete: function () {

            }
        });
    }
};
var registered_invitee_create={
    init:function(){
        var form_create = $('#frmAddRegisteredInvitee');
        session_manage.get_by_eventid($('#ddlEventId',form_create).val(),$('#dllSession',form_create),$('div[name=IsHadSession]'));
        registered_invitee_create.register_event();
    },
    register_event:function(){
        var form_create = $('#frmAddRegisteredInvitee');
        $('#ddlEventId',form_create).off('change').on('change', function () {
            session_manage.get_by_eventid($('#ddlEventId',form_create).val(),$('#dllSession',form_create),$('div[name=IsHadSession]'));
        });

        $('#btnSave',form_create).off('click');
        $('#btnSave',form_create).on('click',function(){
            sys_utility.clear_errors(form_create);
            if(registered_invitee_create.validate()){
                var ddlEventIdV= $('#ddlEventId',form_create).val();
                var txtCustomerNameV = $('#txtCustomerName',form_create).val();
                var txtIntroduceUserV = $('#txtIntroduceUser',form_create).val();
                var txtCustomerPhoneV = $('#txtCustomerPhone',form_create).val();
                var txtCustomerEmailV = $('#txtCustomerEmail',form_create).val();
                var txtCompanyNameV = $('#txtCompanyName',form_create).val();
                var dllCustomerTypeV = $('#dllCustomerType',form_create).val();
                var dllPlaceOfAttendEventV = $('#dllPlaceOfAttendEvent',form_create).val();
                var dllRegisteredSourceV = $('#dllRegisteredSource',form_create).val();
                var dllPhoningStatusV = $('#dllPhoningStatus',form_create).val();
                var dllConfirmAttend1stV = $('#dllConfirmAttend1st',form_create).val();
                var dllConfirmAttend2stV = $('#dllConfirmAttend2st',form_create).val();
                var txtNoteV = $('#txtNote',form_create).val();
                var txtPositionNameV = $('#txtPositionName',form_create).val();
                var sessions = '';
                if(!$('div[name=IsHadSession]',form_create).hasClass('hide') ){
                    sessions = sys_control.get_val_to_ajax($('#dllSession',form_create));
                }

                $.ajax({
                    url: "/RegisteredInvitee/CreateAction",
                    type: "json",
                    data: {
                        EventId      :ddlEventIdV,
                        CustomerName : txtCustomerNameV,
                        IntroduceUser : txtIntroduceUserV,
                        CustomerPhone : txtCustomerPhoneV,
                        CustomerEmail : txtCustomerEmailV,
                        CompanyName : txtCompanyNameV,
                        CustomerType : dllCustomerTypeV,
                        PositionName:txtPositionNameV,
                        PlaceOfAttendEvent:dllPlaceOfAttendEventV,
                        PhoningStatus:dllPhoningStatusV,
                        ConfirmAttend1st : dllConfirmAttend1stV,
                        ConfirmAttend2st : dllConfirmAttend2stV,
                        Note : txtNoteV,
                        RegisteredSource:dllRegisteredSourceV,
                        Sessions:sessions
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $('#btnSave',$('#frmAddRegisteredInvitee')).attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        if(response.Code == ResponseCode.Success){
                            sysmess.success(response.Message);
                            registered_invitee_index.display('SearchRegisteredInvitee');
                            registered_invitee_index.search();
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    },
                    complete: function () {
                        $('#btnSave',$('#frmAddRegisteredInvitee')).removeAttr("disabled");
                    }
                });
            }
        });

        $('#btnClose',form_create).off('click');
        $('#btnClose',form_create).on('click',function(){
            registered_invitee_index.display('SearchRegisteredInvitee');
        });

        $('#btnCancel',form_create).off('click');
        $('#btnCancel',form_create).on('click',function(){
            registered_invitee_create.reset();
        });

        $('#txtCustomerPhone',form_create).off('keypress');
        $('#txtCustomerPhone',form_create).on('keypress',function(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        });

        $('#btnApprove',form_create).off('click');
        $('#btnApprove',form_create).on('click',function(){
            sys_utility.clear_errors(form_create);
            if(registered_invitee_create.validate()){
                var ddlEventIdV= $('#ddlEventId',form_create).val();
                var txtCustomerNameV = $('#txtCustomerName',form_create).val();
                var txtIntroduceUserV = $('#txtIntroduceUser',form_create).val();
                var txtCustomerPhoneV = $('#txtCustomerPhone',form_create).val();
                var txtCustomerEmailV = $('#txtCustomerEmail',form_create).val();
                var txtCompanyNameV = $('#txtCompanyName',form_create).val();
                var dllCustomerTypeV = $('#dllCustomerType',form_create).val();
                var dllPlaceOfAttendEventV = $('#dllPlaceOfAttendEvent',form_create).val();
                var dllRegisteredSourceV = $('#dllRegisteredSource',form_create).val();
                var dllPhoningStatusV = $('#dllPhoningStatus',form_create).val();
                var dllConfirmAttend1stV = $('#dllConfirmAttend1st',form_create).val();
                var dllConfirmAttend2stV = $('#dllConfirmAttend2st',form_create).val();
                var txtNoteV = $('#txtNote',form_create).val();
                var txtPositionNameV = $('#txtPositionName',form_create).val();

                var sessions = '';
                if(!$('div[name=IsHadSession]',form_create).hasClass('hide') ){
                    sessions = sys_control.get_val_to_ajax($('#dllSession',form_create));
                }
                $.ajax({
                    url: "/RegisteredInvitee/CreateAndApproveAction",
                    type: "json",
                    data: {
                        EventId      :ddlEventIdV,
                        CustomerName : txtCustomerNameV,
                        IntroduceUser : txtIntroduceUserV,
                        CustomerPhone : txtCustomerPhoneV,
                        CustomerEmail : txtCustomerEmailV,
                        CompanyName : txtCompanyNameV,
                        CustomerType : dllCustomerTypeV,
                        PositionName:txtPositionNameV,
                        PlaceOfAttendEvent:dllPlaceOfAttendEventV,
                        PhoningStatus:dllPhoningStatusV,
                        ConfirmAttend1st : dllConfirmAttend1stV,
                        ConfirmAttend2st : dllConfirmAttend2stV,
                        Note : txtNoteV,
                        RegisteredSource:dllRegisteredSourceV,
                        Sessions:sessions
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $('#btnSave',$('#frmAddRegisteredInvitee')).attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        if(response.Code == ResponseCode.Success){
                            sysmess.success(response.Message);
                            registered_invitee_index.display('SearchRegisteredInvitee');
                            registered_invitee_index.search();
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    },
                    complete: function () {
                        $('#btnSave',$('#frmAddRegisteredInvitee')).removeAttr("disabled");
                    }
                });
            }
        });
    },
    validate:function(){
        var form_create = $('#frmAddRegisteredInvitee');
        var result = true;
        if(!sys_utility.require_select($('#ddlEventId',form_create))){
            sys_control.show_msg($('#ddlEventId',form_create),'Sự kiện bắt buộc nhập !',1);
            result = false;
        }

        if(!sys_utility.require_text_box($('#txtCustomerName',form_create))){
            sys_control.show_msg($('#txtCustomerName',form_create),'Tên khách mời bắt buộc nhập !',1);
            result = false;
        }
        if(!sys_utility.require_text_box($('#txtCustomerPhone',form_create))){
            sys_control.show_msg($('#txtCustomerPhone',form_create),'Số điện thoại bắt buộc nhập !',1);
            result = false;
        }
        if(!sys_utility.require_text_box($('#txtCustomerEmail',form_create))){
            sys_control.show_msg($('#txtCustomerEmail',form_create),'Email bắt buộc nhập !',1);
            result = false;
        }

        if(!$('div[name=IsHadSession]',form_create).hasClass('hide') && !sys_utility.require_select($('#dllSession',form_create)) ){
            sys_control.show_msg($('#dllSession',form_create),'Phiên tham dự bắt buộc nhập !',1);
            result = false;
        }

        return result;
    },
    reset:function(){
        var form_create = $('#frmAddRegisteredInvitee');
        $('#txtCustomerName',form_create).val('');
        $('#txtIntroduceUser',form_create).val('');
        $('#txtCustomerPhone',form_create).val('');
        $('#txtCustomerEmail',form_create).val('');
        $('#txtCompanyName',form_create).val('');
        // Lựa chọn
        $('#dllCustomerType option',form_create).val(1);
        $('#txtPositionName',form_create).val('');
        var htmlPlaceOfAttendEvent =  $('#dllPlaceOfAttendEvent',form_create).html();
        $('#dllPlaceOfAttendEvent',form_create).html(htmlPlaceOfAttendEvent);
        $('#dllRegisteredSource',form_create).val(4);
        $('#dllPhoningStatus',form_create).val(1);
        $('#dllConfirmAttend1st',form_create).val(1);
        $('#dllConfirmAttend2st',form_create).val(1);
        $('#txtNote',form_create).val('');
    }
};
var registered_invitee_edit={
    old_scroll_position:0,
    init:function(){
        $(".chzn-select",$('#frmEditRegisteredInvitee')).chosen();
        $(".chzn-select-deselect",$('#frmEditRegisteredInvitee')).chosen({allow_single_deselect:true});
        $(".chzn-choices",$('#frmEditRegisteredInvitee')).attr('style','min-height:32px;');
        registered_invitee_edit.register_event();
    },
    register_event:function(){
        var form_create = $('#frmEditRegisteredInvitee');

        $('#ddlEventId',form_create).off('change').on('change', function () {
            session_manage.get_by_eventid($('#ddlEventId',form_create).val(),$('#dllSession',form_create),$('div[name=IsHadSession]'));
        });

        $('#btnSave',form_create).off('click');
        $('#btnSave',form_create).on('click',function(){
            sys_utility.clear_errors(form_create);
            if(registered_invitee_edit.validate()){
                var ddlEventIdV= $('#ddlEventId',form_create).val();
                var txtCustomerNameV = $('#txtCustomerName',form_create).val();
                var txtIntroduceUserV = $('#txtIntroduceUser',form_create).val();
                var txtCustomerPhoneV = $('#txtCustomerPhone',form_create).val();
                var txtCustomerEmailV = $('#txtCustomerEmail',form_create).val();
                var txtCompanyNameV = $('#txtCompanyName',form_create).val();
                var dllCustomerTypeV = $('#dllCustomerType',form_create).val();
                var dllPlaceOfAttendEventV = $('#dllPlaceOfAttendEvent',form_create).val();
                var dllRegisteredSourceV = $('#dllRegisteredSource',form_create).val();
                var dllPhoningStatusV = $('#dllPhoningStatus',form_create).val();
                var dllConfirmAttend1stV = $('#dllConfirmAttend1st',form_create).val();
                var dllConfirmAttend2stV = $('#dllConfirmAttend2st',form_create).val();
                var txtNoteV = $('#txtNote',form_create).val();
                var txtPositionNameV = $('#txtPositionName',form_create).val();
                var hiddIdV =   $('#hiddId',form_create).val();

                var sessions = '';
                if(!$('div[name=IsHadSession]',form_create).hasClass('hide') ){
                    sessions = sys_control.get_val_to_ajax($('#dllSession',form_create));
                }
                $.ajax({
                    url: "/RegisteredInvitee/EditAction",
                    type: "json",
                    data: {
                        EventId      :ddlEventIdV,
                        CustomerName : txtCustomerNameV,
                        IntroduceUser : txtIntroduceUserV,
                        CustomerPhone : txtCustomerPhoneV,
                        CustomerEmail : txtCustomerEmailV,
                        CompanyName : txtCompanyNameV,
                        CustomerType : dllCustomerTypeV,
                        PositionName:txtPositionNameV,
                        PlaceOfAttendEvent:dllPlaceOfAttendEventV,
                        PhoningStatus:dllPhoningStatusV,
                        ConfirmAttend1st : dllConfirmAttend1stV,
                        ConfirmAttend2st : dllConfirmAttend2stV,
                        Note : txtNoteV,
                        RegisteredSource:dllRegisteredSourceV,
                        Id:hiddIdV,
                        Sessions:sessions
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $('#btnSave',$('#frmEditRegisteredInvitee')).attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        if(response.Code == ResponseCode.Success)
                        {
                            sysmess.success(response.Message);
                            registered_invitee_index.display('SearchRegisteredInvitee');
                            var pageIndex = 0;
                            $(".dataTables_paginate .active a").each(
                                function () {
                                    if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                        pageIndex = parseInt($(this).html().trim());
                                    }
                                }
                            );
                            registered_invitee_index.search(pageIndex);
                            $(window).scrollTop(registered_invitee_edit.old_scroll_position);
                        }
                        else if(response.Code == ResponseCode.Error)
                        {
                            sysmess.error(response.Message);
                        }
                        else
                        {
                            sysmess.warning(response.Message);
                        }
                    },
                    complete: function () {
                        $('#btnSave',$('#frmEditRegisteredInvitee')).removeAttr("disabled");
                    }
                });
            }
        });

        $('#btnClose',form_create).off('click');
        $('#btnClose',form_create).on('click',function(){
            registered_invitee_index.display('SearchRegisteredInvitee');
        });

        $('#btnCancel',form_create).off('click');
        $('#btnCancel',form_create).on('click',function(){
            var id = $('#hiddId',form_create).val();
            $.ajax({
                url: "/RegisteredInvitee/Edit",
                type: "html",
                data: {
                    id:id
                },
                dataType: "html",
                async: true,
                beforeSend: function () { },
                error: function (jqXHR, textStatus, errorThrown) { },
                success: function (response) {
                    registered_invitee_index.display('AddRegisteredInvitee');
                    $(".panel-body",$("#AddRegisteredInvitee")).html(response);
                    registered_invitee_edit.init();
                },
                complete: function () {
                }
            });
        });

        $('#txtCustomerPhone',form_create).off('keypress');
        $('#txtCustomerPhone',form_create).on('keypress',function(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        });

        $('#btnApprove',form_create).off('click');
        $('#btnApprove',form_create).on('click',function(){
            if(registered_invitee_edit.validate()){
                var ddlEventIdV= $('#ddlEventId',form_create).val();
                var txtCustomerNameV = $('#txtCustomerName',form_create).val();
                var txtIntroduceUserV = $('#txtIntroduceUser',form_create).val();
                var txtCustomerPhoneV = $('#txtCustomerPhone',form_create).val();
                var txtCustomerEmailV = $('#txtCustomerEmail',form_create).val();
                var txtCompanyNameV = $('#txtCompanyName',form_create).val();
                var dllCustomerTypeV = $('#dllCustomerType',form_create).val();
                var dllPlaceOfAttendEventV = $('#dllPlaceOfAttendEvent',form_create).val();
                var dllRegisteredSourceV = $('#dllRegisteredSource',form_create).val();
                var dllPhoningStatusV = $('#dllPhoningStatus',form_create).val();
                var dllConfirmAttend1stV = $('#dllConfirmAttend1st',form_create).val();
                var dllConfirmAttend2stV = $('#dllConfirmAttend2st',form_create).val();
                var txtNoteV = $('#txtNote',form_create).val();
                var txtPositionNameV = $('#txtPositionName',form_create).val();
                var hiddIdV =   $('#hiddId',form_create).val();
                var sessions = '';
                if(!$('div[name=IsHadSession]',form_create).hasClass('hide') ){
                    sessions = sys_control.get_val_to_ajax($('#dllSession',form_create));
                }
                $.ajax({
                    url: "/RegisteredInvitee/EditAndApproveAction",
                    type: "json",
                    data: {
                        EventId      :ddlEventIdV,
                        CustomerName : txtCustomerNameV,
                        IntroduceUser : txtIntroduceUserV,
                        CustomerPhone : txtCustomerPhoneV,
                        CustomerEmail : txtCustomerEmailV,
                        CompanyName : txtCompanyNameV,
                        CustomerType : dllCustomerTypeV,
                        PositionName:txtPositionNameV,
                        PlaceOfAttendEvent:dllPlaceOfAttendEventV,
                        PhoningStatus:dllPhoningStatusV,
                        ConfirmAttend1st : dllConfirmAttend1stV,
                        ConfirmAttend2st : dllConfirmAttend2stV,
                        Note : txtNoteV,
                        RegisteredSource:dllRegisteredSourceV,
                        Id:hiddIdV,
                        Sessions:sessions
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $('#btnApprove',$('#frmEditRegisteredInvitee')).attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        if(response.Code == ResponseCode.Success)
                        {
                            sysmess.success(response.Message);
                            registered_invitee_index.display('SearchRegisteredInvitee');
                            var pageIndex = 0;
                            $(".dataTables_paginate .active a").each(
                                function () {
                                    if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                        pageIndex = parseInt($(this).html().trim());
                                    }
                                }
                            );
                            registered_invitee_index.search(pageIndex);
                            $(window).scrollTop(registered_invitee_edit.old_scroll_position);
                        }
                        else if(response.Code == ResponseCode.Error)
                        {
                            sysmess.error(response.Message);
                        }
                        else
                        {
                            sysmess.warning(response.Message);
                        }
                    },
                    complete: function () {
                        $('#btnApprove',$('#frmEditRegisteredInvitee')).removeAttr("disabled");
                    }
                });
            }
        });

        $('#btnUnApprove',form_create).off('click');
        $('#btnUnApprove',form_create).on('click',function(){
            if(registered_invitee_edit.validate())
            {
                var ddlEventIdV= $('#ddlEventId',form_create).val();
                var txtCustomerNameV = $('#txtCustomerName',form_create).val();
                var txtIntroduceUserV = $('#txtIntroduceUser',form_create).val();
                var txtCustomerPhoneV = $('#txtCustomerPhone',form_create).val();
                var txtCustomerEmailV = $('#txtCustomerEmail',form_create).val();
                var txtCompanyNameV = $('#txtCompanyName',form_create).val();
                var dllCustomerTypeV = $('#dllCustomerType',form_create).val();
                var dllPlaceOfAttendEventV = $('#dllPlaceOfAttendEvent',form_create).val();
                var dllRegisteredSourceV = $('#dllRegisteredSource',form_create).val();
                var dllPhoningStatusV = $('#dllPhoningStatus',form_create).val();
                var dllConfirmAttend1stV = $('#dllConfirmAttend1st',form_create).val();
                var dllConfirmAttend2stV = $('#dllConfirmAttend2st',form_create).val();
                var txtNoteV = $('#txtNote',form_create).val();
                var txtPositionNameV = $('#txtPositionName',form_create).val();
                var hiddIdV =   $('#hiddId',form_create).val();
                var sessions = '';
                if(!$('div[name=IsHadSession]',form_create).hasClass('hide') ){
                    sessions = sys_control.get_val_to_ajax($('#dllSession',form_create));
                }
                $.ajax({
                    url: "/RegisteredInvitee/EditAndUnApproveAction",
                    type: "json",
                    data: {
                        EventId      :ddlEventIdV,
                        CustomerName : txtCustomerNameV,
                        IntroduceUser : txtIntroduceUserV,
                        CustomerPhone : txtCustomerPhoneV,
                        CustomerEmail : txtCustomerEmailV,
                        CompanyName : txtCompanyNameV,
                        CustomerType : dllCustomerTypeV,
                        PositionName:txtPositionNameV,
                        PlaceOfAttendEvent:dllPlaceOfAttendEventV,
                        PhoningStatus:dllPhoningStatusV,
                        ConfirmAttend1st : dllConfirmAttend1stV,
                        ConfirmAttend2st : dllConfirmAttend2stV,
                        Note : txtNoteV,
                        RegisteredSource:dllRegisteredSourceV,
                        Id:hiddIdV,
                        Sessions:sessions
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $('#btnUnApprove',$('#frmEditRegisteredInvitee')).attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        if(response.Code == ResponseCode.Success)
                        {
                            sysmess.success(response.Message);
                            registered_invitee_index.display('SearchRegisteredInvitee');
                            var pageIndex = 0;
                            $(".dataTables_paginate .active a").each(
                                function () {
                                    if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                        pageIndex = parseInt($(this).html().trim());
                                    }
                                }
                            );
                            registered_invitee_index.search(pageIndex);
                            $(window).scrollTop(registered_invitee_edit.old_scroll_position);
                        }
                        else if(response.Code == ResponseCode.Error)
                        {
                            sysmess.error(response.Message);
                        }
                        else
                        {
                            sysmess.warning(response.Message);
                        }
                    },
                    complete: function () {
                        $('#btnUnApprove',$('#frmEditRegisteredInvitee')).removeAttr("disabled");
                    }
                });
            }
        });

        $('#toogleHistory').off('click');
        $('#toogleHistory').on('click',function(){
            if($('span',this).html() =='+' )
            {
                $('span',this).html('-');
                if($('#tblHistory').hasClass('hide') )
                {
                    $('#tblHistory').removeClass('hide');
                }
            }
            else
            {
                $('span',this).html('+');
                if(!$('#tblHistory').hasClass('hide') )
                {
                    $('#tblHistory').addClass('hide');
                }
            }
        });
    },
    validate:function(){
        var form_create = $('#frmEditRegisteredInvitee');
        var result = true;
        if(!sys_utility.require_select($('#ddlEventId',form_create))){
            sys_control.show_msg($('#ddlEventId',form_create),'Sự kiện bắt buộc nhập !',1);
            result = false;
        }

        if(!sys_utility.require_text_box($('#txtCustomerName',form_create))){
            sys_control.show_msg($('#txtCustomerName',form_create),'Tên khách mời bắt buộc nhập !',1);
            result = false;
        }
        if(!sys_utility.require_text_box($('#txtCustomerPhone',form_create))){
            sys_control.show_msg($('#txtCustomerPhone',form_create),'Số điện thoại bắt buộc nhập !',1);
            result = false;
        }
        if(!sys_utility.require_text_box($('#txtCustomerEmail',form_create))){
            sys_control.show_msg($('#txtCustomerEmail',form_create),'Email bắt buộc nhập !',1);
            result = false;
        }

        if(!$('div[name=IsHadSession]',form_create).hasClass('hide') && !sys_utility.require_select($('#dllSession',form_create)) ){
            sys_control.show_msg($('#dllSession',form_create),'Phiên tham dự bắt buộc nhập !',1);
            result = false;
        }
        return result;
    },
    reset:function(){
        var form_create = $('#frmEditRegisteredInvitee');
        $('#txtCustomerName',form_create).val('');
        $('#txtIntroduceUser',form_create).val('');
        $('#txtCustomerPhone',form_create).val('');
        $('#txtCustomerEmail',form_create).val('');
        $('#txtCompanyName',form_create).val('');
        // Lựa chọn
        $('#dllCustomerType option',form_create).val(1);
        $('#txtPositionName',form_create).val('');
        var htmlPlaceOfAttendEvent =  $('#dllPlaceOfAttendEvent',form_create).html();
        $('#dllPlaceOfAttendEvent',form_create).html(htmlPlaceOfAttendEvent);

        $('#dllRegisteredSource',form_create).val('4');
        $('#dllPhoningStatus',form_create).val('1');
        $('#dllConfirmAttend1st',form_create).val('1');
        $('#dllConfirmAttend2st',form_create).val('1');
        $('#txtNote',form_create).val('');
    }
};
var bcrm_invitee_edit = {
    old_scroll_position:0,
    init:function(){
        $(".chzn-select",$('#frmEditRegisteredInvitee')).chosen();
        $(".chzn-select-deselect",$('#frmEditRegisteredInvitee')).chosen({allow_single_deselect:true});
        $(".chzn-choices",$('#frmEditRegisteredInvitee')).attr('style','min-height:32px;');
        bcrm_invitee_edit.register_event();
    },
    register_event:function(){
        var form_create = $('#frmEditRegisteredInvitee');
        $('#btnSave',form_create).off('click');
        $('#btnSave',form_create).on('click',function(){
            sys_utility.clear_errors(form_create);
            if(bcrm_invitee_edit.validate()){
                var ddlEventIdV= $('#ddlEventId',form_create).val();
                var txtCustomerNameV = $('#txtCustomerName',form_create).val();
                var txtCustomerPhoneV = $('#dllCustomerPhone',form_create).val();
                var txtCustomerEmailV = $('#txtCustomerEmail',form_create).val();
                var txtNoteV = $('#txtNote',form_create).val();
                var dllCustomerPositionV = $('#dllCustomerPosition',form_create).val();
                var hiddIdV =   $('#hiddId',form_create).val();

                var sessions = '';
                if(!$('div[name=IsHadSession]',form_create).hasClass('hide') ){
                    sessions = sys_control.get_val_to_ajax($('#dllSession',form_create));
                }
                $.ajax({
                    url: "/RegisteredInvitee/BCRMEditAction",
                    type: "POST",
                    data: {
                        CustomerName : txtCustomerNameV,
                        CustomerPhone : txtCustomerPhoneV,
                        CustomerEmail : txtCustomerEmailV,
                        CustomerPosition:dllCustomerPositionV,
                        Note : txtNoteV,
                        Id:hiddIdV,
                        Sessions:sessions
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $('#btnSave',$('#frmEditRegisteredInvitee')).attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        if(response.Code == ResponseCode.Success){
                            sysmess.success(response.Message);
                            registered_invitee_index.display('SearchRegisteredInvitee');
                            var pageIndex = 0;
                            $(".dataTables_paginate .active a").each(
                                function () {
                                    if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                        pageIndex = parseInt($(this).html().trim());
                                    }
                                }
                            );
                            registered_invitee_index.search(pageIndex);
                            $(window).scrollTop(bcrm_invitee_edit.old_scroll_position);
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    },
                    complete: function () {
                        $('#btnSave',$('#frmEditRegisteredInvitee')).removeAttr("disabled");
                    }
                });
            }
        });

        $('#btnClose',form_create).off('click');
        $('#btnClose',form_create).on('click',function(){
            registered_invitee_index.display('SearchRegisteredInvitee');
        });

        $('#btnCancel',form_create).off('click');
        $('#btnCancel',form_create).on('click',function(){
            var id = $('#hiddId',form_create).val();
            $.ajax({
                url: "/RegisteredInvitee/BCRMEdit",
                type: "GET",
                data: {
                    id:id
                },
                dataType: "html",
                async: true,
                beforeSend: function () { },
                error: function (jqXHR, textStatus, errorThrown) { },
                success: function (response) {
                    registered_invitee_index.display('AddRegisteredInvitee');
                    $(".panel-body",$("#AddRegisteredInvitee")).html(response);
                    bcrm_invitee_edit.init();
                },
                complete: function () {
                }
            });
        });

        $('#txtCustomerPhone',form_create).off('keypress');
        $('#txtCustomerPhone',form_create).on('keypress',function(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        });

        $('#toogleHistory').off('click');
        $('#toogleHistory').on('click',function(){
            if($('span',this).html() =='+' )
            {
                $('span',this).html('-');
                if($('#tblHistory').hasClass('hide') )
                {
                    $('#tblHistory').removeClass('hide');
                }
            }
            else
            {
                $('span',this).html('+');
                if(!$('#tblHistory').hasClass('hide') )
                {
                    $('#tblHistory').addClass('hide');
                }
            }
        });
    },
    validate:function(){
        var form_create = $('#frmEditRegisteredInvitee');
        var result = true;
        if(!sys_utility.require_text_box($('#txtCustomerName',form_create))){
            sys_control.show_msg($('#txtCustomerName',form_create),'Tên khách mời bắt buộc nhập !',1);
            result = false;
        }
        if(!sys_utility.require_select($('#dllCustomerPhone',form_create))){
            sys_control.show_msg($('#dllCustomerPhone',form_create),'Số điện thoại bắt buộc nhập !',1);
            result = false;
        }
        if(!sys_utility.require_text_box($('#txtCustomerEmail',form_create))){
            sys_control.show_msg($('#txtCustomerEmail',form_create),'Email bắt buộc nhập !',1);
            result = false;
        }
        if(!$('div[name=IsHadSession]',form_create).hasClass('hide') && !sys_utility.require_select($('#dllSession',form_create)) ){
            sys_control.show_msg($('#dllSession',form_create),'Phiên tham dự bắt buộc nhập !',1);
            result = false;
        }
        return result;
    },
    reset:function(){
        var form_create = $('#frmEditRegisteredInvitee');
        $('#txtCustomerName',form_create).val('');
        $('#txtIntroduceUser',form_create).val('');
        $('#txtCustomerPhone',form_create).val('');
        $('#txtCustomerEmail',form_create).val('');
        $('#txtCompanyName',form_create).val('');
        // Lựa chọn
        $('#dllCustomerType option',form_create).val(1);
        $('#txtPositionName',form_create).val('');
        var htmlPlaceOfAttendEvent =  $('#dllPlaceOfAttendEvent',form_create).html();
        $('#dllPlaceOfAttendEvent',form_create).html(htmlPlaceOfAttendEvent);

        $('#dllRegisteredSource',form_create).val('4');
        $('#dllPhoningStatus',form_create).val('1');
        $('#dllConfirmAttend1st',form_create).val('1');
        $('#dllConfirmAttend2st',form_create).val('1');
        $('#txtNote',form_create).val('');
    }
};
var invitee_assign_to_sale = {
    init: function () {
        $(".chzn-select",$('#wrapAssignToSale')).chosen();
        $(".chzn-select-deselect",$('#wrapAssignToSale')).chosen({allow_single_deselect:true});
        $(".chzn-choices",$('#wrapAssignToSale')).attr('style','min-height:32px;');
        invitee_assign_to_sale.register_event();
    },
    register_event: function () {
        var wrapAssignTo = $('#wrapAssignToSale');
        $('button[name=btnSaveAssignToSale]',wrapAssignTo).off('click').on('click', function () {
            $.ajax({
                url: "/RegisteredInvitee/AssignToSaleAction",
                type: "POST",
                data: {
                    inviteeId : $(this).attr('iid'),
                    assignTo : $('select[name=ddlAssignToSale]').val()
                },
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $('button[name=btnSaveAssignToSale]',wrapAssignTo).attr("disabled","disabled");
                },
                error: function (jqXHR, textStatus, errorThrown) { },
                success: function (response) {
                    if(response.Code == ResponseCode.Success){
                        sysmess.success(response.Message);
                        registered_invitee_index.display('SearchRegisteredInvitee');
                        var pageIndex = 0;
                        $(".dataTables_paginate .active a").each(
                            function () {
                                if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                    pageIndex = parseInt($(this).html().trim());
                                }
                            }
                        );
                        registered_invitee_index.search(pageIndex);
                        $(window).scrollTop(bcrm_invitee_edit.old_scroll_position);
                    }
                    else if(response.Code == ResponseCode.Error){
                        sysmess.error(response.Message);
                    }
                    else{
                        sysmess.warning(response.Message);
                    }
                },
                complete: function () {
                    $('button[name=btnSaveAssignToSale]',wrapAssignTo).removeAttr("disabled");
                }
            });
        });

        $('button[name=btnCloseAssignToSale]',wrapAssignTo).off('click').on('click', function () {
            registered_invitee_index.display('SearchRegisteredInvitee');
        });
    }
}