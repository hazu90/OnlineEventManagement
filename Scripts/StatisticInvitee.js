﻿var statistic_invitee = {
    init:function(){
        $(document).ready(function(){
            statistic_invitee.register_events();
            statistic_invitee.search();

            var selectedEvent = $("#selEvent",$("#frmSearchStatistic")).val();
            var isInCheckin = false;
            if(selectedEvent== null  || selectedEvent == ''){
                isInCheckin = true;
            }
            else{
                var arrV = selectedEvent.split('_');
                if(arrV[1] =='1'){
                    isInCheckin = true  ;
                    $.ajax({
                        url: "/Session/GetByEventId",
                        type: "POST",
                        data: {
                            eventId:arrV[0]
                        },
                        dataType: "json",
                        async: true,
                        beforeSend: function () {

                        },
                        error: function (jqXHR, textStatus, errorThrown) { },
                        success: function (response) {
                            if(response.Data != null){
                                if(response.Data.length >0){
                                    $("#dllSessions",$("#frmSearchStatistic")).parent().removeClass('hide');
                                    var html = '<option value="0">--Chọn tất cả --</option>';
                                    for(var index = 0 ;index < response.Data.length;index++ ){
                                        html +='<option value="'+ response.Data[index].Id + '">'+ response.Data[index].Name +'</option>';
                                    }
                                    $("#dllSessions",$("#frmSearchStatistic")).html(html);
                                    $("#dllSessions",$("#frmSearchStatistic")).trigger('liszt:updated');
                                    $(".chzn-select",$("#dllSessions",$("#frmSearchStatistic")).parent()).chosen();
                                    $(".chzn-select-deselect",$("#dllSessions",$("#frmSearchStatistic")).parent()).chosen({allow_single_deselect:true});
                                    $(".chzn-choices",$("#dllSessions",$("#frmSearchStatistic")).parent()).attr('style','min-height:32px;');
                                }
                                else{
                                    $("#dllSessions",$("#frmSearchStatistic")).parent().addClass('hide');
                                }
                            }
                            else{
                                $("#dllSessions",$("#frmSearchStatistic")).parent().addClass('hide');
                            }

                        },
                        complete: function () {

                        }
                    });
                }
                else
                {
                    isInCheckin = false;
                }
            }

            if(isInCheckin){
                $('#dllPlaceOfAttendEvent',$("#frmSearchStatistic")).parent().removeClass('hide');
                $('#selEvent',$("#frmSearchStatistic")).parent().removeClass('col-md-9');
                $('#selEvent',$("#frmSearchStatistic")).parent().addClass('col-md-5');
            }
            else{
                $('#dllPlaceOfAttendEvent',$("#frmSearchStatistic")).parent().addClass('hide');
                $('#selEvent',$("#frmSearchStatistic")).parent().removeClass('col-md-5');
                $('#selEvent',$("#frmSearchStatistic")).parent().addClass('col-md-9');
            }
        });
    },
    register_events:function(){
        var form_search = $("#frmSearchStatistic");
        $("#btnSearch",form_search).off("click");
        $("#btnSearch",form_search).on("click",function(){
            statistic_invitee.search();
        });

        $("#selEvent",form_search).off("change");
        $("#selEvent",form_search).on("change",function(){
            var selectedEvent = $("#selEvent",$("#frmSearchStatistic")).val();
            var isInCheckin = false;
            if(selectedEvent== null  || selectedEvent == ''){
                isInCheckin = true;
            }
            else{
                var arrV = selectedEvent.split('_');
                if(arrV[1] =='1'){
                    isInCheckin = true  ;
                    $.ajax({
                        url: "/Session/GetByEventId",
                        type: "POST",
                        data: {
                            eventId:arrV[0]
                        },
                        dataType: "json",
                        async: true,
                        beforeSend: function () {},
                        error: function (jqXHR, textStatus, errorThrown) { },
                        success: function (response) {
                            if(response.Data != null){
                                if(response.Data.length >0){
                                    $("#dllSessions",form_search).parent().removeClass('hide');
                                    var html = '<option value="0">--Chọn tất cả --</option>';
                                    for(var index = 0 ;index < response.Data.length;index++ ){
                                        html +='<option value="'+ response.Data[index].Id + '">'+ response.Data[index].Name +'</option>';
                                    }
                                    $("#dllSessions",form_search).html(html);
                                    $("#dllSessions",form_search).trigger('liszt:updated');
                                    $(".chzn-select",$("#dllSessions",form_search).parent()).chosen();
                                    $(".chzn-select-deselect",$("#dllSessions",form_search).parent()).chosen({allow_single_deselect:true});
                                    $(".chzn-choices",$("#dllSessions",form_search).parent()).attr('style','min-height:32px;');
                                }
                                else{
                                    $("#dllSessions",form_search).parent().addClass('hide');
                                }
                            }
                            else{
                                $("#dllSessions",form_search).parent().addClass('hide');
                            }

                        },
                        complete: function () {

                        }
                    });
                }
                else{
                    isInCheckin = false;
                }
            }

            if(isInCheckin){
                $('#dllPlaceOfAttendEvent',$("#frmSearchStatistic")).parent().removeClass('hide');
                $('#selEvent',$("#frmSearchStatistic")).parent().removeClass('col-md-9');
                $('#selEvent',$("#frmSearchStatistic")).parent().addClass('col-md-5');
            }
            else{
                $('#dllPlaceOfAttendEvent',$("#frmSearchStatistic")).parent().addClass('hide');
                $('#selEvent',$("#frmSearchStatistic")).parent().removeClass('col-md-5');
                $('#selEvent',$("#frmSearchStatistic")).parent().addClass('col-md-9');
            }
        });
    },
    search:function(){
        var form_search = $("#frmSearchStatistic");
        var selectedEvent = $("#selEvent",form_search).val();
        var arrV = [];
        var eventId = '0';
        var isInCheckin = true;
        if(selectedEvent== null  || selectedEvent == ''){
            eventId= '0';
            isInCheckin = true;
        }
        else{
            arrV = selectedEvent.split('_');
            eventId = arrV[0];
            if(arrV[1] =='1'){
                isInCheckin = true  ;
            }
            else{
                isInCheckin = false;
            }
        }
        $.ajax({
            url: "/Statistic/Search",
            type: "GET",
            data: {
                eventId:eventId,
                isInWebCheckIn:isInCheckin,
                selectPlaceAttendEvent : $('#dllPlaceOfAttendEvent',form_search).val(),
                sessionId : $('#dllSessions',form_search).val()
            },
            dataType: "html",
            async: true,
            beforeSend: function () {},
            error: function (jqXHR, textStatus, errorThrown) {},
            success: function (response) {
                $(".panel-body",$("#SearchStatistic")).html(response);
            },
            complete: function () {}
        });
    }
}