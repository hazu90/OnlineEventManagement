﻿var event_index = {
    init: function () {
        $(document).ready(function () {
            event_index.register_events();
            event_create.init();
            $("#frmSearchEvent").keydown(event_index.hitting_enter);
            session_manage.init();
            event_index.search();
        });
    },
    hitting_enter:function(e){
        if(e.which == 13) {
            e.preventDefault();
            $(this).blur();
            event_index.search();
        }
    },
    register_events: function () {
        var form = $('#frmSearchEvent');
        $("#txtStartDate",form).datepicker();
        $("#txtEndDate",form).datepicker();

        $('#btnSearch',form).off('click');
        $('#btnSearch',form).on('click',function(){
            var filterTextWord = $('#txtSearchText').val();
            var txtStartDateV = $('#txtStartDate').val();
            var arrStartDate = [];
            if(txtStartDateV == null || txtStartDateV == ''){
                txtStartDateV = null    ;
            }
            else{
                arrStartDate = txtStartDateV.split('/');
            }

            if(arrStartDate.length>0){
                txtStartDateV = arrStartDate[1]+'/'+arrStartDate[0]+'/'+arrStartDate[2];
            }

            var txtEndDateV = $('#txtEndDate').val();
            var arrEndDate = [];
            if(txtEndDateV == null || txtEndDateV == ''){
                txtEndDateV = null;
            }
            else{
                arrEndDate = txtEndDateV.split('/');
            }
            if(arrEndDate.length>0){
                txtEndDateV = arrEndDate[1]+'/'+arrEndDate[0]+'/'+arrEndDate[2];
            }

            if(arrStartDate.length>0 && arrEndDate.length>0){
                var dateStart = new Date(parseInt(arrStartDate[2]),parseInt(arrStartDate[1])-1,parseInt(arrStartDate[0]));
                var dateEnd = new Date(parseInt(arrEndDate[2]),parseInt(arrEndDate[1])-1,parseInt(arrEndDate[0]));
                if(dateStart > dateEnd){
                    sysmess.warning('Ngày bắt đầu không được lớn hơn ngày kết thúc');
                    return ;
                }
            }

            $.ajax({
                url: "/Event/Search",
                type: "GET",
                data: {
                    StartDate:txtStartDateV,
                    EndDate :txtEndDateV,
                    FilterTextSearch:filterTextWord,
                    DeployEventId:$('#ddlDeployEvent',form).val()
                },
                dataType: "html",
                async: true,
                beforeSend: function () {
                    $("#btnSearch",form).attr("disabled","disabled");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    sysmess.error('Có lỗi trong quá trình xử lý ! Liên hệ IT để có thông tin chi tiết .',function(){});
                },
                success: function (response) {
                    $( '.panel-body', $('#SearchEvent')).html(response);
                    $('.dropdown-toggle').dropdown();
                    event_index.register_grid_events();
                    event_index.display('SearchEvent');
                },
                complete: function () {
                    $("#btnSearch",form).removeAttr("disabled");
                }
            });
        });

        $('#btnAddNew',form).off('click');
        $('#btnAddNew',form).on('click',function(){
            event_index.display('CreateEvent');
            event_create.reset();
            sys_utility.clear_errors($('#frmAddNewEvent'));
        });

    },
    register_grid_events:function(){
        var form_result_search = $('#SearchEvent');
        $('[name=btnEdit]',form_result_search).each(function(){
            $(this).off('click');
            $(this).on('click',function(){
                var current = $(this);
                var id = $(this).attr('uid');
                $.ajax({
                    url: "/Event/Edit",
                    type: "html",
                    data: {
                        id:id
                    },
                    dataType: "html",
                    async: true,
                    beforeSend: function () {
                        current.attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        sysmess.error('Có lỗi trong quá trình xử lý ! Liên hệ IT để có thông tin chi tiết .',function(){});
                    },
                    success: function (response) {
                        event_index.display('EditEvent');
                        $('#EditEvent .panel-body').html(response);
                        $('input[type=checkbox]',$('#frmEditEvent')).uniform();
                        event_edit.init();
                    },
                    complete: function () {
                        current.removeAttr("disabled");
                    }
                });
            });
        });
        $('[name=btnStop]',form_result_search).each(function(){
            $(this).off('click');
            $(this).on('click',function(){
                var current = $(this);
                var id = $(this).attr('uid');
                $.ajax({
                    url: "/Event/DeActive",
                    type: "json",
                    data: {
                        id:id
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        current.attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        sysmess.error('Có lỗi trong quá trình xử lý ! Liên hệ IT để có thông tin chi tiết .',function(){});
                    },
                    success: function (response) {
                        if(response.Code == ResponseCode.Success){
                            sysmess.success(response.Message,function(){});
                            event_index.search();
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message,function(){});
                        }
                        else{
                            sysmess.warning(response.Message,function(){});
                        }
                    },
                    complete: function () {
                    }
                });
            });
        });
        $('[name=btnActive]',form_result_search).each(function(){
            $(this).off('click');
            $(this).on('click',function(){
                var current = $(this);
                var id = $(this).attr('uid');
                $.ajax({
                    url: "/Event/Active",
                    type: "json",
                    data: {
                        id:id
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        current.attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        sysmess.error('Có lỗi trong quá trình xử lý ! Liên hệ IT để có thông tin chi tiết .',function(){});
                    },
                    success: function (response) {
                        if(response.Code == ResponseCode.Success){
                            event_index.search();
                            sysmess.success(response.Message,function(){});
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message,function(){});
                        }
                        else{
                            sysmess.warning(response.Message,function(){});
                        }
                    },
                    complete: function () {
                    }
                });
            });
        });
        $('[name=btnSetDefault]',form_result_search).each(function(){
            $(this).off('click');
            $(this).on('click',function(){
                var current = $(this);
                var id = $(this).attr('uid');
                var iinWebCheckin = $(this).attr('iwci');
                var isInWebCheckIn = false;
                if(iinWebCheckin == '0')
                {
                    isInWebCheckIn = false  ;
                }
                else
                {
                    isInWebCheckIn = true;
                }
                $.ajax({
                    url: "/Event/SetDefaultEvent",
                    type: "json",
                    data: {
                        id:id,
                        IsInWebCheckIn:isInWebCheckIn
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        current.attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        sysmess.error('Có lỗi trong quá trình xử lý ! Liên hệ IT để có thông tin chi tiết .',function(){});
                    },
                    success: function (response) {
                        if(response.Code == ResponseCode.Success){
                            event_index.search();
                            sysmess.success(response.Message,function(){});
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message,function(){});
                        }
                        else{
                            sysmess.warning(response.Message,function(){});
                        }
                    },
                    complete: function () {
                    }
                });
            });
        });
        $('a[name=btnListLuckyNumber]').each(function(){
            $(this).off('click').on('click',function(){
                var eventId = $(this).attr('uid');
                var iWebCheckin = $(this).attr('iwci');
                event_index.display('ShowSlotRotate');
                lucky_number_manager.search(eventId,iWebCheckin);
            });
        });
    },
    display:function(divId){
        if(divId == 'SearchEvent'){
            if($('#SearchEvent').hasClass('hide')){
                $('#SearchEvent').removeClass('hide');
            }
        }
        else{
            if(!$('#SearchEvent').hasClass('hide')){
                $('#SearchEvent').addClass('hide');
            }
        }

        if(divId == 'CreateEvent'){
            if($('#CreateEvent').hasClass('hide')){
                $('#CreateEvent').removeClass('hide');
            }
        }
        else{
            if(!$('#CreateEvent').hasClass('hide')){
                $('#CreateEvent').addClass('hide');
            }
        }

        if(divId == 'EditEvent'){
            if($('#EditEvent').hasClass('hide')){
                $('#EditEvent').removeClass('hide');
            }
        }
        else{
            if(!$('#EditEvent').hasClass('hide')){
                $('#EditEvent').addClass('hide');
            }
        }
        if(divId == 'ShowSlotRotate'){
            if($('#ShowSlotRotate').hasClass('hide')){
                $('#ShowSlotRotate').removeClass('hide');
            }
        }
        else{
            if(!$('#ShowSlotRotate').hasClass('hide')){
                $('#ShowSlotRotate').addClass('hide');
            }
        }

        if(divId == 'ManageSession'){
            if($('#ManageSession').hasClass('hide')){
                $('#ManageSession').removeClass('hide');
            }
        }
        else{
            if(!$('#ManageSession').hasClass('hide')){
                $('#ManageSession').addClass('hide');
            }
        }
    },
    search:function(page){
        var form_search = $('#frmSearchEvent');
        var filterTextWord = $('#txtSearchText',form_search).val();
        var txtStartDateV = $('#txtStartDate',form_search).val();
        var arrStartDate = txtStartDateV.split('/');

        var txtEndDateV = $('#txtEndDate',form_search).val();
        var arrEndDate = [];
        if(txtEndDateV == null || txtEndDateV == ''){
            txtEndDateV = null;
        }
        else{
            arrEndDate = txtEndDateV.split('/');
        }
        if(arrEndDate.length>0){
            txtEndDateV = arrEndDate[1]+'/'+arrEndDate[0]+'/'+arrEndDate[2];
        }
        if(arrStartDate.length>0 && arrEndDate.length>0){
            var dateStart = new Date(parseInt(arrStartDate[2]),parseInt(arrStartDate[1])-1,parseInt(arrStartDate[0]));
            var dateEnd = new Date(parseInt(arrEndDate[2]),parseInt(arrEndDate[1])-1,parseInt(arrEndDate[0]));
            if(dateStart > dateEnd){
                sysmess.warning('Ngày bắt đầu không được lớn hơn ngày kết thúc');
                return ;
            }
        }

        $.ajax({
            url: "/Event/Search",
            type: "GET",
            data: {
                StartDate:arrStartDate[1]+'/'+arrStartDate[0]+'/'+arrStartDate[2],
                EndDate :txtEndDateV,
                DeployEventId:$('#ddlDeployEvent',form_search).val(),
                FilterTextSearch:filterTextWord,
                PageIndex: page,
                PageSize: 25
            },
            dataType: "html",
            async: true,
            beforeSend: function () {
                $("#btnSearch",form_search).attr("disabled","disabled");
            },
            error: function (jqXHR, textStatus, errorThrown) {
                sysmess.error('Có lỗi trong quá trình xử lý ! Liên hệ IT để có thông tin chi tiết .',function(){});
            },
            success: function (response) {
                $( '.panel-body', $('#SearchEvent')).html(response);
                $('.dropdown-toggle').dropdown();
                event_index.register_grid_events();
                event_index.display('SearchEvent');
            },
            complete: function () {
                $("#btnSearch",form_search).removeAttr("disabled");
            }
        });
    }
};
var event_create={
    init:function(){
        var form_create = $('#frmAddNewEvent');
        $("#txtStartTime").datepicker();
        $("#txtEndTime").datepicker();
        $("#txtDeadline").datepicker();
        $("#txtRevenueStartDate").datepicker();
        $("#txtRevenueEndDate").datepicker();
        $('input[type=checkbox]').uniform();
        $(".chzn-select",form_create).chosen();
        $(".chzn-select-deselect",form_create).chosen({allow_single_deselect:true});
        $('.chzn-choices',form_create).attr('style','padding:4px 12px;');
        if($('#chkIsBcrmUse',form_create).prop('checked')){
            $('div[name=UsingInBCRM]',form_create).each(function () {
                $(this).removeClass('hide');
            });
        }
        else{
            $('div[name=UsingInBCRM]',form_create).each(function () {
                $(this).addClass('hide');
            });
        }
        event_create.register_events();
        session_manage.init($('div[name=ManageSessionCreate]'));
        event_create.reset();
    },
    register_events:function(){
        var form =  $('#frmAddNewEvent');
        $('#btnSave',form).off('click');
        $('#btnSave',form).on('click',function(){
            sys_utility.clear_errors($('#frmAddNewEvent'));
            var isValid = true  ;
            if(!event_create.validate()){
                isValid = false;
            }
            if(!session_manage.validate($('div[name=ManageSessionCreate]'))){
                isValid = false;
            }
            if(!$('div[name=ManageSessionCreate]').hasClass('hide') ){
                if(!session_manage.has_row($('div[name=ManageSessionCreate]'))){
                    isValid = false;
                    sys_control.show_msg($('#uniform-chkIsHasSession',form),'Bạn phải tạo phiên cho các sự kiện có phiên tham dự !',1);
                }
            }

            if(isValid){
                var isBcrmUse = sys_control.get_val_to_ajax($('#chkIsBcrmUse',form));
                var strDeadline ='';
                var strRevenueStartDate = '';
                var strRevenueEndDate = '';
                if(isBcrmUse){
                    strDeadline = sys_control.get_val_to_ajax($('#txtDeadline',form));
                    strRevenueStartDate = sys_control.get_val_to_ajax($('#txtRevenueStartDate',form));
                    strRevenueEndDate = sys_control.get_val_to_ajax($('#txtRevenueEndDate',form));
                }
                else{
                    strDeadline = null;
                    strRevenueStartDate = null;
                    strRevenueEndDate = null;
                }
                var deployV = $('#ddlDeployEvent',form).val();
                var strDeployV='';
                if(isBcrmUse) {
                    strDeployV = sys_control.get_val_to_ajax($('#ddlDeployEvent',form));
                }

                var wrapSaveSession = $('div[name=ManageSessionCreate]');

                var arrSession = [];
                if(!wrapSaveSession.hasClass('hide')){
                    $('table tbody tr',wrapSaveSession).each(function () {
                        var name = $('input[name=txtName]',$(this)).val();
                        var startDate = sys_control.get_val_to_ajax($('input[name=txtStartDate]',$(this))) ;
                        var endDate = sys_control.get_val_to_ajax($('input[name=txtEndDate]',$(this))) ;
                        var description = sys_control.get_val_to_ajax($('input[name=txtDescription]',$(this)));
                        arrSession.push({
                            Name : name,
                            StartDate : startDate,
                            StartTime : $('input[name=txtStartDateTime]',$(this)).val(),
                            EndDate : endDate,
                            EndTime : $('input[name=txtEndDateTime]',$(this)).val(),
                            Description : description
                        });
                    });
                }
                var model = {
                    EventCode:sys_control.get_val_to_ajax($('#txtEventCode',form)),
                    Name:sys_control.get_val_to_ajax($('#txtEvent',form)),
                    StartDate: sys_control.get_val_to_ajax($('#txtStartTime',form)),
                    EndDate:sys_control.get_val_to_ajax($('#txtEndTime',form)),
                    IsEnable :sys_control.get_val_to_ajax($('#chkActive',form)),
                    IsDefaultEvent:sys_control.get_val_to_ajax($('#chkDefaultEvent',form)),
                    Description :sys_control.get_val_to_ajax($('#txtDescription',form)),
                    SMSTitle:sys_control.get_val_to_ajax($('#txtSMSTitle',form)) ,
                    Deploy : strDeployV,
                    IsBcrmUse :isBcrmUse,
                    IsAllowAttach :sys_control.get_val_to_ajax($('#chkIsAllowAttach',form)),
                    Deadline:strDeadline,
                    RevenueStartDate: strRevenueStartDate,
                    RevenueEndDate : strRevenueEndDate,
                    RevenueCalculation:sys_control.get_val_to_ajax($('#ddlRevenueCalculation',form)),
                    LstSession : arrSession
                };

                $.ajax({
                    url: "/Event/CreateAction",
                    contentType: 'application/json; charset=utf-8',
                    type: "POST",
                    data: JSON.stringify({'model':model } ),
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $("#btnSave",form).attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        sysmess.error('Có lỗi trong quá trình xử lý ! Liên hệ IT để có thông tin chi tiết .',function(){});
                    },
                    success: function (response) {
                        if(response.Code == ResponseCode.Success){
                            sysmess.success(response.Message,function(){});
                            event_index.display('SearchEvent');
                            event_index.search();
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message,function(){});
                        }
                        else{
                            sysmess.warning(response.Message,function(){});
                        }
                    },
                    complete: function () {
                        $("#btnSave",form).removeAttr("disabled");
                    }
                });
            }
        });

        $('#btnCancel',form).off('click');
        $('#btnCancel',form).on('click',function(){
            event_create.reset();
        });

        $('#btnClose',form).off('click');
        $('#btnClose',form).on('click',function(){
            event_index.display('SearchEvent');
        });

        $('#uniform-chkIsBcrmUse',form).off('click').on('click', function () {
            if($('#chkIsBcrmUse',form).prop('checked')){
                $('div[name=UsingInBCRM]',form).each(function () {
                   $(this).removeClass('hide');
                });
            }
            else{
                $('div[name=UsingInBCRM]',form).each(function () {
                   $(this).addClass('hide');
                });
            }
        });

        $('#uniform-chkIsHasSession',form).off('click').on('click', function () {
            if($('#chkIsHasSession',form).prop('checked')){
                $('div[name=ManageSessionCreate]').removeClass('hide');
            }
            else{
                $('div[name=ManageSessionCreate]').addClass('hide');
            }
        });
    },
    validate:function(){
        var isValid = true;
        var form_create = $('#frmAddNewEvent');
        if(!sys_utility.require_text_box($('#txtEventCode',form_create)) ){
            sys_control.show_msg($('#txtEventCode',form_create),'Mã sự kiện bắt buộc nhập !',1);
            isValid = false;
        }

        if(!sys_utility.require_text_box($('#txtEvent',form_create))){
            sys_control.show_msg($('#txtEvent',form_create),'Mã sự kiện bắt buộc nhập !',1);
            isValid = false;
        }

        if(!sys_utility.require_text_box($('#txtSMSTitle',form_create))){
            sys_control.show_msg($('#txtSMSTitle',form_create),'Nội dung SMS bắt buộc nhập !',1);
            isValid = false;
        }

        if(!sys_utility.require_text_box($('#txtStartTime',form_create))){
            sys_control.show_msg($('#txtStartTime',form_create),'Ngày bắt đầu bắt buộc nhập !',1);
            isValid = false;
        }
        else{
            if(!sys_utility.is_date($('#txtStartTime',form_create).val())){
                sys_control.show_msg($('#txtStartTime',form_create),'Định dạng nhập ngày bắt đầu không đúng !',1);
                isValid = false;
            }
        }

        if(!sys_utility.require_text_box($('#txtEndTime',form_create))){
            sys_control.show_msg($('#txtEndTime',form_create),'Ngày kết thúc bắt buộc nhập !',1);
            isValid = false;
        }
        else{
            if(!sys_utility.is_date($('#txtEndTime',form_create).val())){
                sys_control.show_msg($('#txtEndTime',form_create),'Định dạng nhập ngày kết thúc không đúng',1);
                isValid = false;
            }
        }

        if(isValid){
            if(sys_utility.compare_date($('#txtStartTime',form_create).val(),$('#txtEndTime',form_create).val()) > 0){
                sys_control.show_msg($('#txtStartTime',form_create),'Ngày bắt đầu không được lớn hơn ngày kết thúc ',1);
                isValid = false;
            }
        }

        var chkIsBcrmUse = $('#chkIsBcrmUse',form_create).prop('checked');
        if(chkIsBcrmUse == true){
            if(!sys_utility.require_text_box($('#txtDeadline',form_create))){
                sys_control.show_msg($('#txtDeadline',form_create),'Ngày chốt sự kiện bắt buộc nhập !',1);
                isValid = false;
            }
            else{
                if(!sys_utility.is_date($('#txtDeadline',form_create).val())){
                    sys_control.show_msg($('#txtDeadline',form_create),'Định dạng nhập ngày chốt sự kiện không đúng !',1);
                    isValid = false;
                }
            }
            if(!sys_utility.require_text_box($('#txtRevenueStartDate',form_create))){
                sys_control.show_msg($('#txtRevenueStartDate',form_create),'Ngày bắt đầu tính doanh thu bắt buộc nhập !',1);
                isValid = false;
            }
            else{
                if(!sys_utility.is_date($('#txtRevenueStartDate',form_create).val())){
                    sys_control.show_msg($('#txtRevenueStartDate',form_create),'Định dạng nhập ngày bắt đầu tính doanh thu không đúng !',1);
                    isValid = false;
                }
            }
            if(!sys_utility.require_text_box($('#txtRevenueEndDate',form_create))){
                sys_control.show_msg($('#txtRevenueEndDate',form_create),'Ngày kết thúc tính doanh thu bắt buộc nhập !',1);
                isValid = false;
            }
            else{
                if(!sys_utility.is_date($('#txtRevenueEndDate',form_create).val())){
                    sys_control.show_msg($('#txtRevenueEndDate',form_create),'Định dạng nhập ngày kết thúc tính doanh không đúng !',1);
                    isValid = false;
                }
            }
            if(isValid){
                if(sys_utility.compare_date($('#txtRevenueStartDate',form_create).val(),$('#txtRevenueEndDate',form_create).val()) > 0){
                    sys_control.show_msg($('#txtRevenueStartDate',form_create),'Ngày bắt đầu tính doanh thu không được lớn hơn ngày kết thúc tính doanh thu ! ',1);
                    isValid = false;
                }
            }
            if(!sys_utility.require_select($('#ddlDeployEvent',form_create))){
                sys_control.show_msg($('#ddlDeployEvent',form_create),'Bạn phải chọn thông tin nhóm áp dụng !',1);
                isValid = false;
            }
        }

        if(!$('div[name=ManageSessionCreate]').hasClass('hide')){

        }

        return isValid;
    },
    reset:function(){
        var form = $('#frmAddNewEvent');
        $('#txtEventCode',form).val('');
        $('#txtEvent',form).val('');
        $('#txtStartTime',form).val('');
        $('#txtEndTime',form).val('');
        $('#chkActive',form).prop('checked',true);
        $.uniform.update('#chkActive');
        $('#chkDefaultEvent',form).prop('checked',false);
        $.uniform.update('#chkDefaultEvent');
        $('#chkIsBcrmUse',form).prop('checked',false);
        $.uniform.update('#chkIsBcrmUse');
        $('#uniform-chkIsBcrmUse',form).trigger('click');
        $('#txtDeadline',form).val('');
        $('#txtRevenueStartDate',form).val('');
        $('#txtRevenueEndDate',form).val('');
        $('#ddlRevenueCalculation',form).val(1);
        $('#chkIsAllowAttach',form).prop('checked',true);
        $.uniform.update('#chkIsAllowAttach');
        $('#ddlDeployEvent',form).val([]);
        $('#ddlDeployEvent',form).trigger('liszt:updated');
        $('#txtSMSTitle',form).val('');
        $('#txtDescription',form).val('');
        $('table tbody',form).html('');
        $('#chkIsHasSession',form).prop('checked',false);
        $.uniform.update('#chkIsHasSession');
        $('#uniform-chkIsHasSession',form).trigger('click');
    }
};
var event_edit={
    init:function(){
        var form_edit = $('#frmEditEvent');
        $("#txtStartTime",form_edit).datepicker();
        $("#txtEndTime",form_edit).datepicker();
        $('#txtDeadline',form_edit).datepicker();
        $('#txtRevenueStartDate',form_edit).datepicker();
        $('#txtRevenueEndDate',form_edit).datepicker();
        $(".chzn-select",form_edit).chosen();
        $(".chzn-select-deselect",form_edit).chosen({allow_single_deselect:true});
        $('.chzn-choices',form_edit).attr('style','padding:4px 12px;');
        event_edit.register_events();
        session_manage.init($('div[name=ManageSessionEdit]'));
        session_manage.grid_action($('div[name=ManageSessionEdit]'));
        if($('#hiddHasInvitee',form_edit).val()=='True'){
            $('#ddlDeployEventEdit_chzn .search-choice a').each(function () {
                $(this).remove();
            });
        }
    },
    register_events:function(){
        var form_edit = $('#frmEditEvent');
        $('#btnSave',form_edit).off('click');
        $('#btnSave',form_edit).on('click',function(){
            var is_valid = true ;
            if(!event_edit.validate()){
                is_valid = false;
            }
            if(!session_manage.validate($('div[name=ManageSessionEdit]'))){
                is_valid = false;
            }

            if(!$('div[name=ManageSessionEdit]').hasClass('hide') ){
                if(!session_manage.has_row($('div[name=ManageSessionEdit]'))){
                    is_valid = false;
                    sys_control.show_msg($('#uniform-chkIsHasSession',form_edit),'Bạn phải tạo phiên cho các sự kiện có phiên tham dự !',1);
                }
            }

            if(is_valid){
                var isBcrmUse = sys_control.get_val_to_ajax($('#chkIsBcrmUse',form_edit));
                var strDeadline ='';
                var strRevenueStartDate = '';
                var strRevenueEndDate = '';
                if(isBcrmUse){
                    strDeadline = sys_control.get_val_to_ajax($('#txtDeadline',form_edit));
                    strRevenueStartDate = sys_control.get_val_to_ajax($('#txtRevenueStartDate',form_edit));
                    strRevenueEndDate   = sys_control.get_val_to_ajax($('#txtRevenueEndDate',form_edit));
                }
                else{
                    strDeadline = null;
                    strRevenueStartDate = null;
                    strRevenueEndDate = null;
                }
                var deployV = $('#ddlDeployEventEdit',form_edit).val();
                var strDeployV='';
                if(isBcrmUse) {
                    strDeployV = sys_control.get_val_to_ajax($('#ddlDeployEventEdit',form_edit));
                }

                var wrapSaveSession = $('div[name=ManageSessionEdit]');
                var arrSession = [];

                if(!wrapSaveSession.hasClass('hide')){
                    $('table tbody tr',wrapSaveSession).each(function () {
                        var name = $('input[name=txtName]',$(this)).val();
                        var startDate = sys_control.get_val_to_ajax($('input[name=txtStartDate]',$(this))) ;
                        var endDate = sys_control.get_val_to_ajax($('input[name=txtEndDate]',$(this))) ;
                        var description = sys_control.get_val_to_ajax($('input[name=txtDescription]',$(this)));
                        var id = $('input[name=hddSessionId]',$(this)).val();
                        arrSession.push({
                            Id:id,
                            Name : name,
                            StartDate : startDate,
                            StartTime : $('input[name=txtStartDateTime]',$(this)).val(),
                            EndDate : endDate,
                            EndTime : $('input[name=txtEndDateTime]',$(this)).val(),
                            Description : description
                        });
                    });
                }
                var model ={
                    Id:sys_control.get_val_to_ajax($('#hiddId',form_edit)),
                    EventCode:sys_control.get_val_to_ajax($('#txtEventCode',form_edit)),
                    Name: sys_control.get_val_to_ajax($('#txtEvent',form_edit)),
                    StartDate:sys_control.get_val_to_ajax($('#txtStartTime',form_edit)),
                    EndDate: sys_control.get_val_to_ajax($('#txtEndTime',form_edit)),
                    IsEnable :sys_control.get_val_to_ajax($('#chkActive',form_edit)),
                    IsDefaultEvent :sys_control.get_val_to_ajax( $('#chkDefaultEvent',form_edit)),
                    Description :sys_control.get_val_to_ajax($('#txtDescription',form_edit)),
                    SMSTitle:sys_control.get_val_to_ajax($('#txtSMSTitle',form_edit)),
                    Deploy : strDeployV,
                    IsBcrmUse :isBcrmUse,
                    IsAllowAttach :sys_control.get_val_to_ajax($('#chkIsAllowAttach',form_edit)),
                    Deadline:strDeadline,
                    RevenueStartDate: strRevenueStartDate,
                    RevenueEndDate : strRevenueEndDate,
                    RevenueCalculation:sys_control.get_val_to_ajax($('#ddlRevenueCalculation',form_edit)),
                    LstSession : arrSession
                };

                $.ajax({
                    url: "/Event/EditAction",
                    contentType:'application/json; charset=utf-8',
                    type: "json",
                    data: JSON.stringify(model),
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $("#btnSave",form_edit).attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        sysmess.error('Có lỗi trong quá trình xử lý ! Liên hệ IT để có thông tin chi tiết .',function(){});
                    },
                    success: function (response) {
                        if(response.Code == ResponseCode.Success){
                            sysmess.success(response.Message,function(){});
                            event_index.display('SearchEvent');
                            event_index.search();
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message,function(){});
                        }
                        else{
                            sysmess.warning(response.Message,function(){});
                        }
                    },
                    complete: function () {
                        $("#btnSave",form_edit).removeAttr("disabled");
                    }
                });
            }
        });

        $('#btnCancel',form_edit).off('click');
        $('#btnCancel',form_edit).on('click',function(){
            $.ajax({
                url: "/Event/Edit",
                type: "html",
                data: {
                    id:sys_control.get_val_to_ajax($('#hiddId',$('#frmEditEvent')))
                },
                dataType: "html",
                async: true,
                beforeSend: function () {
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    sysmess.error('Có lỗi trong quá trình xử lý ! Liên hệ IT để có thông tin chi tiết .',function(){});
                },
                success: function (response) {
                    event_index.display('EditEvent');
                    $('#EditEvent .panel-body').html(response);
                    $('input[type=checkbox]',$('#frmEditEvent')).uniform();
                    event_edit.init();
                },
                complete: function () {
                }
            });
        });

        $('#btnClose',form_edit).off('click');
        $('#btnClose',form_edit).on('click',function(){
            event_index.display('SearchEvent');
        });

        $('#uniform-chkIsBcrmUse',form_edit).off('click').on('click', function () {
            if($('#chkIsBcrmUse',form_edit).prop('checked')){
                $('div[name=UsingInBCRM]',form_edit).each(function () {
                    $(this).removeClass('hide');
                });
            }
            else{
                $('div[name=UsingInBCRM]',form_edit).each(function () {
                    $(this).addClass('hide');
                });
            }
        });

        $('#uniform-chkIsHasSession',form_edit).off('click').on('click', function () {
            if($('#chkIsHasSession',form_edit).prop('checked')){
                $('div[name=ManageSessionEdit]').removeClass('hide');
            }
            else{
                $('div[name=ManageSessionEdit]').addClass('hide');
            }
        });
    },
    reset:function(){
        var form = $('#frmEditEvent');
        $('#txtEvent',form).val('');
        $('#txtStartTime',form).val('');
        $('#txtEndTime',form).val('');
        $('#txtDescription',form).val('');
    },
    validate:function(){
        var form_edit = $('#frmEditEvent');
        var is_valid = true;
        if(!sys_utility.require_text_box($('#txtEventCode',form_edit))){
            sys_control.show_msg($('#txtEventCode',form_edit),'Mã sự kiện bắt buộc nhập !',1);
            is_valid = false;
        }

        if(!sys_utility.require_text_box($('#txtEvent',form_edit))){
            sys_control.show_msg($('#txtEvent',form_edit),'Tên sự kiện bắt buộc nhập !',1);
            is_valid = false;
        }

        if(!sys_utility.require_text_box($('#txtSMSTitle',form_edit))){
            sys_control.show_msg($('#txtSMSTitle',form_edit),'Nội dung SMS bắt buộc nhập !',1);
            is_valid = false;
        }

        if(!sys_utility.require_text_box( $('#txtStartTime',form_edit))){
            sys_control.show_msg($('#txtStartTime',form_edit),'Ngày bắt đầu bắt buộc nhập !',1);
            is_valid = false;
        }
        else{
            if(!sys_utility.is_date($('#txtStartTime',form_edit).val())){
                sys_control.show_msg($('#txtStartTime',form_edit),'Định dạng nhập ngày bắt đầu không đúng',1);
                is_valid = false;
            }
        }

        if(!sys_utility.require_text_box( $('#txtEndTime',form_edit))){
            sys_control.show_msg($('#txtEndTime',form_edit),'Ngày kết thúc bắt buộc nhập !',1);
            is_valid = false;
        }
        else{
            if(!sys_utility.is_date($('#txtEndTime',form_edit).val())){
                sys_control.show_msg($('#txtEndTime',form_edit),'Định dạng nhập ngày kết thúc không đúng !',1);
                is_valid = false;
            }
        }

        if(is_valid){
            if(sys_utility.compare_date($('#txtStartTime',form_edit).val(),$('#txtEndTime',form_edit).val()) > 0){
                sys_control.show_msg($('#txtStartTime',form_edit),'Ngày bắt đầu không được lớn hơn ngày kết thúc',1);
                is_valid = false;
            }
        }

        var chkIsBcrmUse = $('#chkIsBcrmUse',form_edit).prop('checked');
        if(chkIsBcrmUse == true){
            if(!sys_utility.require_text_box($('#txtDeadline',form_edit))){
                sys_control.show_msg($('#txtDeadline',form_edit),'Ngày chốt sự kiện bắt buộc nhập !',1);
                is_valid = false;
            }
            else{
                if(!sys_utility.is_date($('#txtDeadline',form_edit).val())){
                    sys_control.show_msg($('#txtDeadline',form_edit),'Định dạng nhập ngày chốt sự kiện không đúng !',1);
                    is_valid = false;
                }
            }
            if(!sys_utility.require_text_box($('#txtRevenueStartDate',form_edit))){
                sys_control.show_msg($('#txtRevenueStartDate',form_edit),'Ngày bắt đầu tính doanh thu bắt buộc nhập !',1);
                is_valid = false;
            }
            else{
                if(!sys_utility.is_date($('#txtRevenueStartDate',form_edit).val())){
                    sys_control.show_msg($('#txtRevenueStartDate',form_edit),'Định dạng nhập ngày bắt đầu tính doanh thu không đúng !',1);
                    is_valid = false;
                }
            }
            if(!sys_utility.require_text_box($('#txtRevenueEndDate',form_edit))){
                sys_control.show_msg($('#txtRevenueEndDate',form_edit),'Ngày kết thúc tính doanh thu bắt buộc nhập !',1);
                is_valid = false;
            }
            else{
                if(!sys_utility.is_date($('#txtRevenueEndDate',form_edit).val())){
                    sys_control.show_msg($('#txtRevenueEndDate',form_edit),'Định dạng nhập ngày kết thúc tính doanh không đúng !',1);
                    is_valid = false;
                }
            }

            if(is_valid){
                if(sys_utility.compare_date($('#txtRevenueStartDate',form_edit).val(),$('#txtRevenueEndDate',form_edit).val()) > 0){
                    sys_control.show_msg($('#txtRevenueStartDate',form_edit),'Ngày bắt đầu tính doanh thu không được lớn hơn ngày kết thúc tính doanh thu ! ',1);
                    is_valid = false;
                }
            }

            if(!sys_utility.require_select($('#ddlDeployEventEdit',form_edit))){
                sys_control.show_msg($('#ddlDeployEventEdit',form_edit),'Bạn phải chọn thông tin nhóm áp dụng !',1);
                is_valid = false;
            }
        }
        return is_valid;
    }
};
