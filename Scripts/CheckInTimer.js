﻿var checkin_timer = {
    init:function(){
        $(document).ready(function () {
            checkin_timer.register_events();
            checkin_timer.get_events();
            checkin_timer.search();
        });
    },
    register_events:function(){
        var form_search = $("#frmSearchCheckInTimer");
        $("#btnSearch",form_search).off("click");
        $("#btnSearch",form_search).on("click",function(){
            $("#AddNewCheckInTimer").addClass("hide");
            $("#SearchCheckInTimer").removeClass("hide");
            checkin_timer.search();
        });
        $("#btnAddNew",form_search).off("click");
        $("#btnAddNew",form_search).on("click",function(){
            $("#SearchCheckInTimer").addClass("hide");
            $("#AddNewCheckInTimer").removeClass("hide");
            $.ajax({
                url: "/CheckInTimer/AddNew",
                type: "GET",
                data: {},
                dataType: "html",
                async: true,
                beforeSend: function () { },
                error: function (jqXHR, textStatus, errorThrown) { },
                success: function (response) {
                    $(".panel-body", $("#AddNewCheckInTimer")).html(response);
                },
                complete: function () {
                }
            });
        });

        $("#selStatusEvent",form_search).off("change");
        $("#selStatusEvent",form_search).on("change", function () {
            checkin_timer.get_events();
        });
    },
    get_events:function(){
        var form_search = $("#frmSearchCheckInTimer");
        var selectStatus = $("#selStatusEvent",form_search).val();
        $.ajax({
            url: "/CheckInTimer/GetEvents",
            type: "json",
            data: {selectStatus:selectStatus},
            dataType: "json",
            async: true,
            beforeSend: function () { },
            error: function (jqXHR, textStatus, errorThrown) { },
            success: function (response) {
                if(response.Data.length>0)
                {
                    var html = "<option value=\"0\">--Chọn sự kiện--</option>";
                    for(var index=0;index<response.Data.length;index++)
                    {
                        var iiWebCheckin='0';
                        if(response.Data[index].IsInWebCheckIn == true)
                        {
                            iiWebCheckin ='1';
                        }
                        else
                        {
                            iiWebCheckin ='0';
                        }
                        html += "<option value=\""+response.Data[index].Id+"_"+iiWebCheckin+"\">"+response.Data[index].Name+"</option>";
                    }
                    $("#selEvent","#frmSearchCheckInTimer").html(html);
                }
            },
            complete: function () {
            }
        });
    },
    register_grid_events:function(){
        var grid_search = $("#SearchCheckInTimer");
        $("[name=btnEdit]",grid_search).each(function(){
            var id = $(this).attr("eid");
            var isInWebCheckin = false;
            if($(this).attr("iiwc") == '1')
            {
                isInWebCheckin = true;
            }
            else
            {
                isInWebCheckin = false;
            }
            $(this).off("click");
            $(this).on("click",function(){
                $.ajax({
                    url: "/CheckInTimer/EditAction",
                    type: "GET",
                    data: {
                        id:id,
                        isInWebCheckin:isInWebCheckin
                    },
                    dataType: "html",
                    async: true,
                    beforeSend: function () { },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        $("#SearchCheckInTimer").addClass("hide");
                        $("#AddNewCheckInTimer").removeClass("hide");
                        $(".panel-body", $("#AddNewCheckInTimer")).html(response);
                    },
                    complete: function () {
                    }
                });
            });
        });
        $("[name=btnDelete]",grid_search).each(function(){
            var id = $(this).attr("eid");
            $(this).off("click");
            $(this).on("click",function(){
                sysmess.confirm("Bạn có muốn xóa thông tin này !",function(){
                    $.ajax({
                        url: "/CheckInTimer/Delete",
                        type: "POST",
                        data: {id:id},
                        dataType: "json",
                        async: true,
                        beforeSend: function () { },
                        error: function (jqXHR, textStatus, errorThrown) { },
                        success: function (response) {
                            switch (response.Code)
                            {
                                case ResponseCode.Success:
                                    sysmess.success("Bạn xóa thông tin thành công !" ,function(){});
                                    checkin_timer.search();
                                    break;
                                case  ResponseCode.NotPermitted:
                                    sysmess.warning("Bạn không có quyền thực thi chức năng này !" ,function(){});
                                    break;
                            }
                        },
                        complete: function () {
                        }
                    });
                });
            });
        });
    },
    search:function(){
        var form_search = $("#frmSearchCheckInTimer");
        var statusEvent = $("#selStatusEvent",form_search).val();
        var selEvent = $("#selEvent",form_search).val();
        var eventId ='0';
        var isInWebCheckin = false;
        if(selEvent == null || selEvent ==''|| selEvent =='0')
        {
            eventId ='0';
            isInWebCheckin = false;
        }
        else
        {
            var arrV = $("#selEvent",form_search).val().split('_');
            eventId=arrV[0];
            if(arrV[1]=='1')
            {
                isInWebCheckin = true;
            }
            else
            {
                isInWebCheckin = false;
            }
        }
        if(eventId == null   || eventId =='')
        {
            eventId=0;
        }
        $.ajax({
            url: "/CheckInTimer/Search",
            type: "GET",
            data: {
                statusEvent: statusEvent,
                eventId : eventId,
                isInWebCheckin:isInWebCheckin
            },
            dataType: "html",
            async: true,
            beforeSend: function () { },
            error: function (jqXHR, textStatus, errorThrown) { },
            success: function (response) {
                $(".panel-body", $("#SearchCheckInTimer")).html(response);
                checkin_timer.register_grid_events();
            },
            complete: function () {
            }
        });
    },
    display:function(divId){
        if(divId == 'SearchCheckInTimer')
        {
            if($('#SearchCheckInTimer').hasClass('hide'))
            {
                $('#SearchCheckInTimer').removeClass('hide')
            }
        }
        else
        {
            if(!$('#SearchCheckInTimer').hasClass('hide'))
            {
                $('#SearchCheckInTimer').addClass('hide')
            }
        }

        if(divId == 'AddNewCheckInTimer')
        {
            if($('#AddNewCheckInTimer').hasClass('hide'))
            {
                $('#AddNewCheckInTimer').removeClass('hide')
            }
        }
        else
        {
            if(!$('#AddNewCheckInTimer').hasClass('hide'))
            {
                $('#AddNewCheckInTimer').addClass('hide')
            }
        }
    }
};
var checkin_timer_add={
    init:function(){
        $(document).ready(function(){
            $("#txtStartTime").datepicker();
            $("#txtEndTime").datepicker();
            $("#txtStartHour").timepicker({
                minuteStep: 1,
                appendWidgetTo: 'body',
                showSeconds: false,
                showMeridian: false,
                defaultTime: false
            });
            $("#txtEndHour").timepicker({
                minuteStep: 1,
                appendWidgetTo: 'body',
                showSeconds: false,
                showMeridian: false,
                defaultTime: false
            });
            checkin_timer_add.register_events();
        });
    },
    register_events:function(){
        var  form_add = $("#frmAddNewCheckInTimer");
        $("#btnSave",form_add).off("click");
        $("#btnSave",form_add).on("click",function(){
            var ddlEventSelectV = $("#ddlEvent",form_add).val();
            if(ddlEventSelectV == null || ddlEventSelectV == '')
            {
                sysmess.warning('Sự kiện không được để trống!',function(){});
                return;
            }
            var arrV = ddlEventSelectV.split('_');
            var eventId = arrV[0];
            var iInWebCheckin = arrV[1];
            var isInWebCheckin = false;
            if(iInWebCheckin == '1')
            {
                isInWebCheckin = true;
            }
            else
            {
                isInWebCheckin = false;
            }
            var startTime = $("#txtStartTime",form_add).val();
            var startHour = $("#txtStartHour",form_add).val();
            var endTime = $("#txtEndTime",form_add).val();
            var endHour = $("#txtEndHour",form_add).val();
            if(startHour.trim() == null ||startHour.trim() == '')
            {
                startHour = "00:00";
            }
            if(endHour.trim() == null ||endHour.trim() == '')
            {
                endHour = "00:00";
            }
            var strStartTime = startTime + " " + startHour;
            var strEndTime = endTime + " " + endHour;
            // validate date
            if(checkin_timer_add.validate())
            {
                var arrStartDate = startTime.split("/");
                var arrStartTime = startHour.split(":");
                var arrEndDate = endTime.split("/");
                var arrEndTime = endHour.split(":");
                $.ajax({
                    url: "/CheckInTimer/Insert",
                    type: "json",
                    data: {
                        EventId:eventId,
                        StartTime:arrStartDate[1] + "/" + arrStartDate[0] + "/" + arrStartDate[2] + " " + arrStartTime[0] + ":" + arrStartTime[1],
                        EndTime:  arrEndDate[1]   + "/" + arrEndDate[0]   + "/" + arrEndDate[2]   + " " + arrEndTime[0]   + ":" + arrEndTime[1],
                        IsInWebCheckIn:isInWebCheckin
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $("#btnSave",form_add).attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {

                    },
                    success: function (response) {
                        switch (response.Code)
                        {
                            case ResponseCode.Success:
                                checkin_timer.display('SearchCheckInTimer');
                                checkin_timer.search();
                                sysmess.success("Bạn đã thêm mới thông tin thành công !",function(){});
                                break;
                            case  ResponseCode.NotValid:
                                sysmess.warning("Thông tin bạn nhập vào không đúng !",function(){});
                                break;
                            case  ResponseCode.NotPermitted:
                                sysmess.warning("Bạn không có quyền thực thi chức năng này !",function(){});
                                break;
                            case  ResponseCode.ErrorExist:
                                sysmess.warning("Đã có thông tin cho sự kiện này !",function(){});
                                break;
                        }
                    },
                    complete: function () {
                        $("#btnSave",form_add).removeAttr("disabled");
                    }
                });
            }
        });

        $("#btnCancel",form_add).off("click");
        $("#btnCancel",form_add).on("click",function(){
            $("#txtStartTime",form_add).val("");
            $("#txtStartHour",form_add).val("");
            $("#txtEndTime",form_add).val("");
            $("#txtEndHour",form_add).val("");
        });

        $("#btnClose",form_add).off("click");
        $("#btnClose",form_add).on("click",function(){
            $("#AddNewCheckInTimer").addClass("hide");
            $("#SearchCheckInTimer").removeClass("hide");
        });
    },
    validate:function(){
        var  form_add = $("#frmAddNewCheckInTimer");
        var startTime = $("#txtStartTime",form_add).val();
        var startHour = $("#txtStartHour",form_add).val();
        var endTime = $("#txtEndTime",form_add).val();
        var endHour = $("#txtEndHour",form_add).val();

        if(startTime == null || startTime == '')
        {
            sysmess.warning("Thời gian bắt đầu bắt buộc nhập",function(){});
            return false;
        }
        if(endTime == null || endTime == '')
        {
            sysmess.warning("Thời gian kết thúc bắt buộc nhập",function(){});
            return false;
        }
        if(startHour.trim() == null || startHour.trim() == '')
        {
            startHour = "00:00";
        }
        if(endHour.trim() == null || endHour.trim() == '')
        {
            endHour = "00:00";
        }
        if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(startTime))
        {
            sysmess.warning("Định dạng nhập thời gian bắt đầu không đúng",function(){});
            return false;
        }
        if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(endTime))
        {
            sysmess.warning("Định dạng nhập thời gian kết thúc không đúng",function(){});
            return false;
        }
        if(!/^\d{2}\:\d{2}$/.test(startHour))
        {
            sysmess.warning("Định dạng nhập thời điểm bắt đầu không đúng",function(){});
            return false;
        }
        if(!/^\d{2}\:\d{2}$/.test(endHour))
        {
            sysmess.warning("Định dạng nhập thời điểm kết thúc không đúng",function(){});
            return false;
        }
        var arrStartDate = startTime.split("/");
        var arrStartTime = startHour.split(":");
        var dateStart = new Date(parseInt(arrStartDate[2]),parseInt(arrStartDate[1]),parseInt(arrStartDate[0]),
                                 parseInt(arrStartTime[0]),parseInt(arrStartTime[1]));
        var arrEndDate = endTime.split("/");
        var arrEndTime = endHour.split(":");
        var dateEnd = new Date(parseInt(arrEndDate[2]),parseInt(arrEndDate[1]),parseInt(arrEndDate[0]),
                                parseInt(arrEndTime[0]),parseInt(arrEndTime[1]));

        if(dateStart >= dateEnd)
        {
            sysmess.warning("Thời gian bắt đầu không được lớn hơn thời gian kết thúc",function(){});
            return false;
        }
        return true;
    }
};
var checkin_timer_edit = {
    init:function(){
        $(document).ready(function(){
            $("#txtStartTime").datepicker();
            $("#txtEndTime").datepicker();
            $("#txtStartHour").timepicker({
                minuteStep: 1,
                appendWidgetTo: 'body',
                showSeconds: false,
                showMeridian: false,
                defaultTime: false
            });
            $("#txtEndHour").timepicker({
                minuteStep: 1,
                appendWidgetTo: 'body',
                showSeconds: false,
                showMeridian: false,
                defaultTime: false
            });
            checkin_timer_edit.register_events();
        });
    },
    register_events:function(){
        var  form_edit = $("#frmEditCheckInTimer");
        $("#btnSave",form_edit).off("click");
        $("#btnSave",form_edit).on("click",function(){
            var id = $("#hdfId",form_edit).val();
            var ddlEventSelectV = $("#ddlEvent",form_edit).val();
            if(ddlEventSelectV == null || ddlEventSelectV =='')
            {
                sysmess.warning('Thông tin sự kiện không được trống !',function(){});
                return;
            }

            var arrV = ddlEventSelectV.split('_');
            var eventId = arrV[0];
            var iInWebCheckin = arrV[1];
            var isInWebCheckin = false;
            if(iInWebCheckin == '1')
            {
                isInWebCheckin = true;
            }
            else
            {
                isInWebCheckin = false;
            }
            var startTime = $("#txtStartTime",form_edit).val();
            var startHour = $("#txtStartHour",form_edit).val();
            var endTime = $("#txtEndTime",form_edit).val();
            var endHour = $("#txtEndHour",form_edit).val();
            if(startHour.trim() == null ||startHour.trim() == '')
            {
                startHour = "00:00";
            }
            if(endHour.trim() == null ||endHour.trim() == '')
            {
                endHour = "00:00";
            }
            var strStartTime = startTime + " " + startHour;
            var strEndTime = endTime + " " + endHour;
            // validate date
            if(checkin_timer_edit.validate())
            {
                var arrStartDate = startTime.split("/");
                var arrStartTime = startHour.split(":");
                var arrEndDate = endTime.split("/");
                var arrEndTime = endHour.split(":");
                $.ajax({
                    url: "/CheckInTimer/Edit",
                    type: "json",
                    data: {
                        Id:id
                        ,IsInWebCheckIn:isInWebCheckin
                        ,EventId:eventId
                        ,StartTime:arrStartDate[1] + "/" + arrStartDate[0] + "/" + arrStartDate[2] + " " + arrStartTime[0] + ":" + arrStartTime[1]
                        ,EndTime:  arrEndDate[1]   + "/" + arrEndDate[0]   + "/" + arrEndDate[2]   + " " + arrEndTime[0]   + ":" + arrEndTime[1]
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $("#btnSave",form_edit).attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        switch (response.Code)
                        {
                            case ResponseCode.Success:
                                checkin_timer.display('SearchCheckInTimer');
                                checkin_timer.search();
                                sysmess.success("Bạn đã sửa thông tin thành công",function(){});
                                break;
                            case  ResponseCode.NotValid:
                                sysmess.warning("Thông tin bạn nhập vào không đúng !",function(){});
                                break;
                            case  ResponseCode.NotPermitted:
                                sysmess.warning("Bạn không có quyền thực thi chức năng này !" ,function(){});
                                break;
                            case  ResponseCode.ErrorExist:
                                sysmess.warning("Đã có thông tin cho sự kiện này !",function(){});
                                break;
                        }
                    },
                    complete: function () {
                        $("#btnSave",form_edit).removeAttr("disabled");
                    }
                });
            }
        });

        $("#btnCancel",form_edit).off("click");
        $("#btnCancel",form_edit).on("click",function(){
            $("#txtStartTime",form_edit).val("");
            $("#txtStartHour",form_edit).val("");
            $("#txtEndTime",form_edit).val("");
            $("#txtEndHour",form_edit).val("");
        });

        $("#btnClose",form_edit).off("click");
        $("#btnClose",form_edit).on("click",function(){
            $("#AddNewCheckInTimer").addClass("hide");
            $("#SearchCheckInTimer").removeClass("hide");
        });
    },
    validate:function(){
        var  form_edit = $("#frmEditCheckInTimer");
        var startTime = $("#txtStartTime",form_edit).val();
        var startHour = $("#txtStartHour",form_edit).val();
        var endTime = $("#txtEndTime",form_edit).val();
        var endHour = $("#txtEndHour",form_edit).val();

        if(startTime == null || startTime == '')
        {
            sysmess.warning("Thời gian bắt đầu bắt buộc nhập !",function(){});
            return false;
        }
        if(endTime == null || endTime == '')
        {
            sysmess.warning("Thời gian kết thúc bắt buộc nhập",function(){});
            return false;
        }
        if(startHour.trim() == null || startHour.trim() == '')
        {
            startHour = "00:00";
        }
        if(endHour.trim() == null || endHour.trim() == '')
        {
            endHour = "00:00";
        }
        if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(startTime))
        {
            sysmess.warning("Định dạng nhập thời gian bắt đầu không đúng",function(){});
            return false;
        }
        if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(endTime))
        {
            sysmess.warning("Định dạng nhập thời gian kết thúc không đúng",function(){});
            return false;
        }
        if(!/^\d{2}\:\d{2}$/.test(startHour))
        {
            sysmess.warning("Định dạng nhập thời điểm bắt đầu không đúng",function(){});
            return false;
        }
        if(!/^\d{2}\:\d{2}$/.test(endHour))
        {
            sysmess.warning("Định dạng nhập thời điểm kết thúc không đúng",function(){});
            return false;
        }
        var arrStartDate = startTime.split("/");
        var arrStartTime = startHour.split(":");
        var dateStart = new Date(parseInt(arrStartDate[2]),parseInt(arrStartDate[1]),parseInt(arrStartDate[0]),
            parseInt(arrStartTime[0]),parseInt(arrStartTime[1]));
        var arrEndDate = endTime.split("/");
        var arrEndTime = endHour.split(":");
        var dateEnd = new Date(parseInt(arrEndDate[2]),parseInt(arrEndDate[1]),parseInt(arrEndDate[0]),
            parseInt(arrEndTime[0]),parseInt(arrEndTime[1]));

        if(dateStart >= dateEnd)
        {
            sysmess.warning("Thời gian bắt đầu không được lớn hơn thời gian kết thúc !",function(){});
            return false;
        }
        return true;
    }
};