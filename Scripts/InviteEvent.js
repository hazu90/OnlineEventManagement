﻿var invite_event_index = {
    init: function () {
        $(document).ready(function () {
            invite_event_index.register_events();
            invite_event_index.register_grid_events();
            invitee_create.init();
            invite_event_index.search();
            $("#inviteEventForm").off('keydown');
            $("#inviteEventForm").keydown(invite_event_index.hitting_enter);
            var sV = $('#selEvent',$("#frmSearchInviteEvent")).val();
            if(sV == null || sV ==''){
                invite_event_index.change_filter_condition(true);
            }
            else{
                var arrV = sV.split('_');
                if(arrV[1] == '1'){
                    $.ajax({
                        url: "/Session/GetByEventId",
                        type: "GET",
                        data: {
                            eventId:arrV[0]
                        },
                        dataType: "json",
                        async: true,
                        beforeSend: function () {
                        },
                        error: function (jqXHR, textStatus, errorThrown) { },
                        success: function (response) {
                            invite_event_index.change_filter_condition(true);
                            if(response.Data ==null || response.Data.length == 0){
                                $('#selSessionId').html('<option value="0">--Chọn phiên tham dự--</option>');
                                $('#selSessionId').parent().addClass('hide');
                            }
                            else{
                                var html = '<option value="0">--Chọn phiên tham dự--</option>';
                                for(var index = 0;index < response.Data.length;index++){
                                    html +='<option value="'+ response.Data[index].Id +'">'+ response.Data[index].Name +'</option>';
                                }
                                $('#selSessionId').html(html);
                                $('#selSessionId').parent().removeClass('hide');
                            }
                        },
                        complete: function () {}
                    });
                }
                else
                {
                    invite_event_index.change_filter_condition(false);
                }
            }
            $('#txtSearchText').focus();
        });
    },
    hitting_enter:function(e){
        if(e.which == 13) {
            e.preventDefault();
            $(this).blur();
            invite_event_index.search();
        }
    },
    register_events: function () {
        var form_search = $("#frmSearchInviteEvent");
        $("#btnSearch", form_search).off("click");
        $("#btnSearch", form_search).on("click", function () {
            invite_event_index.display('SearchInviteEvent');
            invite_event_index.search();
        });

        $("#btnAddAnonymous",form_search).off("click");
        $("#btnAddAnonymous",form_search).on("click",function(){
            invite_event_index.display('AddAnonymousInfo');
            $.ajax({
                url: "/InviteEvent/AddAnonymousInfo",
                type: "GET",
                data: {
                },
                dataType: "html",
                async: true,
                beforeSend: function () { },
                error: function (jqXHR, textStatus, errorThrown) { },
                success: function (response) {
                    $(".panel-body",$("#AddAnonymousInfo")).html(response);
                },
                complete: function () {
                }
            });
        });

        $('#selEvent',form_search).off('change');
        $('#selEvent',form_search).on('change',function(){
            var sV = $(this).val();
            if(sV == null || sV =='')
            {
                invite_event_index.change_filter_condition(true);
            }
            else
            {
                var arrV = sV.split('_');
                if(arrV[1]== '1')
                {
                    $.ajax({
                        url: "/Session/GetByEventId",
                        type: "GET",
                        data: {
                            eventId:arrV[0]
                        },
                        dataType: "json",
                        async: true,
                        beforeSend: function () {
                        },
                        error: function (jqXHR, textStatus, errorThrown) { },
                        success: function (response) {
                            invite_event_index.change_filter_condition(true);
                            if(response.Data ==null || response.Data.length == 0){
                                $('#selSessionId').html('<option value="0">--Chọn phiên tham dự--</option>');
                                $('#selSessionId').parent().addClass('hide');
                            }
                            else{
                                var html = '<option value="0">--Chọn phiên tham dự--</option>';
                                for(var index = 0;index < response.Data.length;index++){
                                    html +='<option value="'+ response.Data[index].Id +'">'+ response.Data[index].Name +'</option>';
                                }
                                $('#selSessionId').html(html);
                                $('#selSessionId').parent().removeClass('hide');
                            }
                        },
                        complete: function () {
                        }
                    });

                }
                else
                {
                    invite_event_index.change_filter_condition(false);
                }
            }
        });

        $('#txtSearchText',form_search).off('focus');
        $('#txtSearchText',form_search).on('focus',function(){
            $('#txtInviteeCode',form_search).val('');
            $('#txtInviteeCodeInWebCheckin',form_search).val('');
            $('#txtCode',form_search).val('');
        });

        $('#txtInviteeCode',form_search).off('focus');
        $('#txtInviteeCode',form_search).on('focus',function(){
            $('#txtSearchText',form_search).val('');
            $('#txtCode',form_search).val('');
        });

        $('#txtCode',form_search).off('focus');
        $('#txtCode',form_search).on('focus',function(){
            $('#txtSearchText',form_search).val('');
            $('#txtInviteeCode',form_search).val('');
            $('#txtInviteeCodeInWebCheckin',form_search).val('');
        });

        $('#txtInviteeCodeInWebCheckin',form_search).off('focus');
        $('#txtInviteeCodeInWebCheckin',form_search).on('focus',function(){
            $('#txtSearchText',form_search).val('');
            $('#txtCode',form_search).val('');
        });
    },
    register_grid_events: function () {
        var grid_search = $("#tablInviteEvent");
        $('.dropdown-toggle').dropdown();
        $('[name=btnEditWebCheckin]',grid_search).each(function(){
            $(this).off('click');
            $(this).on('click',function(){
                var inviteId = $(this).attr('uid');
                $.ajax({
                    url: "/InviteEvent/EditInviteeWebCheckin",
                    type: "GET",
                    data: {
                        inviteEventId:inviteId
                    },
                    dataType: "html",
                    async: true,
                    beforeSend: function () { },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        invite_event_index.display('EditInviteeInWebCheckin');
                        $(".panel-body",$("#EditInviteeInWebCheckin")).html(response);
                        invitee_edit_in_webcheckin.init();
                    },
                    complete: function () {
                    }
                });
            });
        });
        $('[name=btnDeleteWebCheckin]',grid_search).each(function(){
            $(this).off('click');
            $(this).on('click',function(){
                var inviteId = $(this).attr('uid');
                sysmess.confirm('Bạn có chắc muốn xóa thông tin này !',function(){
                    $.ajax({
                        url: "/InviteEvent/DeleteInWebCheckin",
                        type: "json",
                        data: {
                            inviteEventId:inviteId
                        },
                        dataType: "json",
                        async: true,
                        beforeSend: function () { },
                        error: function (jqXHR, textStatus, errorThrown) { },
                        success: function (response) {
                            if(response.Code == ResponseCode.Success)
                            {
                                sysmess.success(response.Message);
                                invite_event_index.search();
                            }
                            else if(response.Code == ResponseCode.Error)
                            {
                                sysmess.error(response.Message);
                            }
                            else
                            {
                                sysmess.warning(response.Message);
                            }
                        },
                        complete: function () {
                        }
                    });
                });
            });
        });
        $("[name=btnCheckInWebCheckin]", grid_search).each(function () {
            var inviteEventId = $(this).attr("uie");
            var eventId = $(this).attr("eid");
            var sessionId = $('#selSessionId',$('#frmSearchInviteEvent')).val();
            $(this).off("click");
            $(this).on("click", function () {
                $.ajax({
                    url: "/InviteEvent/AccompanyInviteeInWebChecin",
                    type: "GET",
                    data: {
                        EventId:eventId,
                        InviteEventId: inviteEventId,
                        SessionId:sessionId
                    },
                    dataType: "html",
                    async: true,
                    beforeSend: function () { },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        $(".info",$("#divAddCheckinNum")).html(response);
                        show_dialog_checkin.show($('#frmAddAccompanyInviteeInWebCheckin'),function(){
                            checkin_info_webcheckin.init();
                        });
                    },
                    complete: function () {
                    }
                });
            });
        });
        $("[name=btnCancelInWebCheckin]", grid_search).each(function () {
            var inviteEventId = $(this).attr("uie");
            var eventId = $(this).attr("eid");
            $(this).off("click");
            $(this).on("click", function () {
                sysmess.confirm("Bạn có muốn hủy thông tin checkin này !",function(){
                    $.ajax({
                        url: "/InviteEvent/CancelWebCheckin",
                        type: "POST",
                        data: {
                            eventId:eventId,
                            inviteEventId: inviteEventId
                        },
                        dataType: "json",
                        async: true,
                        beforeSend: function () { },
                        error: function (jqXHR, textStatus, errorThrown) { },
                        success: function (response) {
                            switch (response.Code) {
                                case ResponseCode.Success:
                                    sysmess.success("Bạn đã hủy checkin thông tin thành công !",function(){});
                                    var pageIndex = 0;
                                    $(".dataTables_paginate .active a").each(
                                        function () {
                                            if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                                pageIndex = parseInt($(this).html().trim());
                                            }
                                        }
                                    );
                                    invite_event_index.search(pageIndex);
                                    break;
                                case ResponseCode.NotPermitted:
                                    sysmess.warning("Bạn không có quyền thực thi chức năng này !",function(){});
                                    break;
                                default:
                                    sysmess.error("Có lỗi trong quá trình xử lý !" ,function(){});
                                    break;
                            }
                        },
                        complete: function () {

                        }
                    });
                });
            });
        });
        $("[name=btnAccompanyInWebCheckin]", grid_search).each(function(){
            var eventId = $(this).attr("eid");
            var inviteEventId = $(this).attr("uie");
            var accompanyInvitee = $(this).attr("nai");
            $(this).off("click");
            $(this).on("click", function () {
                invite_event_index.display('AccompanyInvitee');
                $.ajax({
                    url: "/InviteEvent/AddAccompanyMemberWebCheckin",
                    type: "GET",
                    data: {
                        EventId:eventId,
                        InviteEventId: inviteEventId,
                        AccompanyInvitee:accompanyInvitee
                    },
                    dataType: "html",
                    async: true,
                    beforeSend: function () { },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        $(".panel-body",$("#AccompanyInvitee")).html(response);
                    },
                    complete: function () {
                    }
                });
            });
        });
        $('a[name=btnChangeSession]',grid_search).each(function () {
            $(this).off('click').on('click', function () {
                var inviteeId = $(this).attr('uid');
                var eventId  = $(this).attr('eid');
                $.ajax({
                    url: "/InviteEvent/ChangeSession",
                    type: "GET",
                    data: {
                        eventId:eventId,
                        inviteEventId: inviteeId
                    },
                    dataType: "html",
                    async: true,
                    beforeSend: function () { },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        $("#wrapChangeSession").html(response);
                        show_dialog_checkin.show($('#frmChangeSession'),function(){
                            session_change.old_scroll_position = $(window).scrollTop();
                            session_change.init();
                        });
                    },
                    complete: function () {
                    }
                });
            });
        });
        $('a[name=btnCheckinDetail]',grid_search).each(function () {
            $(this).off('click').on('click', function () {
                var inviteeId = $(this).attr('uid');
                var eventId  = $(this).attr('eid');
                $.ajax({
                    url: "/InviteEvent/CheckinDetail",
                    type: "GET",
                    data: {
                        eventId:eventId,
                        inviteEventId: inviteeId
                    },
                    dataType: "html",
                    async: true,
                    beforeSend: function () { },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        $("#wrapCheckinDetail").html(response);
                        show_dialog_checkin.show($('#frmCheckinDetail'),function(){
                        });
                    },
                    complete: function () {
                    }
                });
            });
        });
        $('a[name=lnkRemove]',grid_search).each(function () {
           $(this).off('click').on('click', function () {
               var inviteeId = $(this).attr('ieid');
               var sessionId = $(this).attr('ssid');
               sysmess.confirm('Bạn có muốn hủy thông tin checkin này !', function () {
                   $.ajax({
                       url: "/InviteEvent/RejectSessionCheckin",
                       type: "POST",
                       data: {
                           inviteeId:inviteeId,
                           sessionId: sessionId
                       },
                       dataType: "json",
                       async: true,
                       beforeSend: function () { },
                       error: function (jqXHR, textStatus, errorThrown) { },
                       success: function (response) {
                           if(response.Code == ResponseCode.Success){
                                sysmess.success(response.Message);
                               var pageIndex = 0;
                               $(".dataTables_paginate .active a").each(
                                   function () {
                                       if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                           pageIndex = parseInt($(this).html().trim());
                                       }
                                   }
                               );
                               invite_event_index.search(pageIndex);
                           }
                           else if(response.Code == ResponseCode.Error){
                               sysmess.error(response.Message);
                           }
                           else{
                               sysmess.warning(response.Message);
                           }
                       },
                       complete: function () {
                       }
                   });
               });
           }) ;
        });
    },
    search: function (page) {
        var form_search = $("#frmSearchInviteEvent");
        var searchText = $("#txtSearchText", form_search).val();
        var selectedEvent = $("#selEvent", form_search).val();
        var eventId = 0;
        var isInWebCheckin = false;
        if(selectedEvent == null || selectedEvent ==''){
            eventId = '0';
            isInWebCheckin = false;
        }
        else{
            var arrV = selectedEvent.split('_');
            eventId = arrV[0];
            if(arrV[1] == '1'){
                isInWebCheckin = true;
            }
            else{
                isInWebCheckin = false;
            }
        }
        var isCheckIn = true;
        if ($("#selIsCheckIn", form_search).val() == null || $("#selIsCheckIn", form_search).val() == '' ) {
            isCheckIn = null;
        }
        else
        {
            if($("#selIsCheckIn", form_search).val()=='0'){
                isCheckIn = false;
            }
            else{
                isCheckIn = true;
            }
        }
        var importantanceLevel  = $("#selImportantanceLevel",form_search).val();
        var customerType        = $("#selCustomerType",form_search).val();
        var inviteeCode         = $("#txtInviteeCode",form_search).val();
        var inviteeCodeInWebCheckin = $("#txtInviteeCodeInWebCheckin",form_search).val();
        if(importantanceLevel == null || importantanceLevel == ''){
            importantanceLevel = null;
        }
        if(customerType == null || customerType == ''){
            customerType = null;
        }
        var code = $('#txtCode',form_search).val();

        if(code !=null && code !=''){
            code = code.trim();
        }

        $.ajax({
            url: "/InviteEvent/Search",
            type: "POST",
            data: {
                TextSearch: searchText,
                Code:code,
                SessionId:$('#selSessionId',form_search).val(),
                EventId: eventId,
                IsCheckIn:isCheckIn,
                ImportanceLevel:importantanceLevel,
                CustomerType:customerType,
                InviteeCode:inviteeCode,
                IsInWebCheckin:isInWebCheckin,
                InviteeCodeInWebCheckin:inviteeCodeInWebCheckin,
                PageIndex: page,
                PageSize: 25
            },
            dataType: "html",
            async: true,
            beforeSend: function () {
                $("#btnSearch", form_search).attr("disabled","disabled");
            },
            error: function (jqXHR, textStatus, errorThrown) { },
            success: function (response) {
                $(".panel-body", $("#SearchInviteEvent")).html(response);
                invite_event_index.register_grid_events();
            },
            complete: function () {
                $("#btnSearch", form_search).removeAttr("disabled");
            }
        });
    },
    display:function(divId){
        if(divId == 'SearchInviteEvent'){
            if($('#SearchInviteEvent').hasClass('hide')){
                $('#SearchInviteEvent').removeClass('hide')
            }
        }
        else{
            if(!$('#SearchInviteEvent').hasClass('hide')){
                $('#SearchInviteEvent').addClass('hide');
            }
        }

        if(divId == 'AddAnonymousInfo'){
            if($('#AddAnonymousInfo').hasClass('hide')){
                $('#AddAnonymousInfo').removeClass('hide');
            }
        }
        else{
            if(!$('#AddAnonymousInfo').hasClass('hide')){
                $('#AddAnonymousInfo').addClass('hide');
            }
        }

        if(divId == 'AccompanyInvitee'){
            if($('#AccompanyInvitee').hasClass('hide')){
                $('#AccompanyInvitee').removeClass('hide');
            }
        }
        else{
            if(!$('#AccompanyInvitee').hasClass('hide')){
                $('#AccompanyInvitee').addClass('hide');
            }
        }
        if(divId == 'AddInvitee'){
            if($('#AddInvitee').hasClass('hide')){
                $('#AddInvitee').removeClass('hide');
            }
        }
        else{
            if(!$('#AddInvitee').hasClass('hide')){
                $('#AddInvitee').addClass('hide');
            }
        }
        if(divId == 'EditInviteeInWebCheckin'){
            if($('#EditInviteeInWebCheckin').hasClass('hide')){
                $('#EditInviteeInWebCheckin').removeClass('hide');
            }
        }
        else{
            if(!$('#EditInviteeInWebCheckin').hasClass('hide')){
                $('#EditInviteeInWebCheckin').addClass('hide');
            }
        }
    },
    change_filter_condition:function(isInWebCheckin){
        var form_search = $("#frmSearchInviteEvent");
        if(isInWebCheckin)
        {
            if(!$('#txtInviteeCode',form_search).parent().hasClass('hide'))
            {
                $('#txtInviteeCode',form_search).parent().addClass('hide');
            }
            if($('#txtInviteeCodeInWebCheckin',form_search).parent().hasClass('hide'))
            {
                $('#txtInviteeCodeInWebCheckin',form_search).parent().removeClass('hide');
            }
            if(!$('#selImportantanceLevel',form_search).parent().hasClass('hide'))
            {
                $('#selImportantanceLevel',form_search).parent().addClass('hide');
            }
            var html ='<option value="">- Lựa chọn loại khách -</option>';
                html+='<option value="0">Khách chính</option>';
                html+='<option value="2">Khách vãng lai</option>';
            $('#selCustomerType',form_search).html(html);
            $('#selCustomerType',form_search).parent().removeClass('col-md-4');
            $('#selCustomerType',form_search).parent().addClass('col-md-3');
        }
        else
        {
            if($('#txtInviteeCode',form_search).parent().hasClass('hide'))
            {
                $('#txtInviteeCode',form_search).parent().removeClass('hide');
            }
            if(!$('#txtInviteeCodeInWebCheckin',form_search).parent().hasClass('hide'))
            {
                $('#txtInviteeCodeInWebCheckin',form_search).parent().addClass('hide');
            }
            if($('#selImportantanceLevel',form_search).parent().hasClass('hide'))
            {
                $('#selImportantanceLevel',form_search).parent().removeClass('hide');
            }

            var html ='<option value="">- Lựa chọn loại khách -</option>';
            html    +='<option value="0">Khách chính</option>';
            html    +='<option value="1">Khách đính kèm</option>';
            html    +='<option value="2">Khách vãng lai</option>';
            $('#selCustomerType',form_search).html(html);
            $('#selCustomerType',form_search).parent().removeClass('col-md-3');
            $('#selCustomerType',form_search).parent().addClass('col-md-4');
        }
    }
};
var invite_event_anonymous_invitee = {
    init:function(){
        $(document).ready(function(){
            invite_event_anonymous_invitee.register_events();
            $("#txtCustomerBirthday",$("#frmAddAnonymousInvitee")).datepicker();
            var sV = $('#ddlEventId',$("#frmAddAnonymousInvitee")).val();
            if(sV == null || sV ==''){
                invite_event_anonymous_invitee.change_filter_condition(true);
            }
            else{
                var arrV = sV.split('_');
                if(arrV[1]== '1'){
                    session_manage.get_by_eventid(arrV[0],$('#dllSession',$("#frmAddAnonymousInvitee")),$('div[name=IsHadSession]',$("#frmAddAnonymousInvitee")) );
                    invite_event_anonymous_invitee.change_filter_condition(true);
                }
                else{
                    invite_event_anonymous_invitee.change_filter_condition(false);
                }
            }

        });
    },
    register_events:function(){
        var form_add = $("#frmAddAnonymousInvitee");
        $("#btnSave",form_add).off("click");
        $("#btnSave",form_add).on("click",function(){
            var form_add = $("#frmAddAnonymousInvitee");
            sys_utility.clear_errors(form_add);
            if(invite_event_anonymous_invitee.validate()){
                var arrV = $("#ddlEventId",form_add).val().split('_');
                var eventId = arrV[0];
                var isInWebCheckIn = false;
                if(arrV[1] == '1'){
                    isInWebCheckIn = true;
                }
                else{
                    isInWebCheckIn = false;
                }
                var customerName = $("#txtCustomerName",form_add).val();
                var phoneNumber = $("#txtCustomerPhone",form_add).val();
                var birthday = $("#txtCustomerBirthday",form_add).val();
                var arrBirthday =[];
                if(birthday != null && birthday!=''){
                    if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(birthday)){
                        sysmess.warning("Định dạng nhập ngày sinh không đúng !",function(){});
                        return;
                    }
                    arrBirthday = birthday.split("/");
                }
                var birthdayInfo = "";
                if(arrBirthday.length == 3)
                {
                    birthdayInfo = arrBirthday[1] + "/" + arrBirthday[0] + "/" + arrBirthday[2];
                }
                else
                {
                    birthdayInfo = null;
                }
                var sex = $("#ddlCustomerSex",form_add).val();
                var isMan = null;
                if(sex != null && sex != ''){
                    if(sex == '0'){
                        isMan = false;
                    }
                    else{
                        isMan =  true;
                    }
                }
                var customerEmail ="";
                if(isInWebCheckIn){
                    customerEmail = $("#txtCustomerEmailCheckIn",form_add).val().trim();
                }
                else{
                    customerEmail = $("#txtCustomerEmail",form_add).val();
                }
                var customerRevenue = $("#txtCustomerRevenue",form_add).val();
                var customerType ='';
                if(isInWebCheckIn)
                {
                    customerType = $("#dllCustomerTypeCheckIn",form_add).val();
                }
                else
                {
                    customerType = $("#dllCustomerType",form_add).val();
                }
                var abilityAttend = $("#dllAbilityAttend",form_add).val();
                var companyName ='';
                if(isInWebCheckIn)
                {
                    companyName = $("#txtCompanyNameCheckIn",form_add).val();
                }
                else
                {
                    companyName = $("#txtCompanyName",form_add).val();
                }
                var positionName = $('#txtPositionNameCheckIn',form_add).val();
                var placeOfAttendEvent = $('#dllPlaceOfAttendEvent',form_add).val();
                var registeredSource = $('#dllRegisteredSource',form_add).val();

                var customerPosition = $("#dllCustomerPosition",form_add).val();
                var companySize = $("#dllCompanySize",form_add).val();
                var  companyAddress = $("#txtCompanyAddress",form_add).val();
                var customerVip = $("#dllCustomerVip",form_add).val();
                var ticketAddress = $("#txtTicketAddress",form_add).val();
                var note = $("#txtNote",form_add).val();
                var accompanyMember = $("#txtAccompanyMember",form_add).val().trim();

                var sessions = '';
                if(!$('div[name=IsHadSession]',form_add).hasClass('hide') ){
                    sessions = sys_control.get_val_to_ajax($('#dllSession',form_add));
                }

                $.ajax({
                    url: "/InviteEvent/Insert",
                    type: "json",
                    data: {
                        EventId:eventId,
                        CustomerId:0,
                        CustomerName:customerName,
                        CustomerPhone:phoneNumber,
                        CustomerBirthday:birthdayInfo,
                        CustomerSex:isMan,
                        CustomerEmail:customerEmail,
                        CustomerRevenue:customerRevenue,
                        CustomerType:customerType,
                        CustomerPosition:customerPosition,
                        CompanySize:companySize,
                        CompanyName:companyName,
                        CompanyAddress:companyAddress,
                        TicketAddress:ticketAddress,
                        AbilityAttend:abilityAttend,
                        Note:note,
                        CustomerVip:customerVip,
                        AccompanyMember:accompanyMember,
                        IsInWebCheckin:isInWebCheckIn,
                        PositionName:positionName,
                        PlaceOfAttendEvent:placeOfAttendEvent,
                        RegisteredSource:registeredSource,
                        IntroduceUser:$('#txtIntroduceUser',form_add).val(),
                        Sessions:sessions
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $("#btnSave",form_add).attr("disabled","disabled",function(){
                            
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        sysmess.error("Có lỗi trong quá trình xử lý", function () {});
                    },
                    success: function (response) {
                        if(response.Code == ResponseCode.Success)
                        {
                            sysmess.success(response.Message,function(){});
                            invite_event_anonymous_invitee.refresh_form();
                        }
                        else if(response.Code == ResponseCode.Error)
                        {
                            sysmess.error(response.Message,function(){});
                        }
                        else
                        {
                            sysmess.warning(response.Message,function(){});
                        }
                    },
                    complete: function () {
                        $("#btnSave",form_add).removeAttr("disabled");
                    }
                });
            }
        });
        $("#btnClose",form_add).off("click");
        $("#btnClose",form_add).on("click",function(){
            if($("#SearchInviteEvent").hasClass("hide"))
            {
                $("#SearchInviteEvent").removeClass("hide");
            }
            if(!$("#AddAnonymousInfo").hasClass("hide"))
            {
                $("#AddAnonymousInfo").addClass("hide");
            }
            if(!$("#AccompanyInvitee").hasClass("hide"))
            {
                $("#AccompanyInvitee").addClass("hide");
            }
            invite_event_index.search();
        });
        $("#btnCancel",form_add).off("click");
        $("#btnCancel",form_add).on("click",function(){
            invite_event_anonymous_invitee.refresh_form();
        });

        $('#ddlEventId',form_add).off('change');
        $('#ddlEventId',form_add).on('change',function(){
            var sV = $(this).val();
            if(sV ==null || sV ==''){
                invite_event_anonymous_invitee.change_filter_condition(true);
            }
            else{
                var arrV = sV.split('_');
                if(arrV[1]== '1'){
                    invite_event_anonymous_invitee.change_filter_condition(true);
                    session_manage.get_by_eventid(arrV[0],$('#dllSession',form_add),$('div[name=IsHadSession]',form_add));
                }
                else{
                    invite_event_anonymous_invitee.change_filter_condition(false);
                }
            }
        });

        $('#txtCustomerPhone',form_add).off('keypress');
        $('#txtCustomerPhone',form_add).on('keypress',function(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        });
    },
    load_views:function(){
        $.ajax({
            url: "/InviteEvent/GetCustomerInfo",
            type: "json",
            data: {},
            dataType: "json",
            async: true,
            beforeSend: function () { },
            error: function (jqXHR, textStatus, errorThrown) { },
            success: function (response) {
                if(response.Data !=null )
                {
                    var form_add = $("#frmAddAnonymousInvitee");
                    var html="";
                    if(response.Data.LstEvents.length >0)
                    {
                        for(var index=0;index <response.Data.LstEvents.length;index++)
                        {
                            html += '<option value="'+ response.Data.LstEvents[index].Id +'" >'+response.Data.LstEvents[index].Name +'</option>';
                        }
                    }
                    $("#ddlEventId",form_add).html(html);
                    html="";
                    if(response.Data.ListAbilityAttend.length >0)
                    {
                        for(var index=0;index <response.Data.ListAbilityAttend.length;index++)
                        {
                            html += '<option value="'+ response.Data.ListAbilityAttend[index].Key +'" >'+response.Data.ListAbilityAttend[index].Value +'</option>';
                        }
                    }
                    $("#dllAbilityAttend",form_add).html(html);
                    html ="";
                    if(response.Data.ListCustomerType.length >0)
                    {
                        for(var index=0;index <response.Data.ListCustomerType.length;index++)
                        {
                            html += '<option value="'+ response.Data.ListCustomerType[index].Key +'" >'+response.Data.ListCustomerType[index].Value +'</option>';
                        }
                    }
                    $("#dllCustomerType",form_add).html(html);
                    html ="";
                    if(response.Data.ListCustomerPosition.length >0)
                    {
                        for(var index=0;index <response.Data.ListCustomerPosition.length;index++)
                        {
                            html += '<option value="'+ response.Data.ListCustomerPosition[index].Key +'" >'+response.Data.ListCustomerPosition[index].Value +'</option>';
                        }
                    }
                    $("#dllCustomerPosition",form_add).html(html);
                    // fill company size
                    html ="";
                    if(response.Data.ListCompanySize.length >0)
                    {
                        for(var index=0;index <response.Data.ListCompanySize.length;index++)
                        {
                            html += '<option value="'+ response.Data.ListCompanySize[index].Key +'" >'+response.Data.ListCompanySize[index].Value +'</option>';
                        }
                    }
                    $("#dllCompanySize",form_add).html(html);
                }
            },
            complete: function () {
            }
        });
    },
    validate:function(){
        var form_add = $("#frmAddAnonymousInvitee");
        var result = true;
        if(!sys_utility.require_select($("#ddlEventId",form_add))){
            sys_control.show_msg($("#ddlEventId",form_add),'Sự kiện bắt buộc nhập !',1);
            result = false;
        }
        // Customer Name
        if(!sys_utility.require_text_box($("#txtCustomerName",form_add))){
            sys_control.show_msg($("#txtCustomerName",form_add),'Tên khách hàng bắt buộc nhập !',1);
            result = false;
        }

        // Phone Number
        if(!sys_utility.require_text_box($("#txtCustomerPhone",form_add))){
            sys_control.show_msg($("#txtCustomerPhone",form_add),'Số điện thoại bắt buộc nhập !',1);
            result = false;
        }

        // Email
        var arrV = $("#ddlEventId",form_add).val().split('_');
        var eventId = arrV[0];
        var isInWebCheckIn = false;
        if(arrV[1] == '1'){
            isInWebCheckIn = true;
        }
        else{
            isInWebCheckIn = false;
        }
        if(isInWebCheckIn){
            if(!sys_utility.require_text_box($("#txtCustomerEmailCheckIn",form_add))){
                sys_control.show_msg($("#txtCustomerEmailCheckIn",form_add),'Email khách mời bắt buộc nhập !',1);
                result = false;
            }
        }
        var sessions = '';
        if(!$('div[name=IsHadSession]',form_add).hasClass('hide') ){
            if( !sys_utility.require_select($('#dllSession',form_add))) {
                sys_control.show_msg($('#dllSession',form_add),'Bạn phải chọn phiên tham dự',1);
                result = false;
            }
        }
        return result;
    },
    refresh_form:function(){
        var form_add = $("#frmAddAnonymousInvitee");
        $("#txtCustomerName",form_add).val("");
        $("#txtCustomerPhone",form_add).val("");
        $("#txtCustomerBirthday",form_add).val("");
        $("#ddlCustomerSex",form_add).val("0");
        $("#txtCustomerEmail",form_add).val("");
        $("#txtCustomerRevenue",form_add).val("");
        $("#dllCustomerType",form_add).val("0");
        $("#dllAbilityAttend",form_add).val("0");
        $("#txtCompanyName",form_add).val("");
        $("#dllCustomerPosition",form_add).val("0");
        $("#dllCompanySize",form_add).val("0");
        $("#txtCompanyAddress",form_add).val("");
        $("#dllCustomerVip",form_add).val("0");
        $("#txtTicketAddress",form_add).val("");
        $("#txtNote",form_add).val("");
        $('#txtAccompanyMember',form_add).val(0);

        $('#txtCustomerEmailCheckIn',form_add).val('');
        $('#txtCompanyNameCheckIn',form_add).val('');
        $('#dllCustomerTypeCheckIn',form_add).val('0');
        $('#txtPositionNameCheckIn',form_add).val('');
        $('#dllPlaceOfAttendEvent',form_add).val('Hà Nội');
        $('#dllRegisteredSource',form_add).val('4');
    },
    change_filter_condition:function(isInWebCheckin){
        var form_create = $("#frmAddAnonymousInvitee");
        if(isInWebCheckin)
        {
            $('[name=inbcrm]',form_create).each(function () {
                if(!$(this).hasClass('hide'))
                {
                    $(this).addClass('hide');
                }
            });

            $('[name=inwebcheckin]',form_create).each(function () {
                if($(this).hasClass('hide'))
                {
                    $(this).removeClass('hide');
                }
            });
        }
        else
        {
            $('[name=inbcrm]',form_create).each(function () {
                if($(this).hasClass('hide'))
                {
                    $(this).removeClass('hide');
                }
            });

            $('[name=inwebcheckin]',form_create).each(function () {
                if(!$(this).hasClass('hide'))
                {
                    $(this).addClass('hide');
                }
            });
        }
    }
};
var anonymous_invitee_edit = {
    init:function(){
        $(document).ready(function(){
            anonymous_invitee_edit.register_events();
            $("#txtCustomerBirthday",$("#frmEditAnonymousInvitee")).datepicker();
        });
    },
    register_events:function(){
        var form_edit = $("#frmEditAnonymousInvitee");
        $("#btnSave",form_edit).off("click");
        $("#btnSave",form_edit).on("click",function(){
            if(anonymous_invitee_edit.validate())
            {
                var id = $("#hdfId",form_edit).val();
                var eventId = $("#ddlEventId",form_edit).val();
                var customerName = $("#txtCustomerName",form_edit).val();
                var phoneNumber = $("#txtCustomerPhone",form_edit).val();
                var birthday = $("#txtCustomerBirthday",form_edit).val();
                var arrBirthday =[];
                if(birthday != null && birthday!='')
                {
                    if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(birthday))
                    {
                        sysmess.warning("Định dạng nhập ngày sinh không đúng !",function(){});
                        return;
                    }
                    arrBirthday = birthday.split("/");
                }
                var birthdayInfo = null;
                if(arrBirthday.length == 3)
                {
                    birthdayInfo = arrBirthday[1] + "/" + arrBirthday[0] + "/" + arrBirthday[2];
                }
                var sex = $("#ddlCustomerSex",form_edit).val();
                var isMan = null;
                if(sex != null && sex != '')
                {
                    if(sex == '0')
                    {
                        isMan = false;
                    }
                    else
                    {
                        isMan =  true;
                    }
                }
                var customerEmail = $("#txtCustomerEmail",form_edit).val();
                var customerRevenue = $("#txtCustomerRevenue",form_edit).val();
                var customerType = $("#dllCustomerType",form_edit).val();
                var abilityAttend = $("#dllAbilityAttend",form_edit).val();
                var companyName = $("#txtCompanyName",form_edit).val();
                var customerPosition = $("#dllCustomerPosition",form_edit).val();
                var companySize = $("#dllCompanySize",form_edit).val();
                var  companyAddress = $("#txtCompanyAddress",form_edit).val();
                var customerVip = $("#dllCustomerVip",form_edit).val();
                var ticketAddress = $("#txtTicketAddress",form_edit).val();
                var note = $("#txtNote",form_edit).val();
                var accompanyMember = $("#txtAccompanyMember",form_edit).val().trim();
                $.ajax({
                    url: "/InviteEvent/Update",
                    type: "json",
                    data: {
                        Id:id,
                        EventId:eventId,
                        CustomerId:0,
                        CustomerName:customerName,
                        CustomerPhone:phoneNumber,
                        CustomerBirthday:birthdayInfo,
                        CustomerSex:isMan,
                        CustomerEmail:customerEmail,
                        CustomerRevenue:customerRevenue,
                        CustomerType:customerType,
                        CustomerPosition:customerPosition,
                        CompanySize:companySize,
                        CompanyName:companyName,
                        CompanyAddress:companyAddress,
                        TicketAddress:ticketAddress,
                        AbilityAttend:abilityAttend,
                        Note:note,
                        CustomerVip:customerVip,
                        AccompanyMember:accompanyMember
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $("#btnSave",form_edit).attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        switch (response.Code)
                        {
                            case ResponseCode.Success:
                                sysmess.success("Bạn đã sửa thông tin thành công !");
                                invite_event_index.display('SearchInviteEvent');
                                invite_event_index.search();
                                break;
                            case ResponseCode.ErrorExist:
                                sysmess.warning("Đã có thông tin số điện thoại trùng với thông tin số điện thoại này rồi !",function(){});
                                break;
                            case ResponseCode.NotValid:
                                sysmess.warning("Dữ liệu nhập vào không đúng !",function(){} );
                                break;
                            default :
                                sysmess.error("Có lỗi trong quá trình xử lý",function(){});
                                break;
                        }
                    },
                    complete: function () {
                        $("#btnSave",form_edit).removeAttr("disabled");
                    }
                });
            }
        });

        $("#btnClose",form_edit).off("click");
        $("#btnClose",form_edit).on("click",function(){
            if($("#SearchInviteEvent").hasClass("hide"))
            {
                $("#SearchInviteEvent").removeClass("hide");
            }
            if(!$("#AddAnonymousInfo").hasClass("hide"))
            {
                $("#AddAnonymousInfo").addClass("hide");
            }
            if(!$("#AccompanyInvitee").hasClass("hide"))
            {
                $("#AccompanyInvitee").addClass("hide");
            }
            invite_event_index.search();
        });

        $("#btnCancel",form_edit).off("click");
        $("#btnCancel",form_edit).on("click",function(){
            var inviteEventId = $('#hdfId',form_edit).val();
            $.ajax({
                url: "/InviteEvent/EditAnonymousInfo",
                type: "GET",
                data: {
                    inviteEventId:inviteEventId
                },
                dataType: "html",
                async: true,
                beforeSend: function () { },
                error: function (jqXHR, textStatus, errorThrown) { },
                success: function (response) {
                    invite_event_index.display('AddAnonymousInfo');
                    $(".panel-body",$("#AddAnonymousInfo")).html(response);
                },
                complete: function () {
                }
            });
        });

        $('#txtCustomerPhone',form_edit).off('keypress');
        $('#txtCustomerPhone',form_edit).on('keypress',function(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        });
    },
    validate:function(){
        var form_edit = $("#frmEditAnonymousInvitee");

        var eventId = $("#ddlEventId",form_edit).val();
        if(eventId == null || eventId=='')
        {
            sysmess.warning('Thông tin sự kiện bắt buộc nhập !');
            return false;
        }
        var customerName = $("#txtCustomerName",form_edit).val();
        var phoneNumber = $("#txtCustomerPhone",form_edit).val();
        var accompanyMember = $("#txtAccompanyMember",form_edit).val().trim();

        if(customerName == null || customerName =='')
        {
            sysmess.warning("Tên khách hàng bắt buộc nhập");
            return false;
        }
        else
        {
            var trimV = customerName.trim();
            if(trimV ==null || trimV =='')
            {
                sysmess.warning("Tên khách hàng bắt buộc nhập");
                return false;
            }
        }
        if(phoneNumber == null   || phoneNumber == '')
        {
            sysmess.warning("Số điện thoại bắt buộc nhập");
            return false;
        }
        else
        {
            var trimV = phoneNumber.trim();
            if(trimV == null || trimV =='')
            {
                sysmess.warning("Số điện thoại bắt buộc nhập");
                return false;
            }
        }
        if(accompanyMember != null   && accompanyMember != '')
        {
            if(!$.isNumeric(accompanyMember))
            {
                sysmess.warning("Số người đi cùng là kiểu số");
                return false;
            }
        }
        return true;
    }
};
var checkin_info={
    init:function(){
        $(document).ready(function(){
            checkin_info.register_events();
        });
    },
    register_events:function(){
        var form_add  = $("#frmAddAccompanyInvitee");
        $("#btnSave",form_add).off("click");
        $("#btnSave",form_add).on("click",function(){
            var eventId = $("#hdfEventId",form_add).val();
            var inviteEventId = $("#hdfInviteEventId",form_add).val();
            var inviteEventAttachId = $("#hdfAttachId",form_add).val();
            var accompanyInvitee = $("#txtAccompanyInvitee",form_add).val();

            if(accompanyInvitee == null || accompanyInvitee == ''
                || !$.isNumeric(accompanyInvitee))
            {
                $('#btnCancel',form_add).trigger('click');
                sysmess.warning("Người đi cùng phải nhập số",function(){});
                return;
            }
            $.ajax({
                url: "/InviteEvent/CheckIn",
                type: "POST",
                data: {
                    eventId:eventId,
                    inviteEventId: inviteEventId,
                    attachId: inviteEventAttachId,
                    accompanyInvitee:accompanyInvitee
                },
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $("#btnSave",form_add).attr("disabled","disabled");
                },
                error: function (jqXHR, textStatus, errorThrown) { },
                success: function (response) {
                    switch (response.Code) {
                        case ResponseCode.Success:
                            $('#btnCancel',$('#frmAddAccompanyInvitee')).trigger('click');
                            sysmess.success("Bạn đã checkin thông tin thành công !",function(){});
                            invite_event_index.display('SearchInviteEvent');
                            var pageIndex = 0;
                            $(".dataTables_paginate .active a").each(
                                function () {
                                    if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                        pageIndex = parseInt($(this).html().trim());
                                    }
                                }
                            );
                            invite_event_index.search(pageIndex);
                            break;
                        case ResponseCode.NotPermitted:
                            $('#btnCancel',$('#frmAddAccompanyInvitee')).trigger('click');
                             sysmess.warning("Bạn không có quyền thực thi chức năng này",function(){});
                            break;
                        default:
                            $('#btnCancel',$('#frmAddAccompanyInvitee')).trigger('click');
                            sysmess.error("Có lỗi trong quá trình xử lý !" ,function(){});
                            break;
                    }
                },
                complete: function () {
                    $("#btnSave",form_add).removeAttr("disabled");
                }
            });
        });

        $("#btnCancel",form_add).off("click");
        $("#btnCancel",form_add).on("click",function(){
            invite_event_index.display('SearchInviteEvent');
            invite_event_index.search();
        });

    }
};
var add_accompany_invitee_bcrm={
    init:function(){
        $(document).ready(function(){
            add_accompany_invitee_bcrm.register_events();
        });
    },
    register_events:function(){
        var form_add  = $("#frmAddQuantityAccompanyInvitee");

        $("#btnSave",form_add).off("click");
        $("#btnSave",form_add).on("click",function(){
            var eventId = $("#hdfEventId",form_add).val();
            var inviteEventId = $("#hdfInviteEventId",form_add).val();
            var inviteEventAttachId = $("#hdfAttachId",form_add).val();
            var accompanyInvitee = $("#txtAccompanyInvitee",form_add).val();

            if(accompanyInvitee == null || accompanyInvitee == ''
                || !$.isNumeric(accompanyInvitee))
            {
                $('#btnCancel',form_add).trigger('click');
                sysmess.warning("Người đi cùng phải nhập số",function(){});
                return;
            }
            $.ajax({
                url: "/InviteEvent/AddQuantityAccompanyInviteeAction",
                type: "POST",
                data: {
                    eventId:eventId,
                    inviteEventId: inviteEventId,
                    attachId: inviteEventAttachId,
                    accompanyInvitee:accompanyInvitee
                },
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $("#btnSave",form_add).attr("disabled","disabled");
                },
                error: function (jqXHR, textStatus, errorThrown) { },
                success: function (response) {
                    switch (response.Code) {
                        case ResponseCode.Success:
                            sysmess.success("Bạn đã cập nhật số khách đi cùng thành công !",function(){});
                            invite_event_index.display('SearchInviteEvent');
                            var pageIndex = 0;
                            $(".dataTables_paginate .active a").each(
                                function () {
                                    if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                        pageIndex = parseInt($(this).html().trim());
                                    }
                                }
                            );
                            invite_event_index.search(pageIndex);
                            break;
                        case ResponseCode.NotPermitted:
                            sysmess.warning("Bạn không có quyền thực thi chức năng này",function(){});
                            break;
                        default:
                            sysmess.error("Có lỗi trong quá trình xử lý !" ,function(){});
                            break;
                    }
                },
                complete: function () {
                    $("#btnSave",form_add).removeAttr("disabled");
                }
            });
        });

        $("#btnCancel",form_add).off("click");
        $("#btnCancel",form_add).on("click",function(){
            invite_event_index.display('SearchInviteEvent');
            invite_event_index.search();
        });

    }
};
var checkin_info_webcheckin={
    init:function(){
        $(document).ready(function(){
            checkin_info_webcheckin.register_events();
        });
    },
    register_events:function(){
        var form_add  = $("#frmAddAccompanyInviteeInWebCheckin");

        $("#btnSave",form_add).off("click");
        $("#btnSave",form_add).on("click",function(){
            var eventId = $("#hdfEventId",form_add).val();
            var inviteEventId = $("#hdfInviteEventId",form_add).val();
            var accompanyInvitee = $("#txtAccompanyInvitee",form_add).val();
            if(accompanyInvitee == null || accompanyInvitee == ''|| !$.isNumeric(accompanyInvitee)){
                sys_control.show_msg($("#txtAccompanyInvitee",form_add),'Người đi cùng phải nhập số !',1);
                return;
            }
            var  sessionId = 0;
            if($('#dllSessionId',form_add).length > 0){
                sessionId = $('#dllSessionId',form_add).val();
                if(sessionId == null || sessionId ==''){
                    sys_control.show_msg($("#dllSessionId",form_add),'Bạn phải chọn phiên tham dự !',1);
                    return;
                }
            }

            $.ajax({
                url: "/InviteEvent/CheckInWebCheckin",
                type: "json",
                data: {
                    eventId:eventId,
                    inviteEventId: inviteEventId,
                    accompanyInvitee:accompanyInvitee,
                    sessionId:sessionId
                },
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $("#btnSave",form_add).attr("disabled","disabled");
                },
                error: function (jqXHR, textStatus, errorThrown) { },
                success: function (response) {
                    $('#btnCancel',$('#frmAddAccompanyInviteeInWebCheckin')).trigger('click');
                    if(response.Code == ResponseCode.Success){
                        sysmess.success("Bạn đã checkin thông tin thành công !",function(){});
                        invite_event_index.display('SearchInviteEvent');
                        var pageIndex = 0;
                        $(".dataTables_paginate .active a").each(
                            function () {
                                if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                    pageIndex = parseInt($(this).html().trim());
                                }
                            }
                        );
                        invite_event_index.search(pageIndex);
                    }
                    else if(response.Code == ResponseCode.Error){
                        sysmess.error(response.Message ,function(){});
                    }
                    else{
                        sysmess.warning(response.Message,function(){});
                    }
                },
                complete: function () {
                    $("#btnSave",form_add).removeAttr("disabled");
                }
            });
        });

        $("#btnCancel",form_add).off("click");
        $("#btnCancel",form_add).on("click",function(){
            invite_event_index.display('SearchInviteEvent');
        });

    }
};
var accompany_member_add={
    init:function(){
        $(document).ready(function(){
            accompany_member_add.register_events();
        });
    },
    register_events:function(){
        var form_add  = $("#frmJustAddAccompanyMemberInWebCheckin");

        $("#btnSave",form_add).off("click");
        $("#btnSave",form_add).on("click",function(){
            var eventId = $("#hdfEventId",form_add).val();
            var inviteEventId = $("#hdfInviteEventId",form_add).val();
            var accompanyInvitee = $("#txtAccompanyInvitee",form_add).val();

            if(accompanyInvitee == null || accompanyInvitee == ''
                || !$.isNumeric(accompanyInvitee))
            {
                sysmess.warning("Số lượng người đi cùng bắt buộc nhập và phải là dạng số",function(){});
                return;
            }
            $.ajax({
                url: "/InviteEvent/AddAccompanyMemberActionWebCheckin",
                type: "json",
                data: {
                    eventId:eventId,
                    inviteEventId: inviteEventId,
                    accompanyMember:accompanyInvitee
                },
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $("#btnSave",form_add).attr("disabled","disabled");
                },
                error: function (jqXHR, textStatus, errorThrown) { },
                success: function (response) {
                    switch (response.Code) {
                        case ResponseCode.Success:
                            sysmess.success("Bạn đã thêm khách đi cùng thành công!",function(){});
                            invite_event_index.display('SearchInviteEvent');
                            var pageIndex = 0;
                            $(".dataTables_paginate .active a").each(
                                function () {
                                    if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                        pageIndex = parseInt($(this).html().trim());
                                    }
                                }
                            );
                            invite_event_index.search(pageIndex);
                            break;
                        case ResponseCode.NotPermitted:
                            sysmess.warning("Bạn không có quyền thực thi chức năng này",function(){});
                            break;
                        default:
                            sysmess.error("Có lỗi trong quá trình xử lý !" ,function(){});
                            break;
                    }
                },
                complete: function () {
                    $("#btnSave",form_add).removeAttr("disabled");
                }
            });
        });

        $("#btnCancel",form_add).off("click");
        $("#btnCancel",form_add).on("click",function(){
            invite_event_index.display('SearchInviteEvent');
        });

    }
};
var invitee_create ={
    init:function(){
        invitee_create.register_events();
    },
    register_events:function(){
        var form_create = $('#frmAddInvitee');
        $('#btnSave',form_create).off('click');
        $('#btnSave',form_create).on('click',function(){
            if(invitee_create.validate()){
                var eventId = $('#ddlEventId',form_create).val();
                var cutomerName = $('#txtCustomerName',form_create).val();
                var customerPhone = $('#txtCustomerPhone',form_create).val();
                var note = $('#txtNote',form_create).val();
                $.ajax({
                    url: "/InviteEvent/CreateInviteeAction",
                    type: "json",
                    data: {
                        EventId:eventId,
                        CustomerName:cutomerName,
                        CustomerPhone:customerPhone,
                        Note:note
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $('#btnSave',$('#frmAddInvitee')).attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        if(response.Code == ResponseCode.Success)
                        {
                            sysmess.success(response.Message);
                            invitee_create.reset();
                        }
                        else if(response.Code == ResponseCode.Error)
                        {
                            sysmess.error(response.Message);
                        }
                        else
                        {
                            sysmess.warning(response.Message);
                        }
                    },
                    complete: function () {
                        $('#btnSave',$('#frmAddInvitee')).removeAttr("disabled");
                    }
                });
            }
        });

        $('#btnCancel',form_create).off('click');
        $('#btnCancel',form_create).on('click',function(){
            invitee_create.reset();
        });

        $('#btnClose',form_create).off('click');
        $('#btnClose',form_create).on('click',function(){
            invite_event_index.display('SearchInviteEvent');
        });
    },
    validate:function(){
        var form_create = $('#frmAddInvitee');
        var eventId = $('#ddlEventId',form_create).val();
        var result = true;

        if(!sys_utility.require_select($('#ddlEventId',form_create),'',1)){
            sys_control.show_msg($('#ddlEventId',form_create),'Sự kiện bắt buộc nhập !',1);
            result = false;
        }

        if(!sys_utility.require_text_box($('#txtCustomerName',form_create))){
            sys_control.show_msg($('#txtCustomerName',form_create),'Tên khách hàng không được để trống !',1);
            result= false;
        }

        if(!sys_utility.require_text_box($('#txtCustomerPhone',form_create))){
            sys_control.show_msg($('#txtCustomerPhone',form_create),'Số điện thoại không được để trống !',1);
            result= false;
        }
        return result;
    },
    reset:function(){
        var form_create= $('#frmAddInvitee');
        $('#txtCustomerName',form_create).val('');
        $('#txtCustomerPhone',form_create).val('');
        $('#txtNote',form_create).val('');
    }
};
var invitee_edit_in_webcheckin = {
    init:function(){
        var form_edit = $('#frmEditInviteeInWebCheckIn');
        $(".chzn-select",form_edit).chosen();
        $(".chzn-select-deselect",form_edit).chosen({allow_single_deselect:true});
        $(".chzn-choices",form_edit).attr('style','min-height:32px;');

        var strCheckedinSessionIds = $('#hdfChkedinSsId',form_edit).val();
        var arrCheckedinSession =[];
        if(strCheckedinSessionIds != null && strCheckedinSessionIds != ''){
            arrCheckedinSession = strCheckedinSessionIds.split(',');
        }

        var arrIndex = [];
        $('#dllSessionEdit option').each(function () {
            for(var index = 0;index <arrCheckedinSession.length ;index++ ){
                if(arrCheckedinSession[index] == $(this).val()){
                    arrIndex.push($(this).prop('index') );
                }
            }
        });

        for(var i=0;i<arrIndex.length;i++){
            $('#dllSessionEdit_chzn #dllSessionEdit_chzn_c_'+arrIndex[i] +' a').remove();
        }
        invitee_edit_in_webcheckin.register_events();
    },
    register_events:function(){
        var form_edit = $('#frmEditInviteeInWebCheckIn');
        $('#btnSave',form_edit).off('click');
        $('#btnSave',form_edit).on('click',function(){
            if(invitee_edit_in_webcheckin.validate()){
                var eventId = $('#ddlEventId',form_edit).val();
                var cutomerName = $('#txtCustomerName',form_edit).val();
                var customerPhone = $('#txtCustomerPhone',form_edit).val();
                var customerEmail = $('#txtCustomerEmail',form_edit).val();
                var companyName = $('#txtCompanyName',form_edit).val();
                var customerType = $('#dllCustomerType',form_edit).val();
                var positionName = $('#txtPositionName',form_edit).val();
                var placeOfAttendEvent = $('#dllPlaceOfAttendEvent',form_edit).val();
                var registeredSource = $('#dllPlaceOfAttendEvent',form_edit).val();
                var introduceUser = $('#txtIntroduceUser',form_edit).val();
                var note = $('#txtNote',form_edit).val();
                var id = $('#hddId',form_edit).val();
                var sessions = '';
                if(!$('div[name=IsHadSession]',form_edit).hasClass('hide') ){
                    sessions = sys_control.get_val_to_ajax($('#dllSessionEdit',form_edit));
                }
                $.ajax({
                    url: "/InviteEvent/EditInviteeWebCheckinAction",
                    type: "POST",
                    data: {
                        Id : id,
                        EventId:eventId,
                        CustomerName:cutomerName,
                        CustomerPhone:customerPhone,
                        CustomerEmail :customerEmail,
                        CompanyName:companyName,
                        CustomerType:customerType,
                        PositionName:positionName,
                        PlaceOfAttendEvent:placeOfAttendEvent,
                        RegisteredSource:registeredSource,
                        IntroduceUser:introduceUser,
                        Note:note,
                        Sessions:sessions
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $('#btnSave',$('#frmAddInvitee')).attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        if(response.Code == ResponseCode.Success){
                            sysmess.success(response.Message);
                            invite_event_index.display('SearchInviteEvent');
                            invite_event_index.search();
                        }
                        else if(response.Code == ResponseCode.Error){
                            sysmess.error(response.Message);
                        }
                        else{
                            sysmess.warning(response.Message);
                        }
                    },
                    complete: function () {
                        $('#btnSave',$('#frmAddInvitee')).removeAttr("disabled");
                    }
                });
            }
        });
        $('#btnCancel',form_edit).off('click');
        $('#btnCancel',form_edit).on('click',function(){
            var inviteId = $('#hddId',form_edit).val();
            $.ajax({
                url: "/InviteEvent/EditInviteeWebCheckin",
                type: "GET",
                data: {
                    inviteEventId:inviteId
                },
                dataType: "html",
                async: true,
                beforeSend: function () { },
                error: function (jqXHR, textStatus, errorThrown) { },
                success: function (response) {
                    invite_event_index.display('EditInviteeInWebCheckin');
                    $(".panel-body",$("#EditInviteeInWebCheckin")).html(response);
                    invitee_edit_in_webcheckin.init();
                },
                complete: function () {}
            });
        });
        $('#btnClose',form_edit).off('click');
        $('#btnClose',form_edit).on('click',function(){
            invite_event_index.display('SearchInviteEvent');
        });

        $('#txtCustomerPhone',form_edit).off('keypress');
        $('#txtCustomerPhone',form_edit).on('keypress',function(evt){
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        });
    },
    validate:function(){
        var form_edit = $('#frmEditInviteeInWebCheckIn');
        var result = true;
        if(!sys_utility.require_select($('#ddlEventId',form_edit))){
            sys_control.show_msg($('#ddlEventId',form_edit),'Thông tin sự kiện bắt buộc nhập !',1);
            result = false;
        }

        if(!sys_utility.require_text_box($('#txtCustomerName',form_edit))){
            sys_control.show_msg($('#txtCustomerName',form_edit),'Tên khách hàng không được để trống !',1);
            result = false;
        }
        if(!sys_utility.require_text_box($('#txtCustomerPhone',form_edit))){
            sys_control.show_msg($('#txtCustomerPhone',form_edit),'Số điện thoại không được để trống !',1);
            result = false;
        }
        if(!sys_utility.require_text_box($('#txtCustomerEmail',form_edit))){
            sys_control.show_msg($('#txtCustomerEmail',form_edit),'Số điện thoại không được để trống !',1);
            result = false;
        }

        if(!$('div[name=IsHadSession]',form_edit).hasClass('hide') && !sys_utility.require_select($('#dllSessionEdit',form_edit)) ){
            sys_control.show_msg($('#dllSessionEdit',form_edit),'Phiên tham dự bắt buộc nhập !',1);
            result = false;
        }
        return result;
    },
    reset:function(){
        var form_edit= $('#frmEditInviteeInWebCheckIn');
        $('#txtCustomerName',form_edit).val('');
        $('#txtCustomerPhone',form_edit).val('');
        $('#txtNote',form_edit).val('');
    }
};
var show_dialog_checkin = {
    show:function(element,func){
        element.modal({
            backdrop:true,
            keyboard:true,
            show:true
        });
        if(typeof(func) ==='function' )
            func();
    }
};
var session_change = {
    old_scroll_position:0,
    init: function () {
        $(document).ready(function () {
            $(".chzn-select",$('#wrapChangeSession')).chosen();
            $(".chzn-select-deselect",$('#wrapChangeSession')).chosen({allow_single_deselect:true});
            $(".chzn-choices",$('#wrapChangeSession')).attr('style','min-height:32px;');

            var strCheckedinSessionIds = $('#hdfCheckedinSessionId',$('#wrapChangeSession')).val();
            var arrCheckedinSession =[];
            if(strCheckedinSessionIds != null && strCheckedinSessionIds != ''){
                arrCheckedinSession = strCheckedinSessionIds.split(',');
            }

            var arrIndex = [];
            $('#dllSessions option').each(function () {
                for(var index = 0;index <arrCheckedinSession.length ;index++ ){
                    if(arrCheckedinSession[index] == $(this).val()){
                        arrIndex.push($(this).prop('index') );
                    }
                }
            });

            for(var i=0;i<arrIndex.length;i++){
                $('#dllSessions_chzn #dllSessions_chzn_c_'+arrIndex[i] +' a').remove();
            }

            session_change.action();
        });

    },
    action: function () {
        var frm = $('#frmChangeSession');
        $('#btnSaveChangeSession',frm).off('click').on('click', function () {
            if(!sys_utility.require_select($('#dllSessions',frm))){
                sys_control.show_msg($('#dllSessions',frm),'Bạn phải chọn phiên tham dự !',1);
                return;
            }
            var sessions = sys_control.get_val_to_ajax($('#dllSessions',frm));
            var inviteEventId  = $('#hdfInviteEventId',frm).val();
            $.ajax({
                url: "/InviteEvent/ChangeSessionAction",
                type: "POST",
                data: {
                    inviteeId:inviteEventId,
                    sessions: sessions
                },
                dataType: "json",
                async: true,
                beforeSend: function () {
                    $("#btnSaveChangeSession",frm).attr("disabled","disabled");
                },
                error: function (jqXHR, textStatus, errorThrown) { },
                success: function (response) {
                    $('#btnCancelChangeSession').trigger('click');
                    if(response.Code == ResponseCode.Success){
                        sysmess.success(response.Message, function () {});
                        var pageIndex = 0;
                        $(".dataTables_paginate .active a").each(
                            function () {
                                if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                    pageIndex = parseInt($(this).html().trim());
                                }
                            }
                        );
                        invite_event_index.search(pageIndex);
                        $(window).scrollTop(session_change.old_scroll_position);
                    }
                    else if(response.Code == ResponseCode.Error){
                        sysmess.error(response.Message, function () {});
                    }
                    else{
                        sysmess.warning(response.Message, function () {});
                    }
                },
                complete: function () {
                    $("#btnSaveChangeSession",frm).removeAttr("disabled");
                }
            });
        });
    }
}


