﻿var user_index = {
    init: function () {
        $(document).ready(function () {
            user_index.register_events();
            user_index.register_grid_events();
            user_index.search();
            $('#frmSearchUser').keydown(user_index.hitting_enter);
        });
    },
    hitting_enter:function(e){
        if(e.which == 13) {
            e.preventDefault(); //stops default action: submitting form
            $(this).blur();
            user_index.search();
        }
    },
    register_events: function () {
        var form_search = $("#frmSearchUser");
        $("#btnSearch", form_search).off("click");
        $("#btnSearch", form_search).on("click", function () {
            $("#SearchUser").removeClass("hide");
            if(!$("#AddNewUser").hasClass("hide"))
            {
                $("#AddNewUser").addClass("hide");
            }
            if(!$("#LockTemporaryUser").hasClass("hide"))
            {
                $("#LockTemporaryUser").addClass("hide");
            }
            user_index.search();
        });
        $("#btnAddNew", form_search).off("click");
        $("#btnAddNew", form_search).on("click", function () {
            //$("#SearchUser").addClass("hide");
            if(!$("#SearchUser").hasClass("hide"))
            {
                $("#SearchUser").addClass("hide");
            }
            $("#AddNewUser").removeClass("hide");
            if(!$("#LockTemporaryUser").hasClass("hide"))
            {
                $("#LockTemporaryUser").addClass("hide");
            }
            $.ajax({
                url: "/User/AddNew",
                type: "html",
                data: {},
                dataType: "html",
                async: true,
                beforeSend: function () { },
                error: function (jqXHR, textStatus, errorThrown) { },
                success: function (response) {
                    $(".panel-body", $("#AddNewUser")).html(response);
                },
                complete: function () {
                }
            });
        });
    },
    register_grid_events: function () {
        var grid_search = $("#SearchUser");
        $('.dropdown-toggle').dropdown();
        $("[name=btnEdit]", grid_search).each(function () {
            var userName = $(this).attr("uname");
            $(this).off("click");
            $(this).on("click", function () {
                $.ajax({
                    url: "/User/EditAction",
                    type: "html",
                    data: {
                        userName: userName
                    },
                    dataType: "html",
                    async: true,
                    beforeSend: function () {

                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        $("#SearchUser").addClass("hide");
                        $("#AddNewUser").removeClass("hide");
                        $(".panel-body", $("#AddNewUser")).html(response);
                    },
                    complete: function () {

                    }
                });
            });
        });
        $("[name=btnActive]", grid_search).each(function(){
            var userName = $(this).attr("uname");
            $(this).off("click");
            $(this).on("click", function () {
                $.ajax({
                    url: "/User/Active",
                    type: "json",
                    data: {
                        userName: userName
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {

                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        switch (response.Code) {
                            case ResponseCode.Success:
                                sysmess.success("Bạn đã kích hoạt thành công",function(){});
                                var pageIndex = 0;
                                $(".dataTables_paginate .active a").each(
                                    function () {
                                        if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                            pageIndex = parseInt($(this).html().trim());
                                        }
                                    }
                                );
                                user_index.search(pageIndex);
                                break;
                            case ResponseCode.NotPermitted:
                                sysmess.warning("Bạn không có quyền thực thi chức năng này",function(){});
                            default:
                                sysmess.error("Có lỗi trong quá trình xử lý hoặc tài khoản không tồn tại !",function(){});
                                break;
                        }
                    },
                    complete: function () {

                    }
                });
            });
        });
        $("[name=btnDeActive]", grid_search).each(function(){
            var userName = $(this).attr("uname");
            $(this).off("click");
            $(this).on("click", function () {
                $.ajax({
                    url: "/User/DeActive",
                    type: "json",
                    data: {
                        userName: userName
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {

                    },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        switch (response.Code) {
                            case ResponseCode.Success:
                                sysmess.success("Bạn đã bỏ kích hoạt thành công",function(){});
                                var pageIndex = 0;
                                $(".dataTables_paginate .active a").each(
                                    function () {
                                        if ($.isNumeric($(this).html().trim()) && parseInt($(this).html().trim())) {
                                            pageIndex = parseInt($(this).html().trim());
                                        }
                                    }
                                );
                                user_index.search(pageIndex);
                                break;
                            case ResponseCode.NotPermitted:
                                sysmess.warning("Bạn không có quyền thực thi chức năng này",function(){});
                            default:
                                sysmess.error("Có lỗi trong quá trình xử lý hoặc tài khoản không tồn tại !",function(){});
                                break;
                        }
                    },
                    complete: function () {

                    }
                });
            });
        });
        $("[name=btnLockTemp]", grid_search).each(function(){
            var userName = $(this).attr('uname');
            $(this).off('click');
            $(this).on('click', function (){
                $.ajax({
                    url: '/User/LockTemporary',
                    type: 'html',
                    data: {
                        userName: userName
                    },
                    dataType: 'html',
                    async: true,
                    beforeSend: function () { },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        if(!$('#SearchUser').hasClass('hide'))
                        {
                            $('#SearchUser').addClass('hide')
                        }
                        if(!$('#AddNewUser').hasClass('hide'))
                        {
                            $('#AddNewUser').addClass('hide');
                        }
                        if($('#LockTemporaryUser').hasClass('hide'))
                        {
                            $('#LockTemporaryUser').removeClass('hide');
                        }
                        $('.panel-body', $('#LockTemporaryUser')).html(response);

                        $('.close',$('#LockTemporaryUser')).off('click');
                        $('.close',$('#LockTemporaryUser')).on('click',function(){
                            if($('#SearchUser').hasClass('hide'))
                            {
                                $('#SearchUser').removeClass('hide');
                            }
                            if(!$('#AddNewUser').hasClass('hide'))
                            {
                                $('#AddNewUser').addClass('hide');
                            }
                            if(!$('#LockTemporaryUser').hasClass('hide'))
                            {
                                $('#LockTemporaryUser').addClass('hide');
                            }
                        });
                    },
                    complete: function () {}
                });
            });
        });
        $("[name=btnUnLockTemp]", grid_search).each(function(){
            var userName = $(this).attr('uname');
            $(this).off('click');
            $(this).on('click', function (){
                $.ajax({
                    url: '/User/UnLockTemporary',
                    type: 'json',
                    data: {
                        userName: userName
                    },
                    dataType: 'json',
                    async: true,
                    beforeSend: function () { },
                    error: function (jqXHR, textStatus, errorThrown) { },
                    success: function (response) {
                        switch(response.Code)
                        {
                            case ResponseCode.Success:
                                sysmess.success("Bạn đã mở khóa tài khoản thành công",function(){});
                                user_index.search();
                                break;
                            case  ResponseCode.DataNull:
                                sysmess.warning("Tài khoản mở khóa là trống!",function(){});
                                break;
                            case  ResponseCode.NotPermitted:
                                sysmess.warning("Bạn không có quyền thực thi chức năng này!",function(){});
                                break;
                        }
                    },
                    complete: function () {}
                });
            });
        });
    },
    search: function (page) {
        var form_search = $("#frmSearchUser");
        if (page == null || page == '') {
            page = 1
        }
        var userName = $("#txtSearchText", form_search).val();
        $.ajax({
            url: "/User/Search",
            type: "html",
            data: {
                UserName: userName,
                PageIndex: page,
                PageSize: 25

            },
            dataType: "html",
            async: true,
            beforeSend: function () { },
            error: function (jqXHR, textStatus, errorThrown) { },
            success: function (response) {
                $(".panel-body", $("#SearchUser")).html(response);
                user_index.register_grid_events();
            },
            complete: function () {
            }
        });
    }
};
var user_add={
    init:function(){
        $(document).ready(function () {
            user_add.register_events();
            $('input[type=checkbox]').uniform();
        })
    },
    register_events:function(){
        var form_add = $("#frmAddNewUser");
        $("#btnSave",form_add).off("click");
        $("#btnSave",form_add).on("click",function(){
            var userName = $("#txtUsername", form_add).val();
            var role = 0;
            //var role = $("input:radio[name='grRole']:checked").val();

            // check password and retype password whether the same or not
            var password = $("#txtPassword",form_add).val();
            var retypePassword = $("#txtReTypePassword",form_add).val();

            if(userName== null || userName=='')
            {
                sysmess.warning("Tên tài khoản bắt buộc nhập",function(){});
                return  ;
            }
            if(password== null || password=='')
            {
                sysmess.warning("Mật khẩu bắt buộc nhập",function(){});
                return  ;
            }
            if(retypePassword== null || retypePassword=='')
            {
                sysmess.warning("Nhập lại mật khẩu bắt buộc nhập",function(){});
                return  ;
            }

            var arrRoles = [];
            $('input:checkbox[name=grRole]:checked').each(function () {
                arrRoles.push($(this).attr('renum'));
            });

            if(arrRoles.length == 0)
            {
                sysmess.warning("Bạn phải thiết lập quyền cho tài khoản này.",function(){});
                return  ;
            }

            if(password === retypePassword)
            {
                $.ajax({
                    url: "/User/Insert",
                    type: "json",
                    data: {
                        UserName: userName,
                        PassWord:password,
                        Role: role,
                        UserRoles:arrRoles.join()
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $("#btnSave",form_add).attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {},
                    success: function (response) {
                       switch(response.Code)
                       {
                           case ResponseCode.Success:
                               sysmess.success("Bạn đã thêm mới tài khoản thành công",function(){});
                               user_index.search();
                               break;
                           case  ResponseCode.ErrorExist:
                               sysmess.warning("Tài khoản này đã tồn tại rồi !",function(){});
                               break;
                           case  ResponseCode.NotValid:
                               sysmess.warning("Thông tin nhập vào không đúng!",function(){});
                               break;
                           case  ResponseCode.NotPermitted:
                               sysmess.warning("Bạn không có quyền thực thi chức năng này!",function(){});
                               break;
                       }
                    },
                    complete: function () {
                        $("#btnSave",form_add).removeAttr("disabled");
                    }
                });
            }
            else
            {
                // notify the retype pasword is not correct
            }
        });

        $("#btnCancel",form_add).off("click");
        $("#btnCancel",form_add).on("click",function(){
            var form_add = $("#frmAddNewUser");
            $("#txtUsername",form_add).val("");
            $("#txtPassword",form_add).val("");
            $("#txtReTypePassword",form_add).val("");
        });

        $("#btnClose",form_add).off("click");
        $("#btnClose",form_add).on("click",function(){
            $("#AddNewUser").addClass("hide");
            $("#SearchUser").removeClass("hide");
        });
    }
};
var user_edit={
    init:function(){
        $(document).ready(function () {
            user_edit.register_events();
            $('input[type=checkbox]').uniform();
        })
    },
    register_events:function(){
        var form_add = $("#frmEditUser");
        $("#btnSave",form_add).off("click");
        $("#btnSave",form_add).on("click",function(){
            var userName = $("#txtUsername", form_add).html();
            var role = 0;
            //var role = $("input:radio[name='grRole']:checked").val();
            // check password and retype password whether the same or not
            var password = $("#txtPassword",form_add).val();
            var retypePassword = $("#txtReTypePassword",form_add).val();

            if(password== null || password=='')
            {
                password="";
            }
            if(retypePassword== null || retypePassword=='')
            {
                retypePassword= "";
            }
            var arrRoles = [];
            $('input:checkbox[name=grRole]:checked').each(function () {
                arrRoles.push($(this).attr('renum'));
            });

            if(arrRoles.length == 0)
            {
                sysmess.warning("Bạn phải thiết lập quyền cho tài khoản này.",function(){});
                return  ;
            }
            if(password === retypePassword)
            {
                $.ajax({
                    url: "/User/Edit",
                    type: "json",
                    data: {
                        UserName:userName,
                        PassWord: password,
                        Role: role,
                        UserRoles:arrRoles.join()
                    },
                    dataType: "json",
                    async: true,
                    beforeSend: function () {
                        $("#btnSave",form_add).attr("disabled","disabled");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {},
                    success: function (response) {
                        switch(response.Code)
                        {
                            case ResponseCode.Success:
                                sysmess.success("Bạn đã cập nhật tài khoản thành công",function(){});
                                user_index.search();
                                break;
                            case  ResponseCode.NotValid:
                                sysmess.warning("Thông tin nhập vào không đúng!",function(){});
                                break;
                            case  ResponseCode.NotPermitted:
                                sysmess.warning("Bạn không có quyền thực thi chức năng này!",function(){});
                                break;
                        }
                    },
                    complete: function () {
                        $("#btnSave",form_add).removeAttr("disabled");
                    }
                });
            }
            else
            {
                // notify the retype pasword is not correct
                sysmess.warning("Mật khẩu nhập lại không đúng",function(){});
            }
        });

        $("#btnCancel",form_add).off("click");
        $("#btnCancel",form_add).on("click",function(){
            var form_add = $("#frmEditUser");
            $("#txtPassword",form_add).val("");
            $("#txtReTypePassword",form_add).val("");
        });

        $("#btnClose",form_add).off("click");
        $("#btnClose",form_add).on("click",function(){
            $("#AddNewUser").addClass("hide");
            $("#SearchUser").removeClass("hide");
        });
    }
};
var user_lock_temp = {
    init:function(){
        $(document).ready(function(){
            $('#txtStartTime',$('#frmLockTemporary')).datepicker();
            $('#txtStartHour',$('#frmLockTemporary')).timepicker({
                minuteStep: 1,
                appendWidgetTo: 'body',
                showSeconds: false,
                showMeridian: false,
                defaultTime: false
            });
            $('#txtDayLockTime',$('#frmLockTemporary')).focus();
            user_lock_temp.register_events();
        });
    },
    register_events:function(){
        var user_lock_form = $('#frmLockTemporary');
        $('#btnSave',user_lock_form).off('click');
        $('#btnSave',user_lock_form).on('click',function(){
            var dayLockTime = $('#txtDayLockTime',user_lock_form).val();
            var hourLockTime = $('#txtHourLockTime',user_lock_form).val();
            var minuteLockTime = $('#txtMinuteLockTime',user_lock_form).val();

            if(dayLockTime == null || dayLockTime == '' )
            {
                dayLockTime = '0';
            }
            if(hourLockTime == null || hourLockTime == '' )
            {
                hourLockTime = '0';
            }
            if(minuteLockTime == null || minuteLockTime == '' )
            {
                minuteLockTime = '0';
            }
            var lockTime = parseInt(dayLockTime) * 24 * 60 + parseInt(hourLockTime) * 60 + parseInt(minuteLockTime);

            if(lockTime ===0)
            {
                sysmess.warning('Thời gian khóa tài khoản không được để trống hoặc bằng không !',function(){});
                return;
            }
            if(lockTime > 3*24 * 60)
            {
                sysmess.warning('Thời gian khóa tài khoản chỉ được tối đa 3 ngày !',function(){});
                return;
            }
            var userName = $('#hdfUserName',user_lock_form).val();

            if(user_lock_temp.validate())
            {
                var startTime = $("#txtStartTime",user_lock_form).val();
                var startHour = $("#txtStartHour",user_lock_form).val();
                if(startHour.trim() == null || startHour.trim() == '')
                {
                    startHour = "00:00";
                }
                var arrStartDate = startTime.split("/");

                $.ajax({
                    url: '/User/LockTemporaryTime',
                    type: 'json',
                    data: {
                        UserName:userName,
                        LockTime: lockTime,
                        LockedStartDate:arrStartDate[1]+'/'+arrStartDate[0]+'/'+arrStartDate[2] +' '+startHour + ':00'
                    },
                    dataType: 'json',
                    async: true,
                    beforeSend: function () {
                        $('#btnSave',user_lock_form).attr('disabled','disabled');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {},
                    success: function (response) {
                        switch(response.Code)
                        {
                            case ResponseCode.Success:
                                sysmess.success("Bạn đã khóa tài khoản thành công",function(){});
                                user_index.search();
                                break;
                            case  ResponseCode.NotValid:
                                sysmess.warning("Thời gian khóa tài khoản chỉ được tối đa 3 ngày !",function(){});
                                break;
                            case  ResponseCode.NotPermitted:
                                sysmess.warning("Bạn không có quyền thực thi chức năng này!",function(){});
                                break;
                        }
                    },
                    complete: function () {
                        $('#btnSave',user_lock_form).removeAttr("disabled");
                    }
                });
            }
        });

        $('#btnCancel',user_lock_form).off('click');
        $('#btnCancel',user_lock_form).on('click',function(){
            if($('#SearchUser').hasClass('hide'))
            {
                $('#SearchUser').removeClass('hide');
            }
            if(!$('#AddNewUser').hasClass('hide'))
            {
                $('#AddNewUser').addClass('hide');
            }
            if(!$('#LockTemporaryUser').hasClass('hide'))
            {
                $('#LockTemporaryUser').addClass('hide');
            }
        });


    },
    validate:function(){
        var form_lock_time = $('#frmLockTemporary');
        var startTime = $("#txtStartTime",form_lock_time).val();
        var startHour = $("#txtStartHour",form_lock_time).val();

        if(startTime == null || startTime == '')
        {
            sysmess.warning("Ngày bắt đầu khóa bắt buộc nhập",function(){});
            return false;
        }
        if(startHour.trim() == null || startHour.trim() == '')
        {
            startHour = "00:00";
        }

        if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(startTime))
        {
            sysmess.warning("Định dạng nhập ngày bắt đầu khóa không đúng",function(){});
            return false;
        }
        if(!/^\d{2}\:\d{2}$/.test(startHour))
        {
            sysmess.warning("Định dạng giờ bắt đầu khóa không đúng",function(){});
            return false;
        }
        return true;
    }
};