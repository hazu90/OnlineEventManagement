﻿var log_on = {
    init:function(){
        $(document).ready(function(){
            log_on.register_events();
        });
    },
    register_events:function(){
        var formLogOn = $("#frmLogOn");
        $("#btnLogOn",formLogOn).off("click");
        $("#btnLogOn",formLogOn).on("click",function(){
            $("#frmLogOn").submit();
        });
    }
}