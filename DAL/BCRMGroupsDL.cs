﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCRM.CheckInEvent.Models;

namespace BCRM.CheckInEvent.DAL
{
    public class BCRMGroupsDL
    {
        public List<Group> GetAllGroups(string branch = null)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                if (branch == null)
                {
                    branch = string.Empty;
                }
                return context.StoredProcedure("Groups_GetAll").Parameter("Branch", branch).QueryMany<Group>();
            }
        }
    }
}