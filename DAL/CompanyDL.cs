﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Libs;
using BCRM.CheckInEvent.Models;

namespace BCRM.CheckInEvent.DAL
{
    public class CompanyDL
    {
        public List<CompanyModel> GetAllActive(CompanyType type)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_Company_GetAllActive")
                                .Parameter("Type", type)
                                .QueryMany<CompanyModel>();
            }
        }
    }
}