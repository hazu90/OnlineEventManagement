﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCRM.CheckInEvent.Models;
using BCRM.CheckInEvent.Common;

namespace BCRM.CheckInEvent.DAL
{
    public class InviteEventDL
    {
        #region Storeprocedure BCRM3
        public List<InviteEventModel> GetListByInviteeCode(int eventId, int inviteEventId)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                var lstInviteEventModel = context.StoredProcedure("CI_InviteEvent_SearchByInviteeCode")
                                 .Parameter("EventId", eventId)
                                 .Parameter("InviteEventId", inviteEventId)
                                 .QueryMany<InviteEventModel>();
                return lstInviteEventModel;
            }
        }
        /// <summary>
        ///  Lấy danh sách khách mời theo sự kiện và mã code
        /// </summary>
        /// <param name="eventId">Id sự kiện</param>
        /// <param name="code">Mã code </param>
        /// <returns></returns>
        public InviteEventModel GetListByCode(int eventId, string code)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_InviteEvent_SearchByCode")
                                 .Parameter("EventId", eventId)
                                 .Parameter("Code", code)
                                 .QuerySingle<InviteEventModel>();
            }
        }
        /// <summary>
        /// Lấy danh sách khách mời theo sự kiện và mã code để export
        /// </summary>
        /// <param name="eventId">Id sự kiện</param>
        /// <param name="code">Mã code </param>
        /// <returns></returns>
        public List<InviteEventModelToExport> GetListByCodeToExport(int eventId, string code)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_InviteEvent_GetByCodeToExport")
                                 .Parameter("EventId", eventId)
                                 .Parameter("Code", code)
                                 .QueryMany<InviteEventModelToExport>();
            }
        }

        public List<InviteEventModelToExport> GetListByInviteeCodeToExport(int eventId, int inviteEventId)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                var lstInviteEventModel = context.StoredProcedure("CI_InviteEvent_GetByInviteeCodeToExport")
                                 .Parameter("EventId", eventId)
                                 .Parameter("InviteEventId", inviteEventId)
                                 .QueryMany<InviteEventModelToExport>();
                return lstInviteEventModel;
            }
        }
        public List<InviteEventModel> GetListByCondition(EventIndexModel model)
        {
            using (var context = ConnectionDB.BCRMv3())
            {

                return context.StoredProcedure("CI_InviteEvent_SearchByCondition")
                                 .Parameter("SearchText", model.TextSearch)
                                 .Parameter("EventId", model.EventId)
                                 .Parameter("IsCheckIn", model.IsCheckIn)
                                 .Parameter("CustomerVip", model.ImportanceLevel)
                                 .QueryMany<InviteEventModel>();
            }
        }
        public List<InviteEventModelToExport> GetListByConditionToExport(EventIndexModel model)
        {
            using (var context = ConnectionDB.BCRMv3())
            {

                return context.StoredProcedure("CI_InviteEvent_GetByConditionToExport")
                                 .Parameter("SearchText", model.TextSearch)
                                 .Parameter("EventId", model.EventId)
                                 .Parameter("IsCheckIn", model.IsCheckIn)
                                 .Parameter("CustomerVip", model.ImportanceLevel)
                                 .QueryMany<InviteEventModelToExport>();
            }
        }
        public List<InviteEventModel> GetListOfficialByCondition(EventIndexModel model)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_InviteEvent_SearchOfficialByCondition")
                                 .Parameter("SearchText", model.TextSearch)
                                 .Parameter("EventId", model.EventId)
                                 .Parameter("IsCheckIn", model.IsCheckIn)
                                 .Parameter("CustomerVip", model.ImportanceLevel)
                                 .QueryMany<InviteEventModel>();
            }
        }
        public List<InviteEventModel> GetListAnonymousByCondition(EventIndexModel model)
        {
            using (var context = ConnectionDB.BCRMv3())
            {

                return context.StoredProcedure("CI_InviteEvent_SearchAnonymousByCondition")
                                 .Parameter("SearchText", model.TextSearch)
                                 .Parameter("EventId", model.EventId)
                                 .Parameter("IsCheckIn", model.IsCheckIn)
                                 .Parameter("CustomerVip", model.ImportanceLevel)
                                 .QueryMany<InviteEventModel>();
            }
        }
        public List<InviteEventModelToExport> GetListOfficialByConditionToExport(EventIndexModel model)
        {
            using (var context = ConnectionDB.BCRMv3())
            {

                return context.StoredProcedure("CI_InviteEvent_GetOfficialByConditionToExport")
                                 .Parameter("SearchText", model.TextSearch)
                                 .Parameter("EventId", model.EventId)
                                 .Parameter("IsCheckIn", model.IsCheckIn)
                                 .Parameter("CustomerVip", model.ImportanceLevel)
                                 .QueryMany<InviteEventModelToExport>();
            }
        }
        public List<InviteEventModelToExport> GetListAnonymousByConditionToExport(EventIndexModel model)
        {
            using (var context = ConnectionDB.BCRMv3())
            {

                return context.StoredProcedure("CI_InviteEvent_GetAnonymousByConditionToExport")
                                 .Parameter("SearchText", model.TextSearch)
                                 .Parameter("EventId", model.EventId)
                                 .Parameter("IsCheckIn", model.IsCheckIn)
                                 .Parameter("CustomerVip", model.ImportanceLevel)
                                 .QueryMany<InviteEventModelToExport>();
            }
        }
        public void UpdateAccompanyInvitee(int inviteId, int accompanyMember)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                context.StoredProcedure("CI_InviteEvent_UpdateAccompanyMember")
                        .Parameter("Id", inviteId)
                        .Parameter("AccompanyMember", accompanyMember)
                        .Execute();
            }
        }
        public void UpdateInviteEventCheckIn(int inviteId, string checkInBy, int accompanyMember)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                context.StoredProcedure("CI_InviteEvent_CheckInWithAccompanyMember")
                            .Parameter("Id", inviteId)
                            .Parameter("AccompanyMember", accompanyMember)
                            .Parameter("CheckInBy", checkInBy)
                            .Execute();
            }
        }
        public void UpdateInviteEventCancel(int inviteId)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                context.StoredProcedure("CI_InviteEvent_Cancel")
                            .Parameter("Id", inviteId)
                            .Execute();
            }
        }
        public void UpdateAttachCheckIn(int attachId, string checkInBy, int accompanyMember)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                context.StoredProcedure("CI_InviteEventAttach_CheckInWithAccompanyMember")
                            .Parameter("Id", attachId)
                            .Parameter("AccompanyMember", accompanyMember)
                            .Parameter("CheckInBy", checkInBy)
                            .Execute();
            }
        }
        public void UpdateAttachCancel(int attachId)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                context.StoredProcedure("CI_InviteEventAttach_Cancel")
                            .Parameter("Id", attachId)
                            .Execute();
            }
        }
        public List<InviteEventModel> GetListCheckedIn(int eventId)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_InviteEvent_GetCheckedIn")
                                .Parameter("EventId", eventId)
                                .QueryMany<InviteEventModel>();
            }
        }
        public List<InviteEventModel> GetByEventId(int eventId)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_InviteEvent_GetByEventId")
                                .Parameter("EventId", eventId)
                                .QueryMany<InviteEventModel>();
            }
        }
        public bool InsertAnonymous(AnonymousInviteeModel model)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_InviteEvent_CreateAnonymous")
                                .Parameter("EventId", model.EventId)
                                .Parameter("CustomerId", model.CustomerId)
                                .Parameter("CustomerName", model.CustomerName)
                                .Parameter("CustomerPhone", model.CustomerPhone)
                                .Parameter("CustomerBirthday", model.CustomerBirthday)
                                .Parameter("CustomerSex", model.CustomerSex)
                                .Parameter("CustomerEmail", model.CustomerEmail)
                                .Parameter("CustomerRevenue", model.CustomerRevenue)
                                .Parameter("CustomerType", model.CustomerType)
                                .Parameter("CustomerPosition", model.CustomerPosition)
                                .Parameter("CustomerVip", model.CustomerVip)
                                .Parameter("CompanyId", model.CompanyId)
                                .Parameter("CompanyName", model.CompanyName)
                                .Parameter("CompanySize", model.CompanySize)
                                .Parameter("CompanyAddress", model.CompanyAddress)
                                .Parameter("TicketAddress", model.TicketAddress)
                                .Parameter("GroupId", model.GroupId)
                                .Parameter("AbilityAttend", model.AbilityAttend)
                                .Parameter("Note", model.Note)
                                .Parameter("Status", model.Status)
                                .Parameter("CreatedBy", model.CreatedBy)
                                .Parameter("AccompanyMember", model.AccompanyMember)
                                .Parameter("CheckInBy", model.CheckInBy)
                                .Parameter("Code", model.Code)
                                .QuerySingle<int>() > 0;
            }
        }
        public bool UpdateAnonymous(AnonymousInviteeModel model)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_InviteEvent_Update")
                                .Parameter("Id", model.Id)
                                .Parameter("EventId", model.EventId)
                                .Parameter("CustomerName", model.CustomerName)
                                .Parameter("CustomerPhone", model.CustomerPhone)
                                .Parameter("CustomerBirthday", model.CustomerBirthday)
                                .Parameter("CustomerSex", model.CustomerSex)
                                .Parameter("CustomerEmail", model.CustomerEmail)
                                .Parameter("CustomerType", model.CustomerType)
                                .Parameter("CustomerPosition", model.CustomerPosition)
                                .Parameter("CustomerVip", model.CustomerVip)
                                .Parameter("CompanyId", model.CompanyId)
                                .Parameter("CompanyName", model.CompanyName)
                                .Parameter("CompanySize", model.CompanySize)
                                .Parameter("CompanyAddress", model.CompanyAddress)
                                .Parameter("TicketAddress", model.TicketAddress)
                                .Parameter("AbilityAttend", model.AbilityAttend)
                                .Parameter("Note", model.Note)
                                .Parameter("Status", model.Status)
                                .Parameter("CreatedBy", model.CreatedBy)
                                .QuerySingle<int>() > 0;
            }
        }
        public AnonymousInviteeModel GetById(int id)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_InviteEvent_GetById")
                                .Parameter("Id", id)
                                .QuerySingle<AnonymousInviteeModel>();
            }
        }
        public bool CheckPhoneExist(int id, int eventId, string customerPhone)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_InviteEvent_IsPhoneExist")
                                .Parameter("Id", id)
                                .Parameter("CustomerPhone", customerPhone)
                                .Parameter("EventId", eventId)
                                .QuerySingle<int>() > 0;
            }
        }
        public bool BCRM_IsExistCode(int eventId, string code)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_InviteEvent_IsExistCode")
                                .Parameter("EventId", eventId)
                                .Parameter("Code", code)
                                .QuerySingle<int>() > 0;
            }
        }
        #endregion

        #region Storeprocedure LocalCheckin
        public bool WebCheckIn_CheckPhoneExist(int id, int eventId, string customerPhone)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_IsPhoneExist")
                                .Parameter("Id", id)
                                .Parameter("CustomerPhone", customerPhone)
                                .Parameter("EventId", eventId)
                                .QuerySingle<int>() > 0;
            }
        }
        public bool WebCheckIn_CheckEmailExist(int id, int eventId, string email)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_IsEmailExist")
                                .Parameter("Id", id)
                                .Parameter("CustomerEmail", email)
                                .Parameter("EventId", eventId)
                                .QuerySingle<int>() > 0;
            }
        }
        public int WebCheckIn_Create(InviteEventWebCheckinModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_Create")
                                .Parameter("EventId", model.EventId)
                                .Parameter("CustomerName", model.CustomerName)
                                .Parameter("CustomerPhone", model.CustomerPhone)
                                .Parameter("Note", model.Note)
                                .Parameter("CreatedBy", model.CreatedBy)
                                .Parameter("IsNotSuggest", model.IsNotSuggest)
                                .Parameter("CustomerEmail", model.CustomerEmail)
                                .Parameter("PositionName", model.PositionName)
                                .Parameter("CompanyName", model.CompanyName)
                                .Parameter("CustomerType", model.CustomerType)
                                .Parameter("PlaceOfAttendEvent", model.PlaceOfAttendEvent)
                                .Parameter("RegisteredSource", model.RegisteredSource)
                                .Parameter("Code", model.Code)
                                .Parameter("Sessions",model.Sessions)
                                .Parameter("IntroduceUser", model.IntroduceUser)
                                .QuerySingle<int>();
            }
        }
        public int WebCheckIn_Update(InviteEventWebCheckinModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_Update")
                                .Parameter("Id", model.Id)
                                .Parameter("EventId", model.EventId)
                                .Parameter("CustomerName", model.CustomerName)
                                .Parameter("CustomerPhone", model.CustomerPhone)
                                .Parameter("CustomerEmail", model.CustomerEmail)
                                .Parameter("CompanyName", model.CompanyName)
                                .Parameter("CustomerType", model.CustomerType)
                                .Parameter("PositionName", model.PositionName)
                                .Parameter("PlaceOfAttendEvent", model.PlaceOfAttendEvent)
                                .Parameter("RegisteredSource", model.RegisteredSource)
                                .Parameter("IntroduceUser", model.IntroduceUser)
                                .Parameter("Note", model.Note)
                                .Parameter("Sessions",model.Sessions)
                                .QuerySingle<int>();
            }
        }
        public int WebCheckIn_UpdateBCRMInvitee(BCRMInviteeEditRequestModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_UpdateBCRMInvitee")
                                .Parameter("Id", model.Id)
                                .Parameter("CustomerName", model.CustomerName)
                                .Parameter("CustomerPhone", model.CustomerPhone)
                                .Parameter("CustomerEmail", model.CustomerEmail)
                                .Parameter("CustomerPosition", model.CustomerPosition)
                                .Parameter("Sessions", model.Sessions)
                                .Parameter("Note", model.Note)
                                .QuerySingle<int>();
            }
        }
        public List<InviteEventModel> WebCheckIn_GetByCondition(EventIndexModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_GetByCondition")
                                .Parameter("EventId", model.EventId)
                                .Parameter("SessionId",model.SessionId)
                                .Parameter("IsCheckIn", model.IsCheckIn)
                                .Parameter("IsNotSuggest", model.IsNotSuggest)
                                .Parameter("TextSearch", string.IsNullOrEmpty(model.TextSearch) ? "" : CommonMethod.ConvertToUnSign(model.TextSearch.ToLower()))
                                .QueryMany<InviteEventModel>();
            }
        }
        public List<InviteEventModelWebCheckinToExport> WebCheckIn_GetByConditionToExport(EventIndexModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_GetByConditionToExport")
                                .Parameter("EventId", model.EventId)
                                .Parameter("SessionId", model.SessionId)
                                .Parameter("IsCheckIn", model.IsCheckIn)
                                .Parameter("IsNotSuggest", model.IsNotSuggest)
                                .Parameter("TextSearch", string.IsNullOrEmpty(model.TextSearch) ? "" : CommonMethod.ConvertToUnSign(model.TextSearch.ToLower()))
                                .QueryMany<InviteEventModelWebCheckinToExport>();
            }
        }
        public InviteEventModel WebCheckIn_GetById(int id)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_GetById")
                                .Parameter("Id", id)
                                .QuerySingle<InviteEventModel>();
            }
        }
        public InviteEventModelWebCheckinToExport WebCheckIn_GetByIdToExport(int id)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_GetByIdToExport")
                                .Parameter("Id", id)
                                .QuerySingle<InviteEventModelWebCheckinToExport>();
            }
        }
        public void WebCheckIn_Checkin(int id, bool isCheckIn, string checkinBy, int accomanyMemeber)
        {
            using (var context = ConnectionDB.MainDB())
            {
                context.StoredProcedure("InviteEvent_UpdateCheckin")
                        .Parameter("IsCheckIn", isCheckIn)
                        .Parameter("CheckInBy", checkinBy)
                        .Parameter("Id", id)
                        .Parameter("AccompanyMember", accomanyMemeber)
                        .Execute();
            }
        }
        public void WebCheckin_Delete(int id)
        {
            using (var context = ConnectionDB.MainDB())
            {
                context.StoredProcedure("InviteEvent_Delete")
                               .Parameter("Id", id)
                               .Execute();
            }
        }
        public void WebCheckin_UpdateAccompanyMember(int id, int accompanyMember)
        {
            using (var context = ConnectionDB.MainDB())
            {
                context.StoredProcedure("InviteEvent_UpdateAccompanyMember")
                            .Parameter("Id", id)
                            .Parameter("AccompanyMember", accompanyMember)
                            .Execute();
            }
        }
        public List<InviteEventModel> WebCheckIn_GetByEventId(int eventId)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_GetByEventId")
                                .Parameter("EventId", eventId)
                                .QueryMany<InviteEventModel>();
            }
        }
        public int WebCheckIn_API_Insert(OnlineInviteeModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("API_InviteEvent_RegisterOnline")
                                .Parameter("CustomerName", model.CustomerName)
                                .Parameter("PositionName", model.PositionName)
                                .Parameter("CustomerPhone", model.CustomerPhone)
                                .Parameter("IsUsingWebsiteBds", model.IsUsingWebsiteBds)
                                .Parameter("CustomerEmail", model.CustomerEmail)
                                .Parameter("UserName", model.UserName)
                                .Parameter("CustomerType", model.CustomerType)
                                .Parameter("RegisteredDate", model.RegisteredDate)
                                .Parameter("CompanyName", model.CompanyName)
                                .Parameter("PlaceOfAttendEvent", model.PlaceOfAttendEvent)
                                .Parameter("EventId", model.EventId)
                                .Parameter("RegisteredSource", model.RegisteredSource)
                                .Parameter("Code", model.Code)
                                .Parameter("Sessions",model.Sessions)
                                .QuerySingle<int>();
            }
        }
        public bool WebCheckin_IsExistCode(int eventId, string code)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_IsExistCode")
                                .Parameter("EventId", eventId)
                                .Parameter("Code", code)
                                .QuerySingle<int>() > 0;
            }
        }
        public int InsertOnlineInvitee(OnlineInviteeModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_InsertOnlineInvitee")
                                .Parameter("EventId", model.EventId)
                                .Parameter("CustomerName", model.CustomerName)
                                .Parameter("IntroduceUser", model.IntroduceUser)
                                .Parameter("CustomerPhone", model.CustomerPhone)
                                .Parameter("CustomerEmail", model.CustomerEmail)
                                .Parameter("CompanyName", model.CompanyName)
                                .Parameter("CustomerType", model.CustomerType)
                                .Parameter("PositionName", model.PositionName)
                                .Parameter("PlaceOfAttendEvent", model.PlaceOfAttendEvent)
                                .Parameter("RegisteredDate", model.RegisteredDate)
                                .Parameter("CreatedBy", model.CreatedBy)
                                .Parameter("PhoningStatus", model.PhoningStatus)
                                .Parameter("ConfirmAttend1st", model.ConfirmAttend1st)
                                .Parameter("ConfirmAttend2st", model.ConfirmAttend2st)
                                .Parameter("Note", model.Note)
                                .Parameter("RegisteredSource", model.RegisteredSource)
                                .Parameter("Code", model.Code)
                                .Parameter("Sessions",model.Sessions)
                                .QuerySingle<int>();
            }
        }
        public bool UpdateOnlineInvitee(OnlineInviteeModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_UpdateOnlineInvitee")
                                .Parameter("Id", model.Id)
                                .Parameter("EventId", model.EventId)
                                .Parameter("CustomerName", model.CustomerName)
                                .Parameter("IntroduceUser", model.IntroduceUser)
                                .Parameter("CustomerPhone", model.CustomerPhone)
                                .Parameter("CustomerEmail", model.CustomerEmail)
                                .Parameter("CompanyName", model.CompanyName)
                                .Parameter("CustomerType", model.CustomerType)
                                .Parameter("PositionName", model.PositionName)
                                .Parameter("PlaceOfAttendEvent", model.PlaceOfAttendEvent)
                                .Parameter("PhoningStatus", model.PhoningStatus)
                                .Parameter("ConfirmAttend1st", model.ConfirmAttend1st)
                                .Parameter("ConfirmAttend2st", model.ConfirmAttend2st)
                                .Parameter("Note", model.Note)
                                .Parameter("RegisteredSource", model.RegisteredSource)
                                .Parameter("Sessions", model.Sessions)
                                .Execute() > 0;
            }
        }
        public void WebCheckIn_UpdateStatus(int id, bool status, DateTime? approveDate, string appoveBy)
        {
            using (var context = ConnectionDB.MainDB())
            {
                context.StoredProcedure("InviteEvent_UpdateStatus")
                        .Parameter("Id", id)
                        .Parameter("Status", status)
                        .Parameter("ApproveDate", approveDate)
                        .Parameter("ApproveBy", appoveBy)
                        .Execute();
            }
        }
        public OnlineInviteeForSearchModel WebCheckin_GetByCode(int eventId, string code)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_GetByCode")
                                .Parameter("EventId",eventId)
                                .Parameter("Code", code)
                                .QuerySingle<OnlineInviteeForSearchModel>();
            }
        }
        public List<InviteEventModel> WebCheckin_GetByCodeToCheckin(int eventId, string code)
        {
            using(var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_GetByCodeToCheckin")
                                .Parameter("EventId",eventId)
                                .Parameter("Code",code)
                                .QueryMany<InviteEventModel>();
            }
        }
        public List<InviteEventModelWebCheckinToExport> WebCheckin_GetByCodeToExport(int eventId, string code)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_GetByCodeToExport")
                                .Parameter("EventId", eventId)
                                .Parameter("Code", code)
                                .QueryMany<InviteEventModelWebCheckinToExport>();
            }
        }
        public List<OnlineInviteeForSearchModel> GetListOnlineRegisteredInvitee(FilterForOnlineRegisterSearchModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_GetListOnlineRegisterer")
                                .Parameter("EventId", model.EventId)
                                .Parameter("AssignTo", model.AssignTo)
                                .Parameter("SessionId",model.SessionId)
                                .QueryMany<OnlineInviteeForSearchModel>();
            }
        }
        public List<OnlineInviteeForSearchModel> SearchByCondition(FilterForOnlineRegisterSearchModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_SearchInviteeByCondition")
                                .Parameter("EventId", model.EventId)
                                .Parameter("AssignTo", model.AssignTo)
                                .Parameter("SessionId", model.SessionId)
                                .Parameter("PhoningStatus", model.PhoningStatus)
                                .Parameter("ConfirmAttend1st", model.ConfirmAttend1st)
                                .Parameter("ConfirmAttend2st", model.ConfirmAttend2st)
                                .Parameter("Status", model.Status)
                                .Parameter("SendTicketStatus", model.SendTicketStatus)
                                .Parameter("SourceId", model.SourceId)
                                .Parameter("TextSearch", string.IsNullOrEmpty(model.TextSearch) ? "" : CommonMethod.ConvertToUnSign(model.TextSearch.ToLower()))
                                .Parameter("StartDate",model.StartDate)
                                .Parameter("EndDate", model.EndDate)
                                .Parameter("IsAssignTo", model.IsAssignTo)
                                .Parameter("IsBcrmAssignTo", model.IsBcrmAssignTo)
                                .QueryMany<OnlineInviteeForSearchModel>();
            }
        }
        public OnlineInviteeModel GetOnlineRegisteredInviteeById(int id)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_GetOnlineRegisterById")
                                .Parameter("Id", id)
                                .QuerySingle<OnlineInviteeModel>();
            }
        }
        public OnlineInviteeForSearchModel WebCheckIn_GetRegisteredInviteeById(int id)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_SearchOnlineRegistererById")
                                .Parameter("Id", id)
                                .QuerySingle<OnlineInviteeForSearchModel>();
            }
        }
        public bool WebCheckin_IsExistInEventId(int eventId)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_ExistInEventId")
                                .Parameter("EventId", eventId)
                                .QuerySingle<int>() > 0;
            }
        }
        public void WebCheckin_UpdateAssignTo(int id, string assignTo)
        {
            using (var context = ConnectionDB.MainDB())
            {
                context.StoredProcedure("InviteEvent_UpdateAssignTo")
                        .Parameter("Id", id)
                        .Parameter("AssignTo", assignTo)
                        .Execute();
            }
        }
        public void WebCheckin_UpdateSendTicketStatus(int id, int status,int bcrmStatus)
        {
            using (var context = ConnectionDB.MainDB())
            {
                context.StoredProcedure("InviteEvent_UpdateSendTicketStatus")
                        .Parameter("Id", id)
                        .Parameter("SendTicketStatus", status)
                        .Parameter("BcrmStatus", bcrmStatus)
                        .Execute();
            }
        }
        public void WebCheckin_UpdateSessions(int id, string sessions)
        {
            using (var context = ConnectionDB.MainDB())
            {
                context.StoredProcedure("InviteEvent_ChangeSession")
                        .Parameter("Id", id)
                        .Parameter("Sessions", sessions)
                        .Execute();
            }
        }
        public void WebCheckin_SyncToBCRM(InviteEventModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                context.StoredProcedure("InviteEvent_SyncToBCRM")
                        .Parameter("Id", model.Id)
                        .Parameter("CustomerId", model.CustomerId)
                        .Parameter("CustomerBirthday", model.CustomerBirthday)
                        .Parameter("CustomerType", model.CustomerType)
                        .Parameter("CompanyId", model.CompanyId)
                        .Parameter("CompanyName", model.CompanyName)
                        .Parameter("CompanyAddress", model.CompanyAddress)
                        .Parameter("CompanySize", model.CompanySize)
                        .Parameter("CustomerRevenue", model.CustomerRevenue)
                        .Parameter("BcrmStatus", model.BcrmStatus.GetHashCode())
                        .Parameter("IsBcrmProcess", model.IsBcrmProcess)
                        .Parameter("BcrmAssignTo", model.BcrmAssignTo)
                        .Parameter("GroupId", model.GroupId)
                        .Execute();
            }
        }
        public bool WebCheckin_HasInviteeInEvent(int eventId)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEvent_IsHasInviteeInEvent")
                                .Parameter("EventId", eventId)
                                .QuerySingle<int>() > 0;
            }
        }
        #endregion
    }
}