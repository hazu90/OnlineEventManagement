﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCRM.CheckInEvent.Models;

namespace BCRM.CheckInEvent.DAL
{
    public class InviteEventAttachDL
    {
        public List<InviteEventModel> GetListCheckedIn(int eventId)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_InviteEventAttach_GetCheckedIn")
                                .Parameter("EventId", eventId)
                                .QueryMany<InviteEventModel>();
            }       
        }
        public List<InviteEventModel> GetListByInviteeCode(int eventId, int inviteEventId,int attachId)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                var lstInviteEventModel = context.StoredProcedure("CI_InviteEventAttach_SearchByInviteeCode")
                                 .Parameter("EventId", eventId)
                                 .Parameter("InviteEventId", inviteEventId)
                                 .Parameter("AttachId", attachId)
                                 .QueryMany<InviteEventModel>();
                return lstInviteEventModel;
            }
        }
        /// <summary>
        /// Lấy danh sách khách mời theo sự kiện và mã code
        /// </summary>
        /// <param name="eventId">Id sự kiện</param>
        /// <param name="code">Mã code</param>
        /// <returns></returns>
        public InviteEventModel GetListByCode(int eventId, string code)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_InviteEventAttach_SearchByCode")
                                 .Parameter("EventId", eventId)
                                 .Parameter("Code", code)
                                 .QuerySingle<InviteEventModel>();
            }
        }
        /// <summary>
        /// Lấy danh sách khách mời theo sự kiện và mã code để export excel
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public List<InviteEventModelToExport> GetListByCodeToExport(int eventId, string code)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                var lstInviteEventModel = context.StoredProcedure("CI_InviteEventAttach_GetByCodeToExport")
                                 .Parameter("EventId", eventId)
                                 .Parameter("Code", code)
                                 .QueryMany<InviteEventModelToExport>();
                return lstInviteEventModel;
            }
        }

        public List<InviteEventModelToExport> GetListByInviteeCodeToExport(int eventId, int inviteEventId, int attachId)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                var lstInviteEventModel = context.StoredProcedure("CI_InviteEventAttach_GetByInviteeCodeToExport")
                                 .Parameter("EventId", eventId)
                                 .Parameter("InviteEventId", inviteEventId)
                                 .Parameter("AttachId", attachId)
                                 .QueryMany<InviteEventModelToExport>();
                return lstInviteEventModel;
            }
        }
        public List<InviteEventModel> GetListByCondition(EventIndexModel model)
        {
            using (var context = ConnectionDB.BCRMv3())
            {

                return context.StoredProcedure("CI_InviteEventAttach_SearchByCondition")
                                 .Parameter("SearchText", model.TextSearch)
                                 .Parameter("EventId", model.EventId)
                                 .Parameter("IsCheckIn", model.IsCheckIn)
                                 .Parameter("CustomerVip", model.ImportanceLevel)
                                 .QueryMany<InviteEventModel>();
            }
        }
        public List<InviteEventModelToExport> GetListByConditionToExport(EventIndexModel model)
        {
            using (var context = ConnectionDB.BCRMv3())
            {

                return context.StoredProcedure("CI_InviteEventAttach_GetByConditionToExport")
                                 .Parameter("SearchText", model.TextSearch)
                                 .Parameter("EventId", model.EventId)
                                 .Parameter("IsCheckIn", model.IsCheckIn)
                                 .Parameter("CustomerVip", model.ImportanceLevel)
                                 .QueryMany<InviteEventModelToExport>();
            }
        }
        public List<InviteEventModel> GetByEventId(int eventId)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_InviteEventAttach_GetByEventId")
                                .Parameter("EventId", eventId)
                                .QueryMany<InviteEventModel>();
            }
        }
        public void UpdateAccompanyInvitee(int inviteId, int accompanyMember)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                context.StoredProcedure("CI_InviteEventAttach_UpdateAccompanyMember")
                        .Parameter("Id", inviteId)
                        .Parameter("AccompanyMember", accompanyMember)
                        .Execute();
            }
        }
        public bool CheckPhoneExist(int eventId, string customerPhone)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_InviteEventAttach_IsPhoneExist")
                                .Parameter("CustomerPhone", customerPhone)
                                .Parameter("EventId", eventId)
                                .QuerySingle<int>() > 0;
            }
        }
        public bool IsExistCode(int eventId, string code)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_InviteEventAttach_IsExistCode")
                                .Parameter("EventId", eventId)
                                .Parameter("Code", code)
                                .QuerySingle<int>() > 0;
            }
        }
    }
}