﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCRM.CheckInEvent.Models;

namespace BCRM.CheckInEvent.DAL
{
    public class BCRMCompanyDL
    {
        public CompanyModel GetById(int id)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_Company_GetById")
                                .Parameter("Id", id)
                                .QuerySingle<CompanyModel>();
            }
        }
    }
}