﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCRM.CheckInEvent.Models;

namespace BCRM.CheckInEvent.DAL
{
    public class DistrictDL
    {
        public List<DistrictModel> GetAll()
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_District_GetAll").QueryMany<DistrictModel>();
            }
        }
    }
}