﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCRM.CheckInEvent.Models;

namespace BCRM.CheckInEvent.DAL
{
    public class InviteEventHistoryDL
    {
        public int Insert(InviteEventHistoryModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                //return context.StoredProcedure("InviteEventHistory_Insert")
                //                .Parameter("OldPhoningStatus", model.OldPhoningStatus)
                //                .Parameter("OldConfirmAttend1st", model.OldConfirmAttend1st)
                //                .Parameter("OldConfirmAttend2st", model.OldConfirmAttend2st)
                //                .Parameter("OldPlaceOfAttendEvent", model.OldPlaceOfAttendEvent)
                //                .Parameter("NewPhoningStatus", model.NewPhoningStatus)
                //                .Parameter("NewConfirmAttend1st", model.NewConfirmAttend1st)
                //                .Parameter("NewConfirmAttend2st", model.NewConfirmAttend2st)
                //                .Parameter("NewPlaceOfAttendEvent", model.NewPlaceOfAttendEvent)
                //                .Parameter("Status", model.Status)
                //                .Parameter("UpdatedBy", model.UpdatedBy)
                //                .Parameter("InviteEventId", model.InviteEventId)
                //                .Parameter("OtherChange", model.OtherChange)
                //                .QuerySingle<int>() > 0;
                return context.StoredProcedure("InviteEventHistory_Insert")
                                .Parameter("InviteEventId", model.InviteEventId)
                                .Parameter("Action", model.Action)
                                .Parameter("Note", model.Note)
                                .Parameter("OldValue", model.OldValue)
                                .Parameter("NewValue", model.NewValue)
                                .Parameter("UpdatedBy", model.UpdatedBy)
                                .QuerySingle<int>();

            }
        }

        public List<InviteEventHistoryModel> GetByInviteEventId(int inviteEventId)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("InviteEventHistory_GetByInviteEventId")
                                .Parameter("InviteEventId", inviteEventId)
                                .QueryMany<InviteEventHistoryModel>();
            }
        }
    }
}