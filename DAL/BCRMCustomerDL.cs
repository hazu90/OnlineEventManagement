﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCRM.CheckInEvent.Models;

namespace BCRM.CheckInEvent.DAL
{
    public class BCRMCustomerDL
    {
        public List<CustomerModel> GetByPhoneNumber(string phoneNumber)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_Customer_SelectPhoneNumber")
                                .Parameter("PhoneNumber", phoneNumber)
                                .Parameter("IsDeleted", "0")
                                .QueryMany<CustomerModel>();

            }
        }

        public CustomerModel GetById(int customerId)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_Customer_GetById")
                                .Parameter("Id", customerId)
                                .QuerySingle<CustomerModel>();

            }
        }
    }
}