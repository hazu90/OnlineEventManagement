﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCRM.CheckInEvent.Models;

namespace BCRM.CheckInEvent.DAL
{
    public class SetDefaultEventDL
    {
        public bool Create(SetDefaultEventModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("SetDefaultEvent_Create")
                                .Parameter("EventId", model.EventId)
                                .Parameter("CreatedBy", model.CreatedBy)
                                .Parameter("IsInWebCheckin", model.IsInWebCheckin)
                                .QuerySingle<int>() > 0;
            }
        }

        public SetDefaultEventModel GetLastest()
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("SetDefaultEvent_GetLastest")
                                .QuerySingle<SetDefaultEventModel>();
            }
        }

        public void Cancel(int id)
        {
            using (var context = ConnectionDB.MainDB())
            {
                context.StoredProcedure("SetDefaultEvent_Cancel")
                        .Parameter("Id",id)
                        .Execute();
            }
        }
    }
}