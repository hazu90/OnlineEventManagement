﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCRM.CheckInEvent.Models;

namespace BCRM.CheckInEvent.DAL
{
    public class SendSMSHistoryDL
    {
        public bool Insert(SendSMSHistoryModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("SendSMSHistory_Insert")
                            .Parameter("SMSResponse", model.SMSResponse)
                            .Parameter("Description", model.Description)
                            .Parameter("InviteEventId", model.InviteEventId)
                            .Parameter("PhoneNumber", model.PhoneNumber)
                            .Parameter("SMSContent", model.SMSContent)
                            .Parameter("EventId", model.EventId)
                            .QuerySingle<int>() > 0;
            }
        }
    }
}