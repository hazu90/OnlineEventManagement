﻿using BCRM.CheckInEvent.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.DAL
{
    public class SessionCheckinDL
    {
        public int Insert(SessionCheckinModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("SessionCheckin_Insert")
                                .Parameter("InviteEventId", model.InviteEventId)
                                .Parameter("SessionId", model.SessionId)
                                .Parameter("CheckinBy", model.CheckinBy)
                                .Parameter("CheckinDate", model.CheckinDate)
                                .Parameter("EventId", model.EventId)
                                .Parameter("AccompanyMember", model.AccompanyMember)
                                .Execute();
            }
        }


        public bool IsExist(int inviteEventId, int sessionId)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("SessionCheckin_IsExist")
                                .Parameter("InviteEventId", inviteEventId)
                                .Parameter("SessionId", sessionId)
                                .QuerySingle<int>() >0;
            }
        }

        public List<SessionCheckinModel> GetByInviteEventId(int inviteEventId)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("SessionCheckin_GetByInviteEventId")
                                .Parameter("InviteEventId", inviteEventId)
                                .QueryMany<SessionCheckinModel>();
            }
        }

        public List<SessionCheckinModel> GetByInviteeIds(int eventId, string inviteeIds,int sessionId)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("SessionCheckin_GetByInviteeIds")
                                .Parameter("EventId",eventId)
                                .Parameter("InviteeIds", inviteeIds)
                                .Parameter("SessionId",sessionId)
                                .QueryMany<SessionCheckinModel>();
            }
        }
        public void Reject(int inviteeId, int sessionId)
        {
            using (var context = ConnectionDB.MainDB())
            {
                context.StoredProcedure("SessionCheckin_Reject")
                        .Parameter("InviteeId", inviteeId)
                        .Parameter("SessionId", sessionId)
                        .Execute();
            }
        }

        public List<SessionCheckinModel> GetBySessionAndSuggeestInvitee(int eventId, int sessionId, bool isNotSuggest)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("SessionCheckin_GetBySessionAndSuggest")
                                .Parameter("EventId", eventId)
                                .Parameter("SessionId", sessionId)
                                .Parameter("IsNotSuggest", isNotSuggest)
                                .QueryMany<SessionCheckinModel>();
            }
        }
    }
}