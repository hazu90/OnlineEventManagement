﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCRM.CheckInEvent.Models;

namespace BCRM.CheckInEvent.DAL
{
    public class SessionDL
    {
        public int Create(SessionModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("Session_Create")
                                .Parameter("EventId", model.EventId)
                                .Parameter("Name", model.Name)
                                .Parameter("StartDate", model.StartDate)
                                .Parameter("EndDate", model.EndDate)
                                .Parameter("Description", model.Description)
                                .QuerySingle<int>();
            }
        }

        public List<SessionModel> GetByEventId(int eventId)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("Session_GetByEventId")
                                .Parameter("EventId", eventId)
                                .QueryMany<SessionModel>();
            }
        }

        public void Delete(int sessionId)
        {
            using (var context = ConnectionDB.MainDB())
            {
                context.StoredProcedure("Session_Delete")
                        .Parameter("Id", sessionId)
                        .Execute();
            }
        }
    }
}