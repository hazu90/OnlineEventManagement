﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCRM.CheckInEvent.Models;

namespace BCRM.CheckInEvent.DAL
{
    public class CheckInTimerDL
    {
        public List<CheckInTimerModel> GetList(string seriesEventIds,bool isInWebCheckin)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("CheckInTimer_GetByEventIds")
                                .Parameter("EventIds",seriesEventIds)
                                .Parameter("IsInWebCheckin",isInWebCheckin)
                                .QueryMany<CheckInTimerModel>();
            }
        }

        public bool Insert(CheckInTimerModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("CheckInTimer_Insert")
                                .Parameter("EventId", model.EventId)
                                .Parameter("StartTime", model.StartTime)
                                .Parameter("EndTime", model.EndTime)
                                .Parameter("CreatedBy", model.CreatedBy)
                                .Parameter("IsInWebCheckIn", model.IsInWebCheckIn)
                                .QuerySingle<int>()>0;
            }
        }

        public bool IsExist(int eventId,bool isInWebCheckin)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("CheckInTimer_Exists")
                                .Parameter("EventId", eventId)
                                .Parameter("IsInWebCheckin", isInWebCheckin)
                                .QuerySingle<int>() > 0;
            }
        }

        public CheckInTimerModel GetById(int id)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("CheckInTimer_GetById")
                                .Parameter("Id", id)
                                .QuerySingle<CheckInTimerModel>() ;
            }
        }

        public CheckInTimerModel GetByEventId(int eventId)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("CheckInTimer_GetByEventId")
                                .Parameter("EventId", eventId)
                                .QuerySingle<CheckInTimerModel>();
            }
        }

        public bool Update(CheckInTimerModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("CheckInTimer_Update")
                                .Parameter("EventId", model.EventId)
                                .Parameter("StartTime", model.StartTime)
                                .Parameter("EndTime", model.EndTime)
                                .Parameter("UpdatedBy", model.UpdatedBy)
                                .Parameter("Id", model.Id)
                                .QuerySingle<int>()>0;
            }
        }

        public void Delete(int id)
        {
            using (var context = ConnectionDB.MainDB())
            {
                context.StoredProcedure("CheckInTimer_Delete")
                                .Parameter("Id", id)
                                .Execute();
            }
        }

        public List<CheckInTimerModel> GetAll()
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("CheckInTimer_GetAll")
                                .QueryMany<CheckInTimerModel>();
            }
        }
    }
}