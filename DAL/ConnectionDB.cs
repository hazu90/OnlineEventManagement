﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentData;
using System.Configuration;

namespace BCRM.CheckInEvent.Models
{
    public class ConnectionDB
    {
        public static IDbContext MainDB()
        {
            return new DbContext().ConnectionString(ConfigurationManager.AppSettings["DVSCheckIn"], new SqlServerProvider());
        }

        public static IDbContext BCRMv3()
        {
            return new DbContext().ConnectionString(ConfigurationManager.AppSettings["BCRMv3"], new SqlServerProvider());
        }
    }
}