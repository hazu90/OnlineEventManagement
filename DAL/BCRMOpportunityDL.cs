﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCRM.CheckInEvent.Models;

namespace BCRM.CheckInEvent.DAL
{
    public class BCRMOpportunityDL
    {
        public List<OpportunityIncludeTransactionModel> GetByCustomerId(int customerId)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_Opportunity_SelectByCustomerId")
                            .Parameter("CustomerId", customerId)
                            .QueryMany<OpportunityIncludeTransactionModel>();
            }
        }
    }
}