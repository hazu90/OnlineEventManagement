﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCRM.CheckInEvent.Models;

namespace BCRM.CheckInEvent.DAL
{
    public class LuckyNumberDL
    {
        public List<LuckyNumberModel> GetByEventId(int eventId, bool isInWebCheckin)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("LuckyNumber_GetByEventId")
                                .Parameter("EventId", eventId)
                                .Parameter("IsInWebCheckin", isInWebCheckin)
                                .QueryMany<LuckyNumberModel>();

            }
        }

        public int Insert(LuckyNumberModel model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("LuckyNumber_Insert")
                                .Parameter("EventId",model.EventId)
                                .Parameter("IsInWebCheckin", model.IsInWebCheckin)
                                .Parameter("Code", model.Code)
                                .Parameter("CreatedBy", model.CreatedBy)
                                .QuerySingle<int>();
            }
        }

        public LuckyNumberModel GetByCodeAndEventId(int eventId, bool isInWebCheckin, string code)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("LuckyNumber_GetByCodeAndEventId")
                                .Parameter("EventId", eventId)
                                .Parameter("IsInWebCheckin", isInWebCheckin)
                                .Parameter("Code", code)
                                .QuerySingle<LuckyNumberModel>();
            }
        }

        public void Delete(int id)
        {
            using (var context = ConnectionDB.MainDB())
            {
                context.StoredProcedure("LuckyNumber_Delete")
                                .Parameter("Id", id)
                                .Execute();
            }
        }

        public LuckyNumberModel GetById(int id)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("LuckyNumber_GetById")
                                .Parameter("Id", id)
                                .QuerySingle<LuckyNumberModel>();
            }
        }
    }
}