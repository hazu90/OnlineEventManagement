﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCRM.CheckInEvent.Models;

namespace BCRM.CheckInEvent.DAL
{
    public class CityDL
    {
        public List<CityModel> GetAllCity()
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_City_GetAll").QueryMany<CityModel>();
            }
        }
    }
}