﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCRM.CheckInEvent.Models;

namespace BCRM.CheckInEvent.DAL
{
    public class EventDL
    {
        #region Using BCRM db
        public List<EventModel> GetAll()
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_Event_GetAll")
                            .QueryMany<EventModel>();
            }
        }

        public EventModel GetEventById(int eventId)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_Event_GetById")
                            .Parameter("Id", eventId)
                            .QuerySingle<EventModel>();
            }
        }

        public InviteEventModelForAPI GetInfo(int inviteId, int attachId)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_InviteEvent_GetInfo")
                            .Parameter("InviteId", inviteId)
                            .Parameter("AttachId", attachId).QuerySingle<InviteEventModelForAPI>();
            }
        }

        public void UpdateInviteEventCheckIn(int inviteId, string checkInBy)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                context.StoredProcedure("CI_InviteEvent_CheckIn")
                            .Parameter("Id", inviteId)
                            .Parameter("CheckInBy", checkInBy)
                            .Execute();
            }
        }

        public void UpdateAttachCheckIn(int attachId, string checkInBy)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                context.StoredProcedure("CI_InviteEventAttach_CheckIn")
                            .Parameter("Id", attachId)
                            .Parameter("CheckInBy", checkInBy)
                            .Execute();
            }
        }
        #endregion

        #region Using WebCheckin
        public int CreateInWebCheckIn(EventModelForCreate model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("Event_Create")
                                .Parameter("Name", model.Name)
                                .Parameter("Description", model.Description)
                                .Parameter("StartDate", model.StartDate)
                                .Parameter("EndDate", model.EndDate)
                                .Parameter("IsEnable", model.IsEnable)
                                .Parameter("CreatedBy", model.CreatedBy)
                                .Parameter("EventCode", model.EventCode)
                                .Parameter("SMSTitle", model.SMSTitle)
                                .Parameter("IsBcrmUse", model.IsBcrmUse)
                                .Parameter("IsAllowAttach", model.IsAllowAttach)
                                .Parameter("RevenueStartDate", model.RevenueStartDate)
                                .Parameter("RevenueEndDate", model.RevenueEndDate)
                                .Parameter("RevenueCalculation", model.RevenueCalculation)
                                .Parameter("Deploy", model.Deploy)
                                .Parameter("Deadline",model.Deadline)
                                .QuerySingle<int>();
            }
        }

        public void UpdateInWebCheckIn(EventModelForCreate model)
        {
            using (var context = ConnectionDB.MainDB())
            {
                context.StoredProcedure("Event_Edit")
                        .Parameter("EventCode", model.EventCode)
                        .Parameter("IsEnable", model.IsEnable)
                        .Parameter("Name", model.Name)
                        .Parameter("StartDate", model.StartDate)
                        .Parameter("EndDate", model.EndDate)
                        .Parameter("Description", model.Description)
                        .Parameter("SMSTitle", model.SMSTitle)
                        .Parameter("IsBcrmUse", model.IsBcrmUse)
                        .Parameter("IsAllowAttach", model.IsAllowAttach)
                        .Parameter("RevenueStartDate", model.RevenueStartDate)
                        .Parameter("RevenueEndDate", model.RevenueEndDate)
                        .Parameter("RevenueCalculation", model.RevenueCalculation)
                        .Parameter("Deploy", model.Deploy)
                        .Parameter("Deadline", model.Deadline)
                        .Parameter("Id", model.Id)
                        .Execute();
            }
        }

        public void UpdateStatusInWebCheckIn(int id, bool isEnable)
        {
            using (var context = ConnectionDB.MainDB())
            {
                context.StoredProcedure("Event_UpdateStatus")
                        .Parameter("Id", id)
                        .Parameter("IsEnable", isEnable)
                        .Execute();
            }
        }

        public List<EventModel> GetListInWebCheckIn(string filterText,int deployId, DateTime? startDate, DateTime? endDate)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("Event_GetList")
                                .Parameter("DeployId", deployId)
                                .Parameter("FilterText", filterText)
                                .Parameter("StartDate", startDate)
                                .Parameter("EndDate", endDate)
                                .QueryMany<EventModel>();
            }
        }

        public EventModel GetByEventCodeInWebCheckIn(int id, string eventCode)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("Event_GetByCode")
                                .Parameter("EventCode", eventCode)
                                .Parameter("Id", id)
                                .QuerySingle<EventModel>();
            }
        }

        public EventModel GetByIdInWebCheckIn(int id)
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("Event_GetById")
                                .Parameter("Id", id)
                                .QuerySingle<EventModel>();
            }
        }

        public List<EventModel> GetAllInWebCheckIn()
        {
            using (var context = ConnectionDB.MainDB())
            {
                return context.StoredProcedure("Event_GetAll")
                                .QueryMany<EventModel>();
            }
        }
        #endregion
        
    }

}