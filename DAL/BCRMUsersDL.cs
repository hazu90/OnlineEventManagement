﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BCRM.CheckInEvent.Models;

namespace BCRM.CheckInEvent.DAL
{
    public class BCRMUsersDL
    {
        public List<SaleInfoModel> GetByRole(int role)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_Users_GetByRole")
                        .Parameter("RoleId", role)
                        .QueryMany<SaleInfoModel>();
            }
        }

        public SaleInfoModel GetByUsername(string userName)
        {
            using (var context = ConnectionDB.BCRMv3())
            {
                return context.StoredProcedure("CI_Users_GetByUserName")
                                .Parameter("UserName", userName)
                                .QuerySingle<SaleInfoModel>();
            }
        }
    }
}