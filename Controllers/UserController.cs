﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BCRM.CheckInEvent.Models;
using DVS.Algorithm;
using Libs;
using BCRM.CheckInEvent.Common;
using BCRM.CheckInEvent.DAL;

namespace BCRM.CheckInEvent.Controllers
{
    public class UserController : BaseController
    {
        [FilterAuthorize]
        public ActionResult Index()
        {
            if (UserContext.HasPermisson(Permission.Manager) || UserContext.HasPermisson(Permission.Admin))
            {
                ViewBag.CurentUser = UserContext;
                return View();
            }
            else
            {
                return View("~/Views/Common/ErrorManual.cshtml", null, "Bạn không có quyền thực hiện chức năng này");
            }
        }
        [FilterAuthorize]
        public ActionResult Search(UserIndexModel model)
        {
            if (UserContext.HasPermisson(Permission.Manager) || UserContext.HasPermisson(Permission.Admin))
            {
                var userDL = new UserDL();
                var result = userDL.GetList(model);
                var userRoleDL = new UserRoleDL();
                foreach (var userInfo in result.LstUser)
                {
                    // Lấy danh sách các role
                    var lstRoles = userRoleDL.GetByUserName(userInfo.UserName);
                    userInfo.PermissionRoles = lstRoles.Select(o => o.RoleId).ToList();
                }
                var pager = new PagerModel { CurrentPage = result.PageIndex, PageSize = result.PageSize, TotalItem = result.TotalRecord };
                ViewBag.Pager = pager;
                ViewBag.CurentUser = UserContext;
                return View(result);
            }
            else
            {
                return View("~/Views/Common/ErrorManual.cshtml", null, "Bạn không có quyền thực hiện chức năng này");
            }
        }
        [FilterAuthorize]
        public JsonResult Active(string userName)
        {
            var response = new Response();
            if (UserContext.HasPermisson(Permission.Manager) || UserContext.HasPermisson(Permission.Admin))
            {
                if (!string.IsNullOrEmpty(userName))
                {
                    (new UserDL()).ChangeStatus(userName,1);
                    response.Code = SystemCode.Success;
                }
                else
                {
                    response.Code = SystemCode.DataNull;
                }
            }
            else
            {
                response.Code = SystemCode.NotPermitted;
            }
            return Json(response, JsonRequestBehavior.AllowGet);

        }
        [FilterAuthorize]
        public JsonResult DeActive(string userName)
        {
            var response = new Response();
            if (UserContext.HasPermisson(Permission.Manager) || UserContext.HasPermisson(Permission.Admin))
            {
                if (!string.IsNullOrEmpty(userName))
                {
                    (new UserDL()).ChangeStatus(userName, 2);
                    response.Code = SystemCode.Success;
                }
                else
                {
                    response.Code = SystemCode.DataNull;
                }
            }
            else
            {
                response.Code = SystemCode.NotPermitted;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public ActionResult AddNew()
        {
            return View();
        }
        [FilterAuthorize]
        public ActionResult EditAction(string userName)
        {
            var userDL = new UserDL();
            var model = userDL.GetByUserName(userName);

            // Get roles 
            var userRoleDL = new UserRoleDL();
            var userRoles = userRoleDL.GetByUserName(userName);
            model.PermissionRoles = new List<int>();
            model.PermissionRoles.AddRange(userRoles.Select(o => o.RoleId).ToList());
            //var userRoles = 
            return View(model);
        }
        [FilterAuthorize]
        public JsonResult Edit(UserModel model)
        {
            var response = new Response();
            if (UserContext.HasPermisson(Permission.Admin))
            {
                if (!string.IsNullOrEmpty(model.UserName))
                {
                    //convert pass to md5
                    if (string.IsNullOrEmpty(model.PassWord))
                    {
                        model.PassWord = "";
                    }
                    else
                    {
                        model.PassWord = model.PassWord.ToMD5();
                    }

                    var result = (new UserDL()).Update(model);
                    if (result)
                    {
                        // Cập nhật thông tin quyền của người dùng
                        var userRoleDL = new UserRoleDL();
                        var updatedRoles = model.UserRoles.Split(',').ToList();
                        var lstRoles = userRoleDL.GetByUserName(model.UserName);
                        // Insert role mới , xóa các role cũ, giữ nguyên các role không thay đổi
                        // Lấy ra các role cần xóa
                        var lstDeletedRole = new List<UserRoleModel>();
                        foreach(var roleInfo in lstRoles)
                        {
                            if(updatedRoles.FindAll(o=>o == roleInfo.RoleId.ToString()).Count ==0)
                            {
                                lstDeletedRole.Add(roleInfo);
                            }
                        }
                        // Lấy ra các role cần insert
                        var lstInsertedRole = new List<UserRoleModel>();
                        foreach(var roleUser in updatedRoles)
                        {
                            var roleId = int.Parse(roleUser);
                            if (lstRoles.FindAll(o => o.RoleId.ToString() == roleUser).Count == 0)
                            {
                                lstInsertedRole.Add(new UserRoleModel() {
                                    RoleId = int.Parse(roleUser),
                                    UserName = model.UserName
                                });
                            }
                        }

                        // Xóa các role
                        foreach (var item in lstDeletedRole)
                        {
                            userRoleDL.Delete(item.Id);
                        }
                        // Thêm các quyền mới
                        foreach (var item in lstInsertedRole)
                        {
                            userRoleDL.Insert(item);
                        }

                        response.Code = SystemCode.Success;
                    }
                }
                else
                {
                    response.Code = SystemCode.DataNull;
                }
            }

            else
            {
                response.Code = SystemCode.NotPermitted;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult Insert(UserModel model)
        {
            var response = new Response();
            if (UserContext.HasPermisson(Permission.Admin))
            {
                if (string.IsNullOrEmpty(model.UserName) || string.IsNullOrEmpty(model.PassWord))
                {
                    response.Code = SystemCode.NotValid;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                //convert pass to md5
                model.PassWord = model.PassWord.ToMD5();
                var result = (new UserDL()).Insert(model);
                if (result == true)
                {
                    // Khai báo role mới cho nhân viên đó
                    var lstRole = model.UserRoles.Split(',').ToList();
                    var userRoleDL = new UserRoleDL();
                    foreach (var roleUser in lstRole)
                    {
                        userRoleDL.Insert(new UserRoleModel() { 
                            RoleId= int.Parse(roleUser),
                            UserName = model.UserName
                        });
                    }
                    response.Code = SystemCode.Success;
                }
                else
                {
                    response.Code = SystemCode.ErrorExist;
                }
            }
            else
            {
                response.Code = SystemCode.NotPermitted;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public ActionResult LockTemporary(string userName) 
        {
            var userDL = new UserDL();
            var userModel = userDL.GetByUserNameWithLockTime(userName);
            if (userModel.Status != UserState.Active.GetHashCode())
            {
                return View("~/Views/Common/Warning.cshtml", null, "Tài khoản này hiện giờ chưa được kích hoạt , bạn cần kích hoạt để thực hiện chức năng này !");
            }
            var userLockModel = new UserLockModel();
            userLockModel.UserName = userName;
            userLockModel.LockedStartDate = DateTime.Now;
            return View(userLockModel);
        }
        [FilterAuthorize]
        public JsonResult UnLockTemporary(string userName)
        {
            var response = new Response();
            var userDL = new UserDL();
            if (string.IsNullOrEmpty(userName))
            {
                response.Code = SystemCode.DataNull;
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            if (!UserContext.HasPermisson(Permission.Admin) && !UserContext.HasPermisson(Permission.Manager))
            {
                response.Code = SystemCode.NotPermitted;
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            userDL.UnLockUserTemporarily(userName);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult LockTemporaryTime(UserLockModel model)
        {
            var response = new Response();
            //model.LockedStartDate = DateTime.Now;
            if (model.LockTime > 3 * 60 * 24)
            {
                response.Code = SystemCode.NotValid;
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            var userDL = new UserDL();
            userDL.LockUserTemporarily(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
