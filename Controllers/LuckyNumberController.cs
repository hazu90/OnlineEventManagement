﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Libs;
using BCRM.CheckInEvent.DAL;
using BCRM.CheckInEvent.Common;
using BCRM.CheckInEvent.Models;

namespace BCRM.CheckInEvent.Controllers
{
    public class LuckyNumberController : BaseController
    {
        [FilterAuthorize]
        public ActionResult Index()
        {
            //get list events
            var eventDL = new EventDL();
            //var lstEvents = eventDL.GetAll().FindAll(o => o.IsEnable == true);
            var lstEvents = CommonMethod.GetEvents();
            var defaultEvent = CommonMethod.GetDefaultEvent();
            ViewBag.DefaultEvent = defaultEvent;
            return View(lstEvents);
        }
        [FilterAuthorize]
        public ActionResult Rotate(int id, int iwci)
        {
            var model = new LuckyNumberModel();

            if (iwci == 1)
            {
                model.IsInWebCheckin = true;
            }
            else
            {
                model.IsInWebCheckin = false;
            }
            //model.IsInWebCheckin = isWebCheckin;
            model.EventId = id;
            return View(model);
        }

        [FilterAuthorize]
        public ActionResult RotateTet(int id, int iwci)
        {
            var model = new LuckyNumberModel();

            if (iwci == 1)
            {
                model.IsInWebCheckin = true;
            }
            else
            {
                model.IsInWebCheckin = false;
            }
            //model.IsInWebCheckin = isWebCheckin;
            model.EventId = id;
            return View(model);
        }


        [FilterAuthorize]
        [HttpPost]
        public JsonResult Generate(bool isWebCheckin, int eventId)
        {
            var response = new Response();
            try
            {
                var inviteEventDL = new InviteEventDL();
                //TH quay số cho các sự kiện thuộc WebCheckin
                if (isWebCheckin)
                {
                    var eventDL = new EventDL();
                    var eventInfo = eventDL.GetByIdInWebCheckIn(eventId);
                    // Lấy ra danh sách các mã Code của khách mời
                    var lstInvitee = inviteEventDL.WebCheckIn_GetByEventId(eventId);
                    // Lấy ra danh sách các mã Code của sự kiện đã được trúng thưởng từ trước
                    var luckyNumberDL = new LuckyNumberDL();
                    var lstLuckyNumber = luckyNumberDL.GetByEventId(eventId, isWebCheckin).Select(o => o.Code).ToList();

                    // Loại trừ ra các mã Code đã trúng giải để tiến hành quay số
                    lstInvitee = lstInvitee.FindAll(o => !string.IsNullOrEmpty(o.Code) && !lstLuckyNumber.Contains(o.Code));

                    var iRdIndex = -1;
                    if (lstInvitee.Count > 0)
                    {
                        iRdIndex = CommonMethod.RdGenerateCode.Next(0, lstInvitee.Count);
                    }

                    if (iRdIndex >= 0)
                    {
                        response.Code = SystemCode.Success;
                        var result = new LuckyNumberResultModel();
                        if (string.IsNullOrEmpty(eventInfo.EventCode))
                        {
                            result.Number = lstInvitee[iRdIndex].Code;
                        }
                        else
                        {
                            
                            result.Number = lstInvitee[iRdIndex].Code.Substring(eventInfo.EventCode.Length);
                        }
                        
                        result.InviteeName = lstInvitee[iRdIndex].CustomerName.ToUpper();
                        result.CityName = lstInvitee[iRdIndex].CompanyName;
                        result.DepartmentName = lstInvitee[iRdIndex].IntroduceUser;
                        response.Data = result;
                    }
                    else
                    {
                        response.Code = SystemCode.ErrorExist;
                        response.Message = "Không tìm thấy dữ liệu";
                    }
                }
                // TH quay số cho các sự kiện thuộc BCRM
                else
                {
                    var eventDL = new EventDL();
                    var eventInfo = eventDL.GetEventById(eventId);
                    // Lấy ra danh sách các mã Code của khách mời
                    var lstInvitee = inviteEventDL.GetByEventId(eventId);
                    // Lấy ra danh sách các mã Code của sự kiện đã được trúng thưởng từ trước
                    var luckyNumberDL = new LuckyNumberDL();
                    var lstLuckyNumber = luckyNumberDL.GetByEventId(eventId, isWebCheckin).Select(o => o.Code).ToList();

                    // Loại trừ ra các mã Code đã trúng giải để tiến hành quay số
                    lstInvitee = lstInvitee.FindAll(o =>!string.IsNullOrEmpty(o.Code) && !lstLuckyNumber.Contains(o.Code));

                    var iRdIndex = -1;
                    if (lstInvitee.Count > 0)
                    {
                        iRdIndex = CommonMethod.RdGenerateCode.Next(0, lstInvitee.Count);
                    }

                    if (iRdIndex >= 0)
                    {
                        response.Code = SystemCode.Success;
                        var result = new LuckyNumberResultModel();
                        result.Number = lstInvitee[iRdIndex].Code.Substring(eventInfo.EventCode.Length);
                        result.InviteeName = lstInvitee[iRdIndex].CustomerName.ToUpper();
                        result.CityName = lstInvitee[iRdIndex].CompanyName;
                        result.DepartmentName = lstInvitee[iRdIndex].IntroduceUser;
                        response.Data = result;
                    }
                    else
                    {
                        response.Code = SystemCode.ErrorExist;
                        response.Message = "Không tìm thấy dữ liệu phù hợp ";
                    }
                }
            }
            catch
            {
                response.Code = SystemCode.Error;
                response.Message = "Không tìm thấy dữ liệu phù hợp ";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        
        [FilterAuthorize]
        [HttpPost]
        public JsonResult Create(LuckyNumberModel model)
        {
            var response = new Response();
            var eventDL = new EventDL();
            if (model.IsInWebCheckin)
            {
                var eventInfo = eventDL.GetByIdInWebCheckIn(model.EventId);
                if (eventInfo == null)
                {
                    response.Code = SystemCode.DataNull;
                    response.Message ="Không tìm thấy dữ liệu phù hợp";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }

                if (!string.IsNullOrEmpty(eventInfo.EventCode))
                {
                    model.Code = eventInfo.EventCode + model.Code;
                }
            }
            else
            {
                var eventInfo = eventDL.GetEventById(model.EventId);
                if (eventInfo == null)
                {
                    response.Code = SystemCode.DataNull;
                    response.Message = "Không tìm thấy dữ liệu phù hợp";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                if (!string.IsNullOrEmpty(eventInfo.EventCode))
                {
                    model.Code = eventInfo.EventCode + model.Code;
                }
            }

            // Kiểm tra xem có thông tin khách mời hay không , nếu không có
            
            var luckyNumberDL = new LuckyNumberDL();
            if (luckyNumberDL.GetByCodeAndEventId(model.EventId, model.IsInWebCheckin, model.Code) == null)
            {
                luckyNumberDL.Insert(new LuckyNumberModel()
                {
                    Code = model.Code,
                    CreatedBy = UserContext.UserName,
                    EventId = model.EventId,
                    IsInWebCheckin = model.IsInWebCheckin
                });
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public ActionResult Search(int eventId, bool isWebCheckin)
        {
            var lstLuckyNumber = (new LuckyNumberDL()).GetByEventId(eventId, isWebCheckin);
            var result = new List<LuckyNumberForSearchModel>();
            var inviteEventDL = new InviteEventDL();
            var inviteEventAttachDL = new InviteEventAttachDL();
            if (isWebCheckin)
            {
                foreach (var item in lstLuckyNumber)
                {
                    var resultItem = new LuckyNumberForSearchModel()
                    {
                        Id   = item.Id,
                        Code = item.Code,
                        CreatedBy = item.CreatedBy,
                        CreatedDate = item.CreatedDate
                    };
                    // Tìm kiếm thông tin của người trúng thưởng
                    var info= inviteEventDL.WebCheckin_GetByCode(eventId, item.Code);
                    if (info != null)
                    {
                        resultItem.InviteeName = info.CustomerName;
                    }
                    result.Add(resultItem);
                }
            }
            else
            {
                foreach (var item in lstLuckyNumber)
                {
                    var resultItem = new LuckyNumberForSearchModel()
                    {
                        Code = item.Code,
                        CreatedBy = item.CreatedBy,
                        CreatedDate = item.CreatedDate
                    };
                    // Tìm kiếm thông tin của người trúng thưởng
                    var inviteInfo = inviteEventDL.GetListByCode(eventId, item.Code);
                    if (inviteInfo != null)
                    {
                        resultItem.InviteeName = inviteInfo.CustomerName;
                    }
                    var inviteAttachInfo = inviteEventAttachDL.GetListByCode(eventId, item.Code);
                    if (inviteAttachInfo != null)
                    {
                        resultItem.InviteeName = inviteAttachInfo.CustomerName;
                    }
                    result.Add(resultItem);
                }
            }
            ViewBag.EventId = eventId;
            ViewBag.IsInWebCheckin = isWebCheckin;
            return View(result);
        }
        [FilterAuthorize]
        public JsonResult Delete(int id)
        {
            var response = new Response();

            if (UserContext.HasPermisson(Permission.Admin) || UserContext.HasPermisson(Permission.Manager))
            {
                var luckyNumberDL = new LuckyNumberDL();
                var luckNumberInfo = luckyNumberDL.GetById(id);
                if (luckNumberInfo != null)
                {
                    luckyNumberDL.Delete(id);
                }
                response.Code = SystemCode.Success;
                response.Message = "Bạn đã xóa thông tin thành công !";
            }
            else
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
