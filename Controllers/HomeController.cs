﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BCRM.CheckInEvent.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index(bool justLogin = false)
        {
            if (UserContext != null)
            {
                return View();
            }
            return RedirectToAction("LogOn", "Account");
            //return Redirect(System.Configuration.ConfigurationManager.AppSettings["LoginUrl"]);
        }
    }
}
