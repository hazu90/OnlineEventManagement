﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BCRM.CheckInEvent.Models;

namespace BCRM.CheckInEvent.Controllers
{
    public class CommonController : BaseController
    {
        //
        // GET: /Common/

        public ActionResult Paging(PagerModel pager)
        {
            return View(pager);
        }

        public ActionResult Header()
        {
            ViewBag.CurentUser = UserContext;
            return View();
        }
    }
}
