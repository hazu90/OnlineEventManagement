﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BCRM.CheckInEvent.DAL;
using Libs;

namespace BCRM.CheckInEvent.Controllers
{
    public class SessionController : BaseController
    {
        //public ActionResult Index()
        //{
        //    return View();
        //}
        [FilterAuthorize]
        public JsonResult GetByEventId(int eventId)
        {
            var sessionDL = new SessionDL();
            var lstSession = sessionDL.GetByEventId(eventId);
            var response = new Response();
            response.Code = SystemCode.Success;
            response.Data = lstSession;
            return Json(response,JsonRequestBehavior.AllowGet);
        }
    }
}
