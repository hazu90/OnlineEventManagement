﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Libs;
using BCRM.CheckInEvent.Models;
using BCRM.CheckInEvent.DAL;
using BCRM.CheckInEvent.Common;

namespace BCRM.CheckInEvent.Controllers
{
    public class StatisticController : BaseController
    {
        [FilterAuthorize]
        public ActionResult Index()
        {
            //get list events
            var eventDL = new EventDL();
            //var lstEvents = eventDL.GetAll().FindAll(o => o.IsEnable == true);
            var lstEvents = CommonMethod.GetEvents();
            var defaultEvent = CommonMethod.GetDefaultEvent();
            ViewBag.DefaultEvent = defaultEvent;

            var lstPlaceAttendEvent = new List<KeyValuePair<string, string>>();
            lstPlaceAttendEvent.Add(new KeyValuePair<string, string>("Hà Nội", "Hà Nội"));
            lstPlaceAttendEvent.Add(new KeyValuePair<string, string>("Đà Nẵng", "Đà Nẵng"));
            lstPlaceAttendEvent.Add(new KeyValuePair<string, string>("TP. Hồ Chí Minh", "TP. Hồ Chí Minh"));
            ViewBag.LstPlaceAttendEvent = lstPlaceAttendEvent;

            return View(lstEvents);
        }
        [FilterAuthorize]
        public ActionResult Search(int eventId,bool isInWebCheckIn,string selectPlaceAttendEvent,int sessionId )
        {
            var inviteEventDL = new InviteEventDL();
            var lstInvitee = inviteEventDL.WebCheckIn_GetByEventId(eventId);
            if (!string.IsNullOrEmpty(selectPlaceAttendEvent))
            {
                lstInvitee = lstInvitee.FindAll(o => o.PlaceOfAttendEvent == selectPlaceAttendEvent);
            }

            var lstSession = (new SessionDL()).GetByEventId(eventId);
            var sessionCheckinDL = new SessionCheckinDL();

            if (lstSession.Count > 0)
            {
                if (sessionId > 0)
                {
                    // Danh sách các khách mời chính thức
                    var lstOfficial = lstInvitee.FindAll(o => o.IsNotSuggest == false && (o.Sessions.Equals("0") || o.Sessions.Split(',').Contains(sessionId.ToString()) ));
                    // Danh sách các khách mời vãng lai
                    var lstAnonymous = lstInvitee.FindAll(o => o.IsNotSuggest == true && (o.Sessions.Equals("0") || o.Sessions.Split(',').Contains(sessionId.ToString())));
                    // Danh sách khách mời chính thức đã checkin
                    var lstOfficialCheckedIn = sessionCheckinDL.GetBySessionAndSuggeestInvitee(eventId, sessionId, false);
                    // Danh sách khách mời vãng lai đã checkin
                    var lstAnonymousCheckedIn = sessionCheckinDL.GetBySessionAndSuggeestInvitee(eventId, sessionId, true);
                    //Tổng các khách mời đi cùng
                    var totalAccompanyMember = lstOfficialCheckedIn.Sum(o => o.AccompanyMember) + lstAnonymousCheckedIn.Sum(o => o.AccompanyMember);
                    //statistic
                    var checkInStatisticModel = new CheckInStatisticModel();
                    checkInStatisticModel.TotalOfficial = lstOfficial.Count;
                    checkInStatisticModel.TotalOfficialCheckedIn = lstOfficialCheckedIn.Count;
                    checkInStatisticModel.TotalAnonymous = lstAnonymous.Count;
                    checkInStatisticModel.TotalAnonymousCheckedIn = lstAnonymousCheckedIn.Count;
                    checkInStatisticModel.TotalAccompanyInvitee = totalAccompanyMember;
                    checkInStatisticModel.TotalAccompanyInviteeCheckedIn = totalAccompanyMember;
                    checkInStatisticModel.TotalCheckIn = lstOfficialCheckedIn.Count + lstAnonymousCheckedIn.Count;
                    checkInStatisticModel.TotalCustomer = lstOfficial.Count + lstAnonymous.Count;
                    ViewBag.IsInWebCheckIn = isInWebCheckIn;
                    return View(checkInStatisticModel);
                }
                else
                {
                    var lstOfficial = lstInvitee.FindAll(o => o.IsNotSuggest == false);
                    // Danh sách các khách mời vãng lai
                    var lstAnonymous = lstInvitee.FindAll(o => o.IsNotSuggest == true);

                    // Danh sách khách mời chính thức đã checkin
                    var lstOfficialCheckedIn = sessionCheckinDL.GetBySessionAndSuggeestInvitee(eventId, 0,false);
                    // Danh sách khách mời vãng lai đã checkin
                    var lstAnonymousCheckedIn = sessionCheckinDL.GetBySessionAndSuggeestInvitee(eventId, 0, true);

                    //Tổng các khách mời đi cùng
                    var totalAccompanyMember = lstOfficial.Sum(o => o.AccompanyMember) + lstAnonymous.Sum(o => o.AccompanyMember);
                    //statistic
                    var checkInStatisticModel = new CheckInStatisticModel();
                    checkInStatisticModel.TotalOfficial = lstOfficial.Count;
                    checkInStatisticModel.TotalOfficialCheckedIn = lstOfficialCheckedIn.Select(o => o.InviteEventId).Distinct().ToList().Count;
                    checkInStatisticModel.TotalAnonymous = lstAnonymous.Count;
                    checkInStatisticModel.TotalAnonymousCheckedIn = lstAnonymousCheckedIn.Select(o => o.InviteEventId).Distinct().ToList().Count;
                    checkInStatisticModel.TotalAccompanyInvitee = totalAccompanyMember;
                    checkInStatisticModel.TotalAccompanyInviteeCheckedIn = totalAccompanyMember;
                    checkInStatisticModel.TotalCheckIn = checkInStatisticModel.TotalOfficialCheckedIn + checkInStatisticModel.TotalAnonymousCheckedIn;
                    checkInStatisticModel.TotalCustomer = lstOfficial.Count + lstAnonymous.Count;
                    ViewBag.IsInWebCheckIn = isInWebCheckIn;
                    return View(checkInStatisticModel);
                }
            }
            else
            {
                // Danh sách các khách mời chính thức
                var lstOfficial = lstInvitee.FindAll(o => o.IsNotSuggest == false);
                // Danh sách các khách mời vãng lai
                var lstAnonymous = lstInvitee.FindAll(o => o.IsNotSuggest == true);
                // Danh sách khách mời chính thức đã checkin
                var lstOfficialCheckedIn = lstOfficial.FindAll(o => o.IsCheckIn == true);
                // Danh sách khách mời vãng lai đã checkin
                var lstAnonymousCheckedIn = lstAnonymous.FindAll(o => o.IsCheckIn == true);
                //Tổng các khách mời đi cùng
                var totalAccompanyMember = lstOfficial.Sum(o => o.AccompanyMember) + lstAnonymous.Sum(o => o.AccompanyMember);
                //statistic
                var checkInStatisticModel = new CheckInStatisticModel();
                checkInStatisticModel.TotalOfficial = lstOfficial.Count;
                checkInStatisticModel.TotalOfficialCheckedIn = lstOfficialCheckedIn.Count;
                checkInStatisticModel.TotalAnonymous = lstAnonymous.Count;
                checkInStatisticModel.TotalAnonymousCheckedIn = lstAnonymousCheckedIn.Count;
                checkInStatisticModel.TotalAccompanyInvitee = totalAccompanyMember;
                checkInStatisticModel.TotalAccompanyInviteeCheckedIn = totalAccompanyMember;
                checkInStatisticModel.TotalCheckIn = lstOfficialCheckedIn.Count + lstAnonymousCheckedIn.Count;
                checkInStatisticModel.TotalCustomer = lstOfficial.Count + lstAnonymous.Count;
                ViewBag.IsInWebCheckIn = isInWebCheckIn;
                return View(checkInStatisticModel);
            }
        }
    }
}
