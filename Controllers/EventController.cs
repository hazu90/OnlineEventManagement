﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Libs;
using BCRM.CheckInEvent.Models;
using BCRM.CheckInEvent.Common;
using BCRM.CheckInEvent.DAL;

namespace BCRM.CheckInEvent.Controllers
{
    public class EventController : BaseController
    {
        [FilterAuthorize]
        public ActionResult Index()
        {
            var model = new EventIndexSearchModel();
            model.LstGroup = (new BCRMGroupsDL()).GetAllGroups().FindAll(o => o.ParentId == 0).OrderBy(g => g.Order).ToList();
            return View(model);
        }
        [FilterAuthorize]
        public ActionResult Search(EventIndexSearchModel model)
        {
            // Người dùng phải có quyền quản lý mới thực hiện được chức năng này
            if (!UserContext.HasPermisson(Permission.Admin) && !UserContext.HasPermisson(Permission.Manager))
            {
                return View("~/Views/Common/NotPermitted.cshtml");
            }
            if (!string.IsNullOrEmpty(model.FilterTextSearch))
            {
                model.FilterTextSearch = model.FilterTextSearch.Trim();
            }
            var eventDL = new EventDL();
            
            // Lấy thông tin event trên web checkin theo điều kiện tìm kiếm
            var lstEvent = eventDL.GetListInWebCheckIn(model.FilterTextSearch,model.DeployEventId ,model.StartDate,model.EndDate);
            foreach (var item in lstEvent)
            {
                item.IsInWebCheckIn = true;
            }
            var lstSearchResult = new List<EventModel>();
            //lstSearchResult.AddRange(lstEventBCRMs);
            lstSearchResult.AddRange(lstEvent);
            // Sắp xếp theo ngày bắt đầu diễn ra sự kiện
            lstSearchResult = lstSearchResult.OrderByDescending(o => o.StartDate).ToList();
            // Lấy ra thông tin sự kiện được đặt mặc định
            var setDefaultEventDL = new SetDefaultEventDL();
            var setDefaultInfo = setDefaultEventDL.GetLastest();
            foreach (var item in lstSearchResult)
            {
                if (setDefaultInfo != null)
                {
                    if (item.Id == setDefaultInfo.EventId && item.IsInWebCheckIn == setDefaultInfo.IsInWebCheckin && !setDefaultInfo.IsCancel)
                    {
                        item.IsDefaultEvent = true;
                    }
                    else
                    {
                        item.IsDefaultEvent = false;
                    }
                }
                else
                {
                    item.IsDefaultEvent = false;
                }
            }
            lstSearchResult = lstSearchResult.OrderByDescending(o => o.CreatedDate).ToList();
            var lstReturn = lstSearchResult.Skip(model.PageSize * (model.PageIndex - 1))
                                                       .Take(model.PageSize).ToList();
            model.LstEvent = lstReturn;
            model.TotalRecord = lstSearchResult.Count;
            var pager = new PagerModel { CurrentPage = model.PageIndex, PageSize = model.PageSize, TotalItem = model.TotalRecord };
            ViewBag.Pager = pager;
            return View(model);
        }
        [FilterAuthorize]
        public ActionResult Create()
        {
            var groupLayer = new BCRMGroupsDL();
            var model = new EventModelForCreate();
            var allGroups = groupLayer.GetAllGroups().FindAll(o => o.ParentId == 0).OrderBy(g => g.Order).ToList();
            model.LstGroup = allGroups;
            return View(model);
        }
        [FilterAuthorize]
        public JsonResult CreateAction(EventModelForCreate model)
        {
            if (model.LstSession == null)
            {
                model.LstSession = new List<SessionForSaveModel>();
            }
            var response = new Response();
            // Kiểm tra thông tin phân quyền
            // Người dùng phải có quyền Manager hoặc Admin thì mới được thực thi chức năng này
            if ( !UserContext.HasPermisson(Permission.Manager) && !UserContext.HasPermisson(Permission.Admin))
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Validate lại dữ liệu
            if (model.StartDate == DateTime.MinValue || model.EndDate == DateTime.MinValue  )
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Dữ liệu người dùng nhập vào không hợp lệ";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Ngày bắt đầu phải nhỏ hơn hoặc bằng ngày kết thúc
            if (model.StartDate > model.EndDate)
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Ngày bắt đầu phải nhỏ hơn hoặc bằng ngày kết thúc";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Lấy thông tin người tạo
            model.CreatedBy = UserContext.UserName;

            if (model.IsBcrmUse)
            {
                if (model.RevenueStartDate != null)
                {
                    model.RevenueStartDate = model.RevenueStartDate.Value.Date;
                }
                if (model.RevenueEndDate != null)
                {
                    model.RevenueEndDate = model.RevenueEndDate.Value.Date.AddDays(1).AddSeconds(-1);
                }
                var arrDeploy = model.Deploy.Split(',');
                if (arrDeploy.Contains("0"))
                {
                    model.Deploy = "0";
                }
            }
            else
            {
                model.RevenueStartDate = null;
                model.RevenueEndDate = null;
                model.Deploy = "";
                model.IsAllowAttach = null;
                model.RevenueCalculation = 0;
            }
            // Thêm mới sự kiện
            var eventDL = new EventDL();
            // Kiểm tra mã sự kiện đã tồn tại chưa
            var eventCodeInfo = eventDL.GetByEventCodeInWebCheckIn(model.Id, model.EventCode);
            if (eventCodeInfo != null)
            {
                response.Code = SystemCode.ErrorExist;
                response.Message = "Mã sự kiện đã tồn tại rồi";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            var eventId = eventDL.CreateInWebCheckIn(model);

            var sessionDL = new SessionDL();
            if (model.LstSession.Count > 0)
            {
                foreach (var sessionItem in model.LstSession)
                {
                    sessionItem.EventId = eventId;
                    sessionItem.StartDate = sessionItem.StartDate.AddHours(sessionItem.StartTime.Hours).AddMinutes(sessionItem.StartTime.Minutes);
                    sessionItem.EndDate = sessionItem.EndDate.AddHours(sessionItem.EndTime.Hours).AddMinutes(sessionItem.EndTime.Minutes);
                    sessionDL.Create(new SessionModel() { 
                        Name = sessionItem.Name,
                        StartDate = sessionItem.StartDate,
                        EndDate = sessionItem.EndDate,
                        Description = sessionItem.Description,
                        EventId = sessionItem.EventId
                    });
                }
            }

            // Nếu sự kiện được chọn là sự kiện mặc định
            if (model.IsDefaultEvent)
            {
                // Thêm thông tin sự kiện mặc định
                var setDefaultEventDL = new SetDefaultEventDL();
                setDefaultEventDL.Create(new SetDefaultEventModel() { 
                        EventId = eventId,
                        CreatedBy = UserContext.UserName,
                        IsInWebCheckin = true
                });
            }
            // Gửi thông báo thành công
            response.Code = SystemCode.Success;
            response.Message = "Bạn đã thêm mới sự kiện thành công";
            
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public ActionResult Edit(int id)
        {
            // Kiểm tra người dùng có quyền manager / admin hay không
            if (!UserContext.HasPermisson(Permission.Admin) && !UserContext.HasPermisson(Permission.Manager))
            {
                return View("~/Views/Common/NotPermitted.cshtml");
            }
            // Lấy thông tin sự kiện
            var eventDL = new EventDL();
            var eventInfo = eventDL.GetByIdInWebCheckIn(id);
            var result = new EventModelForCreate() { 
                Id = eventInfo.Id,
                EventCode = eventInfo.EventCode,
                StartDate = eventInfo.StartDate,
                EndDate = eventInfo.EndDate,
                Deadline = eventInfo.Deadline,
                Deploy = eventInfo.Deploy,
                Description = eventInfo.Description,
                IsAllowAttach = eventInfo.IsAllowAttach,
                IsBcrmUse    = eventInfo.IsBcrmUse,
                IsDefaultEvent = eventInfo.IsDefaultEvent,
                IsEnable = eventInfo.IsEnable,
                Name = eventInfo.Name,
                SMSTitle = eventInfo.SMSTitle,
                RevenueCalculation = eventInfo.RevenueCalculation,
                RevenueStartDate = eventInfo.RevenueStartDate,
                RevenueEndDate = eventInfo.RevenueEndDate
            };
            if (eventInfo.IsBcrmUse)
            {
                result.LstDeployId = eventInfo.Deploy.Split(',').Select(o => int.Parse(o)).ToList();
            }

            // Kiểm tra xem sự kiện đang sửa có phải là sự kiện  mặc định
            var setDefaultEventDL = new SetDefaultEventDL();
            var setDefaultEvent = setDefaultEventDL.GetLastest();

            ViewBag.IsDefaultEvent = false;
            if (setDefaultEvent != null)
            {
                if (setDefaultEvent.IsInWebCheckin && setDefaultEvent.EventId == eventInfo.Id && !setDefaultEvent.IsCancel)
                {
                    ViewBag.IsDefaultEvent = true;
                }
            }
            var sessionDL = new SessionDL();
            var lstSession = sessionDL.GetByEventId(id);
            result.LstSession = new List<SessionForSaveModel>();
            foreach (var item in lstSession)
            {
                result.LstSession.Add(new SessionForSaveModel()
                {
                    Id = item.Id,
                    EventId = item.EventId,
                    Name = item.Name,
                    StartDate = item.StartDate,
                    StartTime = item.StartDate.TimeOfDay,
                    EndDate = item.EndDate,
                    EndTime = item.EndDate.TimeOfDay,
                    Description = item.Description
                });
            }
            if (result.LstSession.Count > 0)
            {
                result.IsHadSession = true;
            }
            var inviteEventDL = new InviteEventDL();
            if (inviteEventDL.WebCheckin_HasInviteeInEvent(result.Id))
            {
                result.IsHadInvitee = true;
            }

            var groupLayer = new BCRMGroupsDL();
            var allGroups = groupLayer.GetAllGroups().FindAll(o => o.ParentId == 0).OrderBy(g => g.Order).ToList();
            ViewBag.LstGroup = allGroups;
            return View(result);
        }
        [FilterAuthorize]
        public JsonResult EditAction(EventModelForCreate model)
        {
            var response = new Response();
            if (model.LstSession == null)
            {
                model.LstSession = new List<SessionForSaveModel>();
            }
            // Kiểm tra thông tin phân quyền
            // Người dùng phải có quyền Manager hoặc Admin thì mới được thực thi chức năng này
            if (UserContext.HasPermisson(Permission.Manager) || UserContext.HasPermisson(Permission.Admin))
            {
                // Validate lại dữ liệu
                if (model.StartDate == DateTime.MinValue || model.EndDate == DateTime.MinValue )
                {
                    response.Code = SystemCode.NotValid;
                    response.Message = "Dữ liệu người dùng nhập vào không hợp lệ";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                // Ngày bắt đầu phải nhỏ hơn hoặc bằng ngày kết thúc
                if (model.StartDate > model.EndDate)
                {
                    response.Code = SystemCode.NotValid;
                    response.Message = "Ngày bắt đầu phải nhỏ hơn hoặc bằng ngày kết thúc";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                // Thêm mới sự kiện
                var eventDL = new EventDL();
                // Kiểm tra mã sự kiện đã tồn tại chưa
                var eventCodeInfo = eventDL.GetByEventCodeInWebCheckIn(model.Id, model.EventCode);
                if (eventCodeInfo != null)
                {
                    response.Code = SystemCode.ErrorExist;
                    response.Message = "Mã sự kiện đã tồn tại rồi";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                // Kiểm tra xem nếu người dùng đang cập nhật lại mã sự kiện thì khi sự kiện này đã có khách mời => không được sửa mã sự kiện này
                var oldEventInfo = eventDL.GetByIdInWebCheckIn(model.Id);
                var inviteEventDL = new InviteEventDL();
                if (oldEventInfo.EventCode != model.EventCode)
                {
                    if (inviteEventDL.WebCheckin_IsExistInEventId(model.Id))
                    {
                        response.Code = SystemCode.ErrorExist;
                        response.Message = "Đã có khách mời cho sự kiện này , bạn không được thay đổi mã sự kiện !";
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                }
                eventDL.UpdateInWebCheckIn(model);
                // Nếu sự kiện được chọn là sự kiện mặc định
                if (model.IsDefaultEvent)
                {
                    // Thêm thông tin sự kiện mặc định
                    var setDefaultEventDL = new SetDefaultEventDL();
                    // Lấy ra sự kiện mặc định
                    var defaultEvent = CommonMethod.GetDefaultEvent();
                    if (defaultEvent == null || (defaultEvent != null && (defaultEvent.Id != model.Id || !defaultEvent.IsInWebCheckIn )))
                    {
                        setDefaultEventDL.Create(new SetDefaultEventModel()
                        {
                            EventId = model.Id,
                            CreatedBy = UserContext.UserName,
                            IsInWebCheckin = true
                        });
                    }
                }
                else
                {
                    // Lấy thông tin sự kiện mặc định
                    var setDefaultEventDL = new SetDefaultEventDL();
                    var defaultEvent = setDefaultEventDL.GetLastest();
                    if (defaultEvent.IsInWebCheckin && defaultEvent.EventId == model.Id)
                    {
                        //Hủy thông tin sự kiện mặc định
                        setDefaultEventDL.Cancel(defaultEvent.Id);
                    }
                }

                // Lấy ra các phiên của sự kiện
                var sessionDL = new SessionDL();
                var lstSession = (new SessionDL()).GetByEventId(model.Id);
                if (lstSession.Count > 0)
                {
                    var lstId = model.LstSession.Select(o=>o.Id).Distinct().ToList();
                    var lstExcept = lstSession.FindAll(o => !lstId.Contains(o.Id));
                    foreach (var item in lstExcept)
                    {
                        sessionDL.Delete(item.Id);
                    }
                }

                foreach (var sessionItem in model.LstSession)
                {
                    if (sessionItem.Id ==0)
                    {
                        sessionItem.EventId = model.Id;
                        sessionItem.StartDate = sessionItem.StartDate.AddHours(sessionItem.StartTime.Hours).AddMinutes(sessionItem.StartTime.Minutes);
                        sessionItem.EndDate = sessionItem.EndDate.AddHours(sessionItem.EndTime.Hours).AddMinutes(sessionItem.EndTime.Minutes);
                        sessionDL.Create(new SessionModel()
                        {
                            Name = sessionItem.Name,
                            StartDate = sessionItem.StartDate,
                            EndDate = sessionItem.EndDate,
                            Description = sessionItem.Description,
                            EventId = sessionItem.EventId
                        });
                    }
                }
                // Gửi thông báo thành công
                response.Code = SystemCode.Success;
                response.Message = "Bạn đã cập nhật sự kiện thành công";
            }
            else
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult Active(int id)
        {
            var response = new Response();
            if (!UserContext.HasPermisson(Permission.Admin) && !UserContext.HasPermisson(Permission.Manager))
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            var eventDL = new EventDL();
            var eventInfo = eventDL.GetByIdInWebCheckIn(id);
            if (eventInfo != null)
            {
                eventDL.UpdateStatusInWebCheckIn(id, true);
                response.Code = SystemCode.Success;
                response.Message = "Bạn đã dừng sự kiện thành công!";
            }
            else
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult DeActive(int id)
        {
            var response = new Response();
            if (!UserContext.HasPermisson(Permission.Admin) && !UserContext.HasPermisson(Permission.Manager))
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            var eventDL = new EventDL();
            var eventInfo = eventDL.GetByIdInWebCheckIn(id);
            if (eventInfo != null)
            {
                eventDL.UpdateStatusInWebCheckIn(id, false); 
                response.Code = SystemCode.Success;
                response.Message = "Bạn đã dừng sự kiện thành công!";
            }
            else
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult SetDefaultEvent(int id, bool isInWebCheckin)
        {
            var response = new Response();
            var setDefaultEventDL = new SetDefaultEventDL();
            // Lấy thông tin sự kiện mặc định hiện tại của hệ thống
            var defaultEventInfo = setDefaultEventDL.GetLastest();
            if (defaultEventInfo != null)
            {
                if (defaultEventInfo.EventId != id || defaultEventInfo.IsInWebCheckin != isInWebCheckin)
                {
                    setDefaultEventDL.Create(new SetDefaultEventModel()
                    {
                        EventId = id,
                        IsInWebCheckin = isInWebCheckin,
                        CreatedBy = UserContext.UserName
                    });
                }
                else
                {
                    if (defaultEventInfo.IsCancel)
                    {
                        setDefaultEventDL.Create(new SetDefaultEventModel()
                        {
                            EventId = id,
                            IsInWebCheckin = isInWebCheckin,
                            CreatedBy = UserContext.UserName
                        });
                    }
                }
            }
            else
            {
                setDefaultEventDL.Create(new SetDefaultEventModel()
                {
                    EventId = id,
                    IsInWebCheckin = isInWebCheckin,
                    CreatedBy = UserContext.UserName
                });
            }
            response.Code = SystemCode.Success;
            response.Message = "Bạn đã cài đặt sự kiện mặc định thành công !";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
