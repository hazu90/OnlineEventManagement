﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BCRM.CheckInEvent.Models;
using BCRM.CheckInEvent.DAL;
using Libs;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using Newtonsoft.Json;
using DVS.Algorithm;
using System.Web.Configuration;
using BCRM.CheckInEvent.Common;
using System.Text;

namespace BCRM.CheckInEvent.Controllers
{
    public class InviteEventController : BaseController
    {
        #region Methods
        [FilterAuthorize]
        public ActionResult Index()
        {
            ViewBag.CurrentUser = UserContext;
            var eventDL = new EventDL();
            // Lấy danh sách các sự kiện
            var lstAllEvents = CommonMethod.GetEvents();
            var defaultEvent = CommonMethod.GetDefaultEvent();
            ViewBag.DefaultEvent = defaultEvent;
            return View(lstAllEvents);
        }
        [FilterAuthorize]
        public ActionResult Search(EventIndexModel model)
        {
            //if (model.TextSearch == null)
            //{
            //    model.TextSearch = string.Empty;
            //}
            //if (!string.IsNullOrEmpty(model.TextSearch))
            //{
            //    model.TextSearch = model.TextSearch.Trim();
            //}

            //if (model.InviteeCode == null)
            //{
            //    model.InviteeCode = string.Empty;
            //}
            //var result = new EventIndexModel();
            //// Thông tin tìm kiếm theo BCRM
            //if (!model.IsInWebCheckin)
            //{
            //}
            //// Thông tin tìm kiếm theo WebCheckin
            //else
            //{
            //    var inviteEventDL = new InviteEventDL();
            //    // Tìm kiếm theo id của khách mời
            //    if (model.InviteeCodeInWebCheckin > 0)
            //    {
            //        var lstSearchResult = new List<InviteEventModel>();
            //        var info = inviteEventDL.WebCheckIn_GetById(model.InviteeCodeInWebCheckin);
            //        if (info != null && info.Status == true)
            //        {
            //            lstSearchResult.Add(info);
            //        }

            //        result.LstInviteEvents = lstSearchResult;
            //        result.PageIndex = model.PageIndex;
            //        result.PageSize = model.PageSize;
            //        result.TotalRecord = lstSearchResult.Count;
            //    }
            //    else if (!string.IsNullOrEmpty(model.Code))
            //    {
            //        // TH người dùng tìm kiếm theo mã số khách mời
            //        int searchByCode = 0;
            //        if (int.TryParse(model.Code, out searchByCode))
            //        {
            //            var eventDataLayer = new EventDL();
            //            var evtInfo = eventDataLayer.GetByIdInWebCheckIn(model.EventId);
            //            if (evtInfo != null)
            //            {
            //                model.Code = evtInfo.EventCode + model.Code;
            //            }
            //        }

            //        // Lấy danh sách khách khách mời chính theo mã code
            //        var lstInviteEvent = inviteEventDL.WebCheckin_GetByCodeToCheckin(model.EventId, model.Code);
            //        // paging
            //        result.LstInviteEvents = lstInviteEvent.OrderBy(o => o.Code)
            //                                                .Skip(model.PageSize * (model.PageIndex - 1))
            //                                                .Take(model.PageSize).ToList();
            //        result.TotalRecord = lstInviteEvent.Count;
            //        result.PageSize = model.PageSize;
            //        result.PageIndex = model.PageIndex;
            //    }
            //    else
            //    {
            //        if (model.CustomerType == null)
            //        {
            //            model.IsNotSuggest = null;
            //        }
            //        else
            //        {
            //            if (model.CustomerType == 0)
            //            {
            //                model.IsNotSuggest = false;
            //            }
            //            else
            //            {
            //                model.IsNotSuggest = true;
            //            }
            //        }
            //        var lstSearchResult = inviteEventDL.WebCheckIn_GetByCondition(model);
            //        // Tìm kiếm theo thông tin
            //        // find invitee 
            //        if (!string.IsNullOrEmpty(model.TextSearch))
            //        {
            //            var convertUnsign = CommonMethod.ConvertToUnSign(model.TextSearch.ToLower());
            //            var exactMatchInvitee = lstSearchResult.FindAll(o => o.CustomerName.ToLower().Contains(model.TextSearch.ToLower()) || o.CustomerPhone.ToLower().Contains(model.TextSearch.ToLower()));
            //            var opportunityInvitee = lstSearchResult.FindAll(o =>
            //                                                    !o.CustomerName.ToLower().Contains(model.TextSearch.ToLower())
            //                                                    && !o.CustomerPhone.ToLower().Contains(model.TextSearch.ToLower())
            //                                                    && (CommonMethod.ConvertToUnSign(o.CustomerName.ToLower()).Contains(convertUnsign) || o.CustomerEmail.Contains(convertUnsign))
            //                                                    );
            //            var remainInvitee = lstSearchResult.FindAll(o => !o.CustomerName.ToLower().Contains(model.TextSearch.ToLower())
            //                                                            && !o.CustomerPhone.ToLower().Contains(model.TextSearch.ToLower())
            //                                                            && !CommonMethod.ConvertToUnSign(o.CustomerName.ToLower()).Contains(convertUnsign)
            //                                                            && !o.CustomerEmail.Contains(convertUnsign)
            //                                                            && ((o.CompanyName != null && CommonMethod.ConvertToUnSign(o.CompanyName.ToLower()).Contains(convertUnsign))
            //                                                             || (o.PositionName != null && CommonMethod.ConvertToUnSign(o.PositionName.ToLower()).Contains(convertUnsign))
            //                                                             || (o.PlaceOfAttendEvent != null && CommonMethod.ConvertToUnSign(o.PlaceOfAttendEvent.ToLower()).Contains(convertUnsign))
            //                                                             || (o.Note != null && CommonMethod.ConvertToUnSign(o.Note.ToLower()).Contains(convertUnsign))
            //                                                                )
            //                                                            );
            //            var lstAllInvitee = new List<InviteEventModel>();
            //            lstAllInvitee.AddRange(exactMatchInvitee);
            //            lstAllInvitee.AddRange(opportunityInvitee);
            //            lstAllInvitee.AddRange(remainInvitee);
            //            result.LstInviteEvents = lstAllInvitee.Skip(model.PageSize * (model.PageIndex - 1))
            //                                           .Take(model.PageSize).ToList();
            //            result.PageIndex = model.PageIndex;
            //            result.PageSize = model.PageSize;
            //            result.TotalRecord = lstAllInvitee.Count;
            //        }
            //        else
            //        {
            //            result.LstInviteEvents = lstSearchResult.OrderByDescending(o => o.CreatedDate)
            //                                            .Skip(model.PageSize * (model.PageIndex - 1))
            //                                            .Take(model.PageSize).ToList();
            //            result.PageIndex = model.PageIndex;
            //            result.PageSize = model.PageSize;
            //            result.TotalRecord = lstSearchResult.Count;
            //        }

            //    }
            //}
            //var eventDL = new EventDL();
            //var sessionDL = new SessionDL();
            //var sessionCheckinDL = new SessionCheckinDL();
            //#region Lấy thông tin checkin
            //var lstSession = sessionDL.GetByEventId(model.EventId);
            //if (lstSession.Count > 0)
            //{
            //    foreach (var item in result.LstInviteEvents)
            //    {
            //        item.IsUsingSession = true;
            //        var lstCheckin = sessionCheckinDL.GetByInviteEventId(item.Id);
            //        var lstSelectedSessionId = new List<string>();
            //        if (!string.IsNullOrEmpty(item.Sessions))
            //        {
            //            lstSelectedSessionId = item.Sessions.Split(',').ToList();
            //        }
                    
            //        item.LstSessionCheckin = new List<KeyValuePair<string, string>>();
            //        if (lstSelectedSessionId.Contains("0"))
            //        {
            //            foreach (var sessionItem in lstSession)
            //            {
            //                var ifo = lstCheckin.Find(o => o.SessionId == sessionItem.Id);
            //                if (ifo == null)
            //                {
            //                    item.LstSessionCheckin.Add(new KeyValuePair<string, string>(sessionItem.Name, "<span class=\"label  label-danger\">Chưa check in</span>"));
            //                }
            //                else
            //                {
            //                    item.LstSessionCheckin.Add(new KeyValuePair<string, string>(sessionItem.Name, "<span class=\"label  label-success\">Đã check in</span> <a name=\"lnkRemove\" ieid=\"" + ifo.InviteEventId + "\" ssid=\""+ ifo.SessionId +"\"  href=\"javascript:void(0);\" class=\"btn blue \">Hủy</a>"));
            //                }
            //            }
            //        }
            //        else
            //        {
            //            foreach (var selectedSession in lstSelectedSessionId)
            //            {
            //                var sessionItem = lstSession.Find(o => o.Id.ToString() == selectedSession);
            //                var ifo = lstCheckin.Find(o => o.SessionId == sessionItem.Id);
            //                if (ifo == null)
            //                {
            //                    item.LstSessionCheckin.Add(new KeyValuePair<string, string>(sessionItem.Name, "<span class=\"label  label-danger\">Chưa check in</span>"));
            //                }
            //                else
            //                {
            //                    item.LstSessionCheckin.Add(new KeyValuePair<string, string>(sessionItem.Name, "<span class=\"label  label-success\">Đã check in</span> <a name=\"lnkRemove\" ieid=\"" + ifo.InviteEventId + "\" ssid=\"" + ifo.SessionId + "\"  href=\"javascript:void(0);\" class=\"btn blue \">Hủy</a>"));
            //                }
            //            }
            //        }
            //    }
            //}
            //#endregion

            //#region Permission for checkin and cancel checkin
            //var lstPermissionCheckIn = new Dictionary<int, bool>();
            
            //var lstEvents = new List<EventModel>();
            //if (model.IsInWebCheckin)
            //{
            //    lstEvents = eventDL.GetAllInWebCheckIn().FindAll(o => o.IsEnable == true);
            //}
            //else
            //{
            //    lstEvents = eventDL.GetAll().FindAll(o => o.IsEnable == true);
            //}

            //var eventInfo = new List<EventModel>();
            //if (model.EventId > 0)
            //{
            //    eventInfo = lstEvents.FindAll(o => o.Id == model.EventId);
            //}
            //else
            //{
            //    eventInfo = lstEvents;
            //}
            //if (!UserContext.HasPermisson(Permission.Manager) && !UserContext.HasPermisson(Permission.Admin))
            //{
            //    var lstInvitee = result.LstInviteEvents;
            //    foreach (var item in lstInvitee)
            //    {
            //        var lstPhone = item.CustomerPhone.Split(new char[] { ';', ',' });
            //        var lstNewPhone = new List<string>();
            //        for (var index = 0; index < lstPhone.Length; index++)
            //        {
            //            var phoneInfo = lstPhone[index].Trim();
            //            if (!string.IsNullOrEmpty(phoneInfo))
            //            {
            //                if (phoneInfo.Length == 1)
            //                {
            //                    phoneInfo = "x";
            //                }
            //                else
            //                {
            //                    phoneInfo = phoneInfo.Substring(0, phoneInfo.Length - 2) + "xx";
            //                }

            //                lstNewPhone.Add(phoneInfo);
            //            }
            //        }
            //        item.CustomerPhone = string.Join(",", lstNewPhone);
            //    }
            //    result.LstInviteEvents = lstInvitee;

            //    var timeNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);

            //    foreach (var eventItem in eventInfo)
            //    {
            //        if (eventItem.EndDate >= timeNow)
            //        {
            //            lstPermissionCheckIn.Add(eventItem.Id, true);
            //        }
            //        else
            //        {
            //            lstPermissionCheckIn.Add(eventItem.Id, false);
            //        }
            //    }
            //    ViewBag.LstPermissionCheckIn = lstPermissionCheckIn;
            //}
            //else
            //{
            //    foreach (var eventItem in eventInfo)
            //    {
            //        lstPermissionCheckIn.Add(eventItem.Id, true);
            //    }
            //    ViewBag.LstPermissionCheckIn = lstPermissionCheckIn;
            //}
            //#endregion
            //// Paging Info
            //var pager = new PagerModel { CurrentPage = result.PageIndex, PageSize = result.PageSize, TotalItem = result.TotalRecord };
            //ViewBag.IsInWebCheckin = model.IsInWebCheckin;
            //ViewBag.Pager = pager;
            //ViewBag.CurrentUser = UserContext;
            //// render view
            //return View(result);

            if (model.TextSearch == null)
            {
                model.TextSearch = string.Empty;
            }
            if (!string.IsNullOrEmpty(model.TextSearch))
            {
                model.TextSearch = model.TextSearch.Trim();
            }

            if (model.InviteeCode == null)
            {
                model.InviteeCode = string.Empty;
            }
            var result = new EventIndexModel();
            // Thông tin tìm kiếm theo BCRM
            if (!model.IsInWebCheckin)
            {
            }
            // Thông tin tìm kiếm theo WebCheckin
            else
            {
                var inviteEventDL = new InviteEventDL();
                // Tìm kiếm theo id của khách mời
                if (model.InviteeCodeInWebCheckin > 0)
                {
                    var lstSearchResult = new List<InviteEventModel>();
                    var info = inviteEventDL.WebCheckIn_GetById(model.InviteeCodeInWebCheckin);
                    if (info != null && info.Status == true)
                    {
                        lstSearchResult.Add(info);
                    }

                    result.LstInviteEvents = lstSearchResult;
                    result.PageIndex = model.PageIndex;
                    result.PageSize = model.PageSize;
                    result.TotalRecord = lstSearchResult.Count;
                }
                else if (!string.IsNullOrEmpty(model.Code))
                {
                    // TH người dùng tìm kiếm theo mã số khách mời
                    int searchByCode = 0;
                    if (int.TryParse(model.Code, out searchByCode))
                    {
                        var eventDataLayer = new EventDL();
                        var evtInfo = eventDataLayer.GetByIdInWebCheckIn(model.EventId);
                        if (evtInfo != null)
                        {
                            model.Code = evtInfo.EventCode + model.Code;
                        }
                    }

                    // Lấy danh sách khách khách mời chính theo mã code
                    var lstInviteEvent = inviteEventDL.WebCheckin_GetByCodeToCheckin(model.EventId, model.Code);
                    // paging
                    result.LstInviteEvents = lstInviteEvent.OrderBy(o => o.Code)
                                                            .Skip(model.PageSize * (model.PageIndex - 1))
                                                            .Take(model.PageSize).ToList();
                    result.TotalRecord = lstInviteEvent.Count;
                    result.PageSize = model.PageSize;
                    result.PageIndex = model.PageIndex;
                }
                else
                {
                    if (model.CustomerType == null)
                    {
                        model.IsNotSuggest = null;
                    }
                    else
                    {
                        if (model.CustomerType == 0)
                        {
                            model.IsNotSuggest = false;
                        }
                        else
                        {
                            model.IsNotSuggest = true;
                        }
                    }
                    var lstSearchResult = inviteEventDL.WebCheckIn_GetByCondition(model);
                    // Tìm kiếm theo thông tin
                    // find invitee 
                    if (!string.IsNullOrEmpty(model.TextSearch))
                    {
                        var convertUnsign = CommonMethod.ConvertToUnSign(model.TextSearch.ToLower());
                        var exactMatchInvitee = lstSearchResult.FindAll(o => o.CustomerName.ToLower().Contains(model.TextSearch.ToLower()) || o.CustomerPhone.ToLower().Contains(model.TextSearch.ToLower()));
                        var opportunityInvitee = lstSearchResult.FindAll(o =>
                                                                !o.CustomerName.ToLower().Contains(model.TextSearch.ToLower())
                                                                && !o.CustomerPhone.ToLower().Contains(model.TextSearch.ToLower())
                                                                && (CommonMethod.ConvertToUnSign(o.CustomerName.ToLower()).Contains(convertUnsign) || o.CustomerEmail.Contains(convertUnsign))
                                                                );
                        var remainInvitee = lstSearchResult.Except(exactMatchInvitee).Except(opportunityInvitee).ToList();
                        var lstAllInvitee = new List<InviteEventModel>();
                        lstAllInvitee.AddRange(exactMatchInvitee);
                        lstAllInvitee.AddRange(opportunityInvitee);
                        lstAllInvitee.AddRange(remainInvitee);
                        result.LstInviteEvents = lstAllInvitee.Skip(model.PageSize * (model.PageIndex - 1))
                                                       .Take(model.PageSize).ToList();
                        result.PageIndex = model.PageIndex;
                        result.PageSize = model.PageSize;
                        result.TotalRecord = lstAllInvitee.Count;
                    }
                    else
                    {
                        result.LstInviteEvents = lstSearchResult.OrderByDescending(o => o.CreatedDate)
                                                        .Skip(model.PageSize * (model.PageIndex - 1))
                                                        .Take(model.PageSize).ToList();
                        result.PageIndex = model.PageIndex;
                        result.PageSize = model.PageSize;
                        result.TotalRecord = lstSearchResult.Count;
                    }

                }
            }

            // Hiển thị thông tin chức vụ của nhân viên
            foreach (var item in result.LstInviteEvents)
            {
                if (item.IsBcrmProcess)
                {
                    item.PositionName = item.CustomerPosition.ToCustomerPositionName();
                }
            }

            var eventDL = new EventDL();
            var sessionDL = new SessionDL();
            var sessionCheckinDL = new SessionCheckinDL();
            #region Lấy thông tin checkin
            var lstSession = sessionDL.GetByEventId(model.EventId);
            if (lstSession.Count > 0)
            {
                foreach (var item in result.LstInviteEvents)
                {
                    item.IsUsingSession = true;
                    var lstCheckin = sessionCheckinDL.GetByInviteEventId(item.Id);
                    var lstSelectedSessionId = new List<string>();
                    if (!string.IsNullOrEmpty(item.Sessions))
                    {
                        lstSelectedSessionId = item.Sessions.Split(',').ToList();
                    }

                    if (model.SessionId > 0)
                    {
                        var accInfo = lstCheckin.Find(o => o.SessionId == model.SessionId);
                        if (accInfo != null)
                        {
                            item.AccompanyMember = accInfo.AccompanyMember;
                        }
                    }

                    item.LstSessionCheckin = new List<KeyValuePair<string, string>>();
                    if (lstSelectedSessionId.Contains("0"))
                    {
                        foreach (var sessionItem in lstSession)
                        {
                            var ifo = lstCheckin.Find(o => o.SessionId == sessionItem.Id);
                            if (ifo == null)
                            {
                                item.LstSessionCheckin.Add(new KeyValuePair<string, string>(sessionItem.Name, "<span class=\"label  label-danger\">Chưa check in</span>"));
                            }
                            else
                            {
                                item.LstSessionCheckin.Add(new KeyValuePair<string, string>(sessionItem.Name, "<span class=\"label  label-success\">Đã check in</span> <a name=\"lnkRemove\" ieid=\"" + ifo.InviteEventId + "\" ssid=\"" + ifo.SessionId + "\"  href=\"javascript:void(0);\" class=\"btn blue \">Hủy</a>"));
                            }
                        }
                    }
                    else
                    {
                        foreach (var selectedSession in lstSelectedSessionId)
                        {
                            var sessionItem = lstSession.Find(o => o.Id.ToString() == selectedSession);
                            var ifo = lstCheckin.Find(o => o.SessionId == sessionItem.Id);
                            if (ifo == null)
                            {
                                item.LstSessionCheckin.Add(new KeyValuePair<string, string>(sessionItem.Name, "<span class=\"label  label-danger\">Chưa check in</span>"));
                            }
                            else
                            {
                                item.LstSessionCheckin.Add(new KeyValuePair<string, string>(sessionItem.Name, "<span class=\"label  label-success\">Đã check in</span> <a name=\"lnkRemove\" ieid=\"" + ifo.InviteEventId + "\" ssid=\"" + ifo.SessionId + "\"  href=\"javascript:void(0);\" class=\"btn blue \">Hủy</a>"));
                            }
                        }
                    }
                }
            }
            #endregion

            #region Permission for checkin and cancel checkin
            var lstPermissionCheckIn = new Dictionary<int, bool>();

            var lstEvents = new List<EventModel>();
            if (model.IsInWebCheckin)
            {
                lstEvents = eventDL.GetAllInWebCheckIn().FindAll(o => o.IsEnable == true);
            }
            else
            {
                lstEvents = eventDL.GetAll().FindAll(o => o.IsEnable == true);
            }

            var eventInfo = new List<EventModel>();
            if (model.EventId > 0)
            {
                eventInfo = lstEvents.FindAll(o => o.Id == model.EventId);
            }
            else
            {
                eventInfo = lstEvents;
            }
            if (!UserContext.HasPermisson(Permission.Manager) && !UserContext.HasPermisson(Permission.Admin))
            {
                var lstInvitee = result.LstInviteEvents;
                foreach (var item in lstInvitee)
                {
                    var lstPhone = item.CustomerPhone.Split(new char[] { ';', ',' });
                    var lstNewPhone = new List<string>();
                    for (var index = 0; index < lstPhone.Length; index++)
                    {
                        var phoneInfo = lstPhone[index].Trim();
                        if (!string.IsNullOrEmpty(phoneInfo))
                        {
                            if (phoneInfo.Length == 1)
                            {
                                phoneInfo = "x";
                            }
                            else
                            {
                                phoneInfo = phoneInfo.Substring(0, phoneInfo.Length - 2) + "xx";
                            }

                            lstNewPhone.Add(phoneInfo);
                        }
                    }
                    item.CustomerPhone = string.Join(",", lstNewPhone);
                }
                result.LstInviteEvents = lstInvitee;

                var timeNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);

                foreach (var eventItem in eventInfo)
                {
                    if (eventItem.EndDate >= timeNow)
                    {
                        lstPermissionCheckIn.Add(eventItem.Id, true);
                    }
                    else
                    {
                        lstPermissionCheckIn.Add(eventItem.Id, false);
                    }
                }
                ViewBag.LstPermissionCheckIn = lstPermissionCheckIn;
            }
            else
            {
                foreach (var eventItem in eventInfo)
                {
                    lstPermissionCheckIn.Add(eventItem.Id, true);
                }
                ViewBag.LstPermissionCheckIn = lstPermissionCheckIn;
            }
            #endregion
            // Paging Info
            var pager = new PagerModel { CurrentPage = result.PageIndex, PageSize = result.PageSize, TotalItem = result.TotalRecord };
            ViewBag.IsInWebCheckin = model.IsInWebCheckin;
            ViewBag.Pager = pager;
            ViewBag.CurrentUser = UserContext;
            // render view
            return View(result);
        }
        [FilterAuthorize]
        public JsonResult CheckIn(int eventId, int inviteEventId, int attachId, int accompanyInvitee)
        {
            var response = new Response();
            var inviteEventDL = new InviteEventDL();
            // check permission
            if (!UserContext.HasPermisson(Permission.Manager) && !UserContext.HasPermisson(Permission.Admin))
            {
                var eventDL = new EventDL();
                var eventInfo = eventDL.GetEventById(eventId);

                if (eventInfo != null)
                {
                    var timeNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                    if (eventInfo.EndDate < timeNow)
                    {
                        response.Code = SystemCode.NotPermitted;
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    response.Code = SystemCode.NotPermitted;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }
            if (attachId == 0)
            {
                inviteEventDL.UpdateInviteEventCheckIn(inviteEventId, UserContext.UserName, accompanyInvitee);
            }
            else
            {
                inviteEventDL.UpdateAttachCheckIn(attachId, UserContext.UserName, accompanyInvitee);
            }
            response.Code = SystemCode.Success;

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult CheckInWebCheckin(int eventId, int inviteEventId, int accompanyInvitee,int sessionId)
        {
            var response = new Response();
            var inviteEventDL = new InviteEventDL();
            // check permission
            if (!UserContext.HasPermisson(Permission.Manager) && !UserContext.HasPermisson(Permission.Admin))
            {
                // Lấy thông tin của sự kiện
                var eventDL = new EventDL();
                var eventInfo = eventDL.GetByIdInWebCheckIn(eventId);
                if (eventInfo != null)
                {
                    var timeNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                    if (eventInfo.EndDate < timeNow)
                    {
                        response.Code = SystemCode.NotPermitted;
                        response.Message = "Bạn không có quyền thực thi chức năng này !";
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    response.Code = SystemCode.NotPermitted;
                    response.Message = "Bạn không có quyền thực thi chức năng này !";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }
            var inviteInfo = inviteEventDL.WebCheckIn_GetById(inviteEventId);
            var inviteEventHistoryDL = new InviteEventHistoryDL();
            // Xét xem sự kiện có chia theo phiên tham dự
            var lstSession = (new SessionDL()).GetByEventId(eventId);
            if (lstSession.Count > 0)
            {
                // Kiểm tra xem khách mời có quyền checkin phiên này hay không
                var inviteeInfo = inviteEventDL.WebCheckIn_GetById(inviteEventId);
                var lstSelectedSession = inviteeInfo.Sessions.Split(',');
                var isCheckin = false;
                if (lstSelectedSession.Contains("0"))
                {
                    isCheckin = true;
                }
                else
                {
                    if (lstSelectedSession.Contains(sessionId.ToString()))
                    {
                        isCheckin = true;
                    }
                }

                if (!isCheckin)
                {
                    response.Code = SystemCode.ErrorExist;
                    response.Message = "Khách mời không được checkin cho phiên sự kiện này !";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }

                var sessionCheckinDL = new SessionCheckinDL();
                if (!sessionCheckinDL.IsExist(inviteEventId, sessionId))
                {
                    sessionCheckinDL.Insert(new SessionCheckinModel()
                    {
                        CheckinBy = UserContext.UserName,
                        CheckinDate = DateTime.Now,
                        EventId = eventId,
                        InviteEventId = inviteEventId,
                        SessionId = sessionId,
                        AccompanyMember = accompanyInvitee
                    });
                    if (!inviteInfo.IsCheckIn)
                    {
                        inviteEventDL.WebCheckIn_Checkin(inviteEventId, true, UserContext.UserName, accompanyInvitee);
                    }
                    else
                    {
                        inviteEventDL.WebCheckin_UpdateAccompanyMember(inviteEventId, accompanyInvitee);
                    }
                    inviteEventHistoryDL.Insert(new InviteEventHistoryModel()
                    {
                        Action = "Checkin theo phiên",
                        UpdatedBy = UserContext.UserName,
                        UpdatedDate = DateTime.Now,
                        InviteEventId = inviteEventId,
                        NewValue = "Check in " + lstSession.Find(o=>o.Id == sessionId).Name ,
                        OldValue = ""
                    });
                }
                else
                {
                    response.Code = SystemCode.ErrorExist;
                    response.Message = "Khách mời đã được checkin cho phiên sự kiện này rồi !";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }
            // TH sự kiện không chia phiên tham dự
            else
            {
                inviteEventHistoryDL.Insert(new InviteEventHistoryModel()
                {
                    Action = "Checkin sự kiện",
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    InviteEventId = inviteEventId,
                    NewValue = "",
                    OldValue = ""
                });
                inviteEventDL.WebCheckIn_Checkin(inviteEventId, true, UserContext.UserName, accompanyInvitee);
            }
            response.Code = SystemCode.Success;
            response.Message = "Bạn đã checkin thông tin thành công !";

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult Cancel(int eventId, int inviteEventId, int attachId)
        {
            var response = new Response();
            // check permission
            if (!UserContext.HasPermisson(Permission.Manager) && !UserContext.HasPermisson(Permission.Admin))
            {
                var eventDL = new EventDL();
                var eventInfo = eventDL.GetEventById(eventId);

                if (eventInfo != null)
                {
                    var timeNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                    if (eventInfo.EndDate < timeNow)
                    {
                        response.Code = SystemCode.NotPermitted;
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    response.Code = SystemCode.NotPermitted;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }

            var inviteEventDL = new InviteEventDL();
            if (attachId == 0)
            {
                inviteEventDL.UpdateInviteEventCancel(inviteEventId);
            }
            else
            {
                inviteEventDL.UpdateAttachCancel(attachId);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult CancelWebCheckin(int eventId, int inviteEventId)
        {
            var response = new Response();
            // check permission
            if (!UserContext.HasPermisson(Permission.Manager) && !UserContext.HasPermisson(Permission.Admin))
            {
                var eventDL = new EventDL();
                var eventInfo = eventDL.GetByIdInWebCheckIn(eventId);

                if (eventInfo != null)
                {
                    var timeNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                    if (eventInfo.EndDate < timeNow)
                    {
                        response.Code = SystemCode.NotPermitted;
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    response.Code = SystemCode.NotPermitted;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }

            var inviteEventDL = new InviteEventDL();

            var inviteInfo = inviteEventDL.WebCheckIn_GetById(inviteEventId);
            inviteEventDL.WebCheckIn_Checkin(inviteEventId, false, UserContext.UserName, 0);
            if (inviteInfo.SendTicketStatus.Value == 2)
            {
                inviteEventDL.WebCheckin_UpdateSendTicketStatus(inviteEventId, 2, 17);
            }
            else
            {
                inviteEventDL.WebCheckin_UpdateSendTicketStatus(inviteEventId, 1, 16);
            }

            var inviteEventHistoryDL = new InviteEventHistoryDL();
            inviteEventHistoryDL.Insert(new InviteEventHistoryModel() { 
                Action="Hủy checkin",
                InviteEventId = inviteEventId,
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now
            });
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        [HttpPost]
        public ActionResult ExportExcel(EventIndexModel model)
        {
            if (!UserContext.HasPermisson(Permission.Admin) && !UserContext.HasPermisson(Permission.Manager))
            {
                return View("~/Views/Common/NotPermitted.cshtml");
            }

            var arrV = model.EventIdWebCheckIn.Split('_');
            model.EventId = int.Parse(arrV[0]);
            if (arrV[1] == "1")
            {
                model.IsInWebCheckin = true;
            }
            else
            {
                model.IsInWebCheckin = false;
            }

            if (model.TextSearch == null)
            {
                model.TextSearch = "";
            }
            if (!string.IsNullOrEmpty(model.TextSearch))
            {
                model.TextSearch = model.TextSearch.Trim();
            }

            if (model.InviteeCode == null)
            {
                model.InviteeCode = "";
            }

            if (model.CheckInStatus == null)
            {
                model.IsCheckIn = null;
            }
            else
            {
                if (model.CheckInStatus == 0)
                {
                    model.IsCheckIn = false;
                }
                else
                {
                    model.IsCheckIn = true;
                }
            }

            if (!string.IsNullOrEmpty(model.Code))
            {
                model.Code = model.Code.Trim();
            }

            if (model.IsInWebCheckin)
            {
                return WebCheckin_GetInvitee(model);
            }
            else
            {
                return BCRM_GetInvitee(model);
            }
        }
        [FilterAuthorize]
        public ActionResult AddAnonymousInfo()
        {
            var anonymousInfo = new AnonymousInviteeInfoModel();
            anonymousInfo.LstEvents = CommonMethod.GetEvents();
            anonymousInfo.ListCustomerType = Helper.ListCustomerType();
            var defaultEvent = CommonMethod.GetDefaultEvent();
            ViewBag.DefaultEvent = defaultEvent;

            ViewBag.ListCustomerType = Helper.ListCustomerType();
            ViewBag.ListPlaceAttendEvent = new List<KeyValuePair<string, string>>();
            ViewBag.ListPlaceAttendEvent.Add(new KeyValuePair<string, string>("Hà Nội", "Hà Nội"));
            ViewBag.ListPlaceAttendEvent.Add(new KeyValuePair<string, string>("Đà Nẵng", "Đà Nẵng"));
            ViewBag.ListPlaceAttendEvent.Add(new KeyValuePair<string, string>("TP. Hồ Chí Minh", "TP. Hồ Chí Minh"));
            ViewBag.ListRegisteredSource = CheckInHelper.ListRegisteredSource();
            return View(anonymousInfo);
        }
        [FilterAuthorize]
        public ActionResult EditAnonymousInfo(int inviteEventId)
        {
            var anonymousInfo = new AnonymousInviteeInfoModel();
            anonymousInfo.ListAbilityAttend = Helper.ListAbilityAttendName();
            anonymousInfo.LstEvents = (new EventDL()).GetAll().FindAll(o => o.IsEnable == true);
            anonymousInfo.ListCustomerType = Helper.ListCustomerType();
            anonymousInfo.ListCustomerPosition = Helper.ListCustomerPositionName();
            anonymousInfo.ListCompanySize = Helper.ListCompanySizeName();
            anonymousInfo.AnonymousInfo = (new InviteEventDL()).GetById(inviteEventId);
            return View(anonymousInfo);
        }
        [FilterAuthorize]
        public JsonResult GetCustomerInfo()
        {
            var response = new Response();
            var anonymousInfo = new AnonymousInviteeInfoModel();
            anonymousInfo.ListAbilityAttend = Helper.ListAbilityAttendName();
            anonymousInfo.LstEvents = (new EventDL()).GetAll().FindAll(o => o.IsEnable == true);
            anonymousInfo.ListCustomerType = Helper.ListCustomerType();
            anonymousInfo.ListCustomerPosition = Helper.ListCustomerPositionName();
            anonymousInfo.ListCompanySize = Helper.ListCompanySizeName();
            response.Data = anonymousInfo;
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult Insert(AnonymousInviteeModel model)
        {
            var response = new Response();
            //Valid data
            if (string.IsNullOrEmpty(model.CustomerName) || string.IsNullOrEmpty(model.CustomerPhone))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Dữ liệu nhập vào không đúng";
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            // Trường hợp thêm mới khách vãng lai bên WebCheckin
            if (model.IsInWebCheckin)
            {
                var inviteEventDL = new InviteEventDL();

                if (inviteEventDL.WebCheckIn_CheckPhoneExist(0, model.EventId, model.CustomerPhone))
                {
                    response.Code = SystemCode.ErrorExist;
                    response.Message = "Số điện thoại này đã được đăng kí !";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }

                if (inviteEventDL.WebCheckIn_CheckEmailExist(0, model.EventId, model.CustomerEmail))
                {
                    response.Code = SystemCode.ErrorExist;
                    response.Message = "Email này đã được đăng kí rồi";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                var eventDL = new EventDL();
                var eventInfo = eventDL.GetByIdInWebCheckIn(model.EventId);
                // Sinh mã vé cho khách mời ,Lặp tối đa 100 lần nếu như đã tồn tại mã vé này rồi
                var code = "";
                for (var index = 0; index < 100; index++)
                {
                    code = string.Format("{0}{1}", eventInfo.EventCode, CommonMethod.GenerateCode());
                    // Kiểm tra xem mã vé này đã tồn tại chưa
                    if (!inviteEventDL.WebCheckin_IsExistCode(eventInfo.Id, code))
                    {
                        break;
                    }
                }
                // Kiểm tra phiên tham dự của khách mời có chọn tất cả không ,
                // nếu có thì loại bỏ các phiên riêng lẻ được chọn khác
                if (!string.IsNullOrEmpty(model.Sessions))
                {
                    var lstSessions = model.Sessions.Split(',').ToList();
                    if (lstSessions.Contains("0"))
                    {
                        model.Sessions = "0";
                    }
                }
                var id = inviteEventDL.WebCheckIn_Create(new InviteEventWebCheckinModel()
                {
                    EventId = model.EventId,
                    CustomerName = model.CustomerName,
                    CustomerPhone = model.CustomerPhone,
                    Note = model.Note,
                    CreatedBy = UserContext.UserName,
                    IsNotSuggest = true,
                    CustomerEmail = model.CustomerEmail,
                    PositionName = model.PositionName,
                    CompanyName = model.CompanyName,
                    CustomerType = model.CustomerType,
                    PlaceOfAttendEvent = model.PlaceOfAttendEvent,
                    RegisteredSource = model.RegisteredSource,
                    Code = code ,
                    Sessions = model.Sessions,
                    IntroduceUser = model.IntroduceUser
                });
                // Duyệt thông tin cho khách vãng lai
                inviteEventDL.WebCheckIn_UpdateStatus(id, true, DateTime.Now, UserContext.UserName);
                var sessionCheckinDL = new SessionCheckinDL();
                var lstSession = (new SessionDL()).GetByEventId(model.EventId);
                if (lstSession.Count > 0)
                {
                    if (model.Sessions == "0")
                    {
                        foreach (var item in lstSession)
                        {
                            sessionCheckinDL.Insert(new SessionCheckinModel() { 
                                CheckinBy = UserContext.UserName,
                                CheckinDate = DateTime.Now,
                                EventId = model.EventId,
                                InviteEventId = id,
                                SessionId = item.Id
                            });
                        }
                    }
                    else
                    {
                        var lstSessionId = model.Sessions.Split(',').ToList();
                        foreach (var itemId in lstSessionId)
                        {
                            sessionCheckinDL.Insert(new SessionCheckinModel()
                            {
                                CheckinBy = UserContext.UserName,
                                CheckinDate = DateTime.Now,
                                EventId = model.EventId,
                                InviteEventId = id,
                                SessionId = int.Parse(itemId) 
                            });
                        }
                    }

                    // Checkin luôn cho khách vãng lai
                    inviteEventDL.WebCheckIn_Checkin(id, true, UserContext.UserName, model.AccompanyMember);
                }
                else
                {
                    // Checkin luôn cho khách vãng lai
                    inviteEventDL.WebCheckIn_Checkin(id, true, UserContext.UserName, model.AccompanyMember);
                }
                // Thêm lịch sử lưu thông tin 
                var inviteEventHistoryDL = new InviteEventHistoryDL();
                inviteEventHistoryDL.Insert(new InviteEventHistoryModel()
                {
                    Action = "Tạo khách vãng lai",
                    OldValue = "",
                    NewValue = "",
                    UpdatedBy = UserContext.UserName,
                    InviteEventId = id
                });
                response.Code = SystemCode.Success;
                response.Message = "Bạn đã thêm thông tin thành công";
                response.Data = model.EventId;
            }
            // TH thêm mới khách mời bên BCRM (chỉ thêm mới khách vãng lai)
            else
            {
                model.CheckInBy = UserContext.UserName;
                model.CreatedBy = UserContext.UserName;
                model.Status = InviteEventStatus.ApproveLevel3.GetHashCode();
                var eventDL = new EventDL();
                var inviteEventDL = new InviteEventDL();
                var inviteEventAttachDL = new InviteEventAttachDL();
                var eventInfo = eventDL.GetEventById(model.EventId);
                // Sinh mã vé cho khách mời ,Lặp tối đa 100 lần nếu như đã tồn tại mã vé này rồi
                var code = "";
                for (var index = 0; index < 100; index++)
                {
                    code = string.Format("{0}{1}", eventInfo.EventCode, CommonMethod.GenerateCode());
                    // Kiểm tra xem mã vé này đã tồn tại chưa
                    if (!inviteEventDL.BCRM_IsExistCode(eventInfo.Id, code) 
                        && !inviteEventAttachDL.IsExistCode(eventInfo.Id,code) )
                    {
                        break;
                    }
                }
                model.Code = code;
                // check mobile info exists or not
                if (!inviteEventDL.CheckPhoneExist(0, model.EventId, model.CustomerPhone)
                     && !inviteEventAttachDL.CheckPhoneExist(model.EventId, model.CustomerPhone))
                {
                    inviteEventDL.InsertAnonymous(model);
                    response.Code = SystemCode.Success;
                    response.Message = "Bạn đã thêm thông tin thành công";
                    response.Data = model.EventId;
                }
                else
                {
                    response.Code = SystemCode.ErrorExist;
                    response.Message = "Số điện thoại này đã được đăng kí rồi !";
                }
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult Update(AnonymousInviteeModel model)
        {
            var response = new Response();
            //Valid data
            if (string.IsNullOrEmpty(model.CustomerName)
                    || string.IsNullOrEmpty(model.CustomerPhone))
            {
                response.Code = SystemCode.NotValid;
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            if (string.IsNullOrEmpty(model.CustomerEmail))
            {
                model.CustomerEmail = "";
            }
            model.CheckInBy = string.Empty;
            model.CreatedBy = UserContext.UserName;
            //var lstPhone = Ultility.DetectPhoneNumber(model.CustomerPhone);
            model.Status = InviteEventStatus.ApproveLevel3.GetHashCode();
            var inviteEventDL = new InviteEventDL();
            var inviteEventAttachDL = new InviteEventAttachDL();
            if (!inviteEventDL.CheckPhoneExist(model.Id, model.EventId, model.CustomerPhone)
                && !inviteEventAttachDL.CheckPhoneExist(model.EventId, model.CustomerPhone))
            {
                inviteEventDL.UpdateAnonymous(model);
                inviteEventDL.UpdateInviteEventCheckIn(model.Id, model.CheckInBy, model.AccompanyMember);
                response.Code = SystemCode.Success;
            }
            else
            {
                response.Code = SystemCode.ErrorExist;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public ActionResult AccompanyInvitee(CheckInInfo checkInInfo)
        {
            var inviteEventDL = new InviteEventDL();
            var attachDL = new InviteEventAttachDL();

            return View(checkInInfo);
        }
        [FilterAuthorize]
        public ActionResult AddQuantityAccompanyInvitee(CheckInInfo checkInInfo)
        {
            var inviteEventDL = new InviteEventDL();
            var attachDL = new InviteEventAttachDL();
            return View(checkInInfo);
        }
        [FilterAuthorize]
        public JsonResult AddQuantityAccompanyInviteeAction(int eventId, int inviteEventId, int attachId, int accompanyInvitee)
        {
            var response = new Response();
            var inviteEventDL = new InviteEventDL();
            var inviteAttachDL = new InviteEventAttachDL();
            // check permission
            if (!UserContext.HasPermisson(Permission.Manager) && !UserContext.HasPermisson(Permission.Admin))
            {
                var eventDL = new EventDL();
                var eventInfo = eventDL.GetEventById(eventId);

                if (eventInfo != null)
                {
                    var timeNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                    if (eventInfo.EndDate < timeNow)
                    {
                        response.Code = SystemCode.NotPermitted;
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    response.Code = SystemCode.NotPermitted;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }
            if (attachId == 0)
            {
                inviteEventDL.UpdateAccompanyInvitee(inviteEventId, accompanyInvitee);
            }
            else
            {
                inviteAttachDL.UpdateAccompanyInvitee(attachId, accompanyInvitee);
            }
            response.Code = SystemCode.Success;

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public ActionResult AccompanyInviteeInWebChecin(CheckInInfo checkInInfo)
        {
            // Kiểm tra xem sự kiện này có phiên hay không
            var lstSession = (new SessionDL()).GetByEventId(checkInInfo.EventId);
            var inviteeInfo = (new InviteEventDL()).WebCheckIn_GetById(checkInInfo.InviteEventId);
            if (lstSession.Count > 0)
            {
                checkInInfo.IsHasSession = true;
                // Lấy ra danh sách các phiên đã checkin
                var lstCheckedin = (new SessionCheckinDL()).GetByInviteEventId(checkInInfo.InviteEventId).Select(o=>o.SessionId).ToList();
                lstSession = lstSession.FindAll(o => !lstCheckedin.Contains(o.Id));

                var lstSelectedSession = inviteeInfo.Sessions.Split(',');
                if (lstSelectedSession.Contains("0"))
                {
                    // Lấy ra danh sách các phiên mà nhân viên đó đã đăng kí tham gia
                    checkInInfo.LstSession = lstSession;
                }
                else
                {
                    checkInInfo.LstSession = lstSession.FindAll(o => lstSelectedSession.Contains(o.Id.ToString()) );
                }
            }

            checkInInfo.AccompanyInvitee = inviteeInfo.AccompanyMember;
            return View(checkInInfo);
        }
        [FilterAuthorize]
        public ActionResult AddAccompanyMemberWebCheckin(CheckInInfo checkInInfo)
        {
            return View(checkInInfo);
        }
        [FilterAuthorize]
        public JsonResult AddAccompanyMemberActionWebCheckin(int eventId, int inviteEventId, int accompanyMember)
        {
            var response = new Response();
            // check permission
            if (!UserContext.HasPermisson(Permission.Manager) && !UserContext.HasPermisson(Permission.Admin))
            {
                var eventDL = new EventDL();
                var eventInfo = eventDL.GetByIdInWebCheckIn(eventId);

                if (eventInfo != null)
                {
                    var timeNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                    if (eventInfo.EndDate < timeNow)
                    {
                        response.Code = SystemCode.NotPermitted;
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    response.Code = SystemCode.NotPermitted;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }

            var inviteEventDL = new InviteEventDL();
            inviteEventDL.WebCheckin_UpdateAccompanyMember(inviteEventId, accompanyMember);

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public ActionResult Create()
        {
            //if (!UserContext.HasPermisson(Permission.Admin) && !UserContext.HasPermisson(Permission.Manager))
            //{
            //    return View("~/Views/Common/NotPermitted.cshtml");
            //}
            var eventDL = new EventDL();
            var defaultEvent = CommonMethod.GetDefaultEvent();
            if (defaultEvent == null)
            {
                defaultEvent = new EventModel()
                {
                    Id = 0
                };
            }
            var lstAllEventWebCheckin = eventDL.GetAllInWebCheckIn().FindAll(o => o.IsEnable == true);
            if (defaultEvent.IsInWebCheckIn)
            {
                if (lstAllEventWebCheckin.Find(o => o.Id == defaultEvent.Id) != null)
                {
                    ViewBag.DefaultEvent = defaultEvent;
                }
                else
                {
                    ViewBag.DefaultEvent = new EventModel()
                    {
                        Id = 0,
                        IsInWebCheckIn = true
                    };
                }

            }
            ViewBag.LstEvent = lstAllEventWebCheckin.OrderByDescending(o => o.CreatedDate).ToList();
            return View();
        }
        [FilterAuthorize]
        public JsonResult CreateInviteeAction(InviteEventWebCheckinModel model)
        {
            var response = new Response();

            // Kiểm tra tính hợp lệ của dữ liệu nhập vào
            if (string.IsNullOrEmpty(model.CustomerName)
                || string.IsNullOrEmpty(model.CustomerPhone)
                )
            {
                response.Code = SystemCode.DataNull;
                response.Message = "Dữ liệu khách hàng nhập vào không hợp lệ";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra số điện thoại này đã được đăng kí hay chưa
            var inviteEventDL = new InviteEventDL();
            if (inviteEventDL.WebCheckIn_CheckPhoneExist(0, model.EventId, model.CustomerPhone))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Số điện thoại này đã được sử dụng";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Thêm mới
            model.CreatedBy = UserContext.UserName;
            model.IsNotSuggest = false;
            inviteEventDL.WebCheckIn_Create(model);
            response.Code = SystemCode.Success;
            response.Message = "Bạn đã thêm mới khách hàng thành công";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public ActionResult EditInviteeWebCheckin(int inviteEventId)
        {
            // Lấy thông tin khách mời
            var inviteEventDL = new InviteEventDL();
            var inviteEventInfo = inviteEventDL.WebCheckIn_GetById(inviteEventId);
            // Lấy danh sách các sự kiện đang hoạt động
            var eventDL = new EventDL();
            var lstAllEventWebCheckin = eventDL.GetAllInWebCheckIn().FindAll(o => o.IsEnable = true);
            ViewBag.LstEvent = lstAllEventWebCheckin;
            // Lấy danh sách loại khách mời
            ViewBag.ListCustomerType = Helper.ListCustomerType();
            // Lấy danh sách nơi tổ chức sự kiện
            ViewBag.ListPlaceAttendEvent = new List<KeyValuePair<string, string>>();
            ViewBag.ListPlaceAttendEvent.Add(new KeyValuePair<string, string>("Hà Nội", "Hà Nội"));
            ViewBag.ListPlaceAttendEvent.Add(new KeyValuePair<string, string>("Đà Nẵng", "Đà Nẵng"));
            ViewBag.ListPlaceAttendEvent.Add(new KeyValuePair<string, string>("TP. Hồ Chí Minh", "TP. Hồ Chí Minh"));
            // Lấy danh sách nguồn đăng kí
            ViewBag.ListRegisteredSource = CheckInHelper.ListRegisteredSource();

            // Lấy thông tin các phiên của sự kiện của khách mời
            var lstSession = (new SessionDL()).GetByEventId(inviteEventInfo.EventId);
            if (lstSession.Count > 0)
            {
                // Các phiên khách mời đã chọn
                if (!string.IsNullOrEmpty(inviteEventInfo.Sessions))
                {
                    inviteEventInfo.LstSelectedSession = inviteEventInfo.Sessions.Split(',').Select(o => o).ToList();
                }
                else
                {
                    inviteEventInfo.LstSelectedSession = new List<string>();
                }

                inviteEventInfo.LstSessionCheckin = lstSession.Select(o => new KeyValuePair<string, string>(o.Id.ToString(), o.Name)).ToList();
            }
            else
            {
                inviteEventInfo.LstSessionCheckin = new List<KeyValuePair<string, string>>();
            }

            var lstCheckin = (new SessionCheckinDL()).GetByInviteEventId(inviteEventId);
            ViewBag.CheckedinSessionId = string.Join(",", lstCheckin.Select(o => o.SessionId).ToList());

            return View(inviteEventInfo);
        }
        [FilterAuthorize]
        public JsonResult EditInviteeWebCheckinAction(InviteEventWebCheckinModel model)
        {
            var response = new Response();

            // Kiểm tra tính hợp lệ của dữ liệu nhập vào
            if (string.IsNullOrEmpty(model.CustomerName)
                || string.IsNullOrEmpty(model.CustomerPhone)
                )
            {
                response.Code = SystemCode.DataNull;
                response.Message = "Dữ liệu khách hàng nhập vào không hợp lệ";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra số điện thoại này đã được đăng kí hay chưa
            var inviteEventDL = new InviteEventDL();
            if (inviteEventDL.WebCheckIn_CheckPhoneExist(model.Id, model.EventId, model.CustomerPhone))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Số điện thoại này đã được sử dụng";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra email này đã được đăng kí chưa 
            if (inviteEventDL.WebCheckIn_CheckEmailExist(model.Id, model.EventId, model.CustomerEmail))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Email này đã được đăng kí rồi";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Cập nhật
            model.CreatedBy = UserContext.UserName;
            model.IsNotSuggest = false;
            var onlineRegisterInfo = inviteEventDL.GetOnlineRegisteredInviteeById(model.Id);
            // Tạo thêm lịch sử nếu có thay đổi
            InsertEditInviteEventHistory(onlineRegisterInfo, model);
            inviteEventDL.WebCheckIn_Update(model);
            response.Code = SystemCode.Success;
            response.Message = "Bạn đã cập nhật khách hàng thành công";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult DeleteInWebCheckin(int inviteEventId)
        {
            var response = new Response();
            try
            {
                var inviteEventDL = new InviteEventDL();
                var onlineRegisterInfo = inviteEventDL.GetOnlineRegisteredInviteeById(inviteEventId);
                if (onlineRegisterInfo == null)
                {
                    response.Code = SystemCode.DataNull;
                    response.Message = "Không tìm thấy thông tin phù hợp";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                inviteEventDL.WebCheckin_Delete(inviteEventId);
                // Lưu lịch sử thông tin xóa
                var inviteEventHistoryDL = new InviteEventHistoryDL();
                inviteEventHistoryDL.Insert(new InviteEventHistoryModel() { 
                    Action="Xóa khách mời",
                    InviteEventId = inviteEventId,
                    OldValue = "",
                    NewValue = "",
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now
                });

                response.Code = SystemCode.Success;
                response.Message = "Bạn đã xóa thông tin khách mời thành công";
            }
            catch
            {
                response.Code = SystemCode.Error;
                response.Message = "Có lỗi trong quá trình xử lý ! Liên hệ IT để có thông tin chi tiết";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public ActionResult ChangeSession(int eventId, int inviteEventId)
        {
            var model = new InviteEventSessionChangeModel();
            model.InviteEventId = inviteEventId;
            model.EventId = eventId;
            model.LstSession = (new SessionDL()).GetByEventId(eventId);
            var inviteeInfo = (new InviteEventDL()).WebCheckIn_GetById(inviteEventId);
            model.Sessions = inviteeInfo.Sessions;
            if (!string.IsNullOrEmpty(inviteeInfo.Sessions))
            {
                model.LstSelectedSessionId = inviteeInfo.Sessions.Split(',').ToList();
            }
            else
            {
                model.LstSelectedSessionId = new List<string>();
            }

            var lstCheckin = (new SessionCheckinDL()).GetByInviteEventId(inviteEventId);
            model.CheckedinSessionId = string.Join(",", lstCheckin.Select(o => o.SessionId).ToList());
            return View(model);
        }
        [FilterAuthorize]
        public JsonResult ChangeSessionAction(int inviteeId, string sessions)
        {
            var inviteEventDL = new InviteEventDL();
            var inviteInfo = inviteEventDL.WebCheckIn_GetById(inviteeId);
            var lstSession = (new SessionDL()).GetByEventId(inviteInfo.EventId);
            if (lstSession.Count > 0)
            {
                if(inviteInfo.Sessions != sessions)
                {
                    var strOld = "";
                    if (!string.IsNullOrEmpty(inviteInfo.Sessions))
                    {
                        var lstOldSelected = inviteInfo.Sessions.Split(',').ToList();
                        if (lstOldSelected.Contains("0"))
                        {
                            strOld = "Tất cả các phiên";
                        }
                        else
                        {
                            var outSessionId = 0;
                            foreach (var oldSelected in lstOldSelected)
                            {
                                if (int.TryParse(oldSelected.Trim(), out outSessionId))
                                {
                                    if (string.IsNullOrEmpty(strOld))
                                    {
                                        strOld = lstSession.Find(o => o.Id == outSessionId).Name;
                                    }
                                    else
                                    {
                                        strOld += "," + lstSession.Find(o => o.Id == outSessionId).Name;
                                    }
                                }
                            }

                            //strOld = lstSession.Find(o => o.Id == int.Parse(lstOldSelected[0])).Name;
                            //for (var index = 1; index < lstOldSelected.Count; index++)
                            //{
                            //    strOld += "," + lstSession.Find(o => o.Id == int.Parse(lstOldSelected[index])).Name;
                            //}
                        }
                    }

                    var lstNewSelected = sessions.Split(',').ToList();
                    var strNew = "";
                    if (lstNewSelected.Contains("0"))
                    {
                        strNew = "Tất cả các phiên";
                    }
                    else
                    {
                        var outSessionId = 0;
                        foreach (var newSelected in lstNewSelected)
                        {
                            if (int.TryParse(newSelected.Trim(), out outSessionId))
                            {
                                if (string.IsNullOrEmpty(strNew))
                                {
                                    strNew = lstSession.Find(o => o.Id == outSessionId).Name;
                                }
                                else
                                {
                                    strNew += "," + lstSession.Find(o => o.Id == outSessionId).Name;
                                }
                            }
                        }

                        //strNew = lstSession.Find(o => o.Id == int.Parse(lstNewSelected[0])).Name;
                        //for (var index = 1; index < lstNewSelected.Count; index++)
                        //{
                        //    strNew += "," + lstSession.Find(o => o.Id == int.Parse(lstNewSelected[index])).Name;
                        //}
                    }

                    if (strOld != strNew)
                    {
                        // Lưu lịch sư thay đổi phiên
                        var inviteEventHistoryDL = new InviteEventHistoryDL();
                        inviteEventHistoryDL.Insert(new InviteEventHistoryModel()
                        {
                            Action = "Phiên tham dự",
                            UpdatedBy = UserContext.UserName,
                            UpdatedDate = DateTime.Now,
                            NewValue = strNew,
                            OldValue = strOld,
                            InviteEventId = inviteeId
                        });
                    }
                }
            }

            inviteEventDL.WebCheckin_UpdateSessions(inviteeId, sessions);
            var response = new Response();
            response.Code = SystemCode.Success;
            response.Message = "Bạn đã thay đổi phiên tham dự của khách mời thành công";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult RejectSessionCheckin(int inviteeId, int sessionId)
        {
            var sessionCheckinDL = new SessionCheckinDL();
            sessionCheckinDL.Reject(inviteeId, sessionId);
            var inviteEventDL = new InviteEventDL();
            var inviteeInfo = inviteEventDL.WebCheckIn_GetById(inviteeId);
            var lstSessionCheckin =  sessionCheckinDL.GetByInviteEventId(inviteeId);
            if (lstSessionCheckin.Count == 0)
            {
                inviteEventDL.WebCheckIn_Checkin(inviteeId, false, UserContext.UserName, 0);
                if (inviteeInfo.SendTicketStatus.Value == 2)
                {
                    inviteEventDL.WebCheckin_UpdateSendTicketStatus(inviteeId, 2, 17);
                }
                else
                {
                    inviteEventDL.WebCheckin_UpdateSendTicketStatus(inviteeId, 1, 16);
                }
            }
            
            var lstSession = (new SessionDL()).GetByEventId(inviteeInfo.EventId);
            // Lưu lịch sư thay đổi phiên
            var inviteEventHistoryDL = new InviteEventHistoryDL();
            inviteEventHistoryDL.Insert(new InviteEventHistoryModel()
            {
                Action = "Hủy checkin",
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now,
                NewValue = "",
                OldValue = "",
                InviteEventId = inviteeId
            });

            var response = new Response();
            response.Code = SystemCode.Success;
            response.Message = "Bạn đã hủy thông tin checkin thành công";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public ActionResult CheckinDetail(int eventId, int inviteEventId)
        {
            // Lấy thông tin khách mời
            var inviteeDL = new InviteEventDL();
            var inviteeInfo = inviteeDL.WebCheckIn_GetById(inviteEventId);
            // Kiểm tra sự kiện của khách mời có chia theo phiên hay không
            var lstSession = (new SessionDL()).GetByEventId(eventId);
            var checkinInfo = new SessionCheckinInfo();
            //TH sự kiện có chia theo phiên
            if (lstSession.Count > 0)
            {
                var sessionCheckinDL = new SessionCheckinDL();
                var lstSessionCheckin = sessionCheckinDL.GetByInviteEventId(inviteEventId);
                var lstCheckinDetail = new List<SessionCheckinDetailModel>();
                foreach (var item in lstSessionCheckin)
                {
                    var sessionInfo = lstSession.Find(o => o.Id == item.SessionId);
                    lstCheckinDetail.Add(new SessionCheckinDetailModel() { 
                        EventId = item.EventId,
                        InviteEventId = item.InviteEventId,
                        SessionId = item.SessionId,
                        SessionName = sessionInfo.Name,
                        CheckinBy = item.CheckinBy,
                        CheckinDate = item.CheckinDate,
                        AccompanyMember = item.AccompanyMember
                    });
                }
                checkinInfo.LstDetail = lstCheckinDetail;
                checkinInfo.IsHasSession = true;
            }
            else
            {
                var lstCheckinDetail = new List<SessionCheckinDetailModel>();
                lstCheckinDetail.Add(new SessionCheckinDetailModel() {
                    EventId = inviteeInfo.EventId,
                    InviteEventId = inviteeInfo.Id,
                    SessionId = 0,
                    CheckinBy = inviteeInfo.CheckInBy,
                    CheckinDate = inviteeInfo.CheckInDate,
                    AccompanyMember = inviteeInfo.AccompanyMember
                });
                checkinInfo.LstDetail = lstCheckinDetail;
                checkinInfo.IsHasSession = false;
            }


            return View(checkinInfo);
        }
        #endregion

        #region Utility
        private FileStreamResult BCRM_GetInvitee(EventIndexModel model)
        {
            var fileName = "InviteEvent-" + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            var file = new FileInfo(fileName);
            using (var package = new ExcelPackage(file))
            {
                var worksheet = package.Workbook.Worksheets.Add("InviteEvent - " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss"));
                #region Header
                worksheet.Cells["A1:AA2"].Merge = true;
                worksheet.Cells["A1:AA2"].Value = "DANH SÁCH KHÁCH MỜI THAM GIA SỰ KIỆN";
                worksheet.Cells["A1:AA2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["A1:AA2"].Style.Fill.BackgroundColor.SetColor(Color.White);
                worksheet.Cells["A1:AA2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["A1:AA2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["A1:AA2"].Style.Font.Bold = true;
                worksheet.Cells["A1:AA2"].Style.Font.Name = "Calibri";
                worksheet.Cells["A1:AA2"].Style.Font.Size = 18;
                worksheet.Cells["A1:AA2"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["A1:AA2"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                worksheet.Cells["A4:AA4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                worksheet.Cells["A4:AA4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["A4:AA4"].Style.Font.Bold = true;
                worksheet.Column(1).Width = 15;
                worksheet.Column(2).Width = 30;
                worksheet.Column(3).Width = 30;
                worksheet.Column(4).Width = 30;
                worksheet.Column(5).Width = 30;
                worksheet.Column(6).Width = 30;
                worksheet.Column(7).Width = 30;
                worksheet.Column(8).Width = 30;
                worksheet.Column(9).Width = 30;
                worksheet.Column(10).Width = 30;
                worksheet.Column(11).Width = 30;
                worksheet.Column(12).Width = 30;
                worksheet.Column(13).Width = 30;
                worksheet.Column(14).Width = 30;
                worksheet.Column(15).Width = 30;
                worksheet.Column(16).Width = 30;
                worksheet.Column(17).Width = 30;
                worksheet.Column(18).Width = 30;
                worksheet.Column(19).Width = 30;
                worksheet.Column(20).Width = 30;
                worksheet.Column(21).Width = 30;
                worksheet.Column(22).Width = 30;
                worksheet.Column(23).Width = 30;
                worksheet.Column(24).Width = 30;
                worksheet.Column(25).Width = 30;
                worksheet.Column(26).Width = 30;
                worksheet.Column(27).Width = 30;

                worksheet.Cells["A4"].Value = "Id";
                worksheet.Cells["B4"].Value = "Mã vé";
                worksheet.Cells["C4"].Value = "Sự kiện";
                worksheet.Cells["D4"].Value = "Tên KH";
                worksheet.Cells["E4"].Value = "Chức vụ";
                worksheet.Cells["F4"].Value = "SĐT";
                worksheet.Cells["G4"].Value = "Loại khách";
                worksheet.Cells["H4"].Value = "Khách VIP";
                worksheet.Cells["I4"].Value = "Email";
                worksheet.Cells["J4"].Value = "Ngày sinh";
                worksheet.Cells["K4"].Value = "Giới tính";
                worksheet.Cells["L4"].Value = "Doanh thu";
                worksheet.Cells["M4"].Value = "Loại khách hàng";
                worksheet.Cells["N4"].Value = "Tên công ty";
                worksheet.Cells["O4"].Value = "Địa chỉ";
                worksheet.Cells["P4"].Value = "Quy mô doanh nghiệp";
                worksheet.Cells["Q4"].Value = "Thành phố";
                worksheet.Cells["R4"].Value = "Quận huyện";
                worksheet.Cells["S4"].Value = "Khả năng tham dự";
                worksheet.Cells["T4"].Value = "Ghi chú";
                worksheet.Cells["U4"].Value = "Trạng thái";
                worksheet.Cells["V4"].Value = "Ghi chú(trạng thái)";
                worksheet.Cells["W4"].Value = "Người tạo";
                worksheet.Cells["X4"].Value = "Địa chỉ nhận vé";
                worksheet.Cells["Y4"].Value = "Thời gian check in";
                worksheet.Cells["Z4"].Value = "Lễ tân check in";
                worksheet.Cells["AA4"].Value = "Số khách đi cùng";

                #endregion

                #region GetData
                var inviteEventDL = new InviteEventDL();
                var inviteEventAttachDL = new InviteEventAttachDL();
                #region Searching
                var lstData = new List<InviteEventModelToExport>();
                #region Searching to invitee code
                if (!string.IsNullOrEmpty(model.InviteeCode))
                {
                    var arrInviteeCode = model.InviteeCode.Split('-');

                    // split EventId, InviteId and AttachId
                    var eventId = 0;
                    var inviteeId = 0;
                    var attachId = 0;

                    if (arrInviteeCode.Length > 3)
                    {
                        eventId = 0;
                    }
                    else
                    {
                        int.TryParse(arrInviteeCode[0], out eventId);
                        if (arrInviteeCode.Length >= 2)
                        {
                            int.TryParse(arrInviteeCode[1], out inviteeId);
                        }
                        if (arrInviteeCode.Length == 3)
                        {
                            int.TryParse(arrInviteeCode[2], out attachId);
                        }
                    }
                    // searching
                    var lstInviteEvent = new List<InviteEventModelToExport>();
                    var lstInviteEventAttach = new List<InviteEventModelToExport>();
                    if (eventId > 0)
                    {
                        if (attachId == 0)
                        {
                            lstInviteEvent = inviteEventDL.GetListByInviteeCodeToExport(eventId, inviteeId);
                        }
                        lstInviteEventAttach = inviteEventAttachDL.GetListByInviteeCodeToExport(eventId, inviteeId, attachId);
                    }
                    // over all searching result
                    lstData.AddRange(lstInviteEvent);
                    lstData.AddRange(lstInviteEventAttach);
                }
                #endregion
                #region Searching by code
                else if (!string.IsNullOrEmpty(model.Code))
                {
                    // TH người dùng tìm kiếm theo mã số khách mời
                    int searchByCode = 0;
                    if (int.TryParse(model.Code, out searchByCode))
                    {
                        var eventDataLayer = new EventDL();
                        var evtInfo = eventDataLayer.GetEventById(model.EventId);
                        if (evtInfo != null)
                        {
                            model.Code = evtInfo.EventCode + model.Code;
                        }
                    }

                    var lstInviteEvent = inviteEventDL.GetListByCodeToExport(model.EventId, model.Code);
                    var lstInviteEventAttach = inviteEventAttachDL.GetListByCodeToExport(model.EventId, model.Code);
                    lstData.AddRange(lstInviteEvent);
                    lstData.AddRange(lstInviteEventAttach);
                    lstData = lstData.OrderBy(o => o.Code).ToList();
                }
                #endregion
                #region Searching to text search and another condition
                else
                {

                    // searching
                    var lstInviteEvent = new List<InviteEventModelToExport>();
                    var lstInviteEventAttach = new List<InviteEventModelToExport>();
                    // find all Customer Type
                    if (model.CustomerType == null)
                    {
                        lstInviteEvent = inviteEventDL.GetListByConditionToExport(model);
                        lstInviteEventAttach = inviteEventAttachDL.GetListByConditionToExport(model);
                    }
                    // find all official invitee
                    else if (model.CustomerType == 0)
                    {
                        lstInviteEvent = inviteEventDL.GetListOfficialByConditionToExport(model);
                    }
                    else if (model.CustomerType == 1)
                    {
                        lstInviteEventAttach = inviteEventAttachDL.GetListByConditionToExport(model);
                    }
                    else if (model.CustomerType == 2)
                    {
                        lstInviteEvent = inviteEventDL.GetListAnonymousByConditionToExport(model);
                    }
                    // over all searching result
                    var lstAllInvitee = new List<InviteEventModelToExport>();
                    lstAllInvitee.AddRange(lstInviteEvent);
                    lstAllInvitee.AddRange(lstInviteEventAttach);

                    // find invitee 
                    if (string.IsNullOrEmpty(model.TextSearch))
                    {
                        lstData = lstAllInvitee.OrderBy(o => o.EventId)
                                                        .ThenBy(o => o.AttachId).ToList();
                    }
                    else
                    {
                        var convertUnsign = CommonMethod.ConvertToUnSign(model.TextSearch.ToLower());
                        var exactMatchInvitee = lstAllInvitee.FindAll(o => o.CustomerName.ToLower().Contains(model.TextSearch.ToLower()) || o.CustomerPhone.ToLower().Contains(model.TextSearch.ToLower()));
                        var opportunityInvitee = lstAllInvitee.FindAll(o =>
                                                                !o.CustomerName.ToLower().Contains(model.TextSearch.ToLower())
                                                                && !o.CustomerPhone.ToLower().Contains(model.TextSearch.ToLower())
                                                                && (CommonMethod.ConvertToUnSign(o.CustomerName.ToLower()).Contains(convertUnsign))
                                                                );
                        var remainInvitee = lstAllInvitee.FindAll(o => !o.CustomerName.ToLower().Contains(model.TextSearch.ToLower())
                                                                        && !o.CustomerPhone.ToLower().Contains(model.TextSearch.ToLower())
                                                                        && !CommonMethod.ConvertToUnSign(o.CustomerName.ToLower()).Contains(convertUnsign));
                        lstData.AddRange(exactMatchInvitee);
                        lstData.AddRange(opportunityInvitee);
                        lstData.AddRange(remainInvitee);
                    }
                }
                #endregion
                #endregion
                var eventLayer = new EventDL();
                var companyBl = new CompanyDL();
                var cityBl = new CityDL();
                var districtBl = new DistrictDL();
                var lstCompany = companyBl.GetAllActive(CompanyType.Nomal);
                var lstCity = cityBl.GetAllCity();
                var lstDistrict = districtBl.GetAll();
                var row = 5;
                foreach (var inviteEvent in lstData)
                {
                    worksheet.Cells["A" + row].Value = string.Format("{0}-{1}-{2}", inviteEvent.EventId, inviteEvent.InviteId, inviteEvent.AttachId);
                    worksheet.Cells["B" + row].Value = inviteEvent.Code;
                    worksheet.Cells["C" + row].Value = inviteEvent.EventName;
                    worksheet.Cells["D" + row].Value = inviteEvent.CustomerName;
                    worksheet.Cells["E" + row].Value = inviteEvent.CustomerPosition.ToCustomerPositionName();
                    worksheet.Cells["F" + row].Value = inviteEvent.CustomerPhone;
                    worksheet.Cells["G" + row].Value = inviteEvent.AttachId == 0 ? (inviteEvent.CustomerId == 0 ? "Khách vãng lai" : "Khách chính") : "Khách đính kèm";
                    worksheet.Cells["H" + row].Value = inviteEvent.CustomerVip == 0 ? "thường" : "khách VIP";
                    worksheet.Cells["I" + row].Value = inviteEvent.CustomerEmail;
                    worksheet.Cells["J" + row].Value = inviteEvent.CustomerBirthday.HasValue ? inviteEvent.CustomerBirthday.Value.ToString("dd/MM/yyyy") : string.Empty;
                    worksheet.Cells["K" + row].Value = inviteEvent.CustomerSex == null ? string.Empty : (inviteEvent.CustomerSex.Value == true ? "Nam" : "Nữ");
                    worksheet.Cells["L" + row].Value = inviteEvent.CustomerRevenue.ToString("#,###,###");
                    worksheet.Cells["L" + row].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    worksheet.Cells["M" + row].Value = (inviteEvent.CustomerType).ToCustomerType();
                    worksheet.Cells["N" + row].Value = inviteEvent.CompanyName;
                    worksheet.Cells["O" + row].Value = inviteEvent.CompanyAddress;
                    worksheet.Cells["P" + row].Value = inviteEvent.CompanySize.ToCompanySizeName();
                    if (inviteEvent.CompanyId > 0)
                    {
                        var company = lstCompany.Find(c => c.Id == inviteEvent.CompanyId);
                        if (company != null)
                        {
                            var city = lstCity.Find(c => c.Id == company.CityId);
                            var district = lstDistrict.Find(d => d.Id == company.DistrictId);
                            worksheet.Cells["Q" + row].Value = city != null ? city.CityName : string.Empty;
                            worksheet.Cells["R" + row].Value = district != null ? district.DistrictName : string.Empty;
                        }
                    }
                    else
                    {
                        worksheet.Cells["Q" + row].Value = string.Empty;
                        worksheet.Cells["R" + row].Value = string.Empty;
                    }
                    worksheet.Cells["S" + row].Value = inviteEvent.AbilityAttend.ToAbilityAttendName();
                    worksheet.Cells["T" + row].Value = inviteEvent.Note;
                    worksheet.Cells["U" + row].Value = inviteEvent.Status.ToInviteEventStatusName();
                    worksheet.Cells["V" + row].Value = inviteEvent.StatusNote;
                    worksheet.Cells["W" + row].Value = inviteEvent.CreatedBy;
                    worksheet.Cells["X" + row].Value = inviteEvent.TicketAddress;
                    worksheet.Cells["Y" + row].Value = inviteEvent.CheckInDate == null ? string.Empty : inviteEvent.CheckInDate.Value.ToString("dd/MM/yyyy HH:mm:ss");
                    worksheet.Cells["Z" + row].Value = inviteEvent.CheckInBy;
                    worksheet.Cells["AA" + row].Value = inviteEvent.AccompanyMember;
                    worksheet.Row(row).Height = 25;
                    row++;
                }
                #endregion
                var stream = new MemoryStream();
                package.SaveAs(stream);
                const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                stream.Position = 0;
                return File(stream, contentType, fileName);
            }
        }
        private FileStreamResult WebCheckin_GetInvitee(EventIndexModel model)
        {
            var fileName = "InviteEvent-" + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            var file = new FileInfo(fileName);
            using (var package = new ExcelPackage(file))
            {
                var worksheet = package.Workbook.Worksheets.Add("InviteEvent - " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss"));
                #region Header
                worksheet.Cells["A1:X2"].Merge = true;
                worksheet.Cells["A1:X2"].Value = "DANH SÁCH KHÁCH MỜI ĐĂNG KÍ THAM GIA SỰ KIỆN";
                worksheet.Cells["A1:X2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["A1:X2"].Style.Fill.BackgroundColor.SetColor(Color.White);
                worksheet.Cells["A1:X2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["A1:X2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["A1:X2"].Style.Font.Bold = true;
                worksheet.Cells["A1:X2"].Style.Font.Name = "Times New Roman";
                worksheet.Cells["A1:X2"].Style.Font.Size = 18;
                worksheet.Cells["A1:X2"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["A1:X2"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["A4:X4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                worksheet.Cells["A4:X4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["A4:X4"].Style.Font.Bold = true;

                // Width mã khách mời
                worksheet.Column(1).Width = 15;
                // Width mã vé
                worksheet.Column(2).Width = 10;
                // Width sự kiện
                worksheet.Column(3).Width = 30;
                // Width tên KH
                worksheet.Column(4).Width = 20;
                // Width SĐT
                worksheet.Column(5).Width = 20;
                // Width Email
                worksheet.Column(6).Width = 20;
                // Width Loại khách
                worksheet.Column(7).Width = 15;
                // Width chức vụ
                worksheet.Column(8).Width = 20;
                // Width Công ty
                worksheet.Column(9).Width = 40;
                // Width Địa điểm tổ chức
                worksheet.Column(10).Width = 20;
                // Width Tài khoản batdongsan.com
                worksheet.Column(11).Width = 40;
                // Width Nhân viên mời
                worksheet.Column(12).Width = 15;
                // Width Trạng thái cuối
                worksheet.Column(13).Width = 30;
                // Width Ghi chú
                worksheet.Column(14).Width = 30;
                // Width Người tạo
                worksheet.Column(15).Width = 10;
                // Width Chi nhánh mời
                worksheet.Column(16).Width = 20;
                // Width Phòng ban mời
                worksheet.Column(17).Width = 20;
                // Width Nhóm mời
                worksheet.Column(18).Width = 20;
                // Width Sale mời
                worksheet.Column(19).Width = 15;
                // Width Nguồn khách hàng
                worksheet.Column(20).Width = 15;
                // Width Phiên tham dự
                worksheet.Column(21).Width = 15;
                // Width Thời gian checkin
                worksheet.Column(22).Width = 25;
                // Width Lễ tân checkin
                worksheet.Column(23).Width = 25;
                // Width Số khách đi cùng
                worksheet.Column(24).Width = 20;

                worksheet.Cells["A4"].Value = "Mã khách mời";
                worksheet.Cells["B4"].Value = "Mã vé";
                worksheet.Cells["C4"].Value = "Sự kiện";
                worksheet.Cells["D4"].Value = "Tên KH";
                worksheet.Cells["E4"].Value = "SĐT";
                worksheet.Cells["F4"].Value = "Email";
                worksheet.Cells["G4"].Value = "Loại khách mời";
                worksheet.Cells["H4"].Value = "Chức vụ";
                worksheet.Cells["I4"].Value = "Công ty";
                worksheet.Cells["J4"].Value = "Địa điểm tổ chức";
                worksheet.Cells["K4"].Value = "Tài khoản batdongsan.com";
                worksheet.Cells["L4"].Value = "Nhân viên mời";
                worksheet.Cells["M4"].Value = "Trạng thái cuối";
                worksheet.Cells["N4"].Value = "Ghi chú";
                worksheet.Cells["O4"].Value = "Người tạo";
                worksheet.Cells["P4"].Value = "Chi nhánh mời";
                worksheet.Cells["Q4"].Value = "Phòng ban mời";
                worksheet.Cells["R4"].Value = "Nhóm mời";
                worksheet.Cells["S4"].Value = "Sale mời";
                worksheet.Cells["T4"].Value = "Nguồn khách hàng";
                worksheet.Cells["U4"].Value = "Phiên tham dự";
                worksheet.Cells["V4"].Value = "Thời gian check in";
                worksheet.Cells["W4"].Value = "Lễ tân check in";
                worksheet.Cells["X4"].Value = "Số khách đi cùng";

                #endregion
                #region GetData
                #region Searching
                var inviteEventDL = new InviteEventDL();
                var lstData = new List<InviteEventModelWebCheckinToExport>();
                // Tìm kiếm theo id của khách mời
                if (model.InviteeCodeInWebCheckin > 0)
                {
                    var info = inviteEventDL.WebCheckIn_GetByIdToExport(model.InviteeCodeInWebCheckin);
                    if (info != null)
                    {
                        var checkStatusInvite = inviteEventDL.WebCheckIn_GetById(model.InviteeCodeInWebCheckin);
                        if (checkStatusInvite.Status == true)
                            lstData.Add(info);
                    }
                }
                else if (!string.IsNullOrEmpty(model.Code))
                {
                    // TH người dùng tìm kiếm theo mã số khách mời
                    int searchByCode = 0;
                    if (int.TryParse(model.Code, out searchByCode))
                    {
                        var eventDataLayer = new EventDL();
                        var evtInfo = eventDataLayer.GetByIdInWebCheckIn(model.EventId);
                        if (evtInfo != null)
                        {
                            model.Code = evtInfo.EventCode + model.Code;
                        }
                    }
                    // Lấy danh sách khách khách mời chính theo mã code
                    lstData = inviteEventDL.WebCheckin_GetByCodeToExport(model.EventId, model.Code);
                }
                else
                {
                    if (model.CustomerType == null)
                    {
                        model.IsNotSuggest = null;
                    }
                    else
                    {
                        if (model.CustomerType == 0)
                        {
                            model.IsNotSuggest = false;
                        }
                        else
                        {
                            model.IsNotSuggest = true;
                        }
                    }
                    lstData = inviteEventDL.WebCheckIn_GetByConditionToExport(model);
                    // Tìm kiếm theo thông tin
                    // find invitee 
                    if (!string.IsNullOrEmpty(model.TextSearch))
                    {
                        var convertUnsign = CommonMethod.ConvertToUnSign(model.TextSearch.ToLower());
                        var exactMatchInvitee = lstData.FindAll(o => o.CustomerName.ToLower().Contains(model.TextSearch.ToLower()) || o.CustomerPhone.ToLower().Contains(model.TextSearch.ToLower()));
                        var opportunityInvitee = lstData.FindAll(o =>
                                                                !o.CustomerName.ToLower().Contains(model.TextSearch.ToLower())
                                                                && !o.CustomerPhone.ToLower().Contains(model.TextSearch.ToLower())
                                                                && (CommonMethod.ConvertToUnSign(o.CustomerName.ToLower()).Contains(convertUnsign) || o.CustomerEmail.Contains(convertUnsign))
                                                                );
                        var remainInvitee = lstData.Except(exactMatchInvitee).Except(opportunityInvitee).ToList();
                        var lstAllInvitee = new List<InviteEventModelWebCheckinToExport>();
                        lstAllInvitee.AddRange(exactMatchInvitee);
                        lstAllInvitee.AddRange(opportunityInvitee);
                        lstAllInvitee.AddRange(remainInvitee);
                        lstData = lstAllInvitee;
                    }
                    else
                    {
                        lstData = lstData.OrderByDescending(o => o.CreatedDate).ToList();
                    }
                }

                foreach (var inviteEvent in lstData)
                {
                    if (inviteEvent.IsBcrmProcess)
                    {
                        inviteEvent.PositionName = inviteEvent.CustomerPosition.ToCustomerPositionName();
                    }
                }
                #endregion
                var row = 5;
                var lstAllGroup = (new BCRMGroupsDL()).GetAllGroups();
                var lstSession = (new SessionDL()).GetByEventId(model.EventId);
                foreach (var inviteEvent in lstData)
                {
                    if (string.IsNullOrEmpty(inviteEvent.EventCode))
                    {
                        worksheet.Cells["A" + row].Value = string.Format("{0}", inviteEvent.InviteId);
                    }
                    else
                    {
                        worksheet.Cells["A" + row].Value = string.Format("{0}-{1}", inviteEvent.EventCode, inviteEvent.InviteId);
                    }
                    worksheet.Cells["B" + row].Value = inviteEvent.Code;
                    worksheet.Cells["C" + row].Value = inviteEvent.EventName;
                    worksheet.Cells["D" + row].Value = inviteEvent.CustomerName;
                    worksheet.Cells["E" + row].Value = inviteEvent.CustomerPhone;
                    worksheet.Cells["F" + row].Value = inviteEvent.CustomerEmail;
                    worksheet.Cells["G" + row].Value = ((TypeOfCustomer)inviteEvent.CustomerType).ToCustomerType();
                    worksheet.Cells["H" + row].Value = inviteEvent.PositionName;
                    worksheet.Cells["H" + row].Style.WrapText = true;
                    worksheet.Cells["I" + row].Value = inviteEvent.CompanyName;
                    worksheet.Cells["I" + row].Style.WrapText = true;
                    worksheet.Cells["J" + row].Value = inviteEvent.PlaceOfAttendEvent;
                    worksheet.Cells["J" + row].Style.WrapText = true;
                    worksheet.Cells["K" + row].Value = inviteEvent.UserName;
                    worksheet.Cells["K" + row].Style.WrapText = true;
                    worksheet.Cells["L" + row].Value = inviteEvent.IntroduceUser;
                    worksheet.Cells["M" + row].Value = GetLastStatus(inviteEvent);
                    worksheet.Cells["N" + row].Value = inviteEvent.Note;
                    worksheet.Cells["N" + row].Style.WrapText = true;
                    worksheet.Cells["O" + row].Value = inviteEvent.CreatedBy;
                    // Chi nhánh mời
                    var branch = CommonMethod.GetRootParent(lstAllGroup, inviteEvent.GroupId);
                    if (branch != null)
                    {
                        worksheet.Cells["P" + row].Value = branch.GroupName;
                    }
                    // Phòng ban và nhóm mời
                    var group = lstAllGroup.FirstOrDefault(m => m.GroupId == inviteEvent.GroupId);
                    if (group != null)
                    {
                        var department = lstAllGroup.FirstOrDefault(m => m.GroupId == group.ParentId);
                        worksheet.Cells["Q" + row].Value = department != null ? department.GroupName.Replace("-", "") : string.Empty;
                        worksheet.Cells["R" + row].Value = group.GroupName.Replace("-", "");
                    }
                    // Sale mời
                    worksheet.Cells["S" + row].Value = inviteEvent.BcrmAssignTo;
                    // Nguồn khách hàng
                    worksheet.Cells["T" + row].Value = inviteEvent.SourceId == 2 ? "Landingpage" : "BCRM";

                    if (!string.IsNullOrEmpty(inviteEvent.Sessions) && lstSession.Count > 0)
                    {
                        var lstSelected = inviteEvent.Sessions.Split(',').ToList();
                        inviteEvent.SessionName = string.Join(",", lstSession.FindAll(o => lstSelected.Contains(o.Id.ToString())).Select(o => o.Name).ToList());
                        worksheet.Cells["U" + row].Value = inviteEvent.SessionName;
                    }

                    if (lstSession.Count > 0)
                    {
                        var lstCheckedin = (new SessionCheckinDL()).GetByInviteEventId(inviteEvent.InviteId);
                        var strCheckinBy = new StringBuilder();
                        var strCheckinDate = new StringBuilder();
                        var strAccompanyMember = new StringBuilder();

                        foreach (var checkedIn in lstCheckedin)
                        {
                            var sessionInfo = lstSession.Find(o => o.Id == checkedIn.SessionId);
                            if (sessionInfo != null)
                            {
                                strCheckinDate.Append(sessionInfo.Name + " : " + checkedIn.CheckinDate.ToString("dd/MM/yyyy HH:mm:ss") + "\n");
                                strCheckinBy.Append(sessionInfo.Name + " : " + checkedIn.CheckinBy + "\n");
                                strAccompanyMember.Append(sessionInfo.Name + " : " + checkedIn.AccompanyMember + "\n");
                            }
                        }
                        worksheet.Cells["V" + row].Value = strCheckinDate.ToString();
                        worksheet.Cells["V" + row].Style.WrapText = true;
                        worksheet.Cells["W" + row].Value = strCheckinBy.ToString();
                        worksheet.Cells["W" + row].Style.WrapText = true;
                        worksheet.Cells["X" + row].Value = strAccompanyMember.ToString();
                        worksheet.Cells["X" + row].Style.WrapText = true;
                    }
                    else
                    {
                        worksheet.Cells["V" + row].Value = inviteEvent.CheckInDate == null ? string.Empty : inviteEvent.CheckInDate.Value.ToString("dd/MM/yyyy HH:mm:ss");
                        worksheet.Cells["W" + row].Value = inviteEvent.CheckInBy;
                        worksheet.Cells["X" + row].Value = inviteEvent.AccompanyMember;
                        worksheet.Row(row).Height = 35;
                    }
                    worksheet.Row(row).Height = 35;
                    row++;
                }
                #endregion
                var stream = new MemoryStream();
                package.SaveAs(stream);
                const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                stream.Position = 0;
                return File(stream, contentType, fileName);
            }
        }
        private string GetLastStatus(InviteEventModelWebCheckinToExport model)
        {
            if (model.PhoningStatus == null)
            {
                return "Chưa gọi";
            }
            else
            {
                if (model.PhoningStatus.Value == PhoningStatus.Confirm.GetHashCode())
                {
                    if (model.ConfirmAttend1st == null && model.ConfirmAttend2st == null)
                    {
                        return "Đã gọi";
                    }
                    else
                    {
                        if (model.ConfirmAttend2st != null)
                        {
                            if (model.Status == true)
                            {
                                if (model.SendTicketStatus == 2)
                                {
                                    return "Đã gửi lại SMS";
                                }
                                else
                                {
                                    return "Đã gửi SMS";
                                }
                            }
                            else if (model.ConfirmAttend2st.Value == AttendProbability.Yes.GetHashCode())
                            {
                                return "Xác nhận tham dự lần 2";
                            }
                            else if (model.ConfirmAttend2st.Value == AttendProbability.No.GetHashCode())
                            {
                                return "Không tham dự";
                            }
                            else
                            {
                                return ((AttendProbability)model.ConfirmAttend2st.Value).ToAttendProbabilityName();
                            }
                        }
                        else
                        {
                            if (model.ConfirmAttend1st.Value == AttendProbability.Yes.GetHashCode())
                            {
                                return "Xác nhận tham dự lần 1";
                            }
                            else if (model.ConfirmAttend1st.Value == AttendProbability.No.GetHashCode())
                            {
                                return "Không tham dự";
                            }
                            else
                            {
                                return ((AttendProbability)model.ConfirmAttend1st.Value).ToAttendProbabilityName();
                            }
                        }
                    }
                }
                else
                {
                    return ((PhoningStatus)model.PhoningStatus).ToPhoningStatusName();
                }
            }
        }
        /// <summary>
        /// Lưu lại lịch sử thay đổi
        /// </summary>
        /// <param name="oldData"></param>
        /// <param name="newData"></param>
        /// <returns></returns>
        public bool InsertEditInviteEventHistory(OnlineInviteeModel oldData, InviteEventWebCheckinModel newData)
        {
            //var isInserted = false;
            //var editHistory = new InviteEventHistoryModel();
            //editHistory.InviteEventId = newData.Id;
            //editHistory.Status = InviteeChangeHistory.Edit.GetHashCode();
            //editHistory.UpdatedBy = UserContext.UserName;
            //if (oldData.PlaceOfAttendEvent != newData.PlaceOfAttendEvent)
            //{
            //    isInserted = true;
            //    editHistory.NewPlaceOfAttendEvent = newData.PlaceOfAttendEvent;
            //}
            //var eventDL = new EventDL();
            //var lstOtherChangeItem = new List<OtherChangeItemModel>();
            //if (oldData.EventId != newData.EventId)
            //{
            //    lstOtherChangeItem.Add(new OtherChangeItemModel("Sự kiện", "EventId",
            //        eventDL.GetByIdInWebCheckIn(oldData.EventId).Name,
            //        eventDL.GetByIdInWebCheckIn(newData.EventId).Name
            //    ));
            //    isInserted = true;
            //}
            //if (!oldData.CustomerName.Equals(newData.CustomerName))
            //{
            //    lstOtherChangeItem.Add(new OtherChangeItemModel("Tên khách mời", "CustomerName", oldData.CustomerName, newData.CustomerName));
            //}
            //if (oldData.IntroduceUser != newData.IntroduceUser)
            //{
            //    lstOtherChangeItem.Add(new OtherChangeItemModel("Tên nhân viên mời", "IntroduceUser", oldData.IntroduceUser, newData.IntroduceUser));
            //}
            //if (!oldData.CustomerPhone.Equals(newData.CustomerPhone))
            //{
            //    lstOtherChangeItem.Add(new OtherChangeItemModel("Số điện thoại", "CustomerPhone", oldData.CustomerPhone, newData.CustomerPhone));
            //}
            //if (!oldData.CustomerEmail.Equals(newData.CustomerEmail))
            //{
            //    lstOtherChangeItem.Add(new OtherChangeItemModel("Email", "Email", oldData.CustomerEmail, newData.CustomerEmail));
            //}
            //if (oldData.CompanyName != newData.CompanyName)
            //{
            //    lstOtherChangeItem.Add(new OtherChangeItemModel("Tên công ty", "CompanyName", oldData.CompanyName, newData.CompanyName));
            //}
            //if (oldData.CustomerType != newData.CustomerType)
            //{
            //    lstOtherChangeItem.Add(new OtherChangeItemModel("Loại khách mời", "CustomerType", ((TypeOfCustomer)oldData.CustomerType).ToCustomerType(), ((TypeOfCustomer)newData.CustomerType).ToCustomerType()));
            //}
            //if (oldData.PositionName != newData.PositionName)
            //{
            //    lstOtherChangeItem.Add(new OtherChangeItemModel("Chức vụ", "PositionName", oldData.PositionName, newData.PositionName));
            //}
            //if (oldData.RegisteredSource != newData.RegisteredSource)
            //{
            //    lstOtherChangeItem.Add(new OtherChangeItemModel("Nguồn đăng kí", "RegisteredSource", ((RegisteredSource)oldData.RegisteredSource).ToRegisteredSourceName(), ((RegisteredSource)newData.RegisteredSource).ToRegisteredSourceName()));
            //}

            //if (oldData.Note != newData.Note)
            //{
            //    lstOtherChangeItem.Add(new OtherChangeItemModel("Ghi chú", "Note", string.IsNullOrEmpty(oldData.Note) ? "" : oldData.Note, string.IsNullOrEmpty(newData.Note) ? "" : newData.Note));
            //}

            //if (lstOtherChangeItem.Count > 0)
            //{
            //    editHistory.OtherChange = JsonConvert.SerializeObject(lstOtherChangeItem);
            //    isInserted = true;
            //}

            //var inviteEventHistoryDL = new InviteEventHistoryDL();
            //if (isInserted)
            //{
            //    inviteEventHistoryDL.Insert(editHistory);
            //}

            return true;
        }
        
        #endregion
        
    }
}
