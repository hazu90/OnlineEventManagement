﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BCRM.CheckInEvent.DAL;
using BCRM.CheckInEvent.Models;
using Libs;
using BCRM.CheckInEvent.Common;

namespace BCRM.CheckInEvent.Controllers
{
    public class CheckInTimerController : BaseController
    {
        //
        // GET: /CheckInTimer/
        [FilterAuthorize]
        public ActionResult Index()
        {
            return View();
        }
        [FilterAuthorize]
        public ActionResult Search(int statusEvent, int eventId, bool isInWebCheckin)
        {
            var timeNow = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0);
            var eventDL = new EventDL();
            var seriesEventIds = "";
            // get all events
            var lstEventBCRMs = eventDL.GetAll().FindAll(o => o.IsEnable == true);
            foreach (var item in lstEventBCRMs)
            {
                item.IsInWebCheckIn = false;
            }
            var lstEventWebCheckin = eventDL.GetAllInWebCheckIn().FindAll(o => o.IsEnable == true);
            foreach (var item in lstEventWebCheckin)
            {
                item.IsInWebCheckIn = true;
            }
            // statusEvent = 0 Events is not occuring
            // statusEvent = 1 Events is occuring
            var lstSearchedEvents = new List<EventModel>();

            // Lấy tất cả danh sách gia hạn check in
            var checkInTimerDL = new CheckInTimerDL();
            var lstAllCheckInTimer = checkInTimerDL.GetAll();
            var lstCheckInTimer = new List<CheckInTimerModel>();
            if(eventId ==0)
            {
                if (statusEvent == 0)
                {
                    lstSearchedEvents.AddRange(lstEventBCRMs.FindAll(o => o.EndDate >= timeNow));
                    lstSearchedEvents.AddRange(lstEventWebCheckin.FindAll(o => o.EndDate >= timeNow));
                }
                else
                {
                    lstSearchedEvents.AddRange(lstEventBCRMs.FindAll(o => o.EndDate < timeNow));
                    lstSearchedEvents.AddRange(lstEventWebCheckin.FindAll(o => o.EndDate < timeNow));
                }
                lstCheckInTimer = lstAllCheckInTimer.FindAll(o => lstSearchedEvents.Find(i => i.IsInWebCheckIn == o.IsInWebCheckIn && i.Id == o.EventId ) != null);

                // get list id events
                //seriesEventIds = string.Join(",", lstSearchedEvents.Select(o => o.Id).ToArray()); 
            }
            else
            {
                if (statusEvent == 0)
                {
                    if (isInWebCheckin)
                    {
                        lstSearchedEvents.AddRange(lstEventWebCheckin.FindAll(o => o.EndDate >= timeNow && o.Id == eventId));
                    }
                    else
                    {
                        lstSearchedEvents.AddRange(lstEventBCRMs.FindAll(o => o.EndDate >= timeNow && o.Id == eventId));
                    }
                }
                else
                {
                    if (isInWebCheckin)
                    {
                        lstSearchedEvents = lstEventWebCheckin.FindAll(o => o.EndDate < timeNow && o.Id == eventId );
                    }
                    else
                    {
                        lstSearchedEvents = lstEventBCRMs.FindAll(o => o.EndDate < timeNow && o.Id == eventId );
                    }
                    
                }

                lstCheckInTimer = lstAllCheckInTimer.FindAll(o => lstSearchedEvents.Find(i => i.IsInWebCheckIn == o.IsInWebCheckIn && i.Id == o.EventId) != null);
                
            }
            // find all check in timer with 
            
            //var lstCheckInTimer = checkInTimerDL.GetList(seriesEventIds);
            //assign EventName for each checkin timer
            foreach (var timer in lstCheckInTimer)
            {
                var eventItem = lstSearchedEvents.Find(o => o.Id == timer.EventId);
                if (eventItem != null)
                {
                    timer.EventName = eventItem.Name;
                }
            }
            return View(lstCheckInTimer);
        }
        [FilterAuthorize]
        public ActionResult AddNew()
        {
            var timeNow = DateTime.Now;
            var eventDL = new EventDL();
            // get all events
            var lstAllEvents = CommonMethod.GetEvents();
            // Lấy ra sự kiện được set mặc định
            var defaultEventInfo = CommonMethod.GetDefaultEvent();
            // TH chưa có thông tin về sự kiện được đặt mặc định
            // Lấy ra sự kiện mới nhất
            if (defaultEventInfo == null)
            {
                defaultEventInfo = new EventModel();
                defaultEventInfo.Id = 0;
            }
            ViewBag.DefaultEvent = defaultEventInfo;
            ViewBag.Events = lstAllEvents;
            return View();
        }
        [FilterAuthorize]
        public JsonResult Insert(CheckInTimerModel model)
        {
            var response = new Response();
            if (UserContext.HasPermisson(Permission.Manager) || UserContext.HasPermisson(Permission.Admin))
            {
                if (model.StartTime >= model.EndTime)
                {
                    response.Code = SystemCode.NotValid;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                var checkInTimerDL = new CheckInTimerDL();
                model.CreatedBy = UserContext.UserName;
                if (!checkInTimerDL.IsExist(model.EventId,model.IsInWebCheckIn))
                {
                    checkInTimerDL.Insert(model);
                }
                else
                {
                    response.Code = SystemCode.ErrorExist;
                }
            }
            else
            {
                response.Code = SystemCode.NotPermitted;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public ActionResult EditAction(int id)
        {
            var eventDL = new EventDL();
            var checkInTimerDL = new CheckInTimerDL();
            var model = checkInTimerDL.GetById(id);
            ViewBag.Events = CommonMethod.GetEvents();
            return View(model);
        }
        [FilterAuthorize]
        public JsonResult Edit(CheckInTimerModel model)
        {
            var response = new Response();
            if (UserContext.HasPermisson(Permission.Manager) || UserContext.HasPermisson(Permission.Admin))
            {
                if(model.StartTime >= model.EndTime)
                {
                    response.Code = SystemCode.NotValid;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                
                model.UpdatedBy = UserContext.UserName;
                var checkInTimerDL = new CheckInTimerDL();

                var existCheckInTimer = checkInTimerDL.GetList(model.EventId.ToString(),model.IsInWebCheckIn);
                if (existCheckInTimer.Count>0)
                {
                    if (existCheckInTimer.Find(o=>o.Id != model.Id)!=null)
                    {
                        response.Code = SystemCode.ErrorExist;
                        return Json(response, JsonRequestBehavior.AllowGet);
                    }
                }
                checkInTimerDL.Update(model);
                response.Code = SystemCode.Success;
            }
            else
            {
                response.Code = SystemCode.NotPermitted;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult Delete(int id)
        {
            var response = new Response();
            if (UserContext.HasPermisson(Permission.Manager) || UserContext.HasPermisson(Permission.Admin))
            {
                var checkInTimerDL = new CheckInTimerDL();
                checkInTimerDL.Delete(id);
                response.Code = SystemCode.Success;
            }
            else
            {
                response.Code = SystemCode.NotPermitted;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult GetEvents(int selectStatus)
        {
            var timeNow = new DateTime(DateTime.Now.Year,DateTime.Now.Month,DateTime.Now.Day,0,0,0);
            var response = new Response();
            var eventDL = new EventDL();
            var lstAllEvents = new List<EventModel>();
            var lstEventBCRMs = new List<EventModel>();
            var lstEventWebCheckin = new List<EventModel>();
            if (selectStatus == 0)
            {
                lstEventBCRMs = eventDL.GetAll().FindAll(o => o.IsEnable == true && o.EndDate >= timeNow);
                lstEventWebCheckin = eventDL.GetAllInWebCheckIn().FindAll(o => o.IsEnable == true && o.EndDate >= timeNow);
            }
            else
            {
                lstEventBCRMs = (new EventDL()).GetAll().FindAll(o => o.IsEnable == true && o.EndDate < timeNow);
                lstEventWebCheckin = eventDL.GetAllInWebCheckIn().FindAll(o => o.IsEnable == true && o.EndDate < timeNow);
            }

            foreach (var item in lstEventBCRMs)
            {
                item.IsInWebCheckIn = false;
            }
            foreach (var item in lstEventWebCheckin)
            {
                item.IsInWebCheckIn = true;
            }

            lstAllEvents.AddRange(lstEventBCRMs);
            lstAllEvents.AddRange(lstEventWebCheckin);
            lstAllEvents = lstAllEvents.OrderByDescending(o => o.StartDate).ToList();

            response.Code = SystemCode.Success;
            response.Data = lstAllEvents;
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
