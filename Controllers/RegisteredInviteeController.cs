﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BCRM.CheckInEvent.Models;
using BCRM.CheckInEvent.Common;
using BCRM.CheckInEvent.DAL;
using Libs;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using BCRM.CheckInEvent.SendSMSGAPIT;
using System.Configuration;
using Newtonsoft.Json;
using System.Data;
using System.Text;

namespace BCRM.CheckInEvent.Controllers
{
    public class RegisteredInviteeController : BaseController
    {
        #region methods
        [FilterAuthorize]
        public ActionResult Index()
        {
            // authentication
            if (!UserContext.HasPermisson(Permission.Admin) 
                && !UserContext.HasPermisson(Permission.InviteeManager)
                && !UserContext.HasPermisson(Permission.Manager))
            {
                return View("~/Views/Common/NotPermitted.cshtml");
            }
            var defaultEvent = CommonMethod.GetDefaultEvent();
            ViewBag.CurrentUser = UserContext;
            ViewBag.DefaultEvent = defaultEvent;
            var userDL = new UserDL();
            ViewBag.LstAssignTo = userDL.GetByRole(Permission.InviteeManager.GetHashCode()).OrderBy(o=>o.UserName).ToList();
            // Lấy danh sách các sự kiện
            var inviteInventDL = new InviteEventDL();
            var eventDL = new EventDL();
            var lstAllEvents = eventDL.GetAllInWebCheckIn().FindAll(o => o.IsEnable == true).OrderByDescending(o => o.CreatedDate).ToList();
            foreach (var eventInfo in lstAllEvents)
            {
                eventInfo.IsInWebCheckIn = true;
            }
            return View(lstAllEvents);
        }
        [FilterAuthorize]
        public ActionResult Create()
        {
            // authentication
            if (!UserContext.HasPermisson(Permission.Admin) && !UserContext.HasPermisson(Permission.InviteeManager))
            {
                return View("~/Views/Common/NotPermitted.cshtml");
            }
            var eventDL = new EventDL();
            var defaultEvent = CommonMethod.GetDefaultEvent();
            if (defaultEvent == null)
            {
                defaultEvent = new EventModel()
                {
                    Id = 0
                };
            }
            var lstAllEventWebCheckin = eventDL.GetAllInWebCheckIn().FindAll(o => o.IsEnable == true).OrderByDescending(o => o.CreatedDate).ToList();
            if (defaultEvent.IsInWebCheckIn)
            {
                if (lstAllEventWebCheckin.Find(o => o.Id == defaultEvent.Id) != null)
                {
                    ViewBag.DefaultEvent = defaultEvent;
                }
                else
                {
                    ViewBag.DefaultEvent = new EventModel()
                    {
                        Id = 0,
                        IsInWebCheckIn = true
                    };
                }

            }
            ViewBag.LstEvent = lstAllEventWebCheckin.OrderByDescending(o => o.CreatedDate).ToList();

            var onlineInviteeForCreate = new OnlineInviteeForCreateModel();
            
            ViewBag.CurrentUser = UserContext;

            return View(onlineInviteeForCreate);
        }
        [FilterAuthorize]
        public JsonResult CreateAction(OnlineInviteeModel model)
        {
            var response = new Response();
            var inviteEventDL = new InviteEventDL();
            #region Kiểm tra quyền và validate dữ liệu đầu vào
            if (!UserContext.HasPermisson(Permission.InviteeManager) && !UserContext.HasPermisson(Permission.Admin))
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            //validate thông tin
            // Tên khách hàng bắt buộc nhập
            // Số điện thoại bắt buộc nhập
            // Email bắt buộc nhập
            if (string.IsNullOrEmpty(model.CustomerName) || string.IsNullOrEmpty(model.CustomerPhone) || string.IsNullOrEmpty(model.CustomerEmail))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Dữ liệu không hợp lệ";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra số điện thoại
            
            if (inviteEventDL.WebCheckIn_CheckPhoneExist(0, model.EventId, model.CustomerPhone))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Số điện thoại này đã được sử dụng";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra địa chỉ email
            if (inviteEventDL.WebCheckIn_CheckEmailExist(0, model.EventId, model.CustomerEmail))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Email này đã được sử dụng !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            var msg = "";
            if (!ValidateConfirmAttend(model, ref msg))
            {
                response.Code = SystemCode.NotValid;
                response.Message = msg;
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            #endregion
            // Lấy thông tin sự kiện
            var eventInfo = (new EventDL()).GetByIdInWebCheckIn(model.EventId);
            // Sinh mã vé cho khách mời ,Lặp tối đa 100 lần nếu như đã tồn tại mã vé này rồi
            for (var index = 0; index < 100; index++)
            {
                model.Code = string.Format("{0}{1}", eventInfo.EventCode, CommonMethod.GenerateCode());
                // Kiểm tra xem mã vé này đã tồn tại chưa
                if (!inviteEventDL.WebCheckin_IsExistCode(eventInfo.Id, model.Code))
                {
                    break;
                }
            }
            // Lấy thông tin người tạo
            model.CreatedBy = UserContext.UserName;
            // Lấy thông tin ngày đăng kí
            model.RegisteredDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            // Kiểm tra phiên tham dự của khách mời có chọn tất cả không ,
            // nếu có thì loại bỏ các phiên riêng lẻ được chọn khác
            if (!string.IsNullOrEmpty(model.Sessions))
            {
                var lstSessions = model.Sessions.Split(',').ToList();
                if (lstSessions.Contains("0"))
                {
                    model.Sessions = "0";
                }
            }
            // Lưu thông tin khách mời được tạo mới
            var id = inviteEventDL.InsertOnlineInvitee(model);
            // Lưu lại lịch sử thêm mới khách mời
            var inviteEventHistoryDL = new InviteEventHistoryDL();
            inviteEventHistoryDL.Insert(new InviteEventHistoryModel()
            {
                Action= "Tạo mới",
                OldValue = "",
                NewValue ="",
                UpdatedBy = UserContext.UserName,
                InviteEventId = id
            });

            response.Code = SystemCode.Success;
            response.Message = "Bạn đã thêm thông tin thành công !";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public ActionResult Search(FilterForOnlineRegisterSearchModel model)
        {
            if (!UserContext.HasPermisson(Permission.Admin) && 
                !UserContext.HasPermisson(Permission.InviteeManager) &&
                !UserContext.HasPermisson(Permission.Manager))
            {
                return View("~/Views/Common/NotPermitted.cshtml");
            }
            if (!string.IsNullOrEmpty(model.TextSearch))
            {
                model.TextSearch = model.TextSearch.Trim();
            }
            else
            {
                model.TextSearch = "";
            }
            if (string.IsNullOrEmpty(model.AssignTo))
            {
                model.AssignTo = "";
            }
            if (!string.IsNullOrEmpty(model.IntroduceUser))
            {
                model.IntroduceUser = model.IntroduceUser.Trim();
            }
            else
            {
                model.IntroduceUser = "";
            }

            if (model.StartDate == null || model.StartDate == DateTime.MinValue)
            {
                model.StartDate = new DateTime(2000, 1, 1, 0, 0, 0);
            }
            if (model.EndDate == null || model.EndDate == DateTime.MinValue)
            {
                model.EndDate = new DateTime(9999, 1, 1, 0, 0, 0);
            }
            else
            {
                model.EndDate = model.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59);
            }

            var inviteEventDL = new InviteEventDL();
            var result = new FilterForOnlineRegisterSearchModel();
            var lstSearchResult = new List<OnlineInviteeForSearchModel>();
            // Tìm kiếm theo mã số khách mời
            if (model.InviteeCode > 0)
            {
                var info = inviteEventDL.WebCheckIn_GetRegisteredInviteeById(model.InviteeCode);
                if (info != null)
                {
                    lstSearchResult.Add(info);
                }
            }
            // Tìm kiếm theo mã vé
            else if (!string.IsNullOrEmpty(model.Code))
            {
                var iCode = 0;
                if (int.TryParse(model.Code, out iCode))
                {
                    var eventDL = new EventDL();
                    var eventInfo = eventDL.GetByIdInWebCheckIn(model.EventId);
                    if (eventInfo != null)
                    {
                        model.Code = eventInfo.EventCode + model.Code;
                    }
                }
                var inviteInfo = inviteEventDL.WebCheckin_GetByCode(model.EventId, model.Code);
                if (inviteInfo != null)
                {
                    lstSearchResult.Add(inviteInfo);
                }
            }
            // Tìm kiếm theo các điều kiện tìm kiếm còn lại
            // Họ tên, SĐT, email
            // Loại khách
            // Trạng thái
            else
            {
                // Tìm kiếm 
                // Chưa giao cho ai
                if (model.FilterAssignTo == 1)
                {
                    model.IsAssignTo = false;
                    model.IsBcrmAssignTo = false;
                }
                // Chưa giao cho Sale
                else if (model.FilterAssignTo == 2)
                {
                    model.IsAssignTo = null;
                    model.IsBcrmAssignTo = false;
                }
                // Chưa giao cho CSKH
                else if (model.FilterAssignTo == 3)
                {
                    model.IsAssignTo = false;
                    model.IsBcrmAssignTo = null;
                }
                // Đã giao cho Sale
                else if (model.FilterAssignTo == 4)
                {
                    model.IsAssignTo = null;
                    model.IsBcrmAssignTo = true;
                }
                // Đã giao cho CSKH
                else if (model.FilterAssignTo == 5)
                {
                    model.IsAssignTo = true;
                    model.IsBcrmAssignTo = null;
                }
                else
                {
                    model.IsAssignTo = null;
                    model.IsBcrmAssignTo = null;
                }

                if (model.InviteeResponseStatus == 0)
                {
                    model.PhoningStatus = 0;
                    model.ConfirmAttend1st = 0;
                    model.ConfirmAttend2st = 0;
                    model.Status = null;
                    model.SendTicketStatus = 0;
                }
                else
                {
                    switch (model.InviteeResponseStatus)
                    {
                        // Trường hợp lấy danh sách khách mời xác nhận lần 1
                        case 1:
                            model.PhoningStatus = 0;
                            model.ConfirmAttend1st = AttendProbability.Yes.GetHashCode();
                            model.ConfirmAttend2st = 0;
                            model.Status = false;
                            model.SendTicketStatus = 0;
                            break;
                        // Trường hợp lấy danh sách khách mời xác nhận lần 2
                        case 2:
                            model.PhoningStatus = 0;
                            model.ConfirmAttend1st = 0;
                            model.ConfirmAttend2st = AttendProbability.Yes.GetHashCode();
                            model.Status = false;
                            model.SendTicketStatus = 0;
                            break;
                        // Chưa gọi
                        case 3:
                            model.PhoningStatus = -1;
                            model.ConfirmAttend1st = 0;
                            model.ConfirmAttend2st = 0;
                            model.Status = false;
                            model.SendTicketStatus = 0;
                            break;
                        // Gọi lại sau
                        case 4:
                            model.PhoningStatus = PhoningStatus.TryLater.GetHashCode();
                            model.ConfirmAttend1st = 0;
                            model.ConfirmAttend2st = 0;
                            model.Status = false;
                            model.SendTicketStatus = 0;
                            break;
                        // Không nghe máy
                        case 5:
                            model.PhoningStatus = PhoningStatus.NoListen.GetHashCode();
                            model.ConfirmAttend1st = 0;
                            model.ConfirmAttend2st = 0;
                            model.Status = false;
                            model.SendTicketStatus = 0;
                            break;
                        // Đã gọi
                        case 6:
                            model.PhoningStatus = PhoningStatus.Confirm.GetHashCode();
                            model.ConfirmAttend1st = 0;
                            model.ConfirmAttend2st = 0;
                            model.Status = false;
                            model.SendTicketStatus = 0;
                            break;
                        // Số máy sai
                        case 7:
                            model.PhoningStatus = PhoningStatus.WrongNumber.GetHashCode();
                            model.ConfirmAttend1st = 0;
                            model.ConfirmAttend2st = 0;
                            model.Status = false;
                            model.SendTicketStatus = 0;
                            break;
                        // Đang xem xét
                        case 8:
                            model.PhoningStatus = 0;
                            model.ConfirmAttend1st = AttendProbability.Considering.GetHashCode();
                            model.ConfirmAttend2st = AttendProbability.Considering.GetHashCode();
                            model.Status = false;
                            model.SendTicketStatus = 0;
                            break;
                        // Không tham gia
                        case 9:
                            model.PhoningStatus = 0;
                            model.ConfirmAttend1st = AttendProbability.No.GetHashCode();
                            model.ConfirmAttend2st = AttendProbability.No.GetHashCode();
                            model.Status = false;
                            model.SendTicketStatus = 0;
                            break;
                        // Đã gửi vé
                        case 10:
                            model.PhoningStatus = 0;
                            model.ConfirmAttend1st = 0;
                            model.ConfirmAttend2st = 0;
                            model.Status = true;
                            model.SendTicketStatus = 1;
                            break;
                        // Gửi lại vé
                        case 11:
                            model.PhoningStatus = 0;
                            model.ConfirmAttend1st = 0;
                            model.ConfirmAttend2st = 0;
                            model.Status = true;
                            model.SendTicketStatus = 2;
                            break;
                        // Không mời
                        case 12:
                            model.PhoningStatus = PhoningStatus.Ignore.GetHashCode();
                            model.ConfirmAttend1st = 0;
                            model.ConfirmAttend2st = 0;
                            model.Status = false;
                            model.SendTicketStatus = 0;
                            break;
                        // Trưởng phòng duyệt
                        case 13:
                            model.PhoningStatus = PhoningStatus.LeaderAppoved.GetHashCode();
                            model.ConfirmAttend1st = 0;
                            model.ConfirmAttend2st = 0;
                            model.Status = false;
                            model.SendTicketStatus = 0;
                            break;
                    }
                }

                lstSearchResult = inviteEventDL.SearchByCondition(model);

                var strId = string.Join(",", lstSearchResult.Select(o => o.Id).ToList());

                // Tìm kiếm theo nhân viên mời
                if (!string.IsNullOrEmpty(model.IntroduceUser))
                {
                    var convertUnsign = CommonMethod.ConvertToUnSign(model.IntroduceUser.ToLower());
                    var lstExactSearchCondition = lstSearchResult.FindAll(o => !string.IsNullOrEmpty(o.IntroduceUser) && o.IntroduceUser.Contains(model.IntroduceUser));
                    var lstRelativeSearchCondition = lstSearchResult.FindAll(o => !string.IsNullOrEmpty(o.IntroduceUser)
                                                                                && !o.IntroduceUser.Contains(model.IntroduceUser)
                                                                                && CommonMethod.ConvertToUnSign(o.IntroduceUser.ToLower()).Contains(convertUnsign));
                    var lstAllInvitee = new List<OnlineInviteeForSearchModel>();
                    lstAllInvitee.AddRange(lstExactSearchCondition);
                    lstAllInvitee.AddRange(lstRelativeSearchCondition);
                    lstSearchResult = lstAllInvitee;
                }
                // Tìm kiếm theo filter text
                if (!string.IsNullOrEmpty(model.TextSearch))
                {
                    var convertUnsign = CommonMethod.ConvertToUnSign(model.TextSearch.ToLower());
                    // Tìm chính xác theo thông tin tìm kiếm
                    var exactMatchInvitee = lstSearchResult.FindAll(o => o.CustomerName.ToLower().Contains(model.TextSearch.ToLower())
                                                                      || o.CustomerPhone.ToLower().Contains(model.TextSearch.ToLower()));
                    // Tìm gần đúng theo thông tin tìm kiếm
                    var opportunityInvitee = lstSearchResult.FindAll(o =>
                                                            !o.CustomerName.ToLower().Contains(model.TextSearch.ToLower())
                                                            && !o.CustomerPhone.ToLower().Contains(model.TextSearch.ToLower())
                                                            && (CommonMethod.ConvertToUnSign(o.CustomerName.ToLower()).Contains(convertUnsign) || o.CustomerEmail.Contains(convertUnsign))
                                                            );

                    // Tìm theo ghi chú của thông tin tìm kiếm
                    var remainInvitee = lstSearchResult.Except(exactMatchInvitee).Except(opportunityInvitee).ToList();
                    var lstAllInvitee = new List<OnlineInviteeForSearchModel>();
                    lstAllInvitee.AddRange(exactMatchInvitee);
                    lstAllInvitee.AddRange(opportunityInvitee);
                    lstAllInvitee.AddRange(remainInvitee);
                    lstSearchResult = lstAllInvitee;
                }
                else if (string.IsNullOrEmpty(model.IntroduceUser))
                {
                    lstSearchResult = lstSearchResult.OrderByDescending(o => o.CreatedDate).ToList();
                }
            }

            var totalRecord = lstSearchResult.Count;
            lstSearchResult = lstSearchResult.Skip(model.PageSize * (model.PageIndex - 1))
                                                        .Take(model.PageSize).ToList();

            foreach (var item in lstSearchResult)
            {
                if (item.IsBcrmProcess == 1)
                {
                    item.PositionName = ((CustomerPosition)item.CustomerPosition).ToCustomerPositionName();
                }
            }

            var lstSession = (new SessionDL()).GetByEventId(model.EventId);
            if (lstSession.Count > 0)
            {
                foreach (var item in lstSearchResult)
                {
                    if (!string.IsNullOrEmpty(item.Sessions))
                    {
                        var lstSelected = item.Sessions.Split(',').ToList();
                        if (lstSelected.Contains("0"))
                        {
                            item.SessionName = "Tất cả các phiên";
                        }
                        else
                        {
                            item.SessionName = string.Join(",", lstSession.FindAll(o => lstSelected.Contains(o.Id.ToString())).Select(o => o.Name).ToList());
                        }
                    }
                }
            }

            result.LstInvitees = lstSearchResult;
            result.PageIndex = model.PageIndex;
            result.PageSize = model.PageSize;
            result.TotalRecord = totalRecord;
            var pager = new PagerModel { CurrentPage = result.PageIndex, PageSize = result.PageSize, TotalItem = result.TotalRecord };
            ViewBag.Pager = pager;
            ViewBag.CurrentUser = UserContext;
            return View(lstSearchResult);
        }
        [FilterAuthorize]
        public ActionResult Edit(int id)
        {
            // authentication
            if (!UserContext.HasPermisson(Permission.Admin) 
                && !UserContext.HasPermisson(Permission.InviteeManager)
                && !UserContext.HasPermisson(Permission.Manager) )
            {
                return View("~/Views/Common/NotPermitted.cshtml");
            }
            
            var eventDL = new EventDL();
            var lstAllEventWebCheckin = eventDL.GetAllInWebCheckIn().FindAll(o => o.IsEnable == true);
            var inviteeInfo = new OnlineInviteeForCreateModel();
            // Lấy danh sách các sự kiện đang hoạt động
            inviteeInfo.LstEvent = lstAllEventWebCheckin.OrderByDescending(o => o.CreatedDate).ToList();
            // Lấy thông tin khách mời
            var inviteEventDL = new InviteEventDL();
            inviteeInfo.OnlineRegisterInfo = inviteEventDL.GetOnlineRegisteredInviteeById(id);
            // Lấy thông tin các phiên của sự kiện của khách mời
            inviteeInfo.LstSession = (new SessionDL()).GetByEventId(inviteeInfo.OnlineRegisterInfo.EventId);
            // Các phiên khách mời đã chọn
            if (!string.IsNullOrEmpty(inviteeInfo.OnlineRegisterInfo.Sessions))
            {
                inviteeInfo.LstSelectedSession = new List<int>();
                var arrSession = inviteeInfo.OnlineRegisterInfo.Sessions.Split(',');
                foreach (var sessionItem in arrSession)
                {
                    var outSessionId = 0;
                    var cvt = sessionItem.Trim();
                    if (!string.IsNullOrEmpty(cvt))
                    {
                        if (int.TryParse(cvt, out outSessionId))
                        {
                            inviteeInfo.LstSelectedSession.Add(outSessionId);
                        }
                    }
                }
                //inviteeInfo.LstSelectedSession = inviteeInfo.OnlineRegisterInfo.Sessions.Split(',').Select(o => int.Parse(o)).ToList();
            }
            else
            {
                inviteeInfo.LstSelectedSession = new List<int>();
            }
            // Lấy lịch sử thay đổi thông tin khách mời
            var inviteEventHistoryDL = new InviteEventHistoryDL();
            inviteeInfo.LstInviteEventHistory = inviteEventHistoryDL.GetByInviteEventId(id);
            ViewBag.CurrentUser = UserContext;
            return View(inviteeInfo);
        }
        [FilterAuthorize]
        public JsonResult EditAction(OnlineInviteeModel model)
        {
            var response = new Response();
            if (!UserContext.HasPermisson(Permission.InviteeManager) 
                && !UserContext.HasPermisson(Permission.Admin)
                && !UserContext.HasPermisson(Permission.Manager) )
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            //validate thông tin
            // Tên khách hàng bắt buộc nhập
            if (string.IsNullOrEmpty(model.CustomerName) || string.IsNullOrEmpty(model.CustomerPhone) || string.IsNullOrEmpty(model.CustomerEmail))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Dữ liệu không hợp lệ";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            model.CustomerName = model.CustomerName.Trim();
            model.CustomerPhone = model.CustomerPhone.Trim();
            model.CustomerEmail = model.CustomerEmail.Trim();
            // Kiểm tra số điện thoại
            var inviteEventDL = new InviteEventDL();
            if (inviteEventDL.WebCheckIn_CheckPhoneExist(model.Id, model.EventId, model.CustomerPhone))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Số điện thoại này đã được sử dụng";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra địa chỉ email
            if (inviteEventDL.WebCheckIn_CheckEmailExist(model.Id, model.EventId, model.CustomerEmail))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Email này đã được sử dụng !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            var onlineRegisterInfo = inviteEventDL.GetOnlineRegisteredInviteeById(model.Id);
            var msg = "";
            if (!ValidateConfirmAttend(model, ref msg))
            {
                response.Code = SystemCode.NotValid;
                response.Message = msg;
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Thêm lịch sử lưu thông tin khách mời
            InsertEditInviteEventHistory(onlineRegisterInfo, model);

            if (!string.IsNullOrEmpty(model.Sessions))
            {
                var lstSessions = model.Sessions.Split(',').ToList();
                if (lstSessions.Contains("0"))
                {
                    model.Sessions = "0";
                }
            }
            inviteEventDL.UpdateOnlineInvitee(model);
            // Kiểm tra 
            response.Code = SystemCode.Success;
            response.Message = "Bạn đã sửa thông tin thành công !";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public ActionResult BCRMEdit(int id)
        {
            var inviteEventDL = new InviteEventDL();
            var model = new BCRMInviteeEditModel();
            model.BCRMInviteeInfo = inviteEventDL.WebCheckIn_GetById(id);
            var customerInfo = (new BCRMCustomerDL()).GetById(model.BCRMInviteeInfo.CustomerId);
            model.LstPhoneNumber = Ultility.DetectPhoneNumber(customerInfo.PhoneNumber);
            var eventDL = new EventDL();
            model.LstEvent = eventDL.GetAllInWebCheckIn().FindAll(o => o.IsEnable == true);

            model.LstSession = (new SessionDL()).GetByEventId(model.BCRMInviteeInfo.EventId);

            if (model.LstSession.Count > 0 && !string.IsNullOrEmpty(model.BCRMInviteeInfo.Sessions))
            {
                model.LstSelectedSession = new List<int>();
                var arrSession = model.BCRMInviteeInfo.Sessions.Split(',');
                foreach (var sessionItem in arrSession)
                {
                    var outSessionId = 0;
                    var cvt = sessionItem.Trim();
                    if (!string.IsNullOrEmpty(cvt))
                    {
                        if (int.TryParse(cvt, out outSessionId))
                        {
                            model.LstSelectedSession.Add(outSessionId);
                        }
                    }
                }
                //model.LstSelectedSession = model.BCRMInviteeInfo.Sessions.Split(',').Select(o => int.Parse(o)).ToList();
            }
            else
            {
                model.LstSelectedSession = new List<int>();
            }
            // Lấy lịch sử thay đổi thông tin khách mời
            var inviteEventHistoryDL = new InviteEventHistoryDL();
            model.LstInviteEventHistory = inviteEventHistoryDL.GetByInviteEventId(id);
            return View(model);
        }
        [FilterAuthorize]
        public JsonResult BCRMEditAction(BCRMInviteeEditRequestModel model)
        {
            var inviteEventDL = new InviteEventDL();
            var inviteeInfo = inviteEventDL.WebCheckIn_GetById(model.Id);
            var response = new Response();
            if (!UserContext.HasPermisson(Permission.InviteeManager)
                && !UserContext.HasPermisson(Permission.Admin)
                && !UserContext.HasPermisson(Permission.Manager))
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            //validate thông tin
            // Tên khách hàng bắt buộc nhập
            if (string.IsNullOrEmpty(model.CustomerName) || string.IsNullOrEmpty(model.CustomerPhone) || string.IsNullOrEmpty(model.CustomerEmail))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Dữ liệu không hợp lệ";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            model.CustomerName = model.CustomerName.Trim();
            model.CustomerPhone = model.CustomerPhone.Trim();
            model.CustomerEmail = model.CustomerEmail.Trim();
            // Kiểm tra số điện thoại
            if (inviteEventDL.WebCheckIn_CheckPhoneExist(model.Id, inviteeInfo.EventId, model.CustomerPhone))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Số điện thoại này đã được sử dụng";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra địa chỉ email
            if (inviteEventDL.WebCheckIn_CheckEmailExist(model.Id, inviteeInfo.EventId, model.CustomerEmail))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Email này đã được sử dụng !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            // Cập nhật lại thông tin thay đổi
            inviteEventDL.WebCheckIn_UpdateBCRMInvitee(model);
            // Lưu lịch sử thay đổi thông tin khách mời
            #region Lưu lịch sử thông tin thay đổi khách mời
            var lstChangeHistory = new List<InviteEventHistoryModel>();
            if (inviteeInfo.CustomerPosition.GetHashCode() != model.CustomerPosition)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    Action = "Chức vụ",
                    InviteEventId = model.Id,
                    OldValue = inviteeInfo.CustomerPosition.ToCustomerPositionCode(),
                    NewValue = ((CustomerPosition)model.CustomerPosition).ToCustomerPositionCode(),
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now
                });
            }
            if (inviteeInfo.CustomerName != model.CustomerName)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    Action = "Tên khách mời",
                    InviteEventId = model.Id,
                    OldValue = inviteeInfo.CustomerName,
                    NewValue = model.CustomerName,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now
                });
            }
            if (inviteeInfo.CustomerPhone != model.CustomerPhone)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    Action = "Số điện thoại",
                    InviteEventId = model.Id,
                    OldValue = inviteeInfo.CustomerPhone,
                    NewValue = model.CustomerPhone,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now
                });
            }

            var lstSession = (new SessionDL()).GetByEventId(inviteeInfo.EventId);
            if (lstSession.Count > 0)
            {
                if (string.IsNullOrEmpty(inviteeInfo.Sessions))
                {
                    inviteeInfo.Sessions = "";
                }

                if (inviteeInfo.Sessions != model.Sessions)
                {
                    var strOld = "";
                    if (!string.IsNullOrEmpty(inviteeInfo.Sessions))
                    {
                        var lstOldSelected = inviteeInfo.Sessions.Split(',').ToList();
                        if (lstOldSelected.Count > 0)
                        {
                            if (lstOldSelected.Contains("0"))
                            {
                                strOld = "Tất cả các phiên";
                            }
                            else
                            {
                                //var outSessionId = 0;
                                var outSessionId = 0;
                                foreach (var oldSelected in lstOldSelected)
                                {
                                    if (int.TryParse(oldSelected.Trim(), out outSessionId))
                                    {
                                        if (string.IsNullOrEmpty(strOld))
                                        {
                                            strOld = lstSession.Find(o => o.Id == outSessionId).Name;
                                        }
                                        else
                                        {
                                            strOld += "," + lstSession.Find(o => o.Id == outSessionId).Name;
                                        }
                                    }
                                }

                                //strOld = lstSession.Find(o => o.Id == int.Parse(lstOldSelected[0])).Name;
                                //for (var index = 1; index < lstOldSelected.Count; index++)
                                //{
                                //    strOld += "," + lstSession.Find(o => o.Id == int.Parse(lstOldSelected[index])).Name;
                                //}
                            }
                        }
                    }

                    var lstNewSelected = model.Sessions.Split(',').ToList();
                    var strNew = "";
                    if (lstNewSelected.Contains("0"))
                    {
                        strNew = "Tất cả các phiên";
                    }
                    else
                    {
                        var outSessionId = 0;
                        foreach (var newSelected in lstNewSelected)
                        {
                            if (int.TryParse(newSelected.Trim(), out outSessionId))
                            {
                                if (string.IsNullOrEmpty(strNew))
                                {
                                    strNew = lstSession.Find(o => o.Id == outSessionId).Name;
                                }
                                else
                                {
                                    strNew += "," + lstSession.Find(o => o.Id == outSessionId).Name;
                                }
                            }
                        }
                    }

                    lstChangeHistory.Add(new InviteEventHistoryModel()
                    {
                        UpdatedBy = UserContext.UserName,
                        UpdatedDate = DateTime.Now,
                        Action = "Phiên tham dự",
                        InviteEventId = inviteeInfo.Id,
                        OldValue = strOld,
                        NewValue = strNew
                    });
                }
            }

            if (inviteeInfo.CustomerEmail != model.CustomerEmail)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    Action = "Email",
                    InviteEventId = model.Id,
                    OldValue = inviteeInfo.CustomerEmail,
                    NewValue = model.CustomerEmail,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now
                });
            }

            if (inviteeInfo.Note != model.Note)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    Action = "Ghi chú",
                    InviteEventId = model.Id,
                    OldValue = inviteeInfo.Note,
                    NewValue = model.Note,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now
                });
            }

            var inviteEventHistoryDL = new InviteEventHistoryDL();
            foreach (var item in lstChangeHistory)
            {
                inviteEventHistoryDL.Insert(item);
            }
            #endregion
            
            response.Code = SystemCode.Success;
            response.Message = "Bạn đã sửa thông tin khách mời thành công";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult Approve(int id)
        {
            var response = new Response();
            if (!UserContext.HasPermisson(Permission.TakeInvitee) && !UserContext.HasPermisson(Permission.Admin))
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            var inviteEventDL = new InviteEventDL();
            var onlineRegisterInfo = inviteEventDL.GetOnlineRegisteredInviteeById(id);
            if (onlineRegisterInfo == null)
            {
                response.Code = SystemCode.DataNull;
                response.Message = "Không tìm thấy thông tin phù hợp";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            if (onlineRegisterInfo.Status == true)
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Khách mời này đã được duyệt";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra việc xác nhận của khách mời trước khi duyệt
            if (ValidateApproveStatus(onlineRegisterInfo))
            {
                inviteEventDL.WebCheckIn_UpdateStatus(id, true, DateTime.Now, UserContext.UserName);
            }
            else
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Khách hàng chỉ được duyệt khi đã gọi điện và xác nhận lần 1 và 2";
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            // Lưu lịch sử thông tin duyệt
            InsertApproveInviteEventHistory(id, onlineRegisterInfo);
            inviteEventDL.WebCheckin_UpdateSendTicketStatus(id, 1,16 );
            // Gửi SMS đến người dungg
            SendSMS(id);

            response.Code = SystemCode.Success;
            response.Message = "Bạn đã duyệt khách mời thành công , thông tin mã vé đã được gửi tới khách hàng để xác nhận tham dự .";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult UnApprove(int id)
        {
            var response = new Response();
            if (!UserContext.HasPermisson(Permission.TakeInvitee) && !UserContext.HasPermisson(Permission.Admin))
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            var inviteEventDL = new InviteEventDL();
            var onlineRegisterInfo = inviteEventDL.GetOnlineRegisteredInviteeById(id);
            if (onlineRegisterInfo == null)
            {
                response.Code = SystemCode.DataNull;
                response.Message = "Không tìm thấy thông tin phù hợp";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            if (onlineRegisterInfo.Status == false)
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Khách mời này đã hủy duyệt";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra khách mời này được được checkin chưa , nếu đã checkin thì không cho hủy duyệt
            if (onlineRegisterInfo.IsCheckIn)
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Khách mời đã checkin , bạn không thể hủy duyệt";
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            inviteEventDL.WebCheckIn_UpdateStatus(id, false, DateTime.Now, UserContext.UserName);
            // Lưu lịch sử thông tin hủy duyệt
            InsertUnApproveInviteEventHistory(id, onlineRegisterInfo);
            //var inviteEventHistoryDL = new InviteEventHistoryDL();
            //var historyInfo = new InviteEventHistoryModel();
            //historyInfo.OldPhoningStatus = onlineRegisterInfo.PhoningStatus;
            //historyInfo.OldConfirmAttend1st = onlineRegisterInfo.ConfirmAttend1st;
            //historyInfo.OldConfirmAttend2st = onlineRegisterInfo.ConfirmAttend2st;
            //historyInfo.OldPlaceOfAttendEvent = onlineRegisterInfo.PlaceOfAttendEvent;
            //historyInfo.NewPhoningStatus = onlineRegisterInfo.PhoningStatus;
            //historyInfo.NewConfirmAttend1st = onlineRegisterInfo.ConfirmAttend1st;
            //historyInfo.NewConfirmAttend2st = onlineRegisterInfo.ConfirmAttend2st;
            //historyInfo.NewPlaceOfAttendEvent = onlineRegisterInfo.PlaceOfAttendEvent;
            //historyInfo.Status = InviteeChangeHistory.UnApprove.GetHashCode();
            //historyInfo.UpdatedBy = UserContext.UserName;
            //historyInfo.InviteEventId = id;
            //inviteEventHistoryDL.Insert(historyInfo);

            response.Code = SystemCode.Success;
            response.Message = "Bạn đã hủy duyệt khách mời thành công";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        [HttpPost]
        public ActionResult ExportExcel(FilterForOnlineRegisterSearchModel model)
        {
            if (!UserContext.HasPermisson(Permission.Admin) && !UserContext.HasPermisson(Permission.Manager))
            {
                return View("~/Views/Common/NotPermitted.cshtml");
            }

            var fileName = "RegisteredInvitee-" + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss") + ".xlsx";
            var file = new FileInfo(fileName);
            using (var package = new ExcelPackage(file))
            {
                var worksheet = package.Workbook.Worksheets.Add("Khách mời đăng kí - " + DateTime.Now.ToString("yyyy-MM-dd-hh-mm-ss"));

                #region Header
                worksheet.Cells["A1:X2"].Merge = true;
                worksheet.Cells["A1:X2"].Value = "DANH SÁCH KHÁCH MỜI ĐĂNG KÍ THAM GIA SỰ KIỆN";
                worksheet.Cells["A1:X2"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells["A1:X2"].Style.Fill.BackgroundColor.SetColor(Color.White);
                worksheet.Cells["A1:X2"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Cells["A1:X2"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["A1:X2"].Style.Font.Bold = true;
                worksheet.Cells["A1:X2"].Style.Font.Name = "Times New Roman";
                worksheet.Cells["A1:X2"].Style.Font.Size = 18;
                worksheet.Cells["A1:X2"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["A1:X2"].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                worksheet.Cells["A4:X4"].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                worksheet.Cells["A4:X4"].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Cells["A4:X4"].Style.Font.Bold = true;
                // Width mã khách mời
                worksheet.Column(1).Width = 10;
                // Width mã vé
                worksheet.Column(2).Width = 15;
                // Width sự kiện
                worksheet.Column(3).Width = 30;
                // Width tên KH
                worksheet.Column(4).Width = 30;
                // Width SĐT
                worksheet.Column(5).Width = 20;
                // Width Email
                worksheet.Column(6).Width = 30;
                // Width Loại khách
                worksheet.Column(7).Width = 15;
                // Width chức vụ
                worksheet.Column(8).Width = 30;
                // Width Công ty
                worksheet.Column(9).Width = 40;
                // Width Địa điểm tổ chức
                worksheet.Column(10).Width = 20;
                // Width Tài khoản batdongsan.com
                worksheet.Column(11).Width = 40;
                // Width Nhân viên mời
                worksheet.Column(12).Width = 15;
                // Width Trạng thái cuối
                worksheet.Column(13).Width = 30;
                // Width Ghi chú
                worksheet.Column(14).Width = 30;
                // Width Người tạo
                worksheet.Column(15).Width = 10;
                // Width Chi nhánh mời
                worksheet.Column(16).Width = 20;
                // Width Phòng ban mời
                worksheet.Column(17).Width = 20;
                // Width Nhóm mời
                worksheet.Column(18).Width = 20;
                // Width Sale mời
                worksheet.Column(19).Width = 15;
                // Width Nguồn khách hàng
                worksheet.Column(20).Width = 15;
                // Width Phiên tham dự
                worksheet.Column(21).Width = 15;
                // Width Thời gian checkin
                worksheet.Column(22).Width = 25;
                // Width Lễ tân checkin
                worksheet.Column(23).Width = 25;
                // Width Số khách đi cùng
                worksheet.Column(24).Width = 20;


                worksheet.Cells["A4"].Value = "Mã khách mời";
                worksheet.Cells["B4"].Value = "Mã vé";
                worksheet.Cells["C4"].Value = "Sự kiện";
                worksheet.Cells["D4"].Value = "Tên KH";
                worksheet.Cells["E4"].Value = "SĐT";
                worksheet.Cells["F4"].Value = "Email";
                worksheet.Cells["G4"].Value = "Loại khách mời";
                worksheet.Cells["H4"].Value = "Chức vụ";
                worksheet.Cells["I4"].Value = "Công ty";
                worksheet.Cells["J4"].Value = "Địa điểm tổ chức";
                worksheet.Cells["K4"].Value = "Tài khoản batdongsan.com";
                worksheet.Cells["L4"].Value = "Nhân viên mời";
                worksheet.Cells["M4"].Value = "Trạng thái cuối";
                worksheet.Cells["N4"].Value = "Ghi chú";
                worksheet.Cells["O4"].Value = "Người tạo";
                worksheet.Cells["P4"].Value = "Chi nhánh mời";
                worksheet.Cells["Q4"].Value = "Phòng ban mời";
                worksheet.Cells["R4"].Value = "Nhóm mời";
                worksheet.Cells["S4"].Value = "Sale mời";
                worksheet.Cells["T4"].Value = "Nguồn khách hàng";
                worksheet.Cells["U4"].Value = "Phiên tham dự";
                worksheet.Cells["V4"].Value = "Thời gian check in";
                worksheet.Cells["W4"].Value = "Lễ tân check in";
                worksheet.Cells["X4"].Value = "Số khách đi cùng";

                #endregion

                #region GetData
                #region Searching
                if (model.TextSearch == null)
                {
                    model.TextSearch = string.Empty;
                }
                if (!string.IsNullOrEmpty(model.TextSearch))
                {
                    model.TextSearch = model.TextSearch.Trim();
                }
                if (!string.IsNullOrEmpty(model.IntroduceUser))
                {
                    model.IntroduceUser = model.IntroduceUser.Trim();
                }
                if (!string.IsNullOrEmpty(model.Code))
                {
                    model.Code = model.Code.Trim();
                }
                if (string.IsNullOrEmpty(model.AssignTo))
                {
                    model.AssignTo = "";
                }

                if (model.StartDate == null || model.StartDate == DateTime.MinValue)
                {
                    model.StartDate = new DateTime(2000, 1, 1, 0, 0, 0);
                }
                if (model.EndDate == null || model.EndDate == DateTime.MinValue)
                {
                    model.EndDate = new DateTime(9999, 1, 1, 0, 0, 0);
                }
                else
                {
                    model.EndDate = model.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59);
                }

                var inviteEventDL = new InviteEventDL();
                var result = new FilterForOnlineRegisterSearchModel();
                var lstSearchResult = new List<OnlineInviteeForSearchModel>();
                if (model.InviteeCode > 0)
                {
                    var info = inviteEventDL.WebCheckIn_GetRegisteredInviteeById(model.InviteeCode);
                    if (info != null)
                    {
                        lstSearchResult.Add(info);
                    }
                }
                // Tìm kiếm theo mã vé
                else if (!string.IsNullOrEmpty(model.Code))
                {
                    var iCode = 0;
                    if (int.TryParse(model.Code, out iCode))
                    {
                        var eventDL = new EventDL();
                        var eventInfo = eventDL.GetByIdInWebCheckIn(model.EventId);
                        if (eventInfo != null)
                        {
                            model.Code = eventInfo.EventCode + model.Code;
                        }
                    }
                    var inviteInfo = inviteEventDL.WebCheckin_GetByCode(model.EventId, model.Code);
                    if (inviteInfo != null)
                    {
                        lstSearchResult.Add(inviteInfo);
                    }
                }
                else
                {
                    // Chưa giao cho ai
                    if (model.FilterAssignTo == 1)
                    {
                        model.IsAssignTo = false;
                        model.IsBcrmAssignTo = false;
                    }
                    // Chưa giao cho Sale
                    else if (model.FilterAssignTo == 2)
                    {
                        model.IsAssignTo = null;
                        model.IsBcrmAssignTo = false;
                    }
                    // Chưa giao cho CSKH
                    else if (model.FilterAssignTo == 3)
                    {
                        model.IsAssignTo = false;
                        model.IsBcrmAssignTo = null;
                    }
                    // Đã giao cho Sale
                    else if (model.FilterAssignTo == 4)
                    {
                        model.IsAssignTo = null;
                        model.IsBcrmAssignTo = true;
                    }
                    // Đã giao cho CSKH
                    else if (model.FilterAssignTo == 5)
                    {
                        model.IsAssignTo = true;
                        model.IsBcrmAssignTo = null;
                    }
                    else
                    {
                        model.IsAssignTo = null;
                        model.IsBcrmAssignTo = null;
                    }

                    if (model.InviteeResponseStatus == 0)
                    {
                        model.PhoningStatus = 0;
                        model.ConfirmAttend1st = 0;
                        model.ConfirmAttend2st = 0;
                        model.Status = null;
                        model.SendTicketStatus = 0;
                    }
                    else
                    {
                        switch (model.InviteeResponseStatus)
                        {
                            // Trường hợp lấy danh sách khách mời xác nhận lần 1
                            case 1:
                                model.PhoningStatus = 0;
                                model.ConfirmAttend1st = AttendProbability.Yes.GetHashCode();
                                model.ConfirmAttend2st = 0;
                                model.Status = false;
                                model.SendTicketStatus = 0;
                                break;
                            // Trường hợp lấy danh sách khách mời xác nhận lần 2
                            case 2:
                                model.PhoningStatus = 0;
                                model.ConfirmAttend1st = 0;
                                model.ConfirmAttend2st = AttendProbability.Yes.GetHashCode();
                                model.Status = false;
                                model.SendTicketStatus = 0;
                                break;
                            // Chưa gọi
                            case 3:
                                model.PhoningStatus = -1;
                                model.ConfirmAttend1st = 0;
                                model.ConfirmAttend2st = 0;
                                model.Status = false;
                                model.SendTicketStatus = 0;
                                break;
                            // Gọi lại sau
                            case 4:
                                model.PhoningStatus = PhoningStatus.TryLater.GetHashCode();
                                model.ConfirmAttend1st = 0;
                                model.ConfirmAttend2st = 0;
                                model.Status = false;
                                model.SendTicketStatus = 0;
                                break;
                            // Không nghe máy
                            case 5:
                                model.PhoningStatus = PhoningStatus.NoListen.GetHashCode();
                                model.ConfirmAttend1st = 0;
                                model.ConfirmAttend2st = 0;
                                model.Status = false;
                                model.SendTicketStatus = 0;
                                break;
                            // Đã gọi
                            case 6:
                                model.PhoningStatus = PhoningStatus.Confirm.GetHashCode();
                                model.ConfirmAttend1st = 0;
                                model.ConfirmAttend2st = 0;
                                model.Status = false;
                                model.SendTicketStatus = 0;
                                break;
                            // Số máy sai
                            case 7:
                                model.PhoningStatus = PhoningStatus.WrongNumber.GetHashCode();
                                model.ConfirmAttend1st = 0;
                                model.ConfirmAttend2st = 0;
                                model.Status = false;
                                model.SendTicketStatus = 0;
                                break;
                            // Đang xem xét
                            case 8:
                                model.PhoningStatus = 0;
                                model.ConfirmAttend1st = AttendProbability.Considering.GetHashCode();
                                model.ConfirmAttend2st = AttendProbability.Considering.GetHashCode();
                                model.Status = false;
                                model.SendTicketStatus = 0;
                                break;
                            // Không tham gia
                            case 9:
                                model.PhoningStatus = 0;
                                model.ConfirmAttend1st = AttendProbability.No.GetHashCode();
                                model.ConfirmAttend2st = AttendProbability.No.GetHashCode();
                                model.Status = false;
                                model.SendTicketStatus = 0;
                                break;
                            // Đã gửi vé
                            case 10:
                                model.PhoningStatus = 0;
                                model.ConfirmAttend1st = 0;
                                model.ConfirmAttend2st = 0;
                                model.Status = true;
                                model.SendTicketStatus = 1;
                                break;
                            // Gửi lại vé
                            case 11:
                                model.PhoningStatus = 0;
                                model.ConfirmAttend1st = 0;
                                model.ConfirmAttend2st = 0;
                                model.Status = true;
                                model.SendTicketStatus = 2;
                                break;
                            // Không mời
                            case 12:
                                model.PhoningStatus = PhoningStatus.Ignore.GetHashCode();
                                model.ConfirmAttend1st = 0;
                                model.ConfirmAttend2st = 0;
                                model.Status = false;
                                model.SendTicketStatus = 0;
                                break;
                            // Trưởng phòng duyệt
                            case 13:
                                model.PhoningStatus = PhoningStatus.LeaderAppoved.GetHashCode();
                                model.ConfirmAttend1st = 0;
                                model.ConfirmAttend2st = 0;
                                model.Status = false;
                                model.SendTicketStatus = 0;
                                break;
                        }
                    }

                    lstSearchResult = inviteEventDL.SearchByCondition(model);

                    if (!string.IsNullOrEmpty(model.IntroduceUser))
                    {
                        var convertUnsign = CommonMethod.ConvertToUnSign(model.IntroduceUser.ToLower());
                        var lstExactSearchCondition = lstSearchResult.FindAll(o => !string.IsNullOrEmpty(o.IntroduceUser) && o.IntroduceUser.Contains(model.IntroduceUser));
                        var lstRelativeSearchCondition = lstSearchResult.FindAll(o => !string.IsNullOrEmpty(o.IntroduceUser)
                                                                                    && !o.IntroduceUser.Contains(model.IntroduceUser)
                                                                                    && CommonMethod.ConvertToUnSign(o.IntroduceUser.ToLower()).Contains(convertUnsign));
                        var lstAllInvitee = new List<OnlineInviteeForSearchModel>();
                        lstAllInvitee.AddRange(lstExactSearchCondition);
                        lstAllInvitee.AddRange(lstRelativeSearchCondition);
                        lstSearchResult = lstAllInvitee;
                    }

                    if (!string.IsNullOrEmpty(model.TextSearch))
                    {
                        var convertUnsign = CommonMethod.ConvertToUnSign(model.TextSearch.ToLower());
                        // Tìm chính xác theo thông tin tìm kiếm
                        var exactMatchInvitee = lstSearchResult.FindAll(o => o.CustomerName.ToLower().Contains(model.TextSearch.ToLower()) || o.CustomerPhone.ToLower().Contains(model.TextSearch.ToLower()));
                        // Tìm gần đúng theo thông tin tìm kiếm
                        var opportunityInvitee = lstSearchResult.FindAll(o =>
                                                                !o.CustomerName.ToLower().Contains(model.TextSearch.ToLower())
                                                                && !o.CustomerPhone.ToLower().Contains(model.TextSearch.ToLower())
                                                                && (CommonMethod.ConvertToUnSign(o.CustomerName.ToLower()).Contains(convertUnsign) || o.CustomerEmail.Contains(convertUnsign))
                                                                );
                        // Tìm theo ghi chú của thông tin tìm kiếm
                        var remainInvitee = lstSearchResult.Except(exactMatchInvitee).Except(opportunityInvitee).ToList();
                        var lstAllInvitee = new List<OnlineInviteeForSearchModel>();
                        lstAllInvitee.AddRange(exactMatchInvitee);
                        lstAllInvitee.AddRange(opportunityInvitee);
                        lstAllInvitee.AddRange(remainInvitee);
                        lstSearchResult = lstAllInvitee;
                    }
                    else if (string.IsNullOrEmpty(model.IntroduceUser))
                    {
                        lstSearchResult = lstSearchResult.OrderByDescending(o => o.CreatedDate).ToList();
                    }
                }

                foreach (var item in lstSearchResult)
                {
                    if (item.IsBcrmProcess == 1)
                    {
                        item.PositionName = ((CustomerPosition)item.CustomerPosition).ToCustomerPositionName();
                    }
                }

                #endregion
                var row = 5;

                var lstAllGroup = (new BCRMGroupsDL()).GetAllGroups();
                var lstSession = (new SessionDL()).GetByEventId(model.EventId);
                foreach (var inviteEvent in lstSearchResult)
                {
                    if (string.IsNullOrEmpty(inviteEvent.EventCode))
                    {
                        worksheet.Cells["A" + row].Value = string.Format("{0}", inviteEvent.Id);
                    }
                    else
                    {
                        worksheet.Cells["A" + row].Value = string.Format("{0}-{1}", inviteEvent.EventCode, inviteEvent.Id);
                    }
                    worksheet.Cells["B" + row].Value = inviteEvent.Code;
                    worksheet.Cells["C" + row].Value = inviteEvent.EventName;
                    worksheet.Cells["D" + row].Value = inviteEvent.CustomerName;
                    worksheet.Cells["E" + row].Value = inviteEvent.CustomerPhone;
                    worksheet.Cells["F" + row].Value = inviteEvent.CustomerEmail;
                    //worksheet.Cells["G" + row].Value = inviteEvent.IsNotSuggest == true ? "Khách vãng lai" : "Khách chính";
                    worksheet.Cells["G" + row].Value = ((TypeOfCustomer)inviteEvent.CustomerType).ToCustomerType();
                    worksheet.Cells["H" + row].Value = inviteEvent.PositionName;
                    worksheet.Cells["H" + row].Style.WrapText = true;
                    worksheet.Cells["I" + row].Value = inviteEvent.CompanyName;
                    worksheet.Cells["I" + row].Style.WrapText = true;
                    worksheet.Cells["J" + row].Value = inviteEvent.PlaceOfAttendEvent;
                    worksheet.Cells["J" + row].Style.WrapText = true;
                    worksheet.Cells["K" + row].Value = inviteEvent.UserName;
                    worksheet.Cells["K" + row].Style.WrapText = true;
                    worksheet.Cells["L" + row].Value = inviteEvent.IntroduceUser;
                    worksheet.Cells["M" + row].Value = GetLastStatus(inviteEvent);
                    worksheet.Cells["N" + row].Value = inviteEvent.Note;
                    worksheet.Cells["N" + row].Style.WrapText = true;
                    worksheet.Cells["O" + row].Value = inviteEvent.CreatedBy;
                    // Chi nhánh mời
                    var branch = CommonMethod.GetRootParent( lstAllGroup,inviteEvent.GroupId);
                    if (branch != null)
                    {
                        worksheet.Cells["P" + row].Value = branch.GroupName;
                    }
                    // Phòng ban và nhóm mời
                    var group = lstAllGroup.FirstOrDefault(m => m.GroupId == inviteEvent.GroupId);
                    if (group != null)
                    {
                        var department = lstAllGroup.FirstOrDefault(m => m.GroupId == group.ParentId);
                        worksheet.Cells["Q" + row].Value = department != null ? department.GroupName.Replace("-","") : string.Empty;
                        worksheet.Cells["R" + row].Value = group.GroupName.Replace("-", "");
                    }
                    // Sale mời
                    worksheet.Cells["S" + row].Value = inviteEvent.BcrmAssignTo;
                    // Nguồn khách hàng
                    worksheet.Cells["T" + row].Value = inviteEvent.SourceId == 2 ? "Landingpage" : "BCRM";

                    if (!string.IsNullOrEmpty(inviteEvent.Sessions) && lstSession.Count >0 )
                    {
                        var lstSelected = inviteEvent.Sessions.Split(',').ToList();
                        inviteEvent.SessionName = string.Join(",", lstSession.FindAll(o => lstSelected.Contains(o.Id.ToString())).Select(o => o.Name).ToList());
                        worksheet.Cells["U" + row].Value = inviteEvent.SessionName;
                    }

                    if (lstSession.Count > 0)
                    {
                        var lstCheckedin = (new SessionCheckinDL()).GetByInviteEventId(inviteEvent.Id);
                        var strCheckinBy = new StringBuilder();
                        var strCheckinDate = new StringBuilder();
                        var strAccompanyMember = new StringBuilder();

                        foreach (var checkedIn in lstCheckedin)
                        {
                            var sessionInfo = lstSession.Find(o => o.Id == checkedIn.SessionId);
                            if (sessionInfo != null)
                            {
                                strCheckinDate.Append(sessionInfo.Name + " : " + checkedIn.CheckinDate.ToString("dd/MM/yyyy HH:mm:ss") + "\n");
                                strCheckinBy.Append(sessionInfo.Name + " : " + checkedIn.CheckinBy + "\n");
                                strAccompanyMember.Append(sessionInfo.Name + " : " + checkedIn.AccompanyMember + "\n");
                            }
                        }
                        worksheet.Cells["V" + row].Value = strCheckinDate.ToString();
                        worksheet.Cells["V" + row].Style.WrapText = true;
                        worksheet.Cells["W" + row].Value = strCheckinBy.ToString() ;
                        worksheet.Cells["W" + row].Style.WrapText = true;
                        worksheet.Cells["X" + row].Value = strAccompanyMember.ToString() ;
                        worksheet.Cells["X" + row].Style.WrapText = true;    
                    }
                    else
                    {
                        worksheet.Cells["V" + row].Value = inviteEvent.CheckInDate == null ? string.Empty : inviteEvent.CheckInDate.Value.ToString("dd/MM/yyyy HH:mm:ss");
                        worksheet.Cells["W" + row].Value = inviteEvent.CheckInBy;
                        worksheet.Cells["X" + row].Value = inviteEvent.AccompanyMember;
                        worksheet.Row(row).Height = 35;
                    }
                    
                    row++;
                }
                #endregion

                var stream = new MemoryStream();
                package.SaveAs(stream);
                const string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                stream.Position = 0;
                return File(stream, contentType, fileName);
            }
        }
        [FilterAuthorize]
        public JsonResult DeleteAction(int id)
        {
            var response = new Response();
            if (!UserContext.HasPermisson(Permission.InviteeManager) && !UserContext.HasPermisson(Permission.Admin))
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            try
            {
                var inviteEventDL = new InviteEventDL();
                var onlineRegisterInfo = inviteEventDL.GetOnlineRegisteredInviteeById(id);
                if (onlineRegisterInfo == null)
                {
                    response.Code = SystemCode.DataNull;
                    response.Message = "Không tìm thấy thông tin phù hợp";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                // Kiểm tra khách mời này được được checkin chưa , nếu đã checkin thì không cho hủy duyệt
                if (onlineRegisterInfo.IsCheckIn)
                {
                    response.Code = SystemCode.NotPermitted;
                    response.Message = "Khách mời đã checkin , bạn không thể xóa !";
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                inviteEventDL.WebCheckin_Delete(id);
                // Lưu lịch sử thông tin xóa
                var inviteEventHistoryDL = new InviteEventHistoryDL();
                inviteEventHistoryDL.Insert(new InviteEventHistoryModel() {
                    UpdatedBy = UserContext.UserName,
                    InviteEventId = id,
                    Action = "Xóa khách mời",
                    NewValue ="",
                    OldValue = ""
                });

                response.Code = SystemCode.Success;
                response.Message = "Bạn đã xóa thông tin khách mời thành công";
            }
            catch
            {
                response.Code = SystemCode.Error;
                response.Message = "Có lỗi trong quá trình xử lý ! Liên hệ IT để có thông tin chi tiết";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult CreateAndApproveAction(OnlineInviteeModel model)
        {
            var response = new Response();
            if (!UserContext.HasPermisson(Permission.TakeInvitee) && !UserContext.HasPermisson(Permission.Admin))
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            //validate thông tin
            // Tên khách hàng bắt buộc nhập
            if (string.IsNullOrEmpty(model.CustomerName)
               || string.IsNullOrEmpty(model.CustomerPhone)
               || string.IsNullOrEmpty(model.CustomerEmail))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Dữ liệu không hợp lệ";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra số điện thoại
            var inviteEventDL = new InviteEventDL();
            if (inviteEventDL.WebCheckIn_CheckPhoneExist(0, model.EventId, model.CustomerPhone))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Số điện thoại này đã được sử dụng";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra địa chỉ email
            if (inviteEventDL.WebCheckIn_CheckEmailExist(0, model.EventId, model.CustomerEmail))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Email này đã được sử dụng !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            var msg = "";
            // Kiểm tra việc xác nhận khách mời
            if (!ValidateConfirmAttend(model, ref msg))
            {
                response.Code = SystemCode.NotValid;
                response.Message = msg;
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra việc xác nhận của khách mời trước khi duyệt
            if (!ValidateApproveStatus(model))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Khách hàng chỉ được duyệt khi đã gọi điện và xác nhận lần 1 và 2";
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            model.CreatedBy = UserContext.UserName;
            model.RegisteredDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            // Sinh mã code cho nhân viên đó
            var eventInfo = (new EventDL()).GetByIdInWebCheckIn(model.EventId);
            // Sinh mã vé cho khách mời ,Lặp tối đa 100 lần nếu như đã tồn tại mã vé này rồi
            var code = "";
            for (var index = 0; index < 100; index++)
            {
                code = string.Format("{0}{1}", eventInfo.EventCode, CommonMethod.GenerateCode());
                // Kiểm tra xem mã vé này đã tồn tại chưa
                if (!inviteEventDL.WebCheckin_IsExistCode(eventInfo.Id, code))
                {
                    break;
                }
            }
            model.Code = code;
            if (!string.IsNullOrEmpty(model.Sessions))
            {
                var lstSessions = model.Sessions.Split(',').ToList();
                if (lstSessions.Contains("0"))
                {
                    model.Sessions = "0";
                }
            }
            var id = inviteEventDL.InsertOnlineInvitee(model);

            // Thêm lịch sử lưu thông tin 
            var inviteEventHistoryDL = new InviteEventHistoryDL();
            inviteEventHistoryDL.Insert(new InviteEventHistoryModel() { 
                Action="Tạo mới",
                InviteEventId = id,
                OldValue = "",
                NewValue = "",
                UpdatedBy = UserContext.UserName,
                UpdatedDate =DateTime.Now
            });

            // Duyệt thông tin khách mời
            var onlineRegisterInfo = inviteEventDL.GetOnlineRegisteredInviteeById(id);
            if (onlineRegisterInfo == null)
            {
                response.Code = SystemCode.DataNull;
                response.Message = "Không tìm thấy thông tin phù hợp";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            inviteEventDL.WebCheckIn_UpdateStatus(id, true, DateTime.Now, UserContext.UserName);
            // Lưu lịch sử thông tin duyệt
            InsertApproveInviteEventHistory(id, onlineRegisterInfo);
            inviteEventDL.WebCheckin_UpdateSendTicketStatus(id, 1,16);
            // Gửi SMS đến người dungg
            SendSMS(id);
            // Kiểm tra 
            response.Code = SystemCode.Success;
            response.Message = "Bạn đã thêm và duyệt khách mời thành công , thông tin mã vé đã được gửi tới khách hàng để xác nhận tham dự .!";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult EditAndApproveAction(OnlineInviteeModel model)
        {
            var response = new Response();
            if (!UserContext.HasPermisson(Permission.TakeInvitee) && !UserContext.HasPermisson(Permission.Admin))
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            //validate thông tin
            // Tên khách hàng bắt buộc nhập
            if (string.IsNullOrEmpty(model.CustomerName) || string.IsNullOrEmpty(model.CustomerPhone) || string.IsNullOrEmpty(model.CustomerEmail))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Dữ liệu không hợp lệ";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra số điện thoại
            var inviteEventDL = new InviteEventDL();
            if (inviteEventDL.WebCheckIn_CheckPhoneExist(model.Id, model.EventId, model.CustomerPhone))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Số điện thoại này đã được sử dụng";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra địa chỉ email
            if (inviteEventDL.WebCheckIn_CheckEmailExist(model.Id, model.EventId, model.CustomerEmail))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Email này đã được sử dụng !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            var onlineRegisterInfo = inviteEventDL.GetOnlineRegisteredInviteeById(model.Id);

            var msg = "";
            if (!ValidateConfirmAttend(model, ref msg))
            {
                response.Code = SystemCode.NotValid;
                response.Message = msg;
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            if (!ValidateApproveStatus(model))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Khách hàng chỉ được duyệt khi đã gọi điện và xác nhận lần 1 và 2";
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            // Tạo thêm lịch sử nếu có thay đổi
            InsertEditInviteEventHistory(onlineRegisterInfo, model);
            if (!string.IsNullOrEmpty(model.Sessions))
            {
                var lstSessions = model.Sessions.Split(',').ToList();
                if (lstSessions.Contains("0"))
                {
                    model.Sessions = "0";
                }
            }
            inviteEventDL.UpdateOnlineInvitee(model);
            //Duyệt thông tin khách mời
            inviteEventDL.WebCheckIn_UpdateStatus(model.Id, true, DateTime.Now, UserContext.UserName);
            // Lưu lịch sử thông tin duyệt
            InsertApproveInviteEventHistory(model.Id, model);

            inviteEventDL.WebCheckin_UpdateSendTicketStatus(model.Id, 1,16);
            // Gửi SMS đến người dungg
            SendSMS(model.Id);
            // Kiểm tra 
            response.Code = SystemCode.Success;
            response.Message = "Bạn đã sửa và duyệt khách mời thành công ,thông tin mã vé đã được gửi tới khách mời để xác nhận tham dự . !";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult EditAndUnApproveAction(OnlineInviteeModel model)
        {
            var response = new Response();
            if (!UserContext.HasPermisson(Permission.TakeInvitee) && !UserContext.HasPermisson(Permission.Admin))
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            //validate thông tin
            // Tên khách hàng bắt buộc nhập
            if (string.IsNullOrEmpty(model.CustomerName) ||
                string.IsNullOrEmpty(model.CustomerPhone) ||
                string.IsNullOrEmpty(model.CustomerEmail))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Dữ liệu không hợp lệ";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra số điện thoại
            var inviteEventDL = new InviteEventDL();
            if (inviteEventDL.WebCheckIn_CheckPhoneExist(model.Id, model.EventId, model.CustomerPhone))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Số điện thoại này đã được sử dụng";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra địa chỉ email
            if (inviteEventDL.WebCheckIn_CheckEmailExist(model.Id, model.EventId, model.CustomerEmail))
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Email này đã được sử dụng !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            var onlineRegisterInfo = inviteEventDL.GetOnlineRegisteredInviteeById(model.Id);
            // Tạo thêm lịch sử nếu có thay đổi
            InsertEditInviteEventHistory(onlineRegisterInfo, model);
            if (!string.IsNullOrEmpty(model.Sessions))
            {
                var lstSessions = model.Sessions.Split(',').ToList();
                if (lstSessions.Contains("0"))
                {
                    model.Sessions = "0";
                }
            }
            // Cập nhật thông tin khách mời
            inviteEventDL.UpdateOnlineInvitee(model);
            //Hủy duyệt thông tin khách mời
            inviteEventDL.WebCheckIn_UpdateStatus(model.Id, false, DateTime.Now, UserContext.UserName);
            // Lưu lịch sử thông tin hủy duyệt
            InsertUnApproveInviteEventHistory(model.Id, model);
            
            // Kiểm tra 
            response.Code = SystemCode.Success;
            response.Message = "Bạn đã sửa và hủy duyệt khách mời thành công !";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult AssignTo(int inviteeId,string assignTo)
        {
            var response = new Response();
            InsertAssignToInviteEventHistory(inviteeId, assignTo);
            var inviteeEventDL = new InviteEventDL();
            inviteeEventDL.WebCheckin_UpdateAssignTo(inviteeId, assignTo);
            response.Code = SystemCode.Success;
            response.Data = assignTo;
            response.Message = "Bạn đã gán thông tin nhân viên thành công";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult GetAssignTo(int inviteeId)
        {
            var inviteeEventDL = new InviteEventDL();
            var inviteeInfo = inviteeEventDL.WebCheckIn_GetById(inviteeId);
            var response = new Response();
            response.Data = inviteeInfo.AssignTo;
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public ActionResult AssignToSale( int inviteeId)
        {
            var inviteEventDL = new InviteEventDL();
            var inviteeInfo = inviteEventDL.WebCheckIn_GetById(inviteeId);
            var customerDL = new BCRMCustomerDL();
            var customerInfo = customerDL.GetByPhoneNumber(inviteeInfo.CustomerPhone);
            var userBCRMDL = new BCRMUsersDL();
            var lstSaler = userBCRMDL.GetByRole(6);
            ViewBag.LstSale = lstSaler.Select(o => o.UserName).Distinct().ToList();
            ViewBag.InviteeId = inviteeId;
            ViewBag.ChosenSaler = string.IsNullOrEmpty(inviteeInfo.BcrmAssignTo) ? "" : inviteeInfo.BcrmAssignTo;
            return View(customerInfo);
        }
        [FilterAuthorize]
        public JsonResult AssignToSaleAction(int inviteeId,string assignTo)
        {
            var inviteEventDL = new InviteEventDL();
            var inviteeInfo = inviteEventDL.WebCheckIn_GetById(inviteeId);
            // Lấy ra thông tin khách hàng từ file khách hàng của sale
            var response = new Response();
            var customerDL = new BCRMCustomerDL();
            var customerInfo = customerDL.GetByPhoneNumber(inviteeInfo.CustomerPhone);
            if (customerInfo.Count == 0)
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Khách mời chưa có thông tin trên BCRM , bạn không thực hiện được thao tác này !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            var newChangeInfo = new InviteEventModel();
            newChangeInfo.Id = inviteeInfo.Id;
            newChangeInfo.CustomerId = customerInfo[0].Id;
            newChangeInfo.CustomerBirthday = customerInfo[0].DOB;
            newChangeInfo.CustomerType = (TypeOfCustomer)customerInfo[0].Type;
            newChangeInfo.CompanyId = customerInfo[0].CompanyId;
            if (newChangeInfo.CompanyId > 0)
            {
                var companyLayer = new BCRMCompanyDL();
                var company = companyLayer.GetById(newChangeInfo.CompanyId);
                if (company.ParentId > 0)
                {
                    var parentCompany = companyLayer.GetById(company.ParentId);
                    newChangeInfo.CompanyName = parentCompany != null ? string.Format("{0} | {1}", parentCompany.Name, company.Name) : company.Name;
                }
                else
                {
                    newChangeInfo.CompanyName = company.Name;
                }
                newChangeInfo.CompanyAddress = company.Address;
                newChangeInfo.CompanySize = (CompanySize)company.Size;
            }

            var opportunityLayer = new BCRMOpportunityDL();
            var lstOpportunity = opportunityLayer.GetByCustomerId(newChangeInfo.CustomerId);
            var eventInfo = (new EventDL()).GetByIdInWebCheckIn(inviteeInfo.EventId);
            var sum = lstOpportunity.Where(o => o.Status == OpportunityStatus.Success.GetHashCode()
                                            && o.IsConfirmed
                                            && o.PaymentDate >= eventInfo.RevenueStartDate
                                            && o.PaymentDate <= eventInfo.RevenueEndDate)
                                    .Sum(o => o.TransactionAmount - o.VAT - o.Discount);
            //Nếu tính trung bình
            if (eventInfo.RevenueCalculation == 2)
            {
                var month = 12 * (eventInfo.RevenueEndDate.Year - eventInfo.RevenueStartDate.Year) + eventInfo.RevenueEndDate.Month - eventInfo.RevenueStartDate.Month;
                sum = (int)Math.Round((double)(sum / month), MidpointRounding.AwayFromZero);
            }
            newChangeInfo.CustomerRevenue = sum;
            newChangeInfo.BcrmStatus = InviteEventStatus.New;
            newChangeInfo.IsBcrmProcess = true;
            newChangeInfo.BcrmAssignTo = assignTo;
            var assigneeInfo = (new BCRMUsersDL()).GetByUsername(assignTo);
            newChangeInfo.GroupId = assigneeInfo.GroupId;
            // Cập nhật lại thông tin khi chuyển thông tin khách mời cho sale
            inviteEventDL.WebCheckin_SyncToBCRM(newChangeInfo);
            // Lưu lịch sử thay đổi thông tin khách mời
            #region Lưu lịch sử thay đổi thông tin khách mời
            var lstChangeHistory = new List<InviteEventHistoryModel>();
            lstChangeHistory.Add(new InviteEventHistoryModel()
            {
                Action = "Giao cho Sale",
                InviteEventId = newChangeInfo.Id,
                OldValue = "",
                NewValue = newChangeInfo.BcrmAssignTo,
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now
            });

            if (newChangeInfo.CustomerBirthday != inviteeInfo.CustomerBirthday)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    Action = "Ngày sinh",
                    InviteEventId = newChangeInfo.Id,
                    OldValue = inviteeInfo.CustomerBirthday == null ? "" : inviteeInfo.CustomerBirthday.Value.ToString("dd/MM/yyyy"),
                    NewValue = newChangeInfo.CustomerBirthday == null ? "" : newChangeInfo.CustomerBirthday.Value.ToString("dd/MM/yyyy"),
                    UpdatedBy = "Auto",
                    UpdatedDate = DateTime.Now
                });
            }

            if (newChangeInfo.CustomerType != inviteeInfo.CustomerType)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    Action = "Loại khách mời",
                    InviteEventId = newChangeInfo.Id,
                    OldValue = inviteeInfo.CustomerType.ToCustomerType(),
                    NewValue = newChangeInfo.CustomerType.ToCustomerType(),
                    UpdatedBy = "Auto",
                    UpdatedDate = DateTime.Now
                });
            }

            if (newChangeInfo.CompanyName != inviteeInfo.CompanyName)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    Action = "Tên công ty",
                    InviteEventId = newChangeInfo.Id,
                    OldValue = inviteeInfo.CompanyName,
                    NewValue = newChangeInfo.CompanyName,
                    UpdatedBy = "Auto",
                    UpdatedDate = DateTime.Now
                });
            }

            if (newChangeInfo.CompanyAddress != inviteeInfo.CompanyAddress)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    Action = "Địa chỉ công ty",
                    InviteEventId = newChangeInfo.Id,
                    OldValue = inviteeInfo.CompanyAddress,
                    NewValue = newChangeInfo.CompanyAddress,
                    UpdatedBy = "Auto",
                    UpdatedDate = DateTime.Now
                });
            }

            if (newChangeInfo.CompanySize != inviteeInfo.CompanySize)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    Action = "Kích thước công ty",
                    InviteEventId = newChangeInfo.Id,
                    OldValue = inviteeInfo.CompanySize.ToCompanySizeName(),
                    NewValue = newChangeInfo.CompanySize.ToCompanySizeName(),
                    UpdatedBy = "Auto",
                    UpdatedDate = DateTime.Now
                });
            }

            if (newChangeInfo.CustomerRevenue != inviteeInfo.CustomerRevenue)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    Action = "Doanh thu",
                    InviteEventId = newChangeInfo.Id,
                    OldValue = inviteeInfo.CustomerRevenue.ToString(),
                    NewValue = newChangeInfo.CustomerRevenue.ToString(),
                    UpdatedBy = "Auto",
                    UpdatedDate = DateTime.Now
                });
            }

            var inviteEventHistoryDL = new InviteEventHistoryDL();
            foreach (var item in lstChangeHistory)
            {
                inviteEventHistoryDL.Insert(item);
            }
            #endregion
            
            response.Code = SystemCode.Success;
            response.Message = "Bạn đã chuyển giao khách mời cho sale thành công !";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult ResendTicket(int id)
        {
            var response = new Response();
            if (!UserContext.HasPermisson(Permission.Admin) && !UserContext.HasPermisson(Permission.Manager) && !UserContext.HasPermisson(Permission.InviteeManager))
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            var inviteEventDL = new InviteEventDL();
            var onlineRegisterInfo = inviteEventDL.GetOnlineRegisteredInviteeById(id);
            if (onlineRegisterInfo == null)
            {
                response.Code = SystemCode.DataNull;
                response.Message = "Không tìm thấy thông tin phù hợp";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            if (onlineRegisterInfo.Status != true)
            {
               response.Code = SystemCode.NotValid;
               response.Message = "Khách mời chưa được phép gửi lại vé mời !";
               return Json(response, JsonRequestBehavior.AllowGet);                   
            }
            var inviteEventHistoryDL = new InviteEventHistoryDL();
            inviteEventHistoryDL.Insert(new InviteEventHistoryModel()
            {
                Action = "Gửi lại vé",
                UpdatedBy = UserContext.UserName,
                UpdatedDate = DateTime.Now,
                InviteEventId = id,
                NewValue = "",
                OldValue = ""
            });
            (new InviteEventDL()).WebCheckin_UpdateSendTicketStatus(id, 2,17);
            SendSMS(id);
            response.Code = SystemCode.Success;
            response.Message = "Bạn đã gửi lại thông tin vé mời thành công";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [FilterAuthorize]
        public JsonResult SendTicket(int id)
        {
            var response = new Response();
            if (!UserContext.HasPermisson(Permission.Admin) && !UserContext.HasPermisson(Permission.Manager) && !UserContext.HasPermisson(Permission.InviteeManager))
            {
                response.Code = SystemCode.NotPermitted;
                response.Message = "Bạn không có quyền thực thi chức năng này !";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            var inviteEventDL = new InviteEventDL();
            var onlineRegisterInfo = inviteEventDL.GetOnlineRegisteredInviteeById(id);
            if (onlineRegisterInfo == null)
            {
                response.Code = SystemCode.DataNull;
                response.Message = "Không tìm thấy thông tin phù hợp";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            if (onlineRegisterInfo.Status == true)
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Khách mời này đã được duyệt";
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            // Kiểm tra việc xác nhận của khách mời trước khi duyệt
            if (ValidateApproveStatus(onlineRegisterInfo))
            {
                inviteEventDL.WebCheckIn_UpdateStatus(id, true, DateTime.Now, UserContext.UserName);
            }
            else
            {
                response.Code = SystemCode.NotValid;
                response.Message = "Khách hàng chỉ được gửi vé khi đã gọi điện và xác nhận lần 1 và 2";
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            // Lưu lịch sử thông tin duyệt
            InsertApproveInviteEventHistory(id, onlineRegisterInfo);
            inviteEventDL.WebCheckin_UpdateSendTicketStatus(id, 1,16);
            // Gửi SMS đến người dungg
            SendSMS(id);

            response.Code = SystemCode.Success;
            response.Message = "Bạn đã duyệt khách mời thành công , thông tin mã vé đã được gửi tới khách hàng để xác nhận tham dự .";
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Utility
        /// <summary>
        /// Gửi SMS
        /// </summary>
        /// <param name="inviteEventId"></param>
        private void SendSMS(int inviteEventId)
        {
            var inviteEventDL = new InviteEventDL();
            var onlineRegisterInfo = inviteEventDL.GetOnlineRegisteredInviteeById(inviteEventId);
            // Gửi SMS đến người dungg
            JetNavSMSSoapClient client = new JetNavSMSSoapClient();
            try
            {
                // Lấy thông tin về sự kiện
                var eventDL = new EventDL();
                var eventInfo = eventDL.GetByIdInWebCheckIn(onlineRegisterInfo.EventId);
                // Lấy thông tin số điện thoại
                var phoneNumber = onlineRegisterInfo.CustomerPhone;
                //// Lấy thông tin về tên sự kiện (chuyển về không dấu)
                //var eventNameUnsign = CommonMethod.ConvertToUnSignIncludeUpper(eventInfo.SMSTitle);
                // Lấy thông tin về nội dung tin nhắn gửi mời sự kiện
                var smsFormat = eventInfo.SMSTitle;
                // Lấy thông tin về tên khách mời (chuyển về không dấu)
                var inviteeName = CommonMethod.ConvertToUnSignIncludeUpper(onlineRegisterInfo.CustomerName);
                // Lấy thông tin về mã vé của khách mời
                //var inviteeCode = eventInfo.EventCode + "-" + onlineRegisterInfo.Id;
                var inviteeCode = onlineRegisterInfo.Code;
                // Tạo nội dung tin nhắn gửi cho khách mời
                var smsContent = smsFormat.Replace("[Ten KH]", inviteeName).Replace("[MaKhachMoi]", inviteeCode);

                var responseResult = client.SendMT(phoneNumber, ConfigurationManager.AppSettings["SMSSourceName"], smsContent, "text", ConfigurationManager.AppSettings["SMSServiceId"], ConfigurationManager.AppSettings["SMSCPID"], ConfigurationManager.AppSettings["SMSUserName"], ConfigurationManager.AppSettings["SMSPassword"]);
                var smsResponse = new SendSMSHistoryModel();
                smsResponse.SMSResponse = responseResult;
                switch (responseResult)
                {
                    case 200:
                        smsResponse.Description = "OK";
                        break;
                    case 401:
                        smsResponse.Description = "FORBIDDEN (invalid source ip address)";
                        break;
                    case 406:
                        smsResponse.Description = "FORBIDDEN (invalid cpid)";
                        break;
                    case 403:
                        smsResponse.Description = "FORBIDDEN (bad username or password)";
                        break;
                    case 400:
                        smsResponse.Description = "BAD REQUEST (invalid format or parameters)";
                        break;
                    case -999:
                        smsResponse.Description = "FAIL (fail received by GAPIT)";
                        break;
                    case 505:
                        smsResponse.Description = "BAD REQUEST (Invalid brand name)";
                        break;
                }
                smsResponse.InviteEventId = inviteEventId;
                smsResponse.PhoneNumber = onlineRegisterInfo.CustomerPhone;
                smsResponse.SMSContent = smsContent;
                smsResponse.EventId = eventInfo.Id;
                var sendSMSHistoryDL = new SendSMSHistoryDL();
                sendSMSHistoryDL.Insert(smsResponse);
            }
            catch
            {
                if (client.State != System.ServiceModel.CommunicationState.Closed)
                {
                    client.Abort();
                }
            }
            finally
            {
                client.Close();
            }
        }
        /// <summary>
        /// Kiểm tra xem việc xác nhận khách mời có hợp lệ hay không
        /// </summary>
        /// <param name="model"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        private bool ValidateConfirmAttend(OnlineInviteeModel model, ref string msg)
        {
            // Trường hợp chưa gọi điện hoặc gọi điện chưa OK thì chưa được xác nhận 
            if (!model.PhoningStatus.HasValue || model.PhoningStatus.Value != 1)
            {
                if (model.ConfirmAttend1st != null || model.ConfirmAttend2st != null)
                {
                    msg = "Bạn không thể xác nhận cho khách mời khi chưa gọi điện";
                    return false;
                }
                else
                {
                    return true;
                }
            }
            // Trường hợp đã gọi điện OK, tuy nhiên 
            else
            {
                if (!model.ConfirmAttend1st.HasValue || (model.ConfirmAttend1st.Value != 1 && model.ConfirmAttend1st.Value != 3))
                {
                    if (model.ConfirmAttend2st != null)
                    {
                        msg = "Bạn chưa được phép xác nhận lần 2 do chưa xác nhận lần 1";
                        return false;
                    }
                    return true;
                }
                else
                {
                    return true;
                }
            }
        }
        /// <summary>
        /// Kiểm tra việc duyệt khách mời có hợp lệ hay không
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private bool ValidateApproveStatus(OnlineInviteeModel model)
        {
            if (model.PhoningStatus.HasValue && model.ConfirmAttend1st.HasValue && model.ConfirmAttend2st.HasValue)
            {
                if (model.PhoningStatus.Value == 1
                    && (model.ConfirmAttend1st.Value == 1 || model.ConfirmAttend1st.Value == 3)
                    && model.ConfirmAttend2st.Value == 1)
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Get message cho trạng thái cuối
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private string GetLastStatus(OnlineInviteeForSearchModel model)
        {
            if (model.PhoningStatus == null)
            {
                return "Chưa gọi";
            }
            else
            {
                if (model.PhoningStatus.Value == PhoningStatus.Confirm.GetHashCode())
                {
                    if (model.ConfirmAttend1st == null && model.ConfirmAttend2st == null)
                    {
                        return "Đã gọi";
                    }
                    else
                    {
                        if (model.ConfirmAttend2st != null)
                        {
                            if (model.Status == true)
                            {
                                if (model.SendTicketStatus == 2)
                                {
                                    return "Đã gửi lại SMS";
                                }
                                else
                                {
                                    return "Đã gửi SMS";
                                }
                            }
                            else if (model.ConfirmAttend2st.Value == AttendProbability.Yes.GetHashCode())
                            {
                                return "Xác nhận tham dự lần 2";
                            }
                            else if (model.ConfirmAttend2st.Value == AttendProbability.No.GetHashCode())
                            {
                                return "Không tham dự";
                            }
                            else
                            {
                                return ((AttendProbability)model.ConfirmAttend2st.Value).ToAttendProbabilityName();
                            }
                        }
                        else
                        {
                            if (model.ConfirmAttend1st.Value == AttendProbability.Yes.GetHashCode())
                            {
                                return "Xác nhận tham dự lần 1";
                            }
                            else if (model.ConfirmAttend1st.Value == AttendProbability.No.GetHashCode())
                            {
                                return "Không tham dự";
                            }
                            else
                            {
                                return ((AttendProbability)model.ConfirmAttend1st.Value).ToAttendProbabilityName();
                            }
                        }
                    }
                }
                else
                {
                    return ((PhoningStatus)model.PhoningStatus).ToPhoningStatusName();
                }
            }
        }
        /// <summary>
        /// Lưu lại lịch sử thay đổi
        /// </summary>
        /// <param name="oldData"></param>
        /// <param name="newData"></param>
        /// <returns></returns>
        public bool InsertEditInviteEventHistory(OnlineInviteeModel oldData,OnlineInviteeModel newData )
        {
            var lstChangeHistory = new List<InviteEventHistoryModel>();
            var oldLastStatus = GetLastStatus(new OnlineInviteeForSearchModel(){
                ConfirmAttend1st = oldData.ConfirmAttend1st,
                ConfirmAttend2st = oldData.ConfirmAttend2st,
                PhoningStatus = oldData.PhoningStatus
            });
            var newLastStatus = GetLastStatus(new OnlineInviteeForSearchModel(){
                ConfirmAttend1st = newData.ConfirmAttend1st,
                ConfirmAttend2st = newData.ConfirmAttend2st,
                PhoningStatus = newData.PhoningStatus
            });
            if(oldLastStatus != newLastStatus )
            {
                lstChangeHistory.Add(new InviteEventHistoryModel(){
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Action = "Trạng thái",
                    InviteEventId = oldData.Id,
                    OldValue = oldLastStatus,
                    NewValue = newLastStatus,
                });
            }

            if(oldData.PlaceOfAttendEvent != newData.PlaceOfAttendEvent)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Action = "Nơi tổ chức sự kiện",
                    InviteEventId = oldData.Id,
                    OldValue = oldData.PlaceOfAttendEvent,
                    NewValue = newData.PlaceOfAttendEvent,
                });
            }

            var eventDL = new EventDL();
            var lstOtherChangeItem = new List<OtherChangeItemModel>();
            if (oldData.EventId != newData.EventId)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Action = "Sự kiện",
                    InviteEventId = oldData.Id,
                    OldValue = eventDL.GetByIdInWebCheckIn(oldData.EventId).Name,
                    NewValue = eventDL.GetByIdInWebCheckIn(newData.EventId).Name,
                });
            }
            if (!oldData.CustomerName.Equals(newData.CustomerName))
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Action = "Tên khách mời",
                    InviteEventId = oldData.Id,
                    OldValue = oldData.CustomerName,
                    NewValue = newData.CustomerName,
                });

                //lstOtherChangeItem.Add(new OtherChangeItemModel("Tên khách mời", "CustomerName", oldData.CustomerName, newData.CustomerName));
            }
            if (oldData.IntroduceUser != newData.IntroduceUser)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Action = "Tên nhân viên mời",
                    InviteEventId = oldData.Id,
                    OldValue = oldData.IntroduceUser,
                    NewValue = newData.IntroduceUser,
                });
            }
            if (!oldData.CustomerPhone.Equals(newData.CustomerPhone))
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Action = "Số điện thoại",
                    InviteEventId = oldData.Id,
                    OldValue = oldData.CustomerPhone,
                    NewValue = newData.CustomerPhone,
                });
            }
            if (!oldData.CustomerEmail.Equals(newData.CustomerEmail))
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Action = "Email",
                    InviteEventId = oldData.Id,
                    OldValue = oldData.CustomerEmail,
                    NewValue = newData.CustomerEmail,
                });
            }
            if (oldData.CompanyName != newData.CompanyName)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Action = "Tên công ty",
                    InviteEventId = oldData.Id,
                    OldValue = oldData.CompanyName,
                    NewValue = newData.CompanyName
                });
            }
            if (oldData.CustomerType != newData.CustomerType)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Action = "Loại khách mời",
                    InviteEventId = oldData.Id,
                    OldValue = ((TypeOfCustomer)oldData.CustomerType).ToCustomerType(),
                    NewValue = ((TypeOfCustomer)newData.CustomerType).ToCustomerType()
                });
            }
            if (oldData.PositionName != newData.PositionName)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Action = "Chức vụ",
                    InviteEventId = oldData.Id,
                    OldValue = oldData.PositionName,
                    NewValue = newData.PositionName,
                });
            }
            if (oldData.RegisteredSource != newData.RegisteredSource)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Action = "Nguồn đăng kí",
                    InviteEventId = oldData.Id,
                    OldValue = ((RegisteredSource)oldData.RegisteredSource).ToRegisteredSourceName(),
                    NewValue = ((RegisteredSource)newData.RegisteredSource).ToRegisteredSourceName(),
                });
            }

            var lstSession = (new SessionDL()).GetByEventId(oldData.EventId);
            if (lstSession.Count > 0)
            {
                if (string.IsNullOrEmpty(oldData.Sessions))
                {
                    oldData.Sessions = "";
                }
                if (oldData.Sessions != newData.Sessions)
                {
                    var strOld = "";
                    if (!string.IsNullOrEmpty(oldData.Sessions))
                    {
                        var lstOldSelected = oldData.Sessions.Split(',').ToList();
                        if (lstOldSelected.Count > 0)
                        {
                            if (lstOldSelected.Contains("0"))
                            {
                                strOld = "Tất cả các phiên";
                            }
                            else
                            {
                                var outSessionId = 0;
                                foreach (var oldSelected in lstOldSelected)
                                {
                                    if (int.TryParse(oldSelected.Trim(), out outSessionId))
                                    {
                                        if (string.IsNullOrEmpty(strOld))
                                        {
                                            strOld = lstSession.Find(o => o.Id == outSessionId).Name;
                                        }
                                        else
                                        {
                                            strOld += "," + lstSession.Find(o => o.Id == outSessionId).Name;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    var lstNewSelected = newData.Sessions.Split(',').ToList();
                    var strNew = "";
                    if (lstNewSelected.Contains("0"))
                    {
                        strNew = "Tất cả các phiên";
                    }
                    else
                    {
                        var outSessionId = 0;
                        foreach (var newSelected in lstNewSelected)
                        {
                            if (int.TryParse(newSelected.Trim(), out outSessionId))
                            {
                                if (string.IsNullOrEmpty(strNew))
                                {
                                    strNew = lstSession.Find(o => o.Id == outSessionId).Name;
                                }
                                else
                                {
                                    strNew += "," + lstSession.Find(o => o.Id == outSessionId).Name;
                                }
                            }
                        }

                        //strNew = lstSession.Find(o => o.Id == int.Parse(lstNewSelected[0])).Name;
                        //for (var index = 1; index < lstNewSelected.Count; index++)
                        //{
                        //    strNew += "," + lstSession.Find(o => o.Id == int.Parse(lstNewSelected[index])).Name;
                        //}
                    }

                    lstChangeHistory.Add(new InviteEventHistoryModel()
                    {
                        UpdatedBy = UserContext.UserName,
                        UpdatedDate = DateTime.Now,
                        Action = "Phiên tham dự",
                        InviteEventId = oldData.Id,
                        OldValue = strOld,
                        NewValue = strNew
                    });
                }
            }
            

            if (oldData.Note != newData.Note)
            {
                lstChangeHistory.Add(new InviteEventHistoryModel()
                {
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now,
                    Action = "Ghi chú",
                    InviteEventId = oldData.Id,
                    OldValue = string.IsNullOrEmpty(oldData.Note) ? "" : oldData.Note,
                    NewValue = string.IsNullOrEmpty(newData.Note) ? "" : newData.Note
                });
            }

            var inviteEventHistoryDL = new InviteEventHistoryDL();
            foreach (var item in lstChangeHistory)
            {
                inviteEventHistoryDL.Insert(item);
            }
            
            return true;
        }
        [NonAction]
        public bool InsertApproveInviteEventHistory(int inviteEventId, OnlineInviteeModel oldData)
        {
            // Lưu lịch sử thông tin duyệt
            var inviteEventHistoryDL = new InviteEventHistoryDL();
            inviteEventHistoryDL.Insert(new InviteEventHistoryModel() { 
                Action = "Gửi vé",
                UpdatedBy = UserContext.UserName,
                UpdatedDate =DateTime.Now,
                InviteEventId = inviteEventId,
                NewValue = "Đã gửi vé",
                OldValue = "Chưa gửi vé"
            });
            return true;
        }
        [NonAction]
        public bool InsertUnApproveInviteEventHistory(int inviteEventId, OnlineInviteeModel oldData)
        {
            // Lưu lịch sử thông tin duyệt
            //var inviteEventHistoryDL = new InviteEventHistoryDL();
            //var historyInfo = new InviteEventHistoryModel();
            //historyInfo.OldPhoningStatus = oldData.PhoningStatus;
            //historyInfo.OldConfirmAttend1st = oldData.ConfirmAttend1st;
            //historyInfo.OldConfirmAttend2st = oldData.ConfirmAttend2st;
            //historyInfo.OldPlaceOfAttendEvent = oldData.PlaceOfAttendEvent;
            //historyInfo.NewPhoningStatus = oldData.PhoningStatus;
            //historyInfo.NewConfirmAttend1st = oldData.ConfirmAttend1st;
            //historyInfo.NewConfirmAttend2st = oldData.ConfirmAttend2st;
            //historyInfo.NewPlaceOfAttendEvent = oldData.PlaceOfAttendEvent;
            //historyInfo.Status = InviteeChangeHistory.UnApprove.GetHashCode();
            //historyInfo.UpdatedBy = UserContext.UserName;
            //historyInfo.InviteEventId = inviteEventId;
            //var lstOtherChangeItem = new List<OtherChangeItemModel>();
            //lstOtherChangeItem.Add(new OtherChangeItemModel("Trạng thái duyệt", "Status", "Đã duyệt", "Chờ duyệt"));
            //historyInfo.OtherChange = JsonConvert.SerializeObject(lstOtherChangeItem);
            //inviteEventHistoryDL.Insert(historyInfo);
            return true;
        }
        [NonAction]
        public bool InsertAssignToInviteEventHistory(int inviteEventId, string assignTo)
        {
            var inviteEventHistoryDL = new InviteEventHistoryDL();
            var inviteDL = new InviteEventDL();
            var lstOtherChange = new List<OtherChangeItemModel>();
            var oldData = inviteDL.WebCheckIn_GetById(inviteEventId);
            if (oldData.AssignTo != assignTo)
            {
                inviteEventHistoryDL.Insert(new InviteEventHistoryModel()
                {
                    Action = "Giao cho CSKH",
                    InviteEventId = inviteEventId,
                    OldValue = string.IsNullOrEmpty(oldData.AssignTo) ? "Chưa có" : oldData.AssignTo,
                    NewValue = assignTo,
                    UpdatedBy = UserContext.UserName,
                    UpdatedDate = DateTime.Now
                });
            }
            return true;
        }

        #endregion
    }
}
