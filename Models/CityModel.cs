﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class CityModel
    {
        public int Id { get; set; }

        public string CityName { get; set; }

        public int Ordering { get; set; }
    }
}