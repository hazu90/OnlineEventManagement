﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class InviteEventSessionChangeModel
    {
        public List<SessionModel> LstSession { get; set; }
        public string Sessions { get; set; }
        public List<string> LstSelectedSessionId { get; set; }
        public int InviteEventId { get; set; }
        public int EventId { get; set; }
        public string CheckedinSessionId { get; set; }
    }
}