﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Libs;
using BCRM.CheckInEvent.Common;

namespace BCRM.CheckInEvent.Models
{
    public class BCRMInviteeEditModel
    {
        public List<KeyValuePair<int, string>> ListCustomerType { get; set; }
        public List<KeyValuePair<string, string>> ListPlaceAttendEvent { get; set; }
        public List<KeyValuePair<int, string>> ListAttendProbability { get; set; }
        public List<KeyValuePair<int, string>> ListPhoningStatus { get; set; }
        public List<KeyValuePair<int, string>> ListRegisteredSource { get; set; }
        public InviteEventModel BCRMInviteeInfo { get; set; }
        public List<EventModel> LstEvent { get; set; }
        public List<SessionModel> LstSession { get; set; }
        public List<string> LstPhoneNumber { get; set; }
        public List<KeyValuePair<int,string>> ListCustomerPosition { get; set; }
        public List<KeyValuePair<int,string>> ListAbilityAttend { get; set; }
        public List<KeyValuePair<int,string>> ListCompanySize { get; set; }
        public List<int> LstSelectedSession { get; set; }
        public List<InviteEventHistoryModel> LstInviteEventHistory { get; set; }
        
        public BCRMInviteeEditModel()
        {
            this.ListCustomerType = Helper.ListCustomerType();
            this.ListPlaceAttendEvent = new List<KeyValuePair<string, string>>();
            this.ListPlaceAttendEvent.Add(new KeyValuePair<string, string>("Hà Nội", "Hà Nội"));
            this.ListPlaceAttendEvent.Add(new KeyValuePair<string, string>("Đà Nẵng", "Đà Nẵng"));
            this.ListPlaceAttendEvent.Add(new KeyValuePair<string, string>("TP. Hồ Chí Minh", "TP. Hồ Chí Minh"));
            this.ListAttendProbability = CheckInHelper.ListAttendProbability();
            this.ListPhoningStatus = CheckInHelper.ListPhoningStatus();
            this.ListRegisteredSource = CheckInHelper.ListRegisteredSource();
            this.ListAbilityAttend = Helper.ListAbilityAttendName();
            this.ListCompanySize = Helper.ListCompanySizeName();
            this.ListCustomerPosition = Helper.ListCustomerPositionName();
        }
    }
    public class BCRMInviteeEditRequestModel
    {
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public int CustomerPosition { get; set; }
        public string Sessions { get; set; }
        public string Note { get; set; }
        public int Id { get; set; }
    }
}