﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class OtherChangeItemModel
    {
        public string FieldDisplay { get; set; }
        public string FieldName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public OtherChangeItemModel(string fieldDisplay, string fieldName, string oldValue, string newValue)
        {
            this.FieldDisplay = fieldDisplay;
            this.FieldName = fieldName;
            this.OldValue = oldValue;
            this.NewValue = newValue;
        }
    }
}