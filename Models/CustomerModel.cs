﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class CustomerModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public DateTime? DOB { get; set; }
        public int Type { get; set; }
        public int CompanyId { get; set; }
        public int Status { get; set; }
        public string AssignTo { get; set; }
        public string Description { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CareDate { get; set; }
        public string UtilityInfo { get; set; }
        public DateTime ExtensionTime { get; set; }
        public bool UseOwnerService { get; set; }
        public int Regional { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsOldCustomer { get; set; }
        public int CustOwnerId { get; set; }
    }
}