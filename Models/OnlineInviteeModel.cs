﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Libs;

namespace BCRM.CheckInEvent.Models
{
    public class OnlineInviteeModel
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string PositionName { get; set; }
        public string CustomerPhone { get; set; }
        public bool IsUsingWebsiteBds { get; set; }
        public int CustomerType { get; set; }
        public string CompanyName { get; set; }
        public string UserName { get; set; }  
        public string CustomerEmail { get; set; }
        public string EventCode { get; set; }
        public int EventId { get; set; }
        public string Note { get; set; }
        public string PlaceOfAttendEvent { get; set; }
        public DateTime RegisteredDate { get; set; }
        public string IntroduceUser { get; set; }
        public string CreatedBy { get; set; }
        public int? PhoningStatus { get; set; }
        public int? ConfirmAttend1st { get; set; }
        public int? ConfirmAttend2st { get; set; }
        public int RegisteredSource { get; set; }
        public bool IsCheckIn { get; set; }
        public string CheckInBy { get; set; }
        public DateTime? CheckInDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public int AccompanyMember { get; set; }
        public bool IsNotSuggest { get; set; }
        public bool Status { get; set; }
        public string ApproveBy { get; set; }
        public DateTime? ApproveDate { get; set; }
        public string Code { get; set; }
        public string Sessions { get; set; }
        public int SendTicketStatus { get; set; }
    }
}