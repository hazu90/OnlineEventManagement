﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class CompanyModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public string Manager { get; set; }
        public string TaxCode { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public int Size { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ApprovedDate { get; set; }
        public string ApprovedBy { get; set; }
        public int TotalCust { get; set; }
        public int TotalSale { get; set; }
        public int ParentId { get; set; }
        public string ParentName { get; set; }
        public int GroupId { get; set; }
    }
}