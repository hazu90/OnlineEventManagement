﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class FilterForOnlineRegisterSearchModel
    {
        public string TextSearch { get; set; }
        public int EventId { get; set; }
        public int InviteeCode { get; set; }
        public string Code { get; set; }
        public int SourceId { get; set; }
        public bool? IsSelfRegisteredInvitee { get; set; }
        public int IsSelfRegisteredInviteeExportExcel { get; set; }
        public int InviteeResponseStatus { get; set; }
        public string IntroduceUser { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalRecord { get; set; }
        public string AssignTo { get; set; }
        public int SessionId { get; set; }
        public int PhoningStatus { get; set; }
        public int ConfirmAttend1st { get; set; }
        public int ConfirmAttend2st { get; set; }
        public bool? Status { get; set; }
        public int SendTicketStatus { get; set; }
        public List<OnlineInviteeForSearchModel> LstInvitees { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int FilterAssignTo { get; set; }
        public bool? IsAssignTo { get; set; }
        public bool? IsBcrmAssignTo { get; set; }
        public FilterForOnlineRegisterSearchModel() 
        {
            PageIndex = 1;
            PageSize = 25;
            LstInvitees = new List<OnlineInviteeForSearchModel>();
        }
    }
}