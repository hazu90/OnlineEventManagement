﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class SendSMSHistoryModel
    {
        public int Id { get; set; }
        public int SMSResponse { get; set; }
        public string Description { get; set; }
        public int InviteEventId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string PhoneNumber { get; set; }
        public int EventId { get; set; }
        public string SMSContent { get; set; }
    }
}