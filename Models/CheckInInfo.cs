﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class CheckInInfo
    {
        public int EventId { get; set; }
        public int InviteEventId { get; set; }
        public int AttachId { get; set; }
        public int AccompanyInvitee { get; set; }
        public int SessionId { get; set; }
        public bool IsHasSession { get; set; }
        public List<SessionModel> LstSession { get; set; }  
    }
}