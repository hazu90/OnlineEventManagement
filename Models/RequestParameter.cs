﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class RequestParameter
    {
        public string SecretKey { get; set; }
        public object JsonData { get; set; }
    }
}