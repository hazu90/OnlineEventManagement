﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class DistrictModel
    {
        public int Id { get; set; }

        public string DistrictName { get; set; }

        public int Ordering { get; set; }

        public int CityId { get; set; }
    }
}