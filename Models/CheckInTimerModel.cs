﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class CheckInTimerModel
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public string EventName { get; set; }
        public DateTime StartTime { get;set; }
        public DateTime EndTime { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool IsInWebCheckIn { get; set; }
    }
}