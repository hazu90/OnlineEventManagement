﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class AnonymousInviteeModel
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public DateTime? CustomerBirthday { get; set; }
        public bool? CustomerSex { get; set; }
        public string CustomerEmail { get; set; }
        public int CustomerRevenue { get; set; }
        public int CustomerType { get; set; }
        public int CustomerPosition { get; set; }
        public int CompanyId { get; set; }
        public int CompanySize { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string TicketAddress { get; set; }
        public int GroupId { get; set; }
        public int AbilityAttend { get; set; }
        public string Note { get; set; }
        public int Status { get; set; }
        public string StatusNote { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool IsCheckIn { get; set; }
        public string CheckInBy { get; set; }
        public DateTime? CheckInDate { get; set; }
        public int CustomerVip { get; set; }
        public int AccompanyMember { get; set; }
        public bool IsInWebCheckin { get; set; }
        public string PositionName { get; set; }
        public string PlaceOfAttendEvent { get; set; }
        public int RegisteredSource { get; set; }
        public string Code { get; set; }
        public string Sessions { get; set; }
        public string IntroduceUser { get; set; }
    }
}
