﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class EventModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        
        public bool IsEnable { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public bool IsInWebCheckIn { get; set; }
        public string EventCode { get; set; }
        public bool IsDefaultEvent { get; set; }
        public string SMSTitle { get; set; }
        public DateTime Deadline { get; set; }
        public string Deploy { get; set; }
        public DateTime RevenueStartDate { get; set; }
        public DateTime RevenueEndDate { get; set; }
        public int RevenueCalculation { get; set; }
        public bool IsBcrmUse { get; set; }
        public bool IsAllowAttach { get; set; }
    }

    public class EventModelForCreate
    {
        public int Id { get; set; }
        public string EventCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime? Deadline { get; set; }
        public bool IsEnable { get; set; }
        public string CreatedBy { get; set; }
        public bool IsDefaultEvent { get; set; }
        public string SMSTitle { get; set; }
        public List<Group> LstGroup { get; set; }
        public string  Deploy { get; set; }
        public List<int> LstDeployId { get; set; }
        public DateTime? RevenueStartDate { get; set; }
        public DateTime? RevenueEndDate { get; set; }
        public int RevenueCalculation { get; set; }
        public bool? IsAllowAttach { get; set; }
        public bool IsBcrmUse { get; set; }
        public List<SessionForSaveModel> LstSession { get; set; }
        public bool IsHadSession { get; set; }
        public bool IsHadInvitee { get; set; }
    }

    public class EventIndexSearchModel
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int TotalRecord { get; set; }
        public List<EventModel> LstEvent { get; set; }

        public string FilterTextSearch{get;set;}
        public int DeployEventId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public List<Group> LstGroup { get; set; }

        public EventIndexSearchModel()
        {
            PageSize = 25;
            PageIndex = 1;
            LstEvent = new List<EventModel>();
        }
    }
}