﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class CheckInStatisticModel
    {
        public int TotalCustomer { get; set; }
        public int TotalCheckIn { get; set; }
        public int TotalVIP { get; set; }
        public int TotalVIPCheckedIn { get; set; }
        public int TotalNormal { get; set; }
        public int TotalNormalCheckedIn { get; set; }
        public int TotalOfficial { get; set; }
        public int TotalOfficialCheckedIn { get; set; }
        public int TotalAttach { get; set; }
        public int TotalAttachCheckedIn { get; set; }
        public int TotalAnonymous { get; set; }
        public int TotalAnonymousCheckedIn { get; set; }
        public int TotalAccompanyInvitee { get; set; }
        public int TotalAccompanyInviteeCheckedIn { get; set; }
    }

    public class CheckInStatisticIndexModel
    {
        public List<EventModel> LstEvents { get; set; }
        public CheckInStatisticModel CheckInStatistic { get; set; }
    }
}