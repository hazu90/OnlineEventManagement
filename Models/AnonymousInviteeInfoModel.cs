﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class AnonymousInviteeInfoModel
    {
        public List<EventModel> LstEvents { get; set; }
        public List<KeyValuePair<int, string>> ListCustomerType { get; set; }
        public List<KeyValuePair<int, string>> ListAbilityAttend { get; set; }
        public List<KeyValuePair<int, string>> ListCompanySize { get; set; }
        public List<KeyValuePair<int, string>> ListCustomerPosition { get; set; }
        public AnonymousInviteeModel AnonymousInfo { get; set; }
        public AnonymousInviteeInfoModel()
        {
            ListCustomerType = new List<KeyValuePair<int, string>>();
            ListAbilityAttend = new List<KeyValuePair<int, string>>();
            ListCompanySize = new List<KeyValuePair<int, string>>();
            ListCustomerPosition = new List<KeyValuePair<int, string>>();
        }
    }
}