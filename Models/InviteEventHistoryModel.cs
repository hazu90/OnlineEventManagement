﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class InviteEventHistoryModel
    {
        public int Id { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public int InviteEventId { get; set; }
        public string Action { get; set; }
        public string Note { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
    }
}