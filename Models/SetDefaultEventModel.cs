﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class SetDefaultEventModel
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public bool IsInWebCheckin { get; set; }
        public bool IsCancel { get; set; }
    }
}