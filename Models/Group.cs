﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Libs;

namespace BCRM.CheckInEvent.Models
{
    public class Group
    {
        public int GroupId { get; set; }

        public string GroupName { get; set; }

        public string GroupAlias { get; set; }

        public string Description { get; set; }

        public int? ParentId { get; set; }

        public int? Leader { get; set; }

        public int HLevel { get; set; }

        public int Order { get; set; }

        public RegionalActivity Regional { get; set; }

        public string GroupType { get; set; }

        public Group()
        {
            this.GroupAlias = string.Empty;
            this.Description = string.Empty;
            this.GroupName = string.Empty;
            this.ParentId = 0;
            this.Leader = 0;
            this.Regional = 0;
            this.Order = 0;
        }
    }
}