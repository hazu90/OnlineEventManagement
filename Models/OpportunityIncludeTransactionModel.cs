﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Libs;

namespace BCRM.CheckInEvent.Models
{
    public class OpportunityIncludeTransactionModel
    {
        public int Id { get; set; }
        public VipType VipType { get; set; }
        public string Name { get; set; }
        public int CustomerId { get; set; }
        public string AssignTo { get; set; }
        public int UserId { get; set; }
        public int Status { get; set; }
        public int Amount { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public TypeOfPayment TypeOfPayment { get; set; }
        public int BdsCustomerId { get; set; }
        public int TransactionAmount { get; set; }
        public int VAT { get; set; }
        public int Discount { get; set; }
        public int PromotionAmount1 { get; set; }
        public DateTime? PaymentDate { get; set; }
        public int TransactionId { get; set; }
        public int Package { get; set; }
        public bool IsConfirmed { get; set; }
        public bool IsRoot { get; set; }
        public RevenueType RevenueType { get; set; }
        public DateTime RevenueMonth { get; set; }
        public BankName BankCode { get; set; }
        public int SeparationAmount { get; set; }
        public int NotConfirmAmount { get; set; }
        public bool IsAllConvertToVAT { get; set; }
        public bool IsAllNotVAT { get; set; }
        public bool IsProposalIncentive { get; set; }
        public OpportunityIncludeTransactionModel()
        {
            this.IsAllConvertToVAT = true;
            this.IsAllNotVAT = true;
        }
    }
}