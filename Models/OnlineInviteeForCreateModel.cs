﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Libs;
using BCRM.CheckInEvent.Common;

namespace BCRM.CheckInEvent.Models
{
    public class OnlineInviteeForCreateModel
    {
        public List<KeyValuePair<int, string>> ListCustomerType { get; set; }
        public List<KeyValuePair<string, string>> ListPlaceAttendEvent { get; set; }
        public List<KeyValuePair<int, string>> ListAttendProbability { get; set; }
        public List<KeyValuePair<int, string>> ListPhoningStatus { get; set; }
        public List<KeyValuePair<int, string>> ListRegisteredSource { get; set; }
        public OnlineInviteeModel OnlineRegisterInfo { get; set; }
        public List<InviteEventHistoryModel> LstInviteEventHistory { get; set; }
        public List<SessionModel> LstSession { get; set; }
        public List<int> LstSelectedSession { get; set; }
        public List<EventModel> LstEvent { get; set; }

        public OnlineInviteeForCreateModel()
        {
            this.ListCustomerType = Helper.ListCustomerType();
            this.ListPlaceAttendEvent = new List<KeyValuePair<string, string>>();
            this.ListPlaceAttendEvent.Add(new KeyValuePair<string, string>("Hà Nội", "Hà Nội"));
            this.ListPlaceAttendEvent.Add(new KeyValuePair<string, string>("Đà Nẵng", "Đà Nẵng"));
            this.ListPlaceAttendEvent.Add(new KeyValuePair<string, string>("TP. Hồ Chí Minh", "TP. Hồ Chí Minh"));
            this.ListAttendProbability = CheckInHelper.ListAttendProbability();
            this.ListPhoningStatus = CheckInHelper.ListPhoningStatus();
            this.ListRegisteredSource = CheckInHelper.ListRegisteredSource();
        }
    }
}