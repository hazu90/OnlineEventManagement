﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class LuckyNumberResultModel
    {
        public string Number { get; set; }
        public string InviteeName { get; set; }
        public string CityName { get; set; }
        public string DepartmentName { get; set; }
    }
}