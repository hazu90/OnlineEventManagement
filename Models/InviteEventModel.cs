﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Libs;

namespace BCRM.CheckInEvent.Models
{
    public class InviteEventModel
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public int CustomerId { get; set; }
        public string EventCode { get; set; }
        public string EventName { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public bool CustomerSex { get; set; }
        public string CustomerEmail { get; set; }
        public DateTime? CustomerBirthday { get; set; }
        public string PositionName { get; set; }
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string OwnerCustomer { get; set; }
        public bool IsCheckIn { get; set; }
        public int AccompanyMember { get; set; }
        public string Note { get; set; }
        public string Code { get; set; }
        public bool IsNotSuggest { get; set; }
        public string PlaceOfAttendEvent { get; set; }
        public string IntroduceUser { get; set; }
        public int RegisteredSource { get; set; }
        public bool Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public string AssignTo { get; set; }
        public string Sessions { get; set; }
        public TypeOfCustomer CustomerType { get; set; }
        public CustomerPosition CustomerPosition { get; set; }
        public CompanySize CompanySize { get; set; }
        public string TicketAddress { get; set; }
        public int BranchId { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public AbilityAttend AbilityAttend { get; set; }
        public string StatusNote { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string CreatedBy { get; set; }
        public string CheckInBy { get; set; }
        public DateTime CheckInDate { get; set; }
        public CustomerVip CustomerVip { get; set; }
        public SendTicketType SendTicketType { get; set; }
        public string UserName { get; set; }
        public bool IsUsingWebsiteBds { get; set; }
        public int? PhoningStatus { get; set; }
        public DateTime? RegisteredDate { get; set; }
        public int? ConfirmAttend1st { get; set; }
        public int? ConfirmAttend2st { get; set; }
        public string ApproveBy { get; set; }
        public DateTime? ApproveDate { get; set; }
        public bool IsSelftRegistered { get; set; }
        public int SourceId { get; set; }
        public int CustomerRevenue { get; set; }
        public InviteEventStatus BcrmStatus { get; set; }
        public bool IsBcrmProcess { get; set; }
        public string BcrmAssignTo { get; set; }
        public int? SendTicketStatus { get; set; }

        public bool IsUsingSession { get; set; }
        public List<KeyValuePair<string,string>> LstSessionCheckin { get; set; }
        public List<string> LstSelectedSession { get; set; }
    }
    public class InviteEventModelToExport
    {
        public string EventName { get; set; }
        public int EventId { get; set; }
        public int InviteId { get; set; }
        public int AttachId { get; set; }
        public string CustomerName { get; set; }
        public CustomerVip CustomerVip { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public CustomerPosition CustomerPosition { get; set; }
        public string CompanyName { get; set; }
        public bool IsCheckIn { get; set; }
        public int CustomerId { get; set; }
        public DateTime? CustomerBirthday { get; set; }
        public bool? CustomerSex { get; set; }
        public int CustomerRevenue { get; set; }
        public TypeOfCustomer CustomerType { get; set; }
        public int CompanyId { get; set; }
        public CompanySize CompanySize { get; set; }
        public string CompanyAddress { get; set; }
        public string TicketAddress { get; set; }
        public int BranchId { get; set; }
        public int GroupId { get; set; }
        public string GroupName { get; set; }
        public AbilityAttend AbilityAttend { get; set; }
        public string Note { get; set; }
        public InviteEventStatus Status { get; set; }
        public string StatusNote { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string QRCode { get; set; }
        public string CheckInBy { get; set; }
        public DateTime? CheckInDate { get; set; }
        public int AccompanyMember { get; set; }
        // Mã code
        public string Code { get; set; }
        // Các thông tin thêm khi tích hợp với khách mời bên Webcheckin
        public bool IsNotSuggest { get; set; }
        public string EventCode { get; set; }
        public string UserName { get; set; }
        public string PositionName { get; set; }
    }
    public class InviteEventModelWebCheckinToExport
    {
        public string EventName { get; set; }
        public int EventId { get; set; }
        public int InviteId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public bool IsCheckIn { get; set; }
        public int CustomerType { get; set; }
        public string Note { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CheckInBy { get; set; }
        public DateTime? CheckInDate { get; set; }
        public int AccompanyMember { get; set; }
        // Các thông tin thêm khi tích hợp với khách mời bên Webcheckin
        public bool IsNotSuggest { get; set; }
        public string EventCode { get; set; }
        public string UserName { get; set; }
        public string PositionName { get; set; }
        public string IntroduceUser { get; set; }
        public int? PhoningStatus { get; set; }
        public int? ConfirmAttend1st { get; set; }
        public int? ConfirmAttend2st { get; set; }
        public string PlaceOfAttendEvent { get; set; }
        public string CompanyName { get; set; }
        public bool Status { get; set; }
        public string Code { get; set; }
        public int SendTicketStatus { get; set; }
        public int GroupId { get; set; }
        public string BcrmAssignTo { get; set; }
        public int SourceId { get; set; }
        public string Sessions { get; set; }
        public string SessionName { get; set; }
        public CustomerPosition CustomerPosition { get; set; }
        public bool IsBcrmProcess { get; set; }
    }
    public class EventIndexModel
    {
        public int SessionId { get; set; }
        public string TextSearch { get; set; }
        public int EventId { get; set; }
        public string EventIdWebCheckIn { get; set; }
        public bool? IsCheckIn { get; set; }
        public int? CheckInStatus { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int TotalRecord { get; set; }
        public string InviteeCode { get; set; }
        public string Code { get; set; }
        public int InviteeCodeInWebCheckin { get; set; }
        public int? ImportanceLevel { get; set; }
        public int? CustomerType { get; set; }
        public bool IsInWebCheckin { get; set; }
        public bool? IsNotSuggest { get; set; }
        
        public List<InviteEventModel> LstInviteEvents { get; set; }
        public EventIndexModel() 
        {
            PageIndex = 1;
            PageSize = 25;
            LstInviteEvents = new List<InviteEventModel>();
        }
    }
    public class InviteEventModelForAPI
    {
        public int EventId { get; set; }
        public string EventName { get; set; }
        public int AttachId { get; set; }
        public int InviteId { get; set; }
        public string CustomerName { get; set; }
        public int CustomerVip { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public CustomerPosition CustomerPosition { get; set; }
        public string CompanyName { get; set; }
        public string OwnerCustomer { get; set; }
        public bool IsCheckIn { get; set; }

    }
    public class InviteEventForAPI
    {
        public string EventName { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerVip { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Position { get; set; }
        public string CompanyName { get; set; }
        public string OwnerCustomer { get; set; }
        public bool CheckinStatus { get; set; }

    }

    public class InviteEventWebCheckinModel
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public string CompanyName { get; set; }
        public int CustomerType { get; set; }
        public string PositionName { get; set; }
        public string PlaceOfAttendEvent { get; set; }
        public int RegisteredSource { get; set; }
        public string IntroduceUser { get; set; }
        public string Note { get; set; }
        public bool IsCheckIn { get; set; }
        public string CheckInBy { get; set; }
        public DateTime? CheckInDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int AccompanyMember { get; set; }
        public bool IsNotSuggest { get; set; }
        public int EventId { get; set; }
        public string Code { get; set; }
        public string Sessions { get; set; }
    }
}