﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class SessionCheckinModel
    {
        public int InviteEventId { get; set; }
        public int SessionId { get; set; }
        public int EventId { get; set; }
        public string CheckinBy { get; set; }
        public DateTime CheckinDate { get; set; }
        public int AccompanyMember { get; set; }
    }
}