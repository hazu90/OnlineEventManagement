﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Models
{
    public class SaleInfoModel
    {
        public string UserName { get; set; }
        public string Avatar { get; set; }
        public int GroupId { get; set; }
    }
}