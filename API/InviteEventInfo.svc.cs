﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Libs;
using System.Configuration;
using DVS.Algorithm;
using BCRM.CheckInEvent.DAL;
using BCRM.CheckInEvent.Models;
using Newtonsoft.Json;
using BCRM.CheckInEvent.Common;

namespace BCRM.CheckInEvent.API
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "InviteEventInfo" in code, svc and config file together.
    public class InviteEventInfo : IInviteEventInfo
    {
        public Response GetInviteInfo(string data,string userName, string secretKey)
        {
            var response = new Response();
            try
            {
                var token = ConfigurationManager.AppSettings["SecretKey"];
                if (token != secretKey)
                {
                    response.Code = SystemCode.NotPermitted;
                    response.Message = "Mã bảo mật không chính xác!";
                    return response;
                }

                if (string.IsNullOrEmpty(data))
                {
                    response.Code = SystemCode.DataNull;
                    response.Message = "Dữ liệu không hợp lệ";
                    return response;
                }

                var decryptData = data.Decrypt();
                var arrData = decryptData.Split('-').ToList();
                if (arrData.Count != 3)
                {
                    response.Code = SystemCode.ErrorParam;
                    response.Message = "Dữ liệu không hợp lệ";
                    return response;
                }

                var eventId = Convert.ToInt32(arrData[0]);
                var inviteId = Convert.ToInt32(arrData[1]);
                var attachId = Convert.ToInt32(arrData[2]);

                var eventBl = new EventDL();
                var eventInfo = eventBl.GetInfo(inviteId, attachId);

                var retVal = new InviteEventForAPI();
                retVal.EventName = string.IsNullOrEmpty(eventInfo.EventName) ? "N/A" : eventInfo.EventName;
                retVal.CustomerId = eventInfo.EventId + "-" + eventInfo.InviteId + "-" + eventInfo.AttachId;
                retVal.CustomerName = string.IsNullOrEmpty(eventInfo.CustomerName) ? "N/A" : eventInfo.CustomerName;
                retVal.CustomerVip = eventInfo.CustomerVip == 0 ? "Khách thường" : "Khách VIP";
                //get phone number 
                if (string.IsNullOrEmpty(userName))
                {
                    response.Code = SystemCode.NotPermitted;
                    response.Message = "Bạn không có quyền thực thi chức năng này";
                    return response;
                }
                else
                {
                    var userModel = (new UserDL()).GetByUserNameWithLockTime(userName);
                    if (userModel == null)
                    {
                        response.Code = SystemCode.DataNull;
                        response.Message = "Dữ liệu không hợp lệ";
                        return response;
                    }
                    else
                    {
                        if (!userModel.HasPermisson(Permission.Manager) && !userModel.HasPermisson(Permission.Admin))
                        {
                            if (string.IsNullOrEmpty(eventInfo.CustomerPhone))
                            {
                                retVal.PhoneNumber = "N/A";
                                
                            }
                            else
                            {
                                var lstPhone = eventInfo.CustomerPhone.Split(new char[] { ';', ',' });
                                var lstNewPhone = new List<string>();
                                for (var index = 0; index < lstPhone.Length; index++)
                                {
                                    var phoneInfo = lstPhone[index].Trim();
                                    if (!string.IsNullOrEmpty(phoneInfo))
                                    {
                                        phoneInfo = phoneInfo.Substring(0, phoneInfo.Length - 2) + "xx";
                                        lstNewPhone.Add(phoneInfo);
                                    }
                                }
                                retVal.PhoneNumber = string.Join(",", lstNewPhone);
                            }
                        }
                        else
                        {
                            retVal.PhoneNumber = string.IsNullOrEmpty(eventInfo.CustomerPhone) ? "N/A" : eventInfo.CustomerPhone;
                        }
                    }
                }
                
                retVal.Email = string.IsNullOrEmpty(eventInfo.CustomerEmail) ? "N/A" : eventInfo.CustomerEmail;
                retVal.Position = eventInfo.CustomerPosition.ToCustomerPositionName();
                retVal.CompanyName = string.IsNullOrEmpty(eventInfo.CompanyName) ? "N/A" : eventInfo.CompanyName;
                retVal.OwnerCustomer = eventInfo.OwnerCustomer;
                retVal.CheckinStatus = eventInfo.IsCheckIn;

                response.Code = SystemCode.Success;
                response.Data = JsonConvert.SerializeObject(retVal, Formatting.None);
            }
            catch (Exception ex)
            {
                response.Code = SystemCode.Error;
                response.Message = ex.Message;
            }
            return response;
        }

        public Response CheckIn(string customerId, string checkInBy, string secretKey)
        {
            var response = new Response();
            try
            {
                var token = ConfigurationManager.AppSettings["SecretKey"];
                if (token != secretKey)
                {
                    response.Code = SystemCode.NotPermitted;
                    response.Message = "Mã bảo mật không chính xác!";
                    return response;
                }
                var decryptData = customerId.Decrypt();
                var arrData = decryptData.Split('-').ToList();
                if (arrData.Count != 3)
                {
                    response.Code = SystemCode.ErrorParam;
                    response.Message = "Dữ liệu không hợp lệ";
                    return response;
                }
                var eventId = Convert.ToInt32(arrData[0]);
                var inviteId = Convert.ToInt32(arrData[1]);
                var attachId = Convert.ToInt32(arrData[2]);
                if (eventId <= 0 || (inviteId <= 0 && attachId <= 0))
                {
                    response.Code = SystemCode.ErrorParam;
                    response.Message = "Dữ liệu không hợp lệ";
                    return response;
                }

                var eventBl = new EventDL();
                var eventInfo = eventBl.GetEventById(eventId);
                if (eventInfo == null)
                {
                    response.Code = SystemCode.DataNull;
                    response.Message = "Sự kiện không tồn tại";
                    return response;
                }

                var checkInTimerBl = new CheckInTimerDL();
                var checkInTimer = checkInTimerBl.GetByEventId(eventId);

                if (    (checkInTimer != null && checkInTimer.StartTime > DateTime.Now)
                    ||  (checkInTimer == null && eventInfo.StartDate > DateTime.Now))
                {
                    response.Code = SystemCode.NotValid;
                    response.Message = "Thời hạn CheckIn sự kiện này chưa đến. Vui lòng kiểm tra lại thông tin!";
                    return response;
                }

                if (    (checkInTimer != null && checkInTimer.EndTime <= DateTime.Now)
                    ||  (checkInTimer == null &&eventInfo.EndDate <= DateTime.Now))
                {
                    response.Code = SystemCode.NotValid;
                    response.Message = "Thời hạn CheckIn sự kiện này đã hết hạn. Vui lòng kiểm tra lại thông tin!";
                    return response;
                }

                if (attachId > 0)
                {
                    eventBl.UpdateAttachCheckIn(attachId, checkInBy);
                }
                else
                {
                    eventBl.UpdateInviteEventCheckIn(inviteId, checkInBy);
                }
            }
            catch (Exception ex)
            {
                response.Code = SystemCode.Error;
                response.Message = ex.Message;
            }
            return response;
        }
    }
}
