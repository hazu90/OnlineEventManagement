﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Libs;
using Newtonsoft.Json;
using BCRM.CheckInEvent.Models;
using BCRM.CheckInEvent.DAL;
using System.Configuration;
using BCRM.CheckInEvent.Common;

namespace BCRM.CheckInEvent.API
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "InviteEventOnline" in code, svc and config file together.
    public class InviteEventOnline : IInviteEventOnline
    {
        public Response AddOnlineInvitee(string secretKey, string data)
        {
            var response = new Response();
            try
            {
                var token = ConfigurationManager.AppSettings["SecretKey"];
                if (token != secretKey)
                {
                    response.Code = SystemCode.NotPermitted;
                    response.Message = "Mã bảo mật không chính xác!";
                    return response;
                }
                var inviteeInfo = JsonConvert.DeserializeObject<OnlineInviteeModel>(data);
                if (inviteeInfo == null)
                {
                    response.Code = SystemCode.DataNull;
                    response.Message = "Dữ liệu không hợp lệ";
                    return response;
                }
                //validate thông tin
                // Tên khách hàng bắt buộc nhập
                if (string.IsNullOrEmpty(inviteeInfo.CustomerName)
                   || string.IsNullOrEmpty(inviteeInfo.CustomerPhone) 
                   || string.IsNullOrEmpty(inviteeInfo.CustomerEmail)
                   || string.IsNullOrEmpty(inviteeInfo.EventCode))
                {
                    response.Code = SystemCode.NotValid;
                    response.Message = "Dữ liệu không hợp lệ";
                    return response;
                }
                //Loại khách hàng bắt buộc nhập
                if (inviteeInfo.CustomerType == 0)
                {
                    response.Code = SystemCode.NotValid;
                    response.Message = "Dữ liệu không hợp lệ";
                    return response;
                }

                var eventDL = new EventDL();
                var eventInfo = eventDL.GetByEventCodeInWebCheckIn(0, inviteeInfo.EventCode);
                if (eventInfo == null)
                {
                    response.Code = SystemCode.DataNull;
                    response.Message = "Không tìm thấy sự kiện phù hợp";
                    return response;
                }
                inviteeInfo.EventId = eventInfo.Id;

                // Kiểm tra số điện thoại
                var inviteEventDL = new InviteEventDL();
                if (inviteEventDL.WebCheckIn_CheckPhoneExist(0, inviteeInfo.EventId, inviteeInfo.CustomerPhone))
                {
                    response.Code = SystemCode.ErrorExist;
                    response.Message = "Đã có tồn tại số điện thoại này !";
                    return response;
                }
                // Kiểm tra địa chỉ email
                if (inviteEventDL.WebCheckIn_CheckEmailExist(0, inviteeInfo.EventId, inviteeInfo.CustomerEmail))
                {
                    response.Code = SystemCode.ErrorExist;
                    response.Message = "Đã có tồn tại email này !";
                    return response;
                }
                // Sinh mã vé cho khách mời ,Lặp tối đa 100 lần nếu như đã tồn tại mã vé này rồi
                var code = "";
                for (var index = 0; index < 100; index++)
                {
                    code = string.Format("{0}{1}", eventInfo.EventCode, CommonMethod.GenerateCode());
                    // Kiểm tra xem mã vé này đã tồn tại chưa
                    if (!inviteEventDL.WebCheckin_IsExistCode(eventInfo.Id, code))
                    {
                        break;
                    }
                }
                inviteeInfo.Code = code;

                var id = inviteEventDL.WebCheckIn_API_Insert(inviteeInfo);

                // Thêm lịch sử lưu thông tin 
                var inviteEventHistoryDL = new InviteEventHistoryDL();
                inviteEventHistoryDL.Insert(new InviteEventHistoryModel()
                {
                    Action = "Tạo mới",
                    OldValue = "",
                    NewValue = "",
                    UpdatedBy = "Online",
                    InviteEventId = id
                });
                return response;
            }
            catch
            {
                response.Code = SystemCode.Error;
                response.Message = "Có lỗi trong quá trình xử lý";
                return response;
            }
            
        }
    }
}
