﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using Libs;

namespace BCRM.CheckInEvent.API
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAccount" in both code and config file together.
    [ServiceContract]
    public interface IAccount
    {
        [OperationContract]
        [WebInvoke(Method = "GET",
            UriTemplate = "CreateUser?data={data}&key={key}",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json)]
        Response CreateUser(string data, string key);

        [OperationContract]
        [WebInvoke(Method = "GET",
            UriTemplate = "BlockUser?data={data}&key={key}",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json)]
        Response BlockUser(string data, string key);

        [OperationContract]
        [WebInvoke(Method = "GET",
            UriTemplate = "ActiveUser?data={data}&key={key}",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json)]
        Response ActiveUser(string data, string key);
    }
}
