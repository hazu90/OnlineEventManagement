﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using Libs;

namespace BCRM.CheckInEvent.API
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IInviteEventInfo" in both code and config file together.
    [ServiceContract]
    public interface IInviteEventInfo
    {
        [OperationContract]
        [WebInvoke(Method = "GET",
            UriTemplate = "GetInviteInfo?data={data}&userName={userName}&secretKey={secretKey}",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json)]
        Response GetInviteInfo(string data,string userName, string secretKey);

        [OperationContract]
        [WebInvoke(Method = "GET",
            UriTemplate = "CheckIn?customerId={customerId}&checkInBy={checkInBy}&secretKey={secretKey}",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json)]
        Response CheckIn(string customerId, string checkInBy, string secretKey);
    }
}
