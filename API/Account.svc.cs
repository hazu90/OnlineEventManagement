﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Libs;
using BCRM.CheckInEvent.Models;
using DVS.Algorithm;
using Newtonsoft.Json;
using BCRM.CheckInEvent.DAL;

namespace BCRM.CheckInEvent.API
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Account" in code, svc and config file together.
    public class Account : IAccount
    {
        public Response CreateUser(string data, string key)
        {
            var response = new Response();
            try
            {
                if (Security.IsSecretKey(key))
                {
                    var userBl = new UserDL();
                    var objData = JsonConvert.DeserializeObject<UserForAPI>(data);
                    var userModel = new UserModel
                    {
                        UserName = objData.UserName,
                        PassWord = "123456".ToMD5(),
                        Status = 1,
                        Role = objData.Role.Contains(1) ? 1 : 0
                    };
                    var ret = userBl.Insert(userModel);
                    if (!ret)
                    {
                        response.Code = SystemCode.ErrorExist;
                        response.Message = "Tài khoản này đã tồn tại trên hệ thống.";
                        return response;
                    }
                    else
                    {
                        var userRoleDL = new UserRoleDL();
                        var lstRoleUser = objData.Role.Distinct().ToList();
                        if (objData.Role.Contains(1))
                        {
                            userRoleDL.Insert(new UserRoleModel()
                            {
                                RoleId = 1,
                                UserName = objData.UserName
                            });
                        }
                        else
                        {
                            userRoleDL.Insert(new UserRoleModel()
                            {
                                RoleId = 0,
                                UserName = objData.UserName
                            });
                        }
                    }
                }
                else
                {
                    response.Code = SystemCode.NotPermitted;
                    response.Message = "Mã xác thực không hợp lệ.";
                    return response;
                }
            }
            catch (Exception e)
            {
                response.Code = SystemCode.Error;
                response.Message = e.Message;
            }
            return response;
        }

        public Response BlockUser(string data, string key)
        {
            var response = new Response();
            try
            {
                if (Security.IsSecretKey(key))
                {
                    var userBl = new UserDL();
                    var objData = JsonConvert.DeserializeObject<UserForAPI>(data);
                    var user = userBl.GetByUserName(objData.UserName);
                    if (user == null)
                    {
                        response.Code = SystemCode.ErrorExist;
                        response.Message = "Nhân viên không tồn tại.";
                        return response;
                    }
                    userBl.ChangeStatus(objData.UserName, 2);
                }
                else
                {
                    response.Code = SystemCode.NotPermitted;
                    response.Message = "Mã xác thực không hợp lệ.";
                    return response;
                }
            }
            catch (Exception e)
            {
                response.Code = SystemCode.Error;
                response.Message = e.Message;
            }
            return response;
        }

        public Response ActiveUser(string data, string key)
        {
            var response = new Response();
            try
            {
                if (Security.IsSecretKey(key))
                {
                    var userBl = new UserDL();
                    var objData = JsonConvert.DeserializeObject<UserForAPI>(data);
                    var user = userBl.GetByUserName(objData.UserName);
                    if (user == null)
                    {
                        response.Code = SystemCode.ErrorExist;
                        response.Message = "Nhân viên không tồn tại.";
                        return response;
                    }
                    userBl.ChangeStatus(objData.UserName, 1);
                }
                else
                {
                    response.Code = SystemCode.NotPermitted;
                    response.Message = "Mã xác thực không hợp lệ.";
                    return response;
                }
            }
            catch (Exception e)
            {
                response.Code = SystemCode.Error;
                response.Message = e.Message;
            }
            return response;
        }
    }
}
