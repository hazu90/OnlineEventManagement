﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Libs;
using System.ServiceModel.Web;

namespace BCRM.CheckInEvent.API
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IInviteEventOnline" in both code and config file together.
    [ServiceContract]
    public interface IInviteEventOnline
    {
        [OperationContract]
        [WebInvoke(Method = "GET",
            UriTemplate = "AddOnlineInvitee?secretKey={secretKey}&data={data}",
            BodyStyle = WebMessageBodyStyle.WrappedRequest,
            ResponseFormat = WebMessageFormat.Json)]
        Response AddOnlineInvitee(string secretKey, string data);
    }
}
