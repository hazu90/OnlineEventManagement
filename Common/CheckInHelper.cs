﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Common
{
    public static class CheckInHelper
    {
        private static readonly string[] AttendProbabilityName = { "", "Có", "Không", "Xem xét" };

        public static string ToAttendProbabilityName(this AttendProbability type)
        {
            return AttendProbabilityName[type.GetHashCode()];
        }
        public static List<KeyValuePair<int, string>> ListAttendProbability()
        {
            Array arrStatus = Enum.GetValues(typeof(AttendProbability));
            return (from AttendProbability type in arrStatus select new KeyValuePair<int, string>(type.GetHashCode(), type.ToAttendProbabilityName())).ToList();
        }


        private static readonly string[] PhoningStatusName = { "", "OK", "Không nghe máy", "Số máy sai","Gọi lại sau","Không mời","Trưởng phòng duyệt" };

        public static string ToPhoningStatusName(this PhoningStatus type)
        {
            return PhoningStatusName[type.GetHashCode()];
        }
        public static List<KeyValuePair<int, string>> ListPhoningStatus()
        {
            Array arrStatus = Enum.GetValues(typeof(PhoningStatus));
            return (from PhoningStatus type in arrStatus select new KeyValuePair<int, string>(type.GetHashCode(), type.ToPhoningStatusName())).ToList();
        }

        private static readonly string[] RegisteredSourceName = { "", "Facebook", "batdongsan.com", "Giới thiệu", "Khác" };

        public static string ToRegisteredSourceName(this RegisteredSource type)
        {
            return RegisteredSourceName[type.GetHashCode()];
        }
        public static List<KeyValuePair<int, string>> ListRegisteredSource()
        {
            Array arrStatus = Enum.GetValues(typeof(RegisteredSource));
            return (from RegisteredSource type in arrStatus select new KeyValuePair<int, string>(type.GetHashCode(), type.ToRegisteredSourceName())).ToList();
        }

        private static readonly string[] PermissionName = { "Nhân viên", "Quản lý sự kiện", "Admin", "Quản lý khách mời" ,"Giao khách mời"};
        public static string ToPermissionName(this Permission type)
        {
            return PermissionName[type.GetHashCode()];
        }
        public static List<KeyValuePair<int, string>> ListPermission()
        {
            Array arrStatus = Enum.GetValues(typeof(Permission));
            return (from Permission type in arrStatus select new KeyValuePair<int, string>(type.GetHashCode(), type.ToPermissionName())).ToList();
        }
    }
}