﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net;
using System.Threading;
using System.IO;
using Libs;
using BCRM.CheckInEvent.Models;
using BCRM.CheckInEvent.DAL;

namespace BCRM.CheckInEvent.Common
{
    public class CommonMethod
    {
        public static Random RdGenerateCode = new Random();
        /// <summary>
        /// Sinh mã vé cho khách mời
        /// </summary>
        /// <returns></returns>
        public static int GenerateCode()
        {
            return RdGenerateCode.Next(10000, 99999);
        }

        //Created by Thuyetnt
        public static string MakePostRequest(string url, string data, string contentType = null, bool keepAlive = true)
        {
            try
            {
                byte[] byteArray = Encoding.UTF8.GetBytes(data);
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = string.IsNullOrEmpty(contentType) ? "application/x-www-form-urlencoded" : contentType;
                request.ContentLength = byteArray.Length;
                request.KeepAlive = keepAlive;
                request.Timeout = Timeout.Infinite;
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36";
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                var dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                //Get response
                WebResponse response = request.GetResponse();
                dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromApi = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
                return responseFromApi;
            }
            catch (Exception ex)
            {
                Logger.GetInstance().Write(url);
                Logger.GetInstance().Write(string.Format("Make Post Request: {0}", ex.Message));
                Logger.GetInstance().Write(string.Format("Make Post Request: {0}", ex.StackTrace));
                return string.Empty;
            }
        }

        public static string ConvertToUnSign(string strVietnamese)
        {
            string charactersVietnamese = "àáảãạâầấẩẫậăằắẳẵặèéẻẽẹêềếểễệđìíỉĩịòóỏõọôồốổỗộơờớởỡợùúủũụưừứửữựỳýỷỹỵ";
            string charactersUnsign = "aaaaaaaaaaaaaaaaaeeeeeeeeeeediiiiiooooooooooooooooouuuuuuuuuuuyyyyy";
            StringBuilder strUnsign = new StringBuilder();
            if (string.IsNullOrEmpty(strVietnamese))
            {
                return "";
            }
            else
            {
                for (var index = 0; index < strVietnamese.Length; index++)
                {
                    var characterIndex= charactersVietnamese.IndexOf(strVietnamese[index]);
                    if (characterIndex != -1)
                    {
                        strUnsign.Append(charactersUnsign[characterIndex]);
                    }
                    else
                    {
                        strUnsign.Append(strVietnamese[index]);
                    }
                }
            }
            return strUnsign.ToString();
        }

        public static string ConvertToUnSignIncludeUpper(string strVietnamese)
        {
            string charactersVietnamese = "àáảãạâầấẩẫậăằắẳẵặèéẻẽẹêềếểễệđìíỉĩịòóỏõọôồốổỗộơờớởỡợùúủũụưừứửữựỳýỷỹỵ";
            var toUpperVietNamese = charactersVietnamese.ToUpper();
            charactersVietnamese += charactersVietnamese + toUpperVietNamese;

            string charactersUnsign = "aaaaaaaaaaaaaaaaaeeeeeeeeeeediiiiiooooooooooooooooouuuuuuuuuuuyyyyy";
            toUpperVietNamese = charactersUnsign.ToUpper();
            charactersUnsign += charactersUnsign + toUpperVietNamese;

            StringBuilder strUnsign = new StringBuilder();
            if (string.IsNullOrEmpty(strVietnamese))
            {
                return "";
            }
            else
            {
                for (var index = 0; index < strVietnamese.Length; index++)
                {
                    var characterIndex = charactersVietnamese.IndexOf(strVietnamese[index]);
                    if (characterIndex != -1)
                    {
                        strUnsign.Append(charactersUnsign[characterIndex]);
                    }
                    else
                    {
                        strUnsign.Append(strVietnamese[index]);
                    }
                }
            }
            return strUnsign.ToString();
        }

        /// <summary>
        /// Lấy danh sách các sự kiện của bên BCRM và WebCheckin
        /// Các sự kiện này là các sự kiện đang hoạt động
        /// và được sắp xếp theo thông tin ngày bắt đầu
        /// </summary>
        /// <returns></returns>
        public static List<EventModel> GetEvents()
        {
            var eventDL = new EventDL();
            // get all events
            var lstAllEvents = new List<EventModel>();
            // Lấy danh sách event trên web BCRM
            //var lstEventBCRMs = eventDL.GetAll().FindAll(o => o.IsEnable == true);
            //foreach (var item in lstEventBCRMs)
            //{
            //    item.IsInWebCheckIn = false;
            //}
            // Lấy thông tin event trên web checkin theo điều kiện tìm kiếm
            var lstEvent = eventDL.GetAllInWebCheckIn().FindAll(o => o.IsEnable == true);
            foreach (var item in lstEvent)
            {
                item.IsInWebCheckIn = true;
            }
            //lstAllEvents.AddRange(lstEventBCRMs);
            lstAllEvents.AddRange(lstEvent);
            // Sắp xếp các sự kiện theo tiêu chí của ngày bắt đầu
            lstAllEvents = lstAllEvents.OrderByDescending(o => o.CreatedDate).ToList();
            return lstAllEvents;
        }
        /// <summary>
        /// Lấy sự kiện được cài đặt mặc định
        /// nếu trường hợp không có sự kiện mặc định sẽ lấy thông tin của sự kiện mới nhất
        /// </summary>
        /// <returns></returns>
        public static EventModel GetDefaultEvent()
        {
            // Lấy ra danh sách sự kiện
            var lstAllEvents = CommonMethod.GetEvents();

            // Lấy ra sự kiện được set mặc định
            var setDefaultEventDL = new SetDefaultEventDL();
            var setDefaultEventInfo = setDefaultEventDL.GetLastest();
            // TH chưa có thông tin về sự kiện được đặt mặc định
            // Lấy ra sự kiện mới nhất
            var lastestEvent = new EventModel();
            if (setDefaultEventInfo == null)
            {
                if (lstAllEvents.Count > 0)
                {
                    lastestEvent = lstAllEvents.FirstOrDefault();
                }
                else
                {
                    return null;
                }
            }
            else
            {
                //Kiểm tra sự kiện mặc định đó đã bị hủy chưa
                if (!setDefaultEventInfo.IsCancel)
                {
                    lastestEvent = lstAllEvents.Find(o => o.Id == setDefaultEventInfo.EventId && o.IsInWebCheckIn == setDefaultEventInfo.IsInWebCheckin);
                    if (lastestEvent == null)
                    {
                        if (lstAllEvents.Count > 0)
                        {
                            lastestEvent = lstAllEvents.FirstOrDefault();
                        }
                    }
                }
                else
                {
                    return null;
                }
            }

            return lastestEvent;
        }

        /// <summary>
        /// Get root group from child group
        /// </summary>
        /// <param name="lstGroup"></param>
        /// <param name="childId"></param>
        /// <returns></returns>
        public static Group GetRootParent(List<Group> lstGroup, int childId)
        {
            var group = lstGroup.Find(g => g.GroupId == childId);
            if (group != null && group.ParentId.Value > 0)
            {
                return CommonMethod.GetRootParent(lstGroup,group.ParentId.Value);
            }
            else
            {
                return group;
            }
        }
    }
}