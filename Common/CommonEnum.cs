﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BCRM.CheckInEvent.Common
{
    public enum UserState
    {
        Active = 1,
        InActive = 2
        //    ,
        //Deleted =3
    }

    public enum Permission
    {
        /// <summary>
        /// Quyền nhân viên
        /// </summary>
        Employee =0,
        /// <summary>
        /// Quyền của người quản lý sự kiện
        /// </summary>
        Manager = 1,
        /// <summary>
        /// Quyền của Admin
        /// </summary>
        Admin = 2,
        /// <summary>
        /// Quyền quản lý khách mời (CSKH)
        /// </summary>
        InviteeManager = 3,
        /// <summary>
        /// Quyền giao khách mời
        /// </summary>
        TakeInvitee = 4
    }

    public enum AttendProbability
    {
        Yes =1,
        No = 2,
        Considering = 3
    }
    /// <summary>
    /// Trạng thái gọi điện thoại
    /// </summary>
    public enum PhoningStatus
    {
        /// <summary>
        /// OK
        /// </summary>
        Confirm = 1,
        /// <summary>
        /// Không nghe máy
        /// </summary>
        NoListen = 2,
        /// <summary>
        /// Số máy sai
        /// </summary>
        WrongNumber =3,
        /// <summary>
        /// Gọi lại sau
        /// </summary>
        TryLater =4,
        /// <summary>
        /// Không mời
        /// </summary>
        Ignore = 5,
        /// <summary>
        /// Trưởng phòng duyệt
        /// </summary>
        LeaderAppoved = 6
    }
    /// <summary>
    /// Nguồn đăng kí
    /// </summary>
    public enum RegisteredSource 
    {
        /// <summary>
        /// Facebook
        /// </summary>
        Facebook  =1,
        /// <summary>
        /// Website Bds
        /// </summary>
        WebsiteBds = 2,
        /// <summary>
        /// Giới thiệu
        /// </summary>
        Introduce = 3,
        /// <summary>
        /// Nguồn khác
        /// </summary>
        Other = 4
    }

    /// <summary>
    /// Lịch sử thay đổi khách mời
    /// </summary>
    public enum InviteeChangeHistory
    {
        /// <summary>
        /// Tạo mới
        /// </summary>
        Create = 1,
        /// <summary>
        /// Sửa
        /// </summary>
        Edit = 2,
        /// <summary>
        /// Duyệt
        /// </summary>
        Approve =3,
        /// <summary>
        /// Hủy duyệt
        /// </summary>
        UnApprove =4,
        /// <summary>
        /// Xóa
        /// </summary>
        Delete = 5
    }
}